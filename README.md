[![build status](https://git.thm.de/compilergeruest/cg-source/badges/master/build.svg)](https://git.thm.de/compilergeruest/cg-source/commits/master)

# Compilergerüst

## Index
1. [Preface](#preface)
2. [Description](#description)
3. [How to use](#how-to-use)
4. [License](#license)

## Preface

This project was developed by Jan Sladek and Björn Pfarr for Prof. Dr. H. Geisse at the University of Applied Science Mittelhessen (Technische Hochschule Mittelhessen) during the summer term 2016. The goal of this project is to support students in the course Compiler Engineering II (Compilerbau II).

## Description

The course Compiler Engineering II presents how compiler optimizations work and how they are implemented. To prevent most of the preliminary work, the project's goal is to provide a working compiler framework which may be extended during the course. 

The framework itself is written in [Scala](http://www.scala-lang.org) and uses the build tool [sbt](http://www.scala-sbt.org). To build and run the project see the next chapter [How to use](#how-to-use).

## How to use

### How to build

- Install [sbt](http://www.scala-sbt.org)
- Clone this project on your computer
- Open a terminal and `cd` into the root directory of the cloned repository
- Choose one of the following commands:

| **Command** | **Description** |
|---------|-------------|
| `sbt clean` | All compiled and generated resources will be deleted |
| `sbt test` | Compiles (if necessary) and runs the tests |
| `sbt packageBin` | Compiles the project and creates a jar-file containing all binaries |
| `sbt packageDoc` | Creates a jar-file only containing the javadoc documentation |
| `sbt packageSrc` | Creates a jar-file only containing the sources |
| `sbt genScripts` | Creates scripts for running the jar on Windows and Unix |
| `sbt release` | Creates a zip-file containing all runtime dependencies and the files of the commands `sbt packageBin`, `sbt packageDoc`, `sbt packageSrc` and `sbt genScripts` |

### How to run

- Run `sbt release`
- Unzip the generated zip-file, which is located at `target/scala-2.11/release.zip`
- Run the appropriate script for your platform

## License

See the [LICENSE](LICENSE) file for license rights and limitations (MIT).
