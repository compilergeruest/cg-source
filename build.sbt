
lazy val scalaV = "2.12.3"

scalaVersion := scalaV

lazy val baseSettings = Seq(
  compileOrder           := CompileOrder.Mixed,
  EclipseKeys.withSource := true
)

scalacOptions ++= Seq(
  "-deprecation",
  "-feature",
  "-unchecked"/*,
  "-Yinline",
  "-Yinline-warnings"*/
)

lazy val customSettings = Seq(
  organization := "de.thm.mni",
  version      := "0.0.1",
  scalaVersion := scalaV,
  name         := "cg",

  libraryDependencies ++= Seq( 
    "org.scalatest" %% "scalatest" % "3.0.1" % "test"
  )
)

lazy val genScripts = taskKey[Seq[File]]("Generates a runnable script for linux and windows.")

genScripts := {
  val (_, jarfile) = packagedArtifact.in(Compile, packageBin).value

  val scriptsDir = jarfile.getParentFile() 
  val cgsh = scriptsDir / "cg.sh"
  val cgbat = scriptsDir / "cg.bat"

  val jar = jarfile.getName
  val batPrefix = s"@echo off\n\nSET jar=./$jar\n\n"
  val bashPrefix = s"#!/bin/bash\n\njar=./$jar\n\n"

  val mainClass = "de.thm.mni.cg.Main"
  val scalaCmd = "scala -J-Xmx2g -cp"

  IO.write(cgsh, bashPrefix + scalaCmd + " $jar " + mainClass + " $*\n")
  IO.write(cgbat, batPrefix + scalaCmd + " %jar% " + mainClass + " %*\n")
  Seq(cgsh, cgbat)
}

lazy val release = taskKey[Unit]("Packs the jar, the natives and start-scripts into a single zip-archive.")

release := {
  val (_, jarfile) = packagedArtifact.in(Compile, packageBin).value
  val (_, sourcefile) = packagedArtifact.in(Compile, packageSrc).value
  val (_, documentation) = packagedArtifact.in(Compile, packageDoc).value

  val scripts = genScripts.in(Compile, packageBin).value
  val baseDir = baseDirectory.value
  
  def subfiles(f: File): Seq[File] = 
  	if (f.isDirectory) {
  		f.listFiles match {
  		  case null => Seq()
  		  case xs   => xs.toSeq flatMap subfiles
  		}
  	} else Seq(f) 
  
  // natives
  val runtimedir = baseDir / "runtime"
  val runtime = subfiles(runtimedir).map { f =>
  	val name = baseDir.toURI().relativize(f.toURI()).getPath()
  	(f, name)
  }
  
  // jarfile, sourcefile, documentation + scripts
  val base = jarfile.getParentFile
  val files = scripts ++ Seq(jarfile, sourcefile, documentation)
  val entries = files.map { f =>
  	val name = base.toURI().relativize(f.toURI()).getPath()
  	(f, name)
  }

  IO.copy(runtime map {
    case (orgFile, name) => (orgFile, base / name)
  })

  IO.zip(entries ++ runtime, base / "release.zip")
}

lazy val proj = (project in file(".")).
  settings(baseSettings: _*).
  settings(customSettings: _*)
