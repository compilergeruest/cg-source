#!/bin/bash

# This script compiles the java native interface for the as.c and ld.c files
# To execute this script under windows, be sure to have mingw and gcc installed

cd ../../src/
echo "Creating java native interface for de.thm.mni.cg.backend.NativeAssembler ..."
javah -jni -o ../runtime/natives/as.h de.thm.mni.cg.backend.NativeAssembler
if [ $? -ne 0 ]; then
	echo "Could not create the java native interface for de.thm.mni.cg.backend.NativeAssembler!"
	exit 1
else
	echo "Done!"
fi
echo "Creating java native interface for de.thm.mni.cg.backend.NativeLinker ..."
javah -jni -o ../runtime/natives/ld.h de.thm.mni.cg.backend.NativeLinker
if [ $? -ne 0 ]; then
	echo "Could not create the java native interface for de.thm.mni.cg.backend.NativeLinker!"
	exit 1
else
	echo "Done!"
fi
cd ../runtime/natives/
echo "Searching for jdk installation ..."
answer=""
while [ "$answer" != "y" ]; do
	if [ "$answer" == "n" ]; then
		echo "Could not locate jdk, please enter the absolute path of your jdk installation"
		printf "> "
		read jdk_installation
	elif [ -z "${JAVA_HOME+xxx}" ]; then
		echo "Could not locate jdk, please enter the absolute path of your jdk installation"
 		printf "> "
		read jdk_installation
	elif [ -z "$JAVA_HOME" ] && [ "${JAVA_HOME+xxx}" = "xxx" ]; then
		echo "Could not locate jdk, please enter the absolute path of your jdk installation"
		printf "> "
		read jdk_installation
	else
		jdk_installation=$JAVA_HOME
	fi
	echo "Your jdk installation is located at '${jdk_installation}'."
	echo "Your java include path is located at '${jdk_installation}/include/'."
	echo "Is this correct? [y/n]"
	printf "> "
	read answer
done
echo "Done!"

echo "Compiling native library ..."
if [ "$(expr substr $(uname -s) 1 10)" == "MINGW64_NT" ] || [ "$(expr substr $(uname -s) 1 10)" == "MINGW32_NT" ]; then
    echo "Compiling with gcc under windows mingw ..."

    echo "Compiling assember natives ..."
    gcc $flag -I"$jdk_installation/include/" -I"$jdk_installation/include/win32/" -m32 -shared -o as_32.dll as.c
    if [ $? -ne 0 ]; then
    	echo "Could not compile the assembler natives!"
    	exit 1
    fi
    if [ "$(expr substr $(uname -s) 1 10)" != "MINGW64_NT" ]; then
      gcc $flag -I"$jdk_installation/include/" -I"$jdk_installation/include/win32/" -m64 -shared -o as_64.dll as.c
      if [ $? -ne 0 ]; then
        echo "Could not compile the assembler natives!"
        exit 1
      else
       echo "Done!"
      fi
    else
      echo "Skipping compilation of x64 assembler!"
    fi

    echo "Compiling linker natives ..."
    gcc $flag -I"$jdk_installation\\include\\" -I"$jdk_installation\\include\\win32\\" -m32 -shared -o ld_32.dll ld.c
    if [ $? -ne 0 ]; then
    	echo "Could not compile the linker natives!"
    	exit 1
    fi
    
    if [ "$(expr substr $(uname -s) 1 10)" != "MINGW64_NT" ]; then
      gcc $flag -I"$jdk_installation\\include\\" -I"$jdk_installation\\include\\win32\\" -m64 -shared -o ld_64.dll ld.c
      if [ $? -ne 0 ]; then
        echo "Could not compile the linker natives!"
        exit 1
      else
       echo "Done!"
      fi
    else
      echo "Skipping compilation of x64 assembler!"
    fi

else
    echo "Compiling with gcc under linux ..."

    echo "Compiling assembler natives ..."
    gcc -I$jdk_installation/include/ -I$jdk_installation/include/linux/ -m32 -shared -o as_32.so -fPIC as.c
    if [ $? -ne 0 ]; then
    	echo "Could not compile the assembler natives!"
    	exit 1
    fi
    gcc -I$jdk_installation/include/ -I$jdk_installation/include/linux/ -m64 -shared -o as_64.so -fPIC as.c
    if [ $? -ne 0 ]; then
      echo "Could not compile the assembler natives!"
      exit 1
    else
     echo "Done!"
    fi

    echo "Compiling linker natives ..."
    gcc -I$jdk_installation/include/ -I$jdk_installation/include/linux/ -m32 -shared -o ld_32.so -fPIC ld.c
    if [ $? -ne 0 ]; then
    	echo "Could not compile the linker natives!"
    	exit 1
    fi
    gcc -I$jdk_installation/include/ -I$jdk_installation/include/linux/ -m64 -shared -o ld_64.so -fPIC ld.c
    if [ $? -ne 0 ]; then
      echo "Could not compile the linker natives!"
      exit 1
    else
     echo "Done!"
    fi
    
    win=""
    while [ "$win" != "y" ] && [ "$win" != "n" ]; do
      echo "Also compile the windows libraries (requires mingw to be installed)? [y/n]"
      printf "> "
      read win
    done
    
    if [ "$win" == "y" ]; then
      echo "Compiling assembler natives ..."
      i686-w64-mingw32-gcc -I$jdk_installation/include/ -I$jdk_installation/include/linux/ -m32 -shared -o as_32.dll as.c
      if [ $? -ne 0 ]; then
        echo "Could not compile the assembler natives!"
        exit 1
      fi
      x86_64-w64-mingw32-gcc -I$jdk_installation/include/ -I$jdk_installation/include/linux/ -m64 -shared -o as_64.dll as.c
      if [ $? -ne 0 ]; then
        echo "Could not compile the assembler natives!"
        exit 1
      else
       echo "Done!"
      fi
  
      echo "Compiling linker natives ..."
      i686-w64-mingw32-gcc -I$jdk_installation/include/ -I$jdk_installation/include/linux/ -m32 -shared -o ld_32.dll ld.c
      if [ $? -ne 0 ]; then
        echo "Could not compile the linker natives!"
        exit 1
      fi
      x86_64-w64-mingw32-gcc -I$jdk_installation/include/ -I$jdk_installation/include/linux/ -m64 -shared -o ld_64.dll ld.c
      if [ $? -ne 0 ]; then
        echo "Could not compile the linker natives!"
        exit 1
      else
       echo "Done!"
      fi
    
    fi
    
fi




