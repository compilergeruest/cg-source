	.code
	.data
	.export	heap_size
	.align	4
heap_size:
	.word	0x400000
	.export	heap_gap
	.align	4
heap_gap:
	.word	0x400
	.export	stack_size
	.align	4
stack_size:
	.word	0x200000
	.align	4
free_ptr:
	.word	0x0
	.align	4
initialized:
	.word	0x0
	.export	_malloc
	.code
	.align	4
_malloc:
	sub	$29,$29,16
	stw	$20,$29,0
	stw	$21,$29,4
	stw	$22,$29,8
	stw	$23,$29,12
	ldw	$24,$0,initialized
	bne	$24,$0,L.3
	ldw	$24,$0,heap_start
	stw	$24,$0,free_ptr
	ldw	$24,$0,free_ptr
	stw	$0,$24,4
	ldw	$24,$0,free_ptr
	stw	$0,$24,0
	ldw	$24,$0,free_ptr
	ldw	$15,$0,heap_size
	sub	$15,$15,12
	stw	$15,$24,8
	add	$24,$0,1
	stw	$24,$0,initialized
L.3:
	ldw	$24,$0,free_ptr
	beq	$24,$0,L.5
	ldw	$23,$0,free_ptr
	add	$22,$0,$0
	add	$24,$0,12
	add	$15,$4,12
	sub	$15,$15,1
	divu	$15,$15,12
	mulu	$4,$24,$15
	ldw	$24,$0,heap_size
	sub	$24,$24,12
	bleu	$4,$24,L.10
	add	$2,$0,$0
	j	L.2
L.9:
	ldw	$23,$23,0
L.10:
	ldw	$24,$23,0
	bne	$24,$0,L.9
L.12:
	ldw	$22,$23,8
	bltu	$22,$4,L.15
	add	$24,$4,24
	bltu	$22,$24,L.17
	stw	$4,$23,8
	add	$24,$0,$23
	add	$24,$24,12
	add	$24,$24,$4
	add	$21,$0,$24
	ldw	$24,$23,0
	stw	$24,$21,0
	ldw	$24,$23,4
	stw	$24,$21,4
	ldw	$24,$23,0
	beq	$24,$0,L.19
	ldw	$24,$23,0
	stw	$21,$24,4
L.19:
	ldw	$24,$23,4
	beq	$24,$0,L.21
	ldw	$24,$23,4
	stw	$21,$24,0
L.21:
	sub	$24,$22,$4
	sub	$24,$24,12
	stw	$24,$21,8
	add	$24,$0,$23
	ldw	$15,$0,free_ptr
	bne	$24,$15,L.18
	stw	$21,$0,free_ptr
	j	L.18
L.17:
	ldw	$24,$23,0
	beq	$24,$0,L.25
	ldw	$24,$23,0
	ldw	$15,$23,4
	stw	$15,$24,4
L.25:
	ldw	$24,$23,4
	beq	$24,$0,L.27
	ldw	$24,$23,4
	ldw	$15,$23,0
	stw	$15,$24,0
L.27:
	ldw	$24,$0,free_ptr
	add	$15,$0,$23
	bne	$24,$15,L.29
	ldw	$24,$23,0
	beq	$24,$0,L.32
	ldw	$20,$23,0
	j	L.33
L.32:
	ldw	$20,$23,4
L.33:
	stw	$20,$0,free_ptr
L.29:
L.18:
	add	$24,$0,$23
	add	$24,$24,12
	add	$2,$0,$24
	j	L.2
L.15:
	ldw	$23,$23,4
L.13:
	add	$24,$0,$23
	bne	$24,$0,L.12
L.5:
	add	$2,$0,$0
L.2:
	ldw	$20,$29,0
	ldw	$21,$29,4
	ldw	$22,$29,8
	ldw	$23,$29,12
	add	$29,$29,16
	jr	$31

	.export	_free
	.align	4
_free:
	sub	$29,$29,16
	stw	$22,$29,0
	stw	$23,$29,4
	add	$24,$0,$4
	sub	$24,$24,12
	add	$23,$0,$24
	ldw	$24,$0,initialized
	beq	$24,$0,L.35
	add	$24,$0,$4
	beq	$24,$0,L.35
	ldw	$15,$0,heap_start
	bltu	$23,$15,L.35
	ldw	$14,$0,heap_size
	add	$15,$15,$14
	bgeu	$23,$15,L.35
	ldw	$24,$0,free_ptr
	bne	$24,$0,L.37
	stw	$23,$0,free_ptr
	stw	$0,$23,4
	stw	$0,$23,0
	j	L.38
L.37:
	ldw	$22,$0,free_ptr
	j	L.40
L.39:
	ldw	$22,$22,0
L.40:
	ldw	$24,$22,0
	bne	$24,$0,L.39
	j	L.43
L.42:
	ldw	$22,$22,4
L.43:
	add	$24,$0,$22
	add	$15,$0,$23
	bgeu	$24,$15,L.45
	ldw	$24,$22,4
	bne	$24,$0,L.42
L.45:
	add	$24,$0,$22
	add	$15,$0,$23
	bleu	$24,$15,L.46
	add	$14,$23,12
	ldw	$13,$23,8
	add	$14,$14,$13
	beq	$14,$22,L.50
	add	$15,$22,12
	ldw	$14,$22,8
	add	$15,$15,$14
	bne	$15,$23,L.48
L.50:
	add	$24,$23,8
	ldw	$15,$24,0
	ldw	$14,$22,8
	add	$14,$14,12
	add	$15,$15,$14
	stw	$15,$24,0
	ldw	$24,$22,0
	stw	$24,$23,0
	ldw	$24,$22,4
	stw	$24,$23,4
	ldw	$24,$23,4
	beq	$24,$0,L.51
	ldw	$24,$23,4
	stw	$23,$24,0
L.51:
	ldw	$24,$23,0
	beq	$24,$0,L.49
	ldw	$24,$23,0
	stw	$23,$24,4
	j	L.49
L.48:
	stw	$22,$23,4
	ldw	$24,$22,0
	stw	$24,$23,0
	stw	$23,$22,0
	ldw	$24,$23,0
	beq	$24,$0,L.55
	ldw	$24,$23,0
	stw	$23,$24,4
L.55:
L.49:
	ldw	$22,$23,0
	add	$24,$0,$22
	beq	$24,$0,L.57
	add	$14,$22,12
	ldw	$13,$22,8
	add	$14,$14,$13
	beq	$14,$23,L.61
	add	$15,$23,12
	ldw	$14,$23,8
	add	$15,$15,$14
	bne	$15,$22,L.47
L.61:
	add	$24,$22,8
	ldw	$15,$24,0
	ldw	$14,$23,8
	add	$14,$14,12
	add	$15,$15,$14
	stw	$15,$24,0
	ldw	$24,$23,4
	stw	$24,$22,4
	ldw	$24,$22,4
	beq	$24,$0,L.47
	ldw	$24,$22,4
	stw	$22,$24,0
	j	L.47
L.57:
	stw	$23,$0,free_ptr
	j	L.47
L.46:
	add	$14,$22,12
	ldw	$13,$22,8
	add	$14,$14,$13
	beq	$14,$23,L.66
	add	$15,$23,12
	ldw	$14,$23,8
	add	$15,$15,$14
	bne	$15,$22,L.64
L.66:
	add	$24,$22,8
	ldw	$15,$24,0
	ldw	$14,$23,8
	add	$14,$14,12
	add	$15,$15,$14
	stw	$15,$24,0
	j	L.65
L.64:
	stw	$23,$22,4
	stw	$22,$23,0
	stw	$0,$23,4
L.65:
L.47:
L.38:
L.35:
L.34:
	ldw	$22,$29,0
	ldw	$23,$29,4
	add	$29,$29,16
	jr	$31

	.import	heap_start
