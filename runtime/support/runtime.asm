; Runtime Support Module

.import msgout
.import exit

.data
.align 4

buffer: .byte "0x00000000", 0

.code
.align 4

.export printx
printx:
  sub $29, $29, 24
  stw $31, $29, 20
  stw $16, $29, 16
  stw $17, $29, 12
  stw $18, $29, 8
  stw $19, $29, 4

  ldw $16, $29, 24 ; load argument
  add $19, $0, 9 ; buffer index
printxLoop:
  remu $17, $16, 16
  add $18, $0, 10
  bgeu $17, $18, alphabetical
  add $17, $17, 48
  j printxLoopEnd
alphabetical:
  add $17, $17, 55
printxLoopEnd:
  stb $17, $19, buffer
  sub $19, $19, 1
  divu $16, $16, 16
  bne $16, $0, printxLoop
  add $18, $0, 1
  add $17, $0, 48
  j printxFillLoopEnd
printxFillLoop:
  stb $17, $19, buffer
  sub $19, $19, 1
printxFillLoopEnd:
  bgtu $19, $18, printxFillLoop
  add $18, $0, buffer
  stw $18, $29, 0
  jal msgout

  ldw $19, $29, 4
  ldw $18, $29, 8
  ldw $17, $29, 12
  ldw $16, $29, 16
  ldw $31, $29, 20
  add $29, $29, 24
  jr $31

.export _stackOverflowException
_stackOverflowException:
  sub $29, $29, 12
  stw $31, $29, 8
  stw $16, $29, 4
  add $16, $0, _stackOverflowExceptionMsg
  stw $16, $29, 0
  jal msgout
  jal exit
  ; following code is for completeness, should never be executed
  ldw $16, $29, 4
  ldw $31, $29, 8
  add $29, $29, 12
  jr $31

.export _nullPointerException
_nullPointerException:
  sub $29, $29, 12
  stw $31, $29, 8
  stw $16, $29, 4
  add $16, $0, _nullPointerExceptionMsg
  stw $16, $29, 0
  jal msgout
  jal exit
  ; following code is for completeness, should never be executed
  ldw $16, $29, 4
  ldw $31, $29, 8
  add $29, $29, 12
  jr $31

.export _illegalArraySize
_illegalArraySize:
  sub $29, $29, 12
  stw $31, $29, 8
  stw $16, $29, 4
  add $16, $0, _illegalArraySizeMsg
  stw $16, $29, 0
  jal msgout
  jal exit
  ; following code is for completeness, should never be executed
  ldw $16, $29, 4
  ldw $31, $29, 8
  add $29, $29, 12
  jr $31

.data
.align 4

_stackOverflowExceptionMsg:
  .byte "Skeleton: stack overflow exception"
  .byte 0xD, 0xA, 0

_nullPointerExceptionMsg:
  .byte "Skeleton: null pointer exception"
  .byte 0xD, 0xA, 0

_illegalArraySizeMsg:
  .byte "Skeleton: illegal array size exception"
  .byte 0xD, 0xA, 0
