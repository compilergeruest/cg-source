#ifndef HEAP_MANAGER_H
#define HEAP_MANAGER_H

/** The null pointer */
#define NULL					((void*) 0)

/** The boolean type */
typedef enum { false, true } bool;

/** The layout of a single free-block */
typedef struct block_t {
	struct block_t* prev;
	struct block_t* next;
	unsigned int size;
}* Block;

/** The size of a free-block header */
#define HEADER_SIZE				(sizeof(struct block_t))

/** The heap size in bytes */
extern const unsigned int heap_size;
/** The gap between heap and stack */
extern const unsigned int heap_gap;
/** The stack size in bytes */
extern const unsigned int stack_size;

#define IS_ADJACENT(a, b)		(((unsigned int) (a)) + HEADER_SIZE + (a)->size == ((unsigned int) (b)) || ((unsigned int) (b)) + HEADER_SIZE + (b)->size == ((unsigned int) (a)))
#define NEXT_MULTIPLE_OF(n, m)  ((((n) + (m) - 1) / (m)) * (m))
#define IS_NULL(ptr)			(((unsigned int) (ptr)) == ((unsigned int) NULL))
#define NON_NULL(ptr)			(!IS_NULL((ptr)))

/**
 * Marks the start of the heap, meaning that this variable
 * holds the address which points to the end of the bss segment
 */
extern const Block heap_start;

/**
 * Allocates the given amount of bytes, rounded up to the nearest
 * multiple of 8 and returns a pointer to the allocated memory
 */
void* _malloc(unsigned int bytes);

/**
 * Frees the given block of memory pointed to by the given address
 */
void _free(void* address);

#endif
