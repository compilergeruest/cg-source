#include "heap-manager.h"

/** The heap size in bytes */
const unsigned int heap_size = 0x400000;
/** The gap between heap and stack */
const unsigned int heap_gap = 1024;
/** The stack size in bytes */
const unsigned int stack_size = 0x200000;

/* ensure that this pointer always holds the first of the whole free-list, otherwise the algorithm will be slowed down */
static Block free_ptr = NULL;
static bool initialized = false;

/**
 * Allocates the given amount of bytes, rounded up to the nearest
 * multiple of 8 and returns a pointer to the allocated memory
 */
void* _malloc(unsigned int bytes) {
	if (!initialized) {
		free_ptr = heap_start;
		free_ptr->next = NULL;
		free_ptr->prev = NULL;
		free_ptr->size = heap_size - HEADER_SIZE;
		initialized = true;
	}
	if (NON_NULL(free_ptr)) {
		Block current = free_ptr;
		Block tmp;
		unsigned int free = 0;
		bytes = NEXT_MULTIPLE_OF(bytes, HEADER_SIZE);
		if (bytes > heap_size - HEADER_SIZE) {
			return NULL;
		}
		// go back to the beginning, if the free_ptr is correctly initialized this loop should be skipped immediately
		while (NON_NULL(current->prev)) {
			current = current->prev;
		}
		do {
			free = current->size;
			// found a block with a large enough data area ?
			if (free >= bytes) {
				// enough free space to split the data area into a new free-block ?
				if (free >= bytes + HEADER_SIZE * 2) {
					current->size = bytes;
					// create the new free-block
					tmp = (Block) (((unsigned int) current) + HEADER_SIZE + bytes);
					tmp->prev = current->prev;
					tmp->next = current->next;
					if (NON_NULL(current->prev)) {
						current->prev->next = tmp;
					}
					if (NON_NULL(current->next)) {
						current->next->prev = tmp;
					}
					// set the size of the created free-block
					tmp->size = free - bytes - HEADER_SIZE;
					// set the free-block for the next call of `_malloc`
					if (current == free_ptr) {
						free_ptr = tmp;
					}
				} else {
					// just skip the allocated block in the free-list
					if (NON_NULL(current->prev)) {
						current->prev->next = current->next;
					}
					if (NON_NULL(current->next)) {
						current->next->prev = current->prev;
					}
					// set the free-block for the next call of `_malloc`
					if (free_ptr == current) {
						free_ptr = NON_NULL(current->prev) ? current->prev : current->next;
					}
				}
				return (void*) (((unsigned int) current) + HEADER_SIZE);
			}
			// get the next pointer
			current = current->next;
		} while (NON_NULL(current));
	}
	return NULL;
}

void _free(void* address) {
	Block allocated = (Block) (((unsigned int) address) - HEADER_SIZE);
	if (initialized && NON_NULL(address) && ((unsigned int) allocated) >= ((unsigned int) heap_start) && ((unsigned int) allocated) < ((unsigned int) heap_start) + heap_size) {
		if (IS_NULL(free_ptr)) {
			// use `address` as new free-list
			free_ptr = allocated;
			allocated->next = NULL;
			allocated->prev = NULL;
		} else {
			Block current = free_ptr;
			// go back to the beginning, if the free_ptr is correctly initialized this loop should be skipped immediately
			while (NON_NULL(current->prev)) {
				current = current->prev;
			}
			// skip blocks as long as they are smaller than the allocated one
			while (((unsigned int) current) < ((unsigned int) allocated) && NON_NULL(current->next)) {
				current = current->next;
			}
			// either: current->next == NULL
			// or: current > allocated (== not possible, because that would mean current == allocated, where allocated is not a free-block)
			if (((unsigned int) current) > ((unsigned int) allocated)) {
				// current is the first free-block with a higher address (compared to `allocated`)
				// current->prev is the last free-block with a lower address (compared to `allocated`)
				if (IS_ADJACENT(allocated, current)) {
					allocated->size += HEADER_SIZE + current->size;
					allocated->prev = current->prev;
					allocated->next = current->next;
					if (NON_NULL(allocated->next)) {
						allocated->next->prev = allocated;
					}
					if (NON_NULL(allocated->prev)) {
						allocated->prev->next = allocated;
					}
				} else {
					allocated->next = current;
					allocated->prev = current->prev;
					current->prev = allocated;
					if (NON_NULL(allocated->prev)) {
						allocated->prev->next = allocated;
					}
				}
				// `allocated` may have consumed `current` -> set previous to current
				current = allocated->prev;
				// `current` is the last free-block with a lower address (compared to `allocated`)
				if (NON_NULL(current)) {
					if (IS_ADJACENT(current, allocated)) {
						current->size += HEADER_SIZE + allocated->size;
						current->next = allocated->next;
						if (NON_NULL(current->next)) {
							current->next->prev = current;
						}
					}
				} else {
					free_ptr = allocated;
				}
			} else {
				// current->next == NULL, meaning that there is no free-block in the free-list with a higher address than `allocated`
				if (IS_ADJACENT(current, allocated)) {
					current->size += HEADER_SIZE + allocated->size;
				} else {
					current->next = allocated;
					allocated->prev = current;
					allocated->next = NULL;
				}
			}
		}
	}
}

