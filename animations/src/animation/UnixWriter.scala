package animation

import java.io.PrintWriter
import java.io.OutputStream

class UnixWriter(out: OutputStream, autoFlush: Boolean)
  extends PrintWriter(out, autoFlush) {

  def this(out: OutputStream) = this(out, true)

  override def println() = print("\n")
}