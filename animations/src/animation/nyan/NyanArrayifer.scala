package animation.nyan

import java.io.File
import java.io.FileOutputStream
import java.io.PrintWriter

import scala.io.Source
import scala.collection.mutable.{Map => MutMap}

import animation._
import scala.collection.mutable.ArrayBuffer

object NyanArrayifer extends EcoAnimation {

  def load(): Frames = {
    val raw = asset("/assets/NyanArray.txt").mkString
    val subArrays = raw.replaceAll("(^\\s*\\{\\s*)|(\\s*\\}\\s*$)", "").split("\\}\\s*,\\s*\\{") map (_.trim)
    subArrays map { _.replaceAll("(^\")|(\"$)", "").split("\"\\s*,\\s*\"") map (_.trim) }
  }

  type ColMap = Mapping[Char, Int]
  val ColMap = Mapping[Char, Int] _

  val map = ColMap(0xff1493, Seq(
    ',' -> 0x00008b,
    '.' -> 0xfefefe,
    39.toChar -> 0x333333,
    '@' -> 0xd3d3d3,
    '$' -> 0xff1493,
    '-' -> 0x8b008b,
    '>' -> 0xdc143c,
    '&' -> 0xff8c00,
    '+' -> 0xffd700,
    '#' -> 0x00ff00,
    '=' -> 0xadd8e6,
    ';' -> 0x00ffff,
    '*' -> 0x808080))

  override def escape(c: Char): String = c match {
    case '\'' => "\\'"
    case c    => c.toString
  }

  private def colorLookup(argname: String, out: PrintWriter, map: ColMap, indent: Int) = {
    import out._
    val indents = MutMap[Int, String]()
    def mkIndent(indent: Int) =
      print(indents.getOrElseUpdate(indent, "\t" * indent))

    def go(map: ColMap, indent: Int): Unit = map match {
      case Leaf(col) =>
        mkIndent(indent)
        println(s"return 0x${col.toHexString};")
      case Node(from, to, left, right) =>
        mkIndent(indent)
        println(s"if ($argname < '${escape(from)}') {")
        go(left, indent + 1)
        mkIndent(indent)
        println(s"} else if ($argname > '${escape(from)}') {")
        go(right, indent + 1)
        mkIndent(indent)
        println(s"} else return 0x${to.toHexString};")
    }
    go(map, indent)
  }

  def write(out: PrintWriter) = {
    import out._
    val frames = load()

    val nFrames = frames.length
    val nLines = frames.head.length
    val nChars = frames.head.head.length

    println()
    println(s"type Chars = char[];")
    println(s"type Lines = Chars[];")
    println(s"type Frames = Lines[];")
    println()
    println(s"Frames arr = new char[$nFrames][$nLines][];")
    println()

    val framez = MutMap[String, String]()
    val delLits = ArrayBuffer[String]()
    val reuseStrLits = true
    val debugIsNull  = false

    if (debugIsNull) {
      println("""
        |unit ensureNonNull(int x, int y)
        |  if (arr[x][y] == null) {
        |    printc('a'); printc('r'); printc('r'); printc('[');
        |    printi(x);
        |    printc(']');printc('[');
        |    printi(y);
        |    printc(']');
        |    printc(' '); printc('i'); printc('s'); printc(' ');
        |    printc('n'); printc('u'); printc('l'); printc('l');
        |    printc('!'); printc('\n');
        |    exit();
        |  }
        |
        |if (arr == null) {
        |  printc('a'); printc('r'); printc('r');
        |  printc(' '); printc('i'); printc('s'); printc(' ');
        |  printc('n'); printc('u'); printc('l'); printc('l');
        |  printc('!'); printc('\n');
        |}
        |""".stripMargin)
    }

    for (fidx <- frames.indices) {
      val frame = frames(fidx)
      println(s"// Frame ${fidx + 1} of ${frame.size}")
      for (lidx <- frame.indices) {
        val line = frame(lidx)

        if (reuseStrLits) {
          if (framez contains line) {
            println(s"""arr[$fidx][$lidx] = ${framez(line)};""")
          } else {
            println(s"""arr[$fidx][$lidx] = "$line";""")
            if (debugIsNull) println(s"ensureNonNull($fidx, $lidx);")
            delLits += s"del arr[$fidx][$lidx];"
            framez += line -> s"arr[$fidx][$lidx]"
          }
        } else {
          println(s"""arr[$fidx][$lidx] = "$line";""")
        }
      }
      println()
    }
    println()

    println("int colorFor(char c) {")
    colorLookup("c", writer, map, 1)
    println("}")
    println()

    println("while (true)");
    val dest = 480
    val src = 64
    val ratio = dest / src
      println(s"\tfor (int i <- 0 .. $nFrames)")
      println(s"\tfor (int j <- 0 .. $nLines)")
      println(s"\tfor (int k <- 0 .. $nChars)")
      println(s"\tfor (int r <- 0 .. $ratio) {")
        println(s"\t\tdrawLine(k * $ratio + r, j * $ratio, k * $ratio + r, j * $ratio + $ratio, colorFor(arr[i][j][k]));")
      println("\t}")

    println()

    if (reuseStrLits) {
      delLits.grouped(4) foreach { line =>
        for (elem <- line) {
          print(elem)
          print(" ")
        }
        println()
      }

      println("for (int i <- 0 .. #arr)")
      println("\tdel arr[i];")
      println()
    } else {
      println("for (int i <- 0 .. #arr)")
      println("\tfor (int j <- 0 .. #arr[i]) {")
        println("\t\tfor (int k <- 0 .. #arr[i][j]) {")
          println("\t\t\tdel arr[i][j];")
        println("\t\t}")
        println("\t\tdel arr[i];")
      println("\t}")
    }

    println("del arr;")
  }

  val writer = new UnixWriter(new FileOutputStream(new File("nyan.xpl")))
  write(writer)
  writer.flush()

}
