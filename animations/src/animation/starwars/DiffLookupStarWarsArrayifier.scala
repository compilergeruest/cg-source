package animation.starwars

import animation.EcoAnimation
import scala.collection.mutable.ArrayBuffer
import scala.collection.mutable.{Map => MutMap}
import java.io.FileOutputStream
import animation.UnixWriter
import java.io.File
import java.io.PrintWriter
import java.awt.Font
import java.awt.image.BufferedImage
import java.awt.image.BufferedImage.TYPE_BYTE_BINARY
import java.awt.RenderingHints._
import java.awt.Color
import java.util.BitSet
import scala.util.Sorting

object DiffLookupStarWarsArrayifier extends EcoAnimation {

  private val eof = "ÿ"
  private val charWidth = 7
  private val charHeight = 9

  private val debugIsNull = false

  def escapes(s: String): String = {
    val b = new StringBuilder()
    var i = 0
    while (i < s.length()) {
      val c = s(i)
      c match {
        case '\\' => b += '\\' += '\\'
        case '"'  => b += '\\' += '"'
        case _    => b ++= escape(c)
      }
      i += 1
    }
    b.toString
  }

  def allchars(frames: Frames): Array[Char] =
    (for {
      frame <- frames
      line <- frame
      char <- line
    } yield char).distinct

  private def write(out: PrintWriter): Unit = {
    import out._
    val (frames, count) = load()

    val nFrames = frames.length
    val nLines = 13
    val nChars = maxLine(frames) // 76

    val bitsetLen = math.ceil(nLines * nChars / 32f).toInt // 39
    val chars = allchars(frames)
    val minChar = chars.minBy(_.toInt).toInt
    val maxChar = chars.maxBy(_.toInt).toInt

    def explode(frame: Frame): Frame =
      frame map { line =>
        (Array.tabulate(nChars) { i =>
          if (line.indices contains i) line(i)
          else ' '
        }).mkString
      }

    def diff(old: Int, neu: Int): (Array[Int], String) =
      if (old >= 0) {
        val frameOld = explode(frames(old))
        val frameNew = explode(frames(neu))

        val bitset = Array.fill(bitsetLen)(0)
        val buffer = new StringBuilder()

        for (line <- 0 until nLines; col <- 0 until nChars) {
          val ochar = frameOld(line)(col)
          val nchar = frameNew(line)(col)

          if (ochar != nchar) {
            val bit = col + line * nChars
            val chunkIdx = bit / 32
            val offs = bit % 32

            var chunk = bitset(chunkIdx)
            chunk |= (1 << offs)
            bitset(chunkIdx) = chunk

            buffer += nchar
          }
        }
        (bitset, buffer.toString())
      } else {
        val bitset = Array.fill(bitsetLen)(0xFFFFFFFF)
        val data = explode(frames(neu)).mkString
        (bitset, data)
      }

    val starWarsLogo =
      Seq(
        " ",
        "                    8888888888  888    88888",
        "                   88     88   88 88   88  88",
        "                    8888  88  88   88  88888",
        "                       88 88 888888888 88   88",
        "                88888888  88 88     88 88    888888",
        " ",
        "                88  88  88   888    88888    888888",
        "                88  88  88  88 88   88  88  88",
        "                88 8888 88 88   88  88888    8888",
        "                 888  888 888888888 88   88     88",
        "                  88  88  88     88 88    8888888",
        " ")
    println()
    println("/* ")
    for (line <- starWarsLogo) {
      print(" * ")
      println(line)
    }
    println(" * star-wars ascii frame information taken from: http://www.asciimation.co.nz/")
    println(" * copyright by Simon Jansen")
    println(" */")

    println(s"""
      |type Frame = struct {
      |  int[] diff;  // bitset, length: $bitsetLen
      |  char[] data; // changed pixels
      |  int length;  // frame length
      |};
      |
      |const int WHITE = 0xFFFFFF;
      |const int BLACK = 0x000000;
      |
      |int sleepTime = 0;""".stripMargin)

    println(s"""
      |const int CWIDTH = $charWidth;
      |const int CHEIGHT = $charHeight;
      |const int LTHRESH = 32;
      |
      |const int[] lookup = new int[${maxChar - minChar + 1} * 2];""".stripMargin)
    if (debugIsNull)
      println(s"""if (lookup == null) {
      |  printc('l'); printc('o'); printc('o'); printc('k'); printc('u'); printc('p');
      |  printc(' '); printc('i'); printc('s'); printc(' ');
      |  printc('n'); printc('u'); printc('l'); printc('l'); printc('!');
      |  exit();
      |}""".stripMargin)

    println(s"""
      |unit draw(Frame frame) {
      |  int pixel = 0;
      |
      |  for (int line <- 0 .. $nLines) {
      |    for (int col <- 0 .. $nChars) {
      |      const int bit = col + line * $nChars;
      |      const int chunk = bit / 32;
      |      const int offs = bit % 32;
      |
      |      if ((frame.diff[chunk] & (1 << offs)) != 0) {
      |        drawChar(frame.data[pixel], col, line);
      |        pixel = pixel + 1;
      |      }
      |    }
      |  }
      |
      | sleep(frame.length);
      | del frame.diff;
      | del frame.data;
      | del frame;
      |}""".stripMargin)

    println("""
      |unit play() {""".stripMargin)
      print("  ")
      println(printPerLine(frames.indices, 10) { (_, x) =>
        s"draw(frame$x());"
      })
    println("""}""")

    println(s"""
      |unit initTime() {
      |  int end = time() + 1;
      |  int cnt = 0;
      |  while (end > time()) cnt = cnt + 1;
      |  sleepTime = cnt;
      |}""".stripMargin)

    println("unit initLookup() {")
    for (char <- chars) {
      val looi = (char.toChar - minChar) * 2
      val (a, b) = drawInstructions(char)
      if (a != 0) println(s"  lookup[${looi + 0}] = 0b${a.toBinaryString};")
      if (b != 0) println(s"  lookup[${looi + 1}] = 0b${b.toBinaryString};")
    }
    println("}")

    println(s"""
      |unit sleep(int length) {
      |  int countdown = sleepTime * length;
      |  while (countdown > 0) {
      |    countdown = countdown - 1;
      |  }
      |}""".stripMargin)

    def printInstructions(drawInsns: Vector[String], indent: Int): String = {
      val b = new StringBuilder()
      val indentstr = "  " * indent

      if (drawInsns.nonEmpty) {
        val n = 3
        var cnt = 0
        val iter = drawInsns.iterator
        b ++= "    "

        while (iter.hasNext) {
          if (cnt != 0 && cnt % n == 0) b ++= indentstr
          b ++= iter.next()
          cnt += 1
          if (iter.hasNext) {
            if (cnt % n == 0) b += '\n'
            else b += ' '
          } else {
            b += '\n'
          }
        }
      }

      b.toString
    }

    println(s"""
      |unit drawChar(char c, int x, int y) {
      |  const int xx = x * 7;
      |  const int yy = y * 9;
      |
      |  const int li = ((int) c - $minChar) * 2;
      |  const int a = lookup[li];
      |  const int b = lookup[li + 1];
      |
      |  for (int y <- 0 .. CHEIGHT) {
      |    for (int x <- 0 .. CWIDTH) {
      |      const int i = x + y * CWIDTH;
      |      int color = BLACK;
      |
      |      if (i < LTHRESH) {
      |        if ((a & (1 << i)) != 0) color = WHITE;
      |      } else {
      |        const int j = i - LTHRESH;
      |        if ((b & (1 << j)) != 0) color = WHITE;
      |      }
      |
      |      setPixel(xx + x, yy + y, color);
      |    }
      |  }
      |}""".stripMargin)

    def frame(x: Int) = { // 0 until 3411
      val (bitset, data) = diff(x - 1, x)
      val b = printPerLine(bitset, 8) { (i, x) =>
        s"diff[$i] = 0b${x.toBinaryString};"
      }

      val diffNull = if (!debugIsNull) "" else
        s"""if (diff == null) {
          |    printc('f'); printc('r'); printc('a'); printc('m'); printc('e'); printc('s');
          |    printc('['); printi($x); printc(']'); printc('.');
          |    printc('d'); printc('i'); printc('f'); printc('f');
          |    printc(' '); printc('i'); printc('s'); printc(' ');
          |    printc('n'); printc('u'); printc('l'); printc('l'); printc('!');
          |    exit();
          |  }
          |  """.stripMargin

      val frameData = if (debugIsNull) "data" else s""""${escapes(data)}""""
      val dataInit = if (!debugIsNull) "" else
        s"""
          |
          |  char[] data = "${escapes(data)}";
          |  if (data == null) {
          |    printc('f'); printc('r'); printc('a'); printc('m'); printc('e'); printc('s');
          |    printc('['); printi($x); printc(']'); printc('.');
          |    printc('d'); printc('a'); printc('t'); printc('a');
          |    printc(' '); printc('i'); printc('s'); printc(' ');
          |    printc('n'); printc('u'); printc('l'); printc('l'); printc('!');
          |    exit();
          |  }""".stripMargin

      val frameReturn = if (debugIsNull) "frame" else
        s"""new Frame {
          |    diff = diff;
          |    data = $frameData;
          |    length = ${count(x)};
          |  }""".stripMargin
      val frameInit = if (!debugIsNull) "" else
        s"""
          |
          |  Frame frame = new Frame {
          |    diff = diff;
          |    data = $frameData;
          |    length = ${count(x)};
          |  };
          |  if (frame == null) {
          |    printc('f'); printc('r'); printc('a'); printc('m'); printc('e');
          |    printc('['); printi($x); printc(']');
          |    printc(' '); printc('i'); printc('s'); printc(' ');
          |    printc('n'); printc('u'); printc('l'); printc('l'); printc('!');
          |    exit();
          |  }""".stripMargin

      println(s"""
      |Frame frame$x() {
      |  int[] diff = new int[$bitsetLen];
      |  $diffNull$b$dataInit$frameInit
      |
      |  printi($x);
      |  printc('\\n');
      |
      |  return $frameReturn;
      |}""".stripMargin)
    }

    for (fidx <- frames.indices) {
      frame(fidx)
    }

    println()
    println("initTime();")
    println("initLookup();")
    println("clearAll(BLACK);")
    println("play();")
    println()
    println("del lookup;");
  }

  private def printPerLine[T](xs: Iterable[T], n: Int)(f: (Int, T) => String) = {
    val b = new StringBuilder()
    var cnt = 0
    val iter = xs.iterator
    while (iter.hasNext) {
      if (cnt != 0 && cnt % n == 0) b ++= "  "
      b ++= f(cnt, iter.next())
      cnt += 1
      if (cnt % n == 0) b += '\n'
      else b += ' '
    }
    b.toString
  }

  private def drawInstructions(c: Char): (Int, Int) = {
    val font = new Font("courier", Font.PLAIN, 9)

    val img = new BufferedImage(charWidth, charHeight, TYPE_BYTE_BINARY)
    val g = img.createGraphics()
    import g._
    setFont(font)
    val metrics = getFontMetrics

    setColor(Color.BLACK)
    fillRect(0, 0, charWidth, charHeight)
    setRenderingHint(KEY_TEXT_ANTIALIASING, VALUE_TEXT_ANTIALIAS_OFF)
    setColor(Color.WHITE)
    drawString(c.toString, 0, 7)
    dispose

    var a = 0
    var b = 0

    def setBits(i: Int, flag: Boolean): Unit =
      if (i < 32) {
        if (flag) a |= (1 << i)
      } else {
        val j = i - 32
        if (flag) b |= (1 << j)
      }

    for (y <- 0 until charHeight; x <- 0 until charWidth)
      setBits(x + y * charWidth,
        img.getRGB(x, y) == Color.WHITE.getRGB)

    (a, b)
  }

  private def maxLine(arr: Frames): Int = {
    var max = 0
    for (frame <- arr) {
      for (line <- frame) {
        max = math.max(max, line.length())
      }
    }
    max
  }

  def load(): (Frames, Array[Int]) = {
    val raw = asset("/assets/ascii-star-wars.txt").mkString
    val lines = raw split "\\\\n"
    val rawframes = (lines grouped 14).toArray
    val framesWithCount = rawframes map { _ filterNot (_ == eof) }

    val frameBuffer = ArrayBuffer[Array[String]]()
    val countBuffer = ArrayBuffer[Int]()

    for (cnt :: arr <- framesWithCount filter (_.size > 0) map (_.toList)) {
      frameBuffer += arr.toArray
      countBuffer += cnt.toInt
    }
    val deescaped = frameBuffer.toArray map (_ map unescapeJS)
    (deescaped, countBuffer.toArray)
  }

  private def unescapeJS(s: String): String =
    if (s.isEmpty) s else s(0) match {
      case '"' => unescapeJS(s.tail)
      case '\\' => s(1) match {
        case 'b' => '\b' + unescapeJS(s.drop(2))
        case 'f' => '\f' + unescapeJS(s.drop(2))
        case 'n' => '\n' + unescapeJS(s.drop(2))
        case 'r' => '\r' + unescapeJS(s.drop(2))
        case 't' => '\t' + unescapeJS(s.drop(2))
        case '"' => '"' + unescapeJS(s.drop(2))
        case c   => c + unescapeJS(s.drop(2))
      }
      case c => c + unescapeJS(s.tail)
    }

  val stream = new FileOutputStream(new File("alongtime.xpl"))
  val writer = new UnixWriter(stream)
  write(writer)
  writer.flush()

}
