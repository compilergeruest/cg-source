package animation

import java.io.PrintWriter
import java.io.InputStream
import scala.io.Source
import scala.annotation.tailrec

trait EcoAnimation extends App {

  case class Weighted[X, W: Ordering](elem: X, weight: W)

  object Mapping {

    //    def withProbability[F, T, W: Ordering](default: T, xs: (Weighted[F, W], T)*): Mapping[F, T] = {
    //
    //      def go(seq: Vector[(Weighted[F, W], T)]): Mapping[F, T] =
    //        if (seq.isEmpty) Leaf[F, T](default)
    //        else {
    //          val (Weighted(from, weight), to) = seq.head
    //          ???
    //
    //          val rest = seq.tail
    //
    //        }
    //
    //      go(xs.toVector.sortBy { case (wt, _) => wt.weight })
    //    }

    private type Vec[F, T] = Vector[(F, T)]
    private def slice[F, T](xs: F Vec T, ys: F Vec T)(implicit ev: Ordering[F]): (F, T, F Vec T, F Vec T) = {
      import ev._
      val (from, to) = ys.head
      val rest = xs ++ ys.tail
      val lt = rest.filter { case (f, _) => f < from }
      val gx = rest.filter { case (f, _) => f >= from }
      (from, to, sort(lt), sort(gx))
    }

    private def sort[F, T](xs: F Vec T)(implicit ev: Ordering[F]) =
      xs.sortBy { case (c, _) => c }

    def apply[F, T](default: T, mappings: (F, T)*)(implicit ev: Ordering[F]): Mapping[F, T] = {
      def go(seq: Vector[(F, T)]): Mapping[F, T] =
        if (seq.isEmpty) Leaf[F, T](default)
        else (seq splitAt (seq.size / 2)) match {
          case (a, b) if a.size < b.size =>
            val (from, to, lt, gx) = slice(a, b)
            Node[F, T](from, to, go(lt), go(gx))
          case (a, b) =>
            val (from, to, lt, gx) = slice(b, a)
            Node[F, T](from, to, go(lt), go(gx))
        }
      go(sort(mappings.toVector))
    }
  }

  sealed trait Mapping[F, T]
  case class Node[F: Ordering, T](from: F, to: T, left: Mapping[F, T], right: Mapping[F, T]) extends Mapping[F, T]
  case class Leaf[F: Ordering, T](default: T) extends Mapping[F, T]

  val letters = 32 to 126
  def escape(c: Char): String = c match {
    case '\t'                    => "\\t"
    case '\n'                    => "\\n"
    case '\f'                    => "\\f"
    case '\r'                    => "\\r"
    case '\b'                    => "\\b"
    case '\\'                    => "\\\\"
    case '"'                     => "\\\""
    case c if letters contains c => c.toString
    case c                       => f"\\u${c.toInt}%04X"
  }

  def escape(s: String): String =
    (s map escape).mkString

  def asset(name: String) =
    getClass.getResourceAsStream(name) match {
      case a if (a != null) => Source.fromInputStream(a)
      case _                => null
    }

  type Frame = Array[String]
  type Frames = Array[Frame]

  def runlength(line: List[Char]): List[(Char, Int)] = line match {
    case hd :: tl => tl span (_ == hd) match {
      case (first, second) => (hd, first.length + 1) :: runlength(second)
    }
    case Nil => Nil
  }

  val tabs = Array.tabulate(50) { n => new String(new Array[Char](n)).replaceAll("\u0000", "\t") }

  def pline(out: PrintWriter, depth: Int, txt: String): Unit = {
    import out._
    var i = 0
    var c = 0

    print(tabs(depth))
    print("{ // prints(\"")
    print(txt)
    println(");")

    print(tabs(depth + 1))
    while (i < txt.length()) {
      print("printc(")
      print(txt(i).toInt)
      print(");")
      i += 1
      c += 1
      if (c >= 8) {
        c = 0
        println()
        print(tabs(depth + 1))
      }
    }
    println(); println()
    print(tabs(depth + 1))
    println("printc(0xA);")

    print(tabs(depth))
    println("}")
  }

}
