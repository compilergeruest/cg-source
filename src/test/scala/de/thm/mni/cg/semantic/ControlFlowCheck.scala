package de.thm.mni.cg.semantic

import org.scalatest.Matchers
import org.scalatest.FlatSpec

import de.thm.mni.cg.compiler.Compiler
import de.thm.mni.cg.compiler.CompilerSettings
import de.thm.mni.cg.util.cache.Caching
import de.thm.mni.cg.util._

import scala.util._

class ControlFlowCheck extends FlatSpec with Matchers with Caching with TestTools {

  val testDir = "de/thm/mni/cg/semantic/controlflow/tests/"

  "A control-flow graph" should "be used during static analysis in a compiler" in {
    assume(true)
  }

  val successfulTestCases =
    (1 to 15).map("correct" + _ + ".test") :+
    "aliasUnit.test"

  val missingReturn   = "Missing return statement"
  val unreachableCode = "Unreachable code!"

  val nonSuccessfulTestCases =
    ((1 to  8) map (_ -> missingReturn  )) ++
    ((9 to 20) map (_ -> unreachableCode))

  def testFor[T <: Try[_]: Manifest](expect: String, testname: String) =
    it should s"$expect when checking the program '$testname'" in {
      val result = asset(testDir + testname).flatMap(x => testControlFlow(x))
      result shouldBe a[T]
    }

  for ((test, message) <- nonSuccessfulTestCases) {
    val testname = s"wrong$test.test"
    it should s"fail when checking the program '$testname'" in {
      val result = asset(testDir + testname).flatMap(x => testControlFlow(x))

      failureMessage(result) { msg =>
        assume(msg contains message)
      }
    }
  }

  for (testname <- successfulTestCases) {
    testFor[Success[_]]("succeed", testname)
  }

}
