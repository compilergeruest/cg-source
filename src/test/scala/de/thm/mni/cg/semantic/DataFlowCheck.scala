package de.thm.mni.cg.semantic

import org.scalatest.Matchers
import org.scalatest.FlatSpec
import de.thm.mni.cg.compiler.Compiler
import de.thm.mni.cg.util.cache.Caching
import de.thm.mni.cg.util._
import scala.util._

class DataFlowCheck extends FlatSpec with Matchers with Caching with TestTools {

  val testDir = "de/thm/mni/cg/semantic/dataflow/tests/"

  val Var = "(?:.|[\n\r])*in line ([0-9]+)(?:.|[\n\r])*Variable '(.*)' may not have been initialized!(?:.|[\n\r])*".r

  "A data-flow analysis" should "be used during static analysis in a compiler" in {
    assume(true)
  }

  val successfulTestCases = Seq(
    "branchUsage1.test",
    "branchUsage2.test",
    "branchUsage3.test",
    "directUsage.test",
    "forUsage1.test",
    "forUsage2.test",
    "functionIndirectRecursion.test",
    "functionRecursion.test",
    "functionShadowing1.test",
    "functionUsage1.test",
    "functionUsage2.test",
    "functionUsage3.test",
    "functionUsage4.test",
    "functionUsage5.test",
    "ifElseUsage1.test",
    "ifElseUsage2.test",
    "ifElseUsage3.test",
    "ifElseUsage4.test",
    "multipleReturn1.test",
    "nestedFunction1.test",
    "nestedFunction2.test",
    "parameterKill.test",
    "scopeDirectUsage1.test",
    "scopeDirectUsage2.test",
    "scopeDirectUsage3.test",
    "scopeFor1.test",
    "scopeFor2.test",
    "scopeIfElse1.test",
    "scopeIfElse2.test",
    "scopeWhile1.test",
    "scopeWhile2.test",
    "whileUsage1.test",
    "whileUsage2.test"
  )

  val nonSuccessfulTestCases = Seq(
    "directUsage.test" -> ("a", 2),
    "firstPositionReport.test" -> ("a", 2),
    "ifElseUsage1.test" -> ("a", 3),
    "forUsage1.test" -> ("c", 3),
    "forUsage2.test" -> ("c", 4),
    "forUsage3.test" -> ("c", 2),
    "forUsage4.test" -> ("c", 2),
    "forUsage5.test" -> ("c", 2),
    "functionCallBinExp1.test" -> ("s", 9),
    "functionCallBinExp2.test" -> ("s", 9),
    "functionIndirectRecursion.test" -> ("l", 13),
    "functionRecursion.test" -> ("l", 9),
    "functionShadowing1.test" -> ("r", 13),
    "functionUsage1.test" -> ("l", 4),
    "functionUsage2.test" -> ("l", 5),
    "functionUsage3.test" -> ("l", 8),
    "functionUsage5.test" -> ("l", 6),
    "ifElseUsage2.test" -> ("a", 5),
    "ifElseUsage3.test" -> ("a", 5),
    "ifElseUsage4.test" -> ("a", 3),
    "ifElseUsage5.test" -> ("a", 2),
    "ifElseUsage6.test" -> ("a", 4),
    "ifElseUsage7.test" -> ("a", 5),
    "ifElseUsage8.test" -> ("a", 6),
    "multipleReturn1.test" -> ("a", 13),
    "multipleReturn2.test" -> ("a", 5),
    "multipleReturn3.test" -> ("a", 10),
    "nestedFunction1.test" -> ("z", 15),
    "nestedFunction2.test" -> ("z", 14),
    "parameterKill.test" -> ("h", 6),
    "scopeDirectUsage1.test" -> ("n", 3),
    "scopeDirectUsage2.test" -> ("n", 3),
    "scopeDirectUsage3.test" -> ("n", 4),
    "scopeIfElse1.test" -> ("m", 4),
    "scopeIfElse2.test" -> ("m", 7),
    "scopeWhile1.test" -> ("p", 4),
    "scopeWhile2.test" -> ("p", 4),
    "scopeFor1.test" -> ("q", 4),
    "scopeFor2.test" -> ("q", 4),
    "scopeBranch1.test" -> ("t", 14),
    "whileUsage1.test" -> ("b", 3),
    "whileUsage2.test" -> ("b", 5),
    "whileUsage3.test" -> ("b", 2)
  )

  for ((testname, expect) <- nonSuccessfulTestCases)
    it should s"fail when checking the program '$testname'" in {
      val result = asset(testDir + "wrong/" + testname).flatMap(x => testDataFlow(x))

      failureMessage(result) { msg =>
        val optInfo = Option(msg) collect { case Var(ln, name) => name -> ln.toInt }
        if (optInfo.isEmpty) {
          fail(s"Invalid format of error message in test '$testname'!")
        }

        val Some((name, line)) = optInfo
        val (ename, eline) = expect

        if (name != ename) {
          fail(s"In program '$testname' variable '$ename' should be uninitialized, but variable '$name' was uninitialized!")
        }
        if (line != eline) {
          fail(s"In program '$testname' the error should be in line '$eline', but it was in '$line'!")
        }
      }
    }

  for (testname <- successfulTestCases)
    it should s"succeed when checking the program '$testname'" in {
      val result = asset(testDir + "correct/" + testname).flatMap(x => testDataFlow(x))
      result shouldBe a[Success[_]]
    }

  it should s"succeed when cats can fly" in {
    val result = asset("de/thm/mni/cg/benchmarks/nyan.benchmark").flatMap(x => testDataFlow(x))
    result shouldBe a[Success[_]]
  }

  it should s"succeed when the force is with you" in {
    val result = asset("de/thm/mni/cg/benchmarks/alongtime.benchmark").flatMap(x => testDataFlow(x))
    result shouldBe a[Success[_]]
  }

}
