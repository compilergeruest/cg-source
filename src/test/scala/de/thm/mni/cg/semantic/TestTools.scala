package de.thm.mni.cg.semantic

import org.scalatest.Finders
import org.scalatest.FlatSpec
import org.scalatest.Matchers
import de.thm.mni.cg.ir._
import de.thm.mni.cg.compiler.Compiler._
import de.thm.mni.cg.semantic.Ty.fromSymbol
import de.thm.mni.cg.semantic.Ty.resolve
import de.thm.mni.cg.table.FieldEntry
import de.thm.mni.cg.table.FunEntry
import de.thm.mni.cg.table.SymbolTable
import de.thm.mni.cg.table.TyEntry
import de.thm.mni.cg.util.cache.Caching
import de.thm.mni.cg.util._
import scala.util.Try
import de.thm.mni.cg.table.VarEntry
import de.thm.mni.cg.table.ParEntry
import scala.util.Failure
import de.thm.mni.cg.compiler.CompilerSettings

trait TestTools { self: FlatSpec with Matchers with Caching =>

  val testDir: String

  private def tupleString(lst: List[Ty]): String =
    lst.mkString("(", ", ", ")")

  private def compareTyList(expectLst: List[Ty], actualLst: List[Ty])(implicit table: SymbolTable): Unit = {
    if (expectLst.size != actualLst.size) {
      val tupleA = tupleString(expectLst)
      val tupleB = tupleString(actualLst)
      fail(s"Expected $tupleA but $tupleB was given!")
    }
    for ((expect, actual) <- expectLst zip actualLst) {
      if (resolve(expect) != resolve(actual)) {
        fail(s"Expected $expect but $actual was given!")
      }
    }
  }

  private def compareTyWithSybol(msg: String, expect: Ty, symbol: TypeSymbol)(implicit table: SymbolTable): Unit = {
    val actual = fromSymbol(symbol)
    if (resolve(expect) != resolve(actual)) {
      fail(s"Expected $msg '$expect', but it has '$actual' instead!")
    }
  }

  private def asBlock(lst: List[Stm], idx: Int): BlockStm = {
    if (idx >= lst.size || idx < 0) {
      fail(s"You are required to have an $idx. BlockStm in your test, but there are only ${lst.size}")
    } else lst(idx) match {
      case b: BlockStm                  => b
      case ForStm(_, _, b: BlockStm, _) => b
      case WhileStm(_, b: BlockStm)     => b
      case IfStm(_, b: BlockStm, _)     => b
      case e                            => fail(s"Expected block statement at index $idx but ${e.getClass.getSimpleName} was given!")
    }
  }

  def test(f: ShortCode)(table: SymbolTable): Unit = {
    def helper(f: ShortCode, index: Int, stms: List[Stm])(table: SymbolTable): Unit = f match {
      case f: Function =>
        val FunEntry(d) = table[FunEntry](Name(f.name)).getOrElse(fail(s"Expected function with name '${f.name}'!"))
        val expectedOut = f.out.toList
        val actualOut = d.getOut map fromSymbol
        compareTyList(expectedOut, actualOut.toList)(table)
        f.in.foreach(helper(_, 0, List(d.getBody))(d.getLocalTable))
        for (code <- f.content) {
          helper(code, 0, List(d.getBody))(d.getLocalTable)
        }
      case t: Type =>
        val TyEntry(d) = table[TyEntry](Name(t.name)).getOrElse(fail(s"Expected type with name '${t.name}'!"))
        compareTyWithSybol(s"struct or union ${t.name} to have shape", t.ty, d.getTypeSymbol)(table)
      case f: Vari =>
        val entry = table[FieldEntry](Name(f.name)).getOrElse(fail(s"Expected variable with name '${f.name}'!"))
        val VarEntry(d) = entry
        if (!entry.isVarDef) {
          fail(s"Expected variable with name '${f.name}', but parameter found!")
        }
        if (f.const != entry.asVarDef.isConst) {
          fail(s"Expected variable with name '${f.name}' to be declared as constant!")
        }
        compareTyWithSybol("variable to have type", f.ty, d.getTypeSymbol)(table)
      case p: Param =>
        val ParEntry(d) = table[FieldEntry](Name(p.name)).getOrElse(fail(s"Expected parameter with name '${p.name}'!"))
        if (!d.isInstanceOf[ParDef]) {
          fail(s"Expected parameter with name '${p.name}', but variable found!")
        }
        compareTyWithSybol("parameter to have type", p.ty, d.getTypeSymbol)(table)
      case b: Block =>
        val blockStm = asBlock(stms, index)
        b.content.zipWithIndex.foreach {
          case (code, idx) =>
            helper(code, idx, blockStm.getBody.toList)(blockStm.getLocalTable)
        }
    }
    helper(f, 0, Nil)(table)
  }

  def failureMessage(result: Try[_])(f: String => Unit) = {
    result shouldBe a[Failure[_]]

    val optExc = Option(result.asInstanceOf[Failure[_]].exception)
    if (optExc.isEmpty) {
      fail("In the Failure case of the Try-Monad is no exception!")
    }

    val optMsg = optExc.flatMap(x => Option(x.getMessage))
    if (optMsg.isEmpty) {
      fail("Empty message in exception!")
    }
    optMsg.foreach(f)
  }

  private val settings = CompilerSettings.defaultSettings

  val testTypeChecker = syntacticAnalysis |> namer |> typer
  def testTypeCheck(input: InputSource) =
    testTypeChecker.run(settings, input)

  val testControlFlower = testTypeChecker |> controlFlow
  def testControlFlow(input: InputSource) =
    testControlFlower.run(settings, input)

  val testDataFlower = testControlFlower |> dataFlow
  def testDataFlow(input: InputSource) =
    testDataFlower.run(settings, input)

}
