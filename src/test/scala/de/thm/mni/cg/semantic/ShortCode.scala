package de.thm.mni.cg.semantic

sealed trait ShortCode

case class Function(name: String)(val out: Ty*)(val in: Param*)(val content: Option[ShortCode]) extends ShortCode
case class Type(name: String)(val ty: Ty) extends ShortCode
case class Vari(name: String)(val const: Boolean, val ty: Ty) extends ShortCode
case class Param(name: String)(val ty: Ty) extends ShortCode
case class Block(content: ShortCode*) extends ShortCode
