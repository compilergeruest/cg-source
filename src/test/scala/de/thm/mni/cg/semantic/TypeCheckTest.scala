package de.thm.mni.cg.semantic

import org.scalatest.Matchers
import org.scalatest.FlatSpec

import scala.util._

import de.thm.mni.cg.compiler.Compiler
import de.thm.mni.cg.ir._
import de.thm.mni.cg.table._
import de.thm.mni.cg.table.SymbolTable
import de.thm.mni.cg.util._
import de.thm.mni.cg.util.cache.Caching
import de.thm.mni.cg.semantic._
import de.thm.mni.cg.semantic.Ty._

class TypeCheckTest extends FlatSpec with Matchers with Caching with TestTools {

  val testDir = "de/thm/mni/cg/semantic/typecheck/tests/"

  "The typechecker" should "succeed when checking an empty program" in {
    val result = asset(testDir + "correct/" + "emptyProgram.test").flatMap(x => testTypeCheck(x))
    result shouldBe a [Success[_]]
  }

  def testResult(testname: String, expect: Any) = expect match {
    case code: ShortCode =>
      it should s"succeed when checking the program '$testname'" in {
        val result = asset(testDir + "correct/" + testname).flatMap(x => testTypeCheck(x))

        result shouldBe a[Success[_]]

        for (Program(_, table) <- result) {
          test(code)(table)
        }
      }
    case msg: String =>
      it should s"fail when checking the program '$testname'" in {
        val result = asset(testDir + "wrong/" + testname).flatMap(x => testTypeCheck(x))

        result shouldBe a[Failure[_]]

        val Fail(err) = result
        assume(err contains msg)
      }
  }

  def wrapMain(code: ShortCode*) =
    Function("main")(unitTy)()(
      Some(Block(code: _*))
    )

  val nonSuccessfulTestCases = Seq(
    "varNotDefined.test" ->
      "Undefined parameter or variable with name 'a'!",
    "unknownType.test" ->
      "Undefined type with name 'Abc'!",
    "freePrimitive.test" ->
      "Can not free memory of primitive value with type 'bool'!",
    "simpleFunctionWrongReturn1.test" ->
      "Function requires return value to have type 'unit', but a value of type 'int' is returned!",
    "simpleFunctionWrongReturn2.test" ->
      "Function requires return value to have type '(int, int)', but a value of type '(int, char)' is returned!",
    "doubleDefVar.test" ->
      "Variable 'a' is already defined in scope!",
    "doubleDefParam.test" ->
      "Parameter 'a' is already defined in scope!",
    "doubleDefFunction.test" ->
      "Function 'f' is already defined in scope!",
    "doubleDefType.test" ->
      "Type 'Int' is already defined in scope!",
    "duplicateMemberInit1.test" ->
      "Duplicate initialization of the component 'left' in type 'Tree'!",
    "duplicateMemberInit2.test" ->
      "Duplicate initialization of the components 'left' and 'left' in type 'Tree'!",
    "duplicateMemberInit3.test" ->
      "Duplicate initialization of the components 'left', 'left' and 'right' in type 'Tree'!",
    "uninitializedMember.test" ->
      "The field 'y' of the complex type 'Point' is not initialized!",
    "moreThanOneInitializedMember.test" ->
      "A union type must only be initialized with one argument ('x' and 'y' are initialized here)!",
    "initializationOfInvalidMember.test" ->
      "Unknown component with name 'w' in complex type 'Point3d'!",
    "primitiveGlobalRedefinition.test" ->
      "Type 'int' is already defined in scope!",
    "primitiveLocalRedefinition.test" ->
      "Redefinition of primitive type 'int'!",
    "simpleVarDefUnit.test" ->
      "Can not define a variable of type 'unit'!",
    "structContainsUnitMember.test" ->
      "Can not define the struct member 'b' of type 'unit'!",
    "unionContainsUnitMember.test" ->
      "Can not define the union member 'b' of type 'unit'!",
    "arrayWithUnitBase.test" ->
      "Can not define an array of base type 'unit'!",
    "arrayWithUnitBaseDef.test" ->
      "Can not define an array of base type 'unit'!",
    "allocUnitArray.test" ->
      "Can not create an instance of an array with the base type 'unit'!",
    "simpleFunctionWrongParam.test" ->
      "Can not define a parameter of type 'unit'!",
    "castToBoolean.test" ->
      "Can not cast expression of type 'int' to type 'bool'!",
    "castBoolean.test" ->
      "Can not cast expression of type 'bool' to type 'int'!",
    "castStructToBool.test" ->
      "Can not cast expression of type 'Point' to type 'bool'!",
    "castStructToInt.test" ->
      "Can not cast expression of type 'Point' to type 'int', because complex types must not be casted!",
    "castToArray.test" ->
      "Can not cast expression of type 'int[][]' to type 'IntMatrix', because casting to complex types is forbidden!",
    "castToStruct.test" ->
      "Can not cast expression of type 'null' to type 'Point', because casting to complex types is forbidden!",
    "cyclicType1.test" ->
      "Cyclic definition of type 'T'!",
    "cyclicType2.test" ->
      "Cyclic definition of type 'A'!",
    "delNull.test" ->
      "Can not free memory of null reference!",
    "ifStmType.test" ->
      "Expected expression of type 'bool' in test of if-statement, but got expression of type 'int'!",
    "whileStmType.test" ->
      "Expected expression of type 'bool' in test of while-statement, but got expression of type 'char'!",
    "forStmType.test" ->
      "Iteration on values of type 'char' is not supported, expected type 'int'!",
    "forStmRange1.test" ->
      "Expected expression of type 'int' for start value in for-statement range, but got expression of type 'char'!",
    "forStmRange2.test" ->
      "Expected expression of type 'int' for next value in for-statement range, but got expression of type 'char'!",
    "forStmRange3.test" ->
      "Expected expression of type 'int' for end value in for-statement range, but got expression of type 'char'!",
    "unionTwoInitializers.test" ->
      "A union type must only be initialized with one argument ('a' and 'b' are initialized here)!",
    "primitiveFree.test" ->
      "Can not free memory of primitive value with type 'int'!",
    "structInitialisation.test" ->
      "The fields 'x', 'y' and 'z' of the complex type 'Point4D' are not initialized!",
    "nullAssignPrimtive.test" ->
      "Can't assign expression of type 'null' to a variable of type 'int'!",
    "newNonComplex.test" ->
      "Expected complex type, but got 'int'!",
    "arrayIndex.test" ->
      "Expected expression of type 'int' as an array index, but got expression of type 'bool'!",
    "arrayDimension.test" ->
      "Expected expression of type 'int' as an array dimension, but got expression of type 'bool'!",
    "partialAppliedArrayDimension1.test" ->
      "Cannot specify an array dimension after an empty dimension!",
    "partialAppliedArrayDimension2.test" ->
      "Cannot create an array without specifying an array dimension!",
    "recursiveInitStm.test" ->
      "Recursive use of variable with name 'i'!",
    "unitMixReturn1.test" ->
      "Can not mix 'unit' return type with multiple return values!",
    "unitMixReturn2.test" ->
      "Can not mix 'unit' return type with multiple return values!",
    "typeAsVar.test" ->
      "'f' is not a parameter or variable, but a type!",
    "typeAsFunc.test" ->
      "'f' is not a function, but a type!",
    "funcAsVar.test" ->
      "'f' is not a parameter or variable, but a function!",
    "funcAsType.test" ->
      "'f' is not a type, but a function!",
    "varAsFunc.test" ->
      "'n' is not a function, but a variable!",
    "varAsType.test" ->
      "'n' is not a type, but a variable!",
    "binExpLhsNeRhs.test" ->
      "Expected expression of type 'char', but got expression of type 'int'!",
    "binExpNotArith.test" ->
      "Arithmetic binary operation ('+') expects expression of type 'int' or 'char', but got expression of type 'bool'!",
    "binExpNotLogical.test" ->
      "Logical binary operation ('&&') expects expression of type 'bool', but got expression of type 'int'!",
    "unaryExpArithmetic.test" ->
      "Arithmetic unary operation ('-') expects expression of type 'int' or 'char', but got expression of type 'bool'!",
    "unaryExpBitwise.test" ->
      "Bitwise unary operation ('~') expects expression of type 'int' or 'char', but got expression of type 'bool'!",
    "unaryExpLogical.test" ->
      "Logical unary operation ('!') expects expression of type 'bool', but got expression of type 'int'!",
    "unaryExpSize.test" ->
      "Size operation ('#') expects expression of an array type, but got expression of type 'int'!",
    "funExpFewArgs.test" ->
      "Too few arguments given for function with name 'f'!",
    "funExpManyArgs.test" ->
      "Too many arguments given for function with name 'f'!",
    "funExpWrongArg.test" ->
      "Expected expression of type 'char' for parameter 'c' of function 'f', but got expression of type 'int'!",
    "invalidMemberAccess.test" ->
      "Unknown component with name 'z'!",
    "nonComplexMemberAccess.test" ->
      "Expected complex type, but got 'int'!",
    "indexingOfNonArray1.test" ->
      "Expected expression of type 'int[]' for index access, but got expression of type 'int'!",
    "indexingOfNonArray2.test" ->
      "Expected expression of type 'int[]' for index access, but got expression of type 'int'!",
    "indexingOfNonArray3.test" ->
      "Expected expression of type 'int[]' for index access, but got expression of type 'int'!",
    "assignTuple1.test" ->
      "Can't assign expression of type '(int, int)' to a variable of type 'int'!",
    "assignTuple2.test" ->
      "Can't assign expression of type 'int' to a variable of type '(int, int)'!",
    "assignToVariable.test" ->
      "Can't assign expression of type 'int' to a variable of type 'char'!",
    "assignToArray.test" ->
      "Can't assign expression of type 'int[]' to a variable of type 'char[]'!",
    "assignToStruct.test" ->
      "Can't assign expression of type 'char' to a variable of type 'int'!",
    "assignToUnion.test" ->
      "Can't assign expression of type 'char' to a variable of type 'int'!",
    "assignToNonVariable.test" ->
      "The left-hand side of an assignment must be a variable!",
    "initTuple1.test" ->
      "Can't assign expression of type '(int, int)' to a variable of type 'int'!",
    "initTuple2.test" ->
      "Can't assign expression of type 'int' to a variable of type '(int, int)'!",
    "initTuple3.test" ->
      "Can't assign expression of type 'char' to a variable of type 'int'!",
    "initVariable.test" ->
      "Can't assign expression of type 'char' to a variable of type 'int'!",
    "accessOnPrimitive.test" ->
      "Expected complex type, but got 'int'!",
    "constDefNotInit.test" ->
      "Expected token '=', but got token ';'!",
    "constReassignmentVari1.test" ->
      "Reassignment to constant with name 'n'!",
    "constReassignmentVari2.test" ->
      "Reassignment to constant with name 'b'!",
    "constReassignmentParam.test" ->
      "Reassignment to parameter with name 'p'!"
  )

  // common definitions for tests
  val pointDef =
    Type("Point")(StructTy(
      intTy -> "x", intTy -> "y"))
  val pointTy = NameTy("Point")

  val intListDef =
    Type("IntList")(StructTy(
      intTy -> "value",
      NameTy("IntList") -> "next"))
  val intListTy = NameTy("IntList")

  val stringDef =
    Type("String")(ArrayTy(charTy))
  val stringTy = NameTy("String")

  val intArr = ArrayTy(intTy)
  val int2Arr = ArrayTy(ArrayTy(intTy))
  val int3Arr = ArrayTy(ArrayTy(ArrayTy(intTy)))

  val divModDef =
    Function("divMod")(intTy, intTy)(
      Param("a")(intTy), Param("b")(intTy))(None)

  val successfulTestCases = Seq(
    "emptyProgram.test" ->
      wrapMain(),
    "onlyComments.test" ->
      wrapMain(),

    "simpleVarDef.test" ->
      wrapMain(
        Vari("a")(false, intTy)
      ),
    "aliasUnit.test" ->
      wrapMain(
        Type("void")(NameTy("unit")),
        Function("f")(NameTy("void"))()(None)
      ),
    "alloc.test" ->
      wrapMain(
        pointDef
      ),
    "free.test" ->
      wrapMain(
        pointDef,
        Vari("p")(false, pointTy)
      ),
    "complexTypeDef.test" ->
      wrapMain(
        intListDef,
        Type("Either")(UnionTy(
          intListTy -> "list",
          ArrayTy(charTy) -> "string"
        ))
      ),
    "simpleFunctionDef.test" ->
      wrapMain(
        Function("f")(intTy)()(None)
      ),
    "allocIntList.test" ->
      wrapMain(
        intListDef,
        Vari("lst")(false, intListTy)
      ),
    "allocIntList2.test" ->
      wrapMain(
        intListDef,
        Vari("lst")(true, intListTy)
      ),
    "simpleFunctionDefMultipleReturn.test" ->
      wrapMain(
        divModDef
      ),
    "functionTupleReturn.test" ->
      wrapMain(
        stringDef,
        Function("f")(stringTy, intTy)()(Some(
          Block(
            Type("string")(ArrayTy(charTy)),
            Vari("s")(false, NameTy("string"))
          )
        ))
      ),
    "functionComplexReturn.test" ->
      wrapMain(
        pointDef,
        Function("f")(pointTy)()(None)
      ),
    "functionComplexReturnAlias.test" ->
      wrapMain(
        pointDef,
        Function("f")(pointTy)()(Some(
          Block(
            Type("P")(pointTy)
          )
        ))
      ),
    "functionRecursion.test" ->
      wrapMain(
        Function("f")(intTy)()(None),
        Function("g")(intTy, intTy)()(None)
      ),
    "functionIndirectRecursion.test" ->
      wrapMain(
        Function("f")(unitTy)()(None),
        Function("g")(unitTy)()(None)
      ),
    "castIntChar.test" ->
      wrapMain(
        Vari("n")(false, intTy),
        Vari("c")(false, charTy)
      ),
    "multiAssign.test" ->
      wrapMain(
        divModDef,
        Vari("a")(true, intTy),
        Vari("b")(false, intTy)
      ),
    "ifStmLocalTable.test" ->
      wrapMain(
        Block(
          Vari("a")(false, intTy)
        )
      ),
    "whileStmLocalTable.test" ->
      wrapMain(
        Block(
          Vari("a")(false, intTy)
        )
      ),
    "forStmLocalTable.test" ->
      wrapMain(
        Block(
          Vari("a")(false, intTy)
        )
      ),
    "forStmAliasType.test" ->
      wrapMain(),
    "returnOfStringConst.test" ->
      wrapMain(
        Function("f")(ArrayTy(charTy))()(None)
      ),
    "nullAssignArray1.test" ->
      wrapMain(
        Vari("a")(false, ArrayTy(intTy))
      ),
    "nullAssignArray2.test" ->
      wrapMain(
        Vari("a")(false, ArrayTy(intTy))
      ),
    "nullAssignUnion1.test" ->
      wrapMain(
        Type("Either")(UnionTy(
          intTy -> "x",
          intTy -> "y"
        )),
        Vari("p")(false, NameTy("Either"))
      ),
    "nullAssignUnion2.test" ->
      wrapMain(
        Type("Either")(UnionTy(
          intTy -> "x",
          intTy -> "y"
        )),
        Vari("p")(false, NameTy("Either"))
      ),
    "nullAssignStruct1.test" ->
      wrapMain(
        pointDef,
        Vari("p")(false, pointTy)
      ),
    "nullAssignStruct2.test" ->
      wrapMain(
        pointDef,
        Vari("p")(false, pointTy)
      ),
    "nullPassArray.test" ->
      wrapMain(
        Function("f")(unitTy)(Param("arr")(intArr))(None)
      ),
    "nullPassStruct.test" ->
      wrapMain(
        pointDef,
        Function("f")(unitTy)(Param("p")(pointTy))(None)
      ),
    "nullPassUnion.test" ->
      wrapMain(
        Type("Either")(UnionTy(
          intTy -> "x",
          intTy -> "y"
        )),
        Function("f")(unitTy)(Param("e")(NameTy("Either")))(None)
      ),
    "nullReturnArray.test" ->
      wrapMain(
        Function("f")(intArr)()(None)
      ),
    "nullReturnStruct.test" ->
      wrapMain(
        pointDef,
        Function("f")(pointTy)()(None)
      ),
    "nullReturnUnion.test" ->
      wrapMain(
        Type("Either")(UnionTy(
          intTy -> "x",
          intTy -> "y"
        )),
        Function("f")(NameTy("Either"))()(None)
      ),
    "partialAppliedArrayDimension1.test" ->
      wrapMain(
        Vari("arr1")(true, int3Arr)
      ),
    "partialAppliedArrayDimension2.test" ->
      wrapMain(
        Vari("arr2")(false, int3Arr)
      ),
    "partialAppliedArrayDimension3.test" ->
      wrapMain(
        Vari("arr3")(false, int3Arr)
      ),
    "binExpBool.test" ->
      wrapMain(
        Vari("b")(false, boolTy)
      ),
    "binExpInt.test" ->
      wrapMain(
        Vari("n")(false, intTy)
      ),
    "assignToComplexMember.test" ->
      wrapMain(
        Type("IntBox1")(StructTy(intTy -> "value")),
        Type("IntBox2")(ArrayTy(intTy)),
        Vari("n")(true, intTy),
        Function("func1")(NameTy("IntBox1"))()(None),
        Function("func2")(NameTy("IntBox2"))()(None),
        Vari("box1")(false, NameTy("IntBox1")),
        Vari("box2")(false, NameTy("IntBox2"))
      ),
    "constDefInit.test" ->
      wrapMain(
        Vari("n")(true, intTy)
      )
  )

  for {
    tests <- Seq(nonSuccessfulTestCases, successfulTestCases)
    (program, res) <- tests
  } testResult(program, res)

}
