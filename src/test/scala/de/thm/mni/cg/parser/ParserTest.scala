package de.thm.mni.cg.parser

import scala.collection.mutable.ArrayBuffer
import scala.language.implicitConversions
import scala.util.Success
import scala.util.Try

import org.scalatest.FlatSpec
import org.scalatest.Matchers

import de.thm.mni.cg.compiler._
import de.thm.mni.cg.ir._
import de.thm.mni.cg.language.NumberSystem
import de.thm.mni.cg.semantic.TestTools
import de.thm.mni.cg.util._
import de.thm.mni.cg.util.cache.Caching

class ParserTest extends FlatSpec with Matchers with Caching with TestTools {

  val testDir = "de/thm/mni/cg/parser/tests/"

  "A Parser" should "exist" in {
    assume(true)
  }

  private def ty(name: String) =
    NameTypeSymbol(Name(name))

  private def arr(name: String, dims: Int) = {
    val zero: TypeSymbol = NameTypeSymbol(Name(name))
    (0 until dims).foldLeft(zero) { case (acc, _) =>
      ArrayTypeSymbol(acc)
    }
  }

  private implicit def stringToName(name: String): Name = Name(name)

  private val int = ty("int")
  private val char = ty("char")
  private val unit = ty("unit")
  private val string = arr("char", 1)
  private val Str = ty("String")

  val successfulTestCases = Seq[(String, ArrayBuffer[Stm])](
    "access1.test" -> ArrayBuffer(ExpStm(ArrayVar(NameVar("arr"), IntLit(0)))),
    "access2.test" -> ArrayBuffer(ExpStm(AccessVar(NameVar("point"), "x"))),
    "access3.test" -> ArrayBuffer(ExpStm(
      AccessVar(FunExp("f", ArrayBuffer()), "x")
    )),
    "access4.test" -> ArrayBuffer(ExpStm(
      ArrayVar(
        AccessVar(
          ArrayVar(
            AccessVar(FunExp("f", ArrayBuffer()), "x"),
            IntLit(9)),
          "y"),
        IntLit(0))
    )),
    "access5.test" -> ArrayBuffer(ExpStm(
      ArrayVar(AccessVar(
        NewObjExp("Point", ArrayBuffer(
          Name("p") -> NewArrExp(int, ArrayBuffer(Some(IntLit(2)))),
          Name("v") -> IntLit(0)
        )), "p"), IntLit(2))
    )),
    "access6.test" -> ArrayBuffer(ExpStm(
      ArrayVar(IntLit(5), IntLit(0))
    )),
    "assignment1.test" -> ArrayBuffer(
      AssignStm(ArrayBuffer(NameVar("a")), NameVar("value"))
    ),
    "assignment2.test" -> ArrayBuffer(
      AssignStm(ArrayBuffer(NameVar("a"), NameVar("b")), NameVar("value"))
    ),
    "assignment3.test" -> ArrayBuffer(
      AssignStm(ArrayBuffer(AccessVar(NameVar("p"), "x")), NameVar("value"))
    ),
    "assignment4.test" -> ArrayBuffer(
      AssignStm(ArrayBuffer(
        AccessVar(NameVar("p"), "x"),
        AccessVar(NameVar("p"), "y")),
      NameVar("value"))
    ),
    "assignment5.test" -> ArrayBuffer(
      AssignStm(ArrayBuffer(ArrayVar(NameVar("array"), IntLit(0))), NameVar("value"))
    ),
    "assignment6.test" -> ArrayBuffer(
      AssignStm(ArrayBuffer(
        ArrayVar(NameVar("array"), IntLit(0)),
        ArrayVar(NameVar("array"), IntLit(1))),
      NameVar("value"))
    ),
    "blockStmEmpty.test" -> ArrayBuffer(BlockStm(ArrayBuffer(), null)),
    "blockStm50.test" -> ArrayBuffer(
      (0 until 49).foldLeft(BlockStm(ArrayBuffer(), null)) {
        case (stm, _) => BlockStm(ArrayBuffer(stm), null)
      }
    ),
    "conditionals1.test" -> ArrayBuffer(
      WhileStm(BoolLit(true),
        ForStm(
          VarDef(false, int, "i"),
          Range(IntLit(0), Some(IntLit(2)), IntLit(1000)),
          IfStm(
            BinExp(Mod, NameVar("i"), IntLit(15)),
            ExpStm(FunExp("printc", ArrayBuffer(CastExp(char, NameVar("i"))))),
            Some(ExpStm(FunExp("printi", ArrayBuffer(NameVar("i")))))),
          null
        )
      )
    ),
    "conditionals2.test" -> ArrayBuffer(
      WhileStm(BoolLit(true),
        BlockStm(ArrayBuffer(ForStm(
          VarDef(false, int, "i"),
          Range(IntLit(0), Some(IntLit(2)), IntLit(1000)),
          BlockStm(ArrayBuffer(IfStm(
            BinExp(Mod, NameVar("i"), IntLit(15)),
            BlockStm(ArrayBuffer(
              ExpStm(FunExp("printc", ArrayBuffer(CastExp(char, NameVar("i")))))),
              null),
            Some(BlockStm(ArrayBuffer(
              ExpStm(FunExp("printi", ArrayBuffer(NameVar("i"))))), null)))),
            null),
          null
        )),
        null)
      )
    ),
    "defs.test" -> ArrayBuffer(
      TyDef("String", string),
      FunDef(
        "copy",
        ArrayBuffer(Str),
        ArrayBuffer(ParDef(Str, "xs")),
        BlockStm(ArrayBuffer(
          InitStm(
            ArrayBuffer(VarDef(true, Str, "cp")),
            NewArrExp(char, ArrayBuffer(Some(UnaryExp(Size, NameVar("xs")))))),
          ForStm(
            VarDef(false, int, "i"),
            Range(IntLit(0), None, UnaryExp(Size, NameVar("xs"))),
            BlockStm(ArrayBuffer(
              AssignStm(
                ArrayBuffer(ArrayVar(NameVar("cp"), NameVar("i"))),
                ArrayVar(NameVar("xs"), NameVar("i"))
              )
            ), null),
            null),
          ReturnStm(ArrayBuffer(NameVar("cp")))
        ), null),
        null
      )
    ),
    "delStm1.test" -> ArrayBuffer(DelStm(NameVar("a"))),
    "delStm2.test" -> ArrayBuffer(DelStm(IntLit(1))),
    "delStm3.test" -> ArrayBuffer(DelStm(NullLit())),
    "delStm4.test" -> ArrayBuffer(DelStm(StringLit("nothing"))),
    "empty.test" -> ArrayBuffer(),
    "fibonacci.test" -> ArrayBuffer(FunDef(
      "fib",
      ArrayBuffer(int),
      ArrayBuffer(ParDef(int, "n")),
      IfStm(
        BinExp(Gt, NameVar("n"), IntLit(1)),
        ReturnStm(ArrayBuffer(BinExp(
          Add,
          FunExp("fib", ArrayBuffer(BinExp(Sub, NameVar("n"), IntLit(1)))),
          FunExp("fib", ArrayBuffer(BinExp(Sub, NameVar("n"), IntLit(2))))
        ))),
        Some(ReturnStm(ArrayBuffer(
          FunExp("math_max", ArrayBuffer(NameVar("n"), IntLit(0)))))
        )), null)),
    "funDef1.test" -> ArrayBuffer(FunDef(
      "main",
      ArrayBuffer(unit),
      ArrayBuffer(ParDef(string, "args")),
      BlockStm(ArrayBuffer(), null),
      null
    )),
    "funDef2.test" -> ArrayBuffer(
      FunDef(
        "substr",
        ArrayBuffer(string),
        ArrayBuffer(ParDef(string, "input"), ParDef(int, "start"), ParDef(int, "length")),
        BlockStm(ArrayBuffer(
          InitStm(ArrayBuffer(VarDef(true, string, "result")), NewArrExp(char, ArrayBuffer(Some(NameVar("length"))))),
          ForStm(VarDef(false, int, "i"), Range(NameVar("start"), None, BinExp(Add, NameVar("start"), NameVar("length"))), BlockStm(ArrayBuffer(
            AssignStm(ArrayBuffer(ArrayVar(NameVar("result"), BinExp(Sub, NameVar("i"), NameVar("start")))), ArrayVar(NameVar("input"), NameVar("i")))
          ), null), null),
          ReturnStm(ArrayBuffer(NameVar("result")))
        ), null),
      null)
    ),
    "funDef3.test" -> ArrayBuffer(FunDef(
      "f",
      ArrayBuffer(unit), ArrayBuffer(),
      FunDef(
        "g",
        ArrayBuffer(unit), ArrayBuffer(),
        ExpStm(IntLit(0)),
        null
      ),
      null
    )),
    "funExp1.test" -> ArrayBuffer(ExpStm(FunExp("f", ArrayBuffer()))),
    "funExp2.test" -> ArrayBuffer(ExpStm(FunExp("f", ArrayBuffer(
      BinExp(Add, IntLit(7, NumberSystem.Binary), IntLit(42, NumberSystem.Hexadecimal)),
      CharLit('a'),
      NullLit(),
      BoolLit(false),
      StringLit("asd"),
      NameVar("name")
    )))),
    "initStm1.test" ->
      ArrayBuffer(InitStm(ArrayBuffer(VarDef(
        true, string, "msg")),
        StringLit("Hello World!")
      )),
    "initStm2.test" ->
      ArrayBuffer(InitStm(ArrayBuffer(VarDef(
        false, string, "msg")),
        StringLit("Hello World!")
      )),
    "initStm3.test" ->
      ArrayBuffer(InitStm(ArrayBuffer(VarDef(
        false, int, "n")), StringLit("Hello World!")
      )),
    "initStm4.test" ->
      ArrayBuffer(InitStm(ArrayBuffer(VarDef(
        true, char, "c")), CharLit('a')
      )),
    "intLiteralDec1.test" -> ArrayBuffer(ExpStm(IntLit(2147483647))),
    "literals.test" -> ArrayBuffer(
      IntLit(7, NumberSystem.Binary), IntLit(42, NumberSystem.Hexadecimal), IntLit(55),
      CharLit('a'), CharLit('\t'), CharLit('\n'), CharLit('"'), CharLit('\''),
      CharLit('\u0000'), CharLit('\u00C3'), CharLit('\u00FF'),
      NullLit(),
      BoolLit(false), BoolLit(true),
      StringLit("Hello World"),
      StringLit("Hello \u00C2\u00AB\u00cD")
    ).map(ExpStm(_)),
    "macro1.test" -> ArrayBuffer(
      IntLit(1),IntLit(2), IntLit(3), IntLit(10)
    ).map(ExpStm(_)),
    "operatorPrecendence1.test" -> ArrayBuffer(
      ExpStm(BinExp(Sub, UnaryExp(Minus, IntLit(1)), UnaryExp(Minus, IntLit(3))))
    ),
    "operatorPrecendence2.test" -> ArrayBuffer(ExpStm(
      BinExp(Or, IntLit(1),
        BinExp(And, IntLit(2),
          BinExp(Bor, IntLit(3),
            BinExp(Xor, IntLit(4),
              BinExp(Band, IntLit(5),
                BinExp(Ne, IntLit(6),
                  BinExp(Lt, IntLit(7),
                    BinExp(Sar, IntLit(8),
                      BinExp(Add, IntLit(9),
                        BinExp(Mul, IntLit(10),
                          UnaryExp(Size,
                            NameVar("lol"))))))))))))
    )),
    "operatorPrecendence3.test" -> ArrayBuffer(ExpStm(
      BinExp(Or,
        BinExp(And,
          BinExp(Bor,
            BinExp(Xor,
              BinExp(Band,
                BinExp(Eq,
                  BinExp(Ge,
                    BinExp(Sll,
                      BinExp(Sub,
                        BinExp(Mod,
                          UnaryExp(Bnot,
                            NameVar("rofl")),
                          IntLit(10)),
                        IntLit(9)),
                      IntLit(8)),
                    IntLit(7)),
                  IntLit(6)),
                IntLit(5)),
              IntLit(4)),
            IntLit(3)),
          IntLit(2)),
        IntLit(1))
    )),
    "tyDef1.test" -> ArrayBuffer(TyDef("Matrix", arr("int", 2))),
    "tyDef2.test" -> ArrayBuffer(TyDef("Num", int)),
    "tyDef3.test" -> ArrayBuffer(TyDef("Point", StructTypeSymbol(ArrayBuffer(
      Component(int, "x"), Component(int, "y")
    )))),
    "tyDef4.test" -> ArrayBuffer(TyDef("Either", UnionTypeSymbol(ArrayBuffer(
      Component(ty("IntList"), "list"),
      Component(ty("Point"), "point"),
      Component(ty("Either"), "disjunct")
    )))),
    "tyDef5.test" -> ArrayBuffer(TyDef("Frame", StructTypeSymbol(ArrayBuffer(
      Component(arr("int", 1), "diff"),
      Component(string, "data"),
      Component(int, "length")
    )))),
    "unaryExp1.test" -> ArrayBuffer(ExpStm(UnaryExp(Not, NameVar("vari")))),
    "unaryExp2.test" -> ArrayBuffer(ExpStm(UnaryExp(Bnot, NameVar("vari")))),
    "unaryExp3.test" -> ArrayBuffer(ExpStm(UnaryExp(Size, NameVar("vari")))),
    "unaryExp4.test" -> ArrayBuffer(ExpStm(UnaryExp(Minus, NameVar("vari")))),
    "varDef1.test" -> ArrayBuffer(VarDef(false, int, "i")),
    "varDef2.test" -> ArrayBuffer(VarDef(false, ty("String"), "s")),
    "varDef3.test" -> ArrayBuffer(VarDef(false, arr("int", 1), "arr")),
    "varDef4.test" -> ArrayBuffer(VarDef(false, arr("Frame", 5), "many"))
  )

  val nonSuccessfulTestCases = Seq(
    "assignment1.test" -> "Expected expression, but got token ';'!",
    "assignment2.test" -> "Expected expression, but got token '='!",
    "binExp1.test" -> "Expected expression, but got token ';'!",
    "binExp2.test" -> "Expected expression, but got token ';'!",
    "binExp3.test" -> "Expected expression, but got token ';'!",
    "blockStm1.test" -> "Expected token '}', but got end of source!",
    "charLiteral1.test" -> "Unclosed character literal!",
    "charLiteral2.test" -> "Unclosed character literal!",
    "charLiteral3.test" -> "Unclosed character literal!",
    "charLiteral4.test" -> "Illegal escape sequence '\\v'!",
    "charLiteral5.test" -> "Unclosed character literal!",
    "charLiteralUnicode1.test" -> "Illegal unicode character literal!",
    "charLiteralUnicode2.test" -> "Illegal unicode character literal!",
    "charLiteralUnicode3.test" -> "Illegal unicode character literal!",
    "charLiteralUnicode4.test" -> "Illegal unicode character literal!",
    "charLiteralUnicode5.test" -> "Illegal unicode character literal!",
    "charLiteralUnicode6.test" -> "Unclosed character literal!",
    "delStm1.test" -> "Expected expression, but got token 'if'!",
    "delStm2.test" -> "Expected expression, but got token 'for'!",
    "delStm3.test" -> "Expected expression, but got token 'while'!",
    "delStm4.test" -> "Expected token ';', but got token 'i'!",
    "delStm5.test" -> "Expected token ';', but got token 'main'!",
    "delStm6.test" -> "Expected expression, but got token ';'!",
    "delStm7.test" -> "Expected expression, but got end of source!",
    "expStm1.test" -> "Expected token ')', but got token ';'!",
    "expStmFunExp1.test" -> "Expected expression, but got token ';'!",
    "expStmFunExp2.test" -> "Expected expression, but got token ')'!",
    "expStmNoSemic.test" -> "Expected token ';', but got end of source!",
    "forEachJavaStm.test" -> "Unexpected input character ':'!",
    "forEachScalaStm.test" -> "Expected identifier, but got token '<-'!",
    "forStm1.test" -> "Expected token '(', but got token ';'!",
    "forStm2.test" -> "Expected identifier, but got token ')'!",
    "forStm3.test" -> "Expected token '<-', but got token '='!",
    "forStm4.test" -> "Expected token '..', but got token ','!",
    "forStm5.test" -> "Expected expression, but got token '+'!",
    "forStm6.test" -> "Expected expression, but got token ';'!",
    "forStm7.test" -> "Expected token '}', but got end of source!",
    "forStm8.test" -> "Expected expression, but got token '}'!",
    "funDef1.test" -> "Expected identifier, but got token ';'!",
    "funDef2.test" -> "Expected expression, but got token ';'!",
    "funDef3.test" -> "Expected expression, but got token ';'!",
    "funDef4.test" -> "Expected name or assignment, but got token '{'!",
    "funDef5.test" -> "Illegal modifier 'const' for parameter 'r'!",
    "funDef6.test" -> "Expected identifier, but got token 'const'!",
    "identComma1.test" -> "Expected identifier, but got token '1'!",
    "identComma2.test" -> "Expected identifier, but got token ';'!",
    "identComma3.test" -> "Expected token '(', but got token ';'!",
    "identComma4.test" -> "Expected identifier, but got token '1'!",
    "identComma5.test" -> "Expected name or assignment, but got token '1'!",
    "identComma6.test" -> "Expected type, but got expression!",
    "identComma7.test" -> "Expected type, but got expression!",
    "identComma8.test" -> "Expected expression, but got type!",
    "identDot1.test" -> "Expected token '=', but got end of source!",
    "identDot2.test" -> "Expected expression, but got token ';'!",
    "identDot3.test" -> "Expected expression, but got end of source!",
    "identDot4.test" -> "Expected expression, but got token ';'!",
    "identDot5.test" -> "Expected token '=', but got token ';'!",
    "identDot6.test" -> "Expected expression, but got token ';'!",
    "identIdent1.test" -> "Expected identifier, but got token ';'!",
    "identIdent2.test" -> "Expected expression, but got token ';'!",
    "identIdent3.test" -> "Expected token ';', but got token '0x2A'!",
    "identLBrack1.test" -> "Expected expression, but got token ';'!",
    "identLBrack2.test" -> "Expected token ']', but got token ';'!",
    "identLBrack3.test" -> "Expected expression, but got token ';'!",
    "identLBrack4.test" -> "Expected expression, but got token ';'!",
    "identLBrack5.test" -> "Expected identifier, but got token ';'!",
    "identLBrack6.test" -> "Expected identifier, but got token ';'!",
    "identLBrack7.test" -> "Expected identifier, but got token ';'!",
    "identLBrack8.test" -> "Expected identifier, but got token ';'!",
    "identLBrack9.test" -> "Expected expression, but got token ';'!",
    "identLBrack10.test" -> "Expected token ';', but got end of source!",
    "identLParen1.test" -> "Expected token ';', but got end of source!",
    "identLParen2.test" -> "Expected token ';', but got end of source!",
    "identLParen3.test" -> "Expected token ';', but got end of source!",
    "identLParen4.test" -> "Expected token ';', but got end of source!",
    "identLParen5.test" -> "Expected expression, but got token ';'!",
    "identLParen6.test" -> "Expected expression, but got token ';'!",
    "identLParen7.test" -> "Expected expression, but got token ';'!",
    "identLParen8.test" -> "Expected expression, but got token ';'!",
    "identLParen9.test" -> "Expected token '=', but got token ';'!",
    "identLParen10.test" -> "Expected expression, but got token ';'!",
    "ifStm1.test" -> "Expected token '(', but got token ';'!",
    "ifStm2.test" -> "Expected expression, but got token ')'!",
    "ifStm3.test" -> "Expected expression, but got token 'if'!",
    "ifStm4.test" -> "Expected expression, but got token ';'!",
    "ifStm5.test" -> "Expected expression, but got token ';'!",
    "ifStm6.test" -> "Expected expression, but got token ';'!",
    "ifStm7.test" -> "Expected expression, but got token ';'!",
    "initStm1.test" -> "Expected identifier, but got token '='!",
    "initStmNameMissing.test" -> "Expected identifier, but got token '='!",
    "intLiteralBin.test" -> "Binary integer literal '0b111111111111111111111111111111111' is out of range!",
    "intLiteralDec1.test" -> "Decimal integer literal '2147483648' is out of range!",
    "intLiteralDec2.test" -> "Decimal integer literal '4294967295' is out of range!",
    "intLiteralHex.test" -> "Hexadecimal integer literal '0xAFFEBEEF2' is out of range!",
    "invalidExpStm1.test" -> "Expected token ';', but got token 'test'!",
    "invalidInputChar1.test" -> "Unexpected input character '$'!",
    "invalidInputChar2.test" -> "Unexpected input character '@'!",
    "invalidInputChar3.test" -> "Unexpected input character 'ä'!",
    "loremIpsum.test" -> "Expected token ';', but got token 'dolor'!",
    "macro1.test" -> "Expected macro, but got '_'!",
    "macro2.test" -> "Unknown macro '__FOO_BAR__'!",
    "newExp1.test" -> "Expected identifier, but got token ';'!",
    "newExp2.test" -> "Expected identifier, but got token '0'!",
    "newExp3.test" -> "Expected token '[' or token '{', but got token ';'!",
    "newExp4.test" -> "Cannot specify an array dimension after an empty dimension!",
    "newExp5.test" -> "Expected token ']', but got token ';'!",
    "newExp6.test" -> "Expected identifier, but got token '0'!",
    "newExp7.test" -> "Expected identifier, but got token '}'!",
    "newExp8.test" -> "Expected token '=', but got token '}'!",
    "newExp9.test" -> "Expected token ';', but got token '}'!",
    "newExp10.test" -> "Expected token ';', but got end of source!",
    "onlyElse.test" -> "Expected expression, but got token 'else'!",
    "returnStm1.test" -> "Expected expression, but got token ';'!",
    "returnStm2.test" -> "Expected expression, but got token ';'!",
    "returnStm3.test" -> "Expected expression, but got token ')'!",
    "stringLiteral.test" -> "Illegal string literal!",

    "stringLiteralUnicode1.test" -> "Illegal unicode escape in string literal!",
    "stringLiteralUnicode2.test" -> "Illegal unicode escape in string literal!",
    "stringLiteralUnicode3.test" -> "Illegal unicode escape in string literal!",

    "tyDef1.test" -> "Expected identifier, but got token ';'!",
    "tyDef2.test" -> "Expected token '=', but got token ';'!",
    "tyDef3.test" -> "Expected identifier, but got token '0'!",
    "tyDef4.test" -> "Expected token '{', but got token ';'!",
    "tyDef5.test" -> "Expected token '{', but got token ';'!",
    "tyDef6.test" -> "Definition of empty unions is not allowed!",
    "tyDef7.test" -> "Definition of empty structs is not allowed!",
    "tyDef8.test" -> "Member field 'b' may not be constant!",
    "tyDef9.test" -> "Expected identifier, but got token ';'!",
    "tyDef10.test" -> "Expected identifier, but got token '0'!",
    "tyDef11.test" -> "Expected token ';', but got token '}'!",
    "tyDef12.test" -> "Expected token ';', but got token '}'!",
    "tyDef13.test" -> "Expected identifier, but got token ';'!",
    "tyDef14.test" -> "Expected token '}', but got end of source!",
    "tyDef15.test" -> "Expected token ';', but got end of source!",
    "tyDef16.test" -> "Expected token ';', but got token '='!",
    "tyDef17.test" -> "Expected token ';', but got end of source!",
    "tyDef18.test" -> "Expected token ';', but got end of source!",
    "tyDef19.test" -> "Expected identifier, but got token 'struct'!",
    "varDef1.test" -> "Expected token '=', but got token ';'!",
    "whileStm1.test" -> "Expected token '(', but got token ';'!",
    "whileStm2.test" -> "Expected expression, but got token ')'!",
    "whileStm3.test" -> "Expected expression, but got token ';'!"
  )

  for ((test, expect) <- successfulTestCases)
    it should s"succeed when parsing file '$test'" in {
      val settings = CompilerSettings.defaultSettings
      val result   = asset(testDir + "correct/" + test).flatMap(x => Compiler.parse(settings, x))

      result shouldBe a [Success[_]]
      for (actual <- result) {
        val bodyOfMain = actual.globalScope.getBody
        bodyOfMain shouldBe a [BlockStm]

        val stms = bodyOfMain.asInstanceOf[BlockStm].getBody
        assume(stms == expect)
      }
    }

  for ((test, msg) <- nonSuccessfulTestCases)
    it should s"fail when parsing file '$test'" in {
      val settings = CompilerSettings.defaultSettings
      val result   = asset(testDir + "wrong/" + test).flatMap(x => Compiler.parse(settings, x))

      failureMessage(result) { actual =>
        assume(actual contains msg)
      }
    }

}
