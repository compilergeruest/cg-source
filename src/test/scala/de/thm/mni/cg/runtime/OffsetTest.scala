package de.thm.mni.cg.runtime

import org.scalatest.FlatSpec
import org.scalatest.Matchers
import de.thm.mni.cg.runtime._

class OffsetTest extends FlatSpec with Matchers {

  val rel = RelativeOffset

  "Relative Offsets" should "calculate the correct relative byte offset" in {
    for (i <- 0 to 100 by 4) {
      NoOffset(i).relativeByteOffset shouldBe (0)
    }

    for (i <- 0 to 100 by 4) {
      rel(4, NoOffset(i)).relativeByteOffset shouldBe (i)
    }

    rel(0, rel(4, NoOffset(4))).relativeByteOffset shouldBe (8)
    rel(0, RelativeOffset(2, NoOffset(4))).relativeByteOffset shouldBe (6)
    rel(0, rel(4, NoOffset(2))).relativeByteOffset shouldBe (6)
  }

  "Relative Offsets" should "calculate the correct aligned byte offset" in {
    for (i <- 0 to 100 by 4) {
      NoOffset(i).alignedByteOffset shouldBe (0)
    }

    val a = NoOffset(4)
    val b = rel(4, a)
    val c = rel(4, b)
    val d = rel(1, c)
    val e = rel(4, d)
    val f = rel(1, e)
    val g = rel(1, f)

    g.alignedByteOffset shouldBe 21
    f.alignedByteOffset shouldBe 20
    e.alignedByteOffset shouldBe 16
    d.alignedByteOffset shouldBe 12
    c.alignedByteOffset shouldBe 8
    b.alignedByteOffset shouldBe 4
    a.alignedByteOffset shouldBe 0
  }

}
