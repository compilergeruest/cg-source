package de.thm.mni.cg.util

import de.thm.mni.cg.util.traversal.DfgTraverser
import de.thm.mni.cg.parser.Tokenizer
import java.util.{ Stack => JStack }
import java.io.StringReader
import de.thm.mni.cg.parser.Parser
import de.thm.mni.cg.ir.Dfg
import de.thm.mni.cg.util.traversal.TraverseOrder._
import de.thm.mni.cg.util.traversal.TraverseOrder
import de.thm.mni.cg.util.traversal.PartialDfgTraverser
import de.thm.mni.cg.ir._
import de.thm.mni.cg.util.traversal.PartialTraverser
import scala.reflect.ClassTag

class AstTraversalTest extends TraversalTest {

  "An AstTraverser" should "exist" in {
    assume(true)
  }

  private def is[T](implicit ct: ClassTag[T]): Ast => Boolean =
    _ match {
      case ct(_) => true
      case _ => false
    }

  private val tests = Map[(String, TraverseOrder), Seq[Ast => Boolean]](
    ("int i; int b;", PreOrder) ->
      Seq(is[BlockStm], is[VarDef], is[NameTypeSymbol], is[VarDef], is[NameTypeSymbol]),
    ("int i; int b;", PostOrder) ->
      Seq(is[NameTypeSymbol], is[VarDef], is[NameTypeSymbol], is[VarDef], is[BlockStm]),
    ("int i; int b;", InOrder) ->
      Seq(is[NameTypeSymbol], is[VarDef], is[NameTypeSymbol], is[VarDef], is[BlockStm]),

    ("if (false) {} else {}", PreOrder) ->
      Seq(is[BlockStm], is[IfStm], is[BoolLit], is[BlockStm], is[BlockStm]),
    ("if (false) {} else {}", PostOrder) ->
      Seq(is[BoolLit], is[BlockStm], is[BlockStm], is[IfStm], is[BlockStm]),
    ("if (false) {} else {}", InOrder) ->
      Seq(is[BoolLit], is[IfStm], is[BlockStm], is[BlockStm], is[BlockStm]),

    ("", PreOrder) -> Seq(is[BlockStm]),
    ("", InOrder) -> Seq(is[BlockStm]),
    ("", PostOrder) -> Seq(is[BlockStm]),

    ("while (2 > 1) println(\"while (true) {}\");", PreOrder) ->
      Seq(is[BlockStm], is[WhileStm], is[BinExp], is[IntLit], is[IntLit],
          is[ExpStm], is[FunExp], is[StringLit]),

    ("while (2 > 1) println(\"while (true) {}\");", PostOrder) ->
      Seq(is[IntLit], is[IntLit], is[BinExp],
          is[StringLit], is[FunExp], is[ExpStm],
          is[WhileStm], is[BlockStm]),

    ("while (2 > 1) println(\"while (true) {}\");", InOrder) ->
      Seq(is[IntLit], is[BinExp], is[IntLit],
          is[WhileStm],
          is[StringLit], is[FunExp], is[ExpStm],
          is[BlockStm]),

    ("{{{{}}}}", PreOrder) ->
      Seq(is[BlockStm], is[BlockStm], is[BlockStm], is[BlockStm], is[BlockStm]),
    ("{{{{}}}}", InOrder) ->
      Seq(is[BlockStm], is[BlockStm], is[BlockStm], is[BlockStm], is[BlockStm]),
    ("{{{{}}}}", PostOrder) ->
      Seq(is[BlockStm], is[BlockStm], is[BlockStm], is[BlockStm], is[BlockStm]),

    ("int f(int n) if (n < 1) return 1; else return f(n - 1) * n;", PreOrder) ->
      Seq(is[BlockStm], is[FunDef], is[NameTypeSymbol], is[ParDef], is[NameTypeSymbol], is[IfStm], is[BinExp], is[NameVar], is[IntLit],
          is[ReturnStm], is[IntLit], is[ReturnStm], is[BinExp], is[FunExp], is[BinExp], is[NameVar], is[IntLit], is[NameVar]),

    ("int f(int n) if (n < 1) return 1; else return f(n - 1) * n;", PostOrder) ->
      Seq(is[NameTypeSymbol], // return-type
          is[NameTypeSymbol], is[ParDef], // parameter
          is[NameVar], is[IntLit], is[BinExp], // if-test
          is[IntLit], is[ReturnStm], // true-part
          is[NameVar], is[IntLit], is[BinExp], is[FunExp], is[NameVar], is[BinExp], is[ReturnStm], // false-part
          is[IfStm], is[FunDef], is[BlockStm]),

    ("int f(int n) if (n < 1) return 1; else return f(n - 1) * n;", InOrder) ->
      Seq(is[NameTypeSymbol], // return-type
          is[FunDef],
          is[NameTypeSymbol], is[ParDef], // parameter
          is[NameVar], is[BinExp], is[IntLit], // if-test
          is[IfStm],
          is[IntLit], is[ReturnStm], // true-part
          is[NameVar], is[BinExp], is[IntLit], is[FunExp], is[BinExp], is[NameVar], is[ReturnStm], // false-part
          is[BlockStm] 
      )
  )

  private def test(string: String, order: TraverseOrder, expected: Seq[Ast => Boolean]): Unit = {
    val orderName = order match {
      case PreOrder => "pre-order"
      case InOrder => "in-order"
      case PostOrder => "post-order"
      case _ => fail("unknown traverse order")
    }

    it should s"traverse the input '$string' in $orderName" in {
      val stack = new JStack[Ast => Boolean]()
      for (x <- expected.reverse) stack.push(x)   

      val tokenizer = new Tokenizer(new StringReader(string))
      val parser = new Parser(tokenizer)
      val program = parser.program()
      val errorStack = new JStack[String]()

      new PartialTraverser(order, {
        case (element, _) if (!stack.isEmpty) =>
          if (!stack.pop()(element)) {
            errorStack push s"(!) ${element.getClass.getSimpleName} (!)"
            val msg = reverseMkStringStack(
              errorStack, "", " -> ",
              s"\nExpectation was not fulfilled for element of type ${element.getClass.getSimpleName}!"
            )
            fail(msg)
          } else {
            errorStack push element.getClass.getSimpleName
          }
        case _ => fail("Expected end of ast-traversal but got still more elements!")
      }).traverse(program.globalScope.getBody)(program.globalScope.getBody.getTable)
    }
  }

  for {
    ((code, order), expected) <- tests
  } test(code, order, expected)

}
