package de.thm.mni.cg.util

import de.thm.mni.cg.util.traversal.DfgTraverser
import de.thm.mni.cg.parser.Tokenizer
import java.util.{ Stack => JStack }
import java.io.StringReader
import de.thm.mni.cg.parser.Parser
import de.thm.mni.cg.ir.Dfg
import de.thm.mni.cg.util.traversal.TraverseOrder._
import de.thm.mni.cg.util.traversal.TraverseOrder
import de.thm.mni.cg.util.traversal.PartialDfgTraverser
import de.thm.mni.cg.ir._

class DfgTraversalTest extends TraversalTest {

  "A DfgTraverser" should "exist" in {
    assume(true)
  }

  private val tests = Map[(String, TraverseOrder), Seq[Dfg => Boolean]](
    ("int i; int b;", PreOrder) ->
      Seq(_.isInstanceOf[StmDfg], _.isInstanceOf[StmDfg], _.isInstanceOf[ExitDfg]),
    ("int i; int b;", PostOrder) ->
      Seq(_.isInstanceOf[ExitDfg], _.isInstanceOf[StmDfg], _.isInstanceOf[StmDfg]),
    ("int i; int b;", InOrder) ->
      Seq(_.isInstanceOf[ExitDfg], _.isInstanceOf[StmDfg], _.isInstanceOf[StmDfg]),

    ("int i; if (false) { print(n); } else f(0); int j;", PreOrder) ->
      Seq(_.isInstanceOf[StmDfg], _.isInstanceOf[BranchDfg], _.isInstanceOf[ScopeDfg], _.isInstanceOf[StmDfg], _.isInstanceOf[ExitDfg],
                                                             _.isInstanceOf[StmDfg], _.isInstanceOf[StmDfg], _.isInstanceOf[ExitDfg]),
    ("int i; if (false) { print(n); } else f(0); int j;", PostOrder) ->
      Seq(_.isInstanceOf[ExitDfg], _.isInstanceOf[StmDfg], _.isInstanceOf[ScopeDfg],
          _.isInstanceOf[ExitDfg], _.isInstanceOf[StmDfg], _.isInstanceOf[StmDfg], _.isInstanceOf[BranchDfg], _.isInstanceOf[StmDfg]),
    ("int i; if (false) { print(n); } else f(0); int j;", InOrder) ->
      Seq(_.isInstanceOf[ExitDfg], _.isInstanceOf[StmDfg], _.isInstanceOf[ScopeDfg], _.isInstanceOf[BranchDfg],
          _.isInstanceOf[ExitDfg], _.isInstanceOf[StmDfg], _.isInstanceOf[StmDfg], _.isInstanceOf[StmDfg])
  )

  private def test(string: String, order: TraverseOrder, expected: Seq[Dfg => Boolean]): Unit = {
    val orderName = order match {
      case PreOrder => "pre-order"
      case InOrder => "in-order"
      case PostOrder => "post-order"
      case _ => fail("unknown traverse order")
    }
    it should s"traverse the input '$string' in $orderName" in {
      val stack = new JStack[Dfg => Boolean]()
      for (x <- expected.reverse) stack.push(x)

      val tokenizer = new Tokenizer(new StringReader(string))
      val parser = new Parser(tokenizer)
      val program = parser.program()
      val dfg = Dfg(program.globalScope)
      val errorStack = new JStack[String]()

      new PartialDfgTraverser(order, {
        case element if (!stack.isEmpty()) =>
          if (!stack.pop()(element)) {
            errorStack push s"(!) ${element.getClass.getSimpleName} (!)"
            val msg = reverseMkStringStack(
              errorStack, "", " -> ",
              s"\nExpectation was not fulfilled for element of type ${element.getClass.getSimpleName}!"
            )
            fail(msg)
          } else {
            errorStack push element.getClass.getSimpleName
          }
        case _ => fail("Expected end of dfg-traversal but got still more elements!")
      }).traverse(dfg.asInstanceOf[ScopeDfg].getInner)
    }
  }

  for {
    ((code, order), expected) <- tests
  } test(code, order, expected)

}
