package de.thm.mni.cg.util

import org.scalatest.Matchers
import org.scalatest.FlatSpec
import de.thm.mni.cg.util._
import java.io.File

class UtilTest extends FlatSpec with Matchers {

  "Utility functions" should "exist" in {
    de.thm.mni.cg.util.`package` shouldNot be(null)
  }

  "The duplicate function" should "find duplicates in a list" in {
    duplicates(List()) should be (List())
    duplicates(List(1)) should be (List())
    duplicates(List(1, 2, 3)) should be (List())

    duplicates(List(1, 1)) should be (List(1))
    duplicates(List(1, 1, 2)) should be (List(1))

    duplicates(List(1, 1, 1)) should be (List(1, 1))

    duplicates(List(1, 1, 1, 2, 2)) should be (List(1, 1, 2))
  }

  "The enumerate function" should "enumerate elements in a list" in {
    enumerate(List()) should be ("", true)
    enumerate(List(1)) should be ("1", false)
    enumerate(List(1, 2)) should be ("1 and 2", true)
    enumerate(List(1, 2, 3)) should be ("1, 2 and 3", true)
  }

  "The sequence function" should "swap the monads List[Option[_]] to Option[List[_]]" in {
    sequence(List(Some(2))) should be (Some(List(2)))
    sequence(List(Some(2), None)) should be (None)
    sequence(List(Some(2), Some(3))) should be (Some(List(2, 3)))
    sequence(List()) should be (Some(List()))
    sequence(List(None)) should be (None)
  }

  "A string" should "be implicitly converted to an InputSource" in {
    val text = "unit main() {}"
    val input: InputSource = text
    input shouldBe a [StringSource]
    input.asInstanceOf[StringSource].text should be (text)
  }

  "A basename of a filepath" should "be the filename without the file extension" in {
    basename("test") should be (None)
    basename("test.log") should be (Some("test"))
    basename("test.log,bak") should be (Some("test"))
    basename("test.log.bak") should be (Some("test.log"))
  }

  "A default output file" should "be derived correctly from an input file" in {
    defaultFile(new File("test.asm"), "bin") should be (new File("test.bin"))
    defaultFile(new File("test"), "bin") should be (new File("out.bin"))

    defaultFile(new File("/test.asm"), "bin") should be (new File("/test.bin"))
    defaultFile(new File("/test"), "bin") should be (new File("/out.bin"))
  }

  "The normal name-generator" should "generate names incrementally" in {
    val gen = Incremental[String]("s" + _)

    (0  until 10).foreach(i => gen() shouldBe s"s$i")
    (0  until 10).foreach(_ => gen())
    (20 until 30).foreach(i => gen() shouldBe s"s$i")
  }

  "The curried name-generator" should "generate names incrementally" in {
    case class Res(str: String, n: Int)
    val strlen = 20
    val rnd = new scala.util.Random()
    val gen = Incremental.curried(i => Res(_: String, i))

    var chars = rnd.alphanumeric
    def rndStr(f: String => Unit) = {
      val rndstr = chars.take(strlen).mkString
      chars = chars.drop(strlen)
      f(rndstr)
    }

    (0  until 10).foreach(i => rndStr(s => gen()(s) shouldBe Res(s, i)))
    (0  until 10).foreach(_ => gen()(""))
    (20 until 30).foreach(i => rndStr(s => gen()(s) shouldBe Res(s, i)))
  }

  "The flatten function on ast-nodes" should "give back all child-nodes in a flat sequence" in {
    import de.thm.mni.cg.ir._
    import de.thm.mni.cg.meta._

    val one: Exp = exp"1"
    val two: Exp = exp"2"
    val six: Exp = exp"6"
    val v_a: Exp = exp"a"

    val add: Exp = exp"($one + $two)"
    val sub: Exp = exp"($six - $v_a)"

    val res: Exp = exp"$add * $sub"
    val flatRes = res.flatten

    val shouldContain = Seq(one, two, six, v_a, add, sub, res)
    for (elem <- shouldContain) {
      if (!(flatRes contains elem)) {
        fail(s"(${res.code()}).flatten doesn't contain ${elem.code()})")
      }
    }
  }

  "The collect function on ast-nodes" should "give back some child-nodes in a flat sequence" in {
    import de.thm.mni.cg.ir._
    import de.thm.mni.cg.meta._

    val one: Exp = exp"1"
    val two: Exp = exp"2"
    val six: Exp = exp"6"
    val v_a: Exp = exp"a"

    val add: Exp = exp"($one + $two)"
    val sub: Exp = exp"($six - $v_a)"

    val res: Exp = exp"$add * $sub"
    val flatRes = res collect { case l: Lit => l }

    val shouldContain = Seq(one, two, six)
    for (elem <- shouldContain) if (!(flatRes contains elem)) {
      fail(s"(${res.code()}).collect { case l: Lit => l }  doesn't contain ${elem.code()})")
    }

    val shouldNotContain = Seq(v_a, add, sub, res)
    for (elem <- shouldNotContain) if (flatRes contains elem) {
      fail(s"(${res.code()}).collect { case l: Lit => l }  contains ${elem.code()})")
    }
  }

  "Time durations" should "be printed in a human readable format" in {
    an[IllegalArgumentException] shouldBe thrownBy {
      showDuration(-1)
    }
    showDuration(0)            should be ("0ms")
    showDuration(1)            should be ("1ms")
    showDuration(1000)         should be ("1s")
    showDuration(60000)        should be ("1min")
    showDuration(120000)       should be ("2mins")
    showDuration(3600000)      should be ("1h")
    showDuration(7200000)      should be ("2hs")
    showDuration(86400000)     should be ("1d")
    showDuration(172800000)    should be ("2ds")
    showDuration(86313600000L) should be ("999ds")
  }

}
