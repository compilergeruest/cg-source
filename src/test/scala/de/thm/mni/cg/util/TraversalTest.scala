package de.thm.mni.cg.util

import org.scalatest.Matchers
import org.scalatest.FlatSpec
import java.util.{ Stack => JStack }

class TraversalTest extends FlatSpec with Matchers {

  protected def reverseMkStringStack(errorStack_ : JStack[String], start: String, sep: String, end: String): String = {
    val errorStack = errorStack_.clone().asInstanceOf[JStack[String]]
    val builder    = new StringBuilder(start)

    while (!errorStack.isEmpty) {
      val element = errorStack.pop()
      builder ++= element
      if (!errorStack.isEmpty) {
        builder ++= sep
      }
    }

    builder ++= end
    builder.toString
  }

}
