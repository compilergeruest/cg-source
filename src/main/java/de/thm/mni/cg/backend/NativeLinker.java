package de.thm.mni.cg.backend;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

/**
 * A native interface for the eco32 linker of Prof. Geisse
 */
public final class NativeLinker {
	private static boolean nativesLoaded = false;
	private File outputFile;
	private final ArrayList<File> inputFiles;
	private int codeSegment = 0;
	private int dataSegment = 0;
	private int bssSegment = 0;
	private boolean relocateCode = false;
	private boolean relocateData = false;
	private boolean relocateBss = false;
	private boolean produceMapFile = false;
	private boolean writeHeader = false;

	/**
	 * Creates a new native linker
	 */
	public NativeLinker() {
		this.inputFiles = new ArrayList<>();
	}

	/**
	 * Sets the output file for this linker
	 * @param outputFile The output file where the executable code is written to
	 * @throws java.lang.IllegalArgumentException If the output file doesn't exist
	 * or can not be written to
	 */
	public final void setOutputFile(final File outputFile) {
		this.outputFile = outputFile;
		if (outputFile.exists()) {
			if (!outputFile.canWrite()) {
				throw new IllegalArgumentException(String.format("Permission denied for output file '%s'!", outputFile.getAbsolutePath()));
			} else if (!outputFile.isFile()) {
				throw new IllegalArgumentException(String.format("Output file '%s' already exists!", outputFile.getAbsolutePath()));
			}
		} else {
			try {
				if (!outputFile.createNewFile()) {
					throw new IllegalArgumentException(String.format("Could not create output file '%s'!", outputFile.getAbsolutePath()));
				}
			} catch (final IOException e) {
				throw new IllegalArgumentException(String.format("Permission denied for output file '%s'!", outputFile.getAbsolutePath()));
			}
		}
	}

	/**
	 * Adds an input file to the link process
	 * @param inputFile The input file to add
	 */
	public final void addInputFile(final File inputFile) {
		this.inputFiles.add(inputFile);
	}

	/**
	 * Clears the current linker setup
	 */
	public final void clear() {
		this.codeSegment = 0;
		this.dataSegment = 0;
		this.bssSegment = 0;
		this.relocateCode = true;
		this.relocateData = true;
		this.relocateBss = true;
		this.produceMapFile = false;
		this.writeHeader = false;
		this.inputFiles.clear();
		this.outputFile = null;
	}

	/**
	 * Relocates the code segment to the given address
	 * @param address The address to relocate the code segment to
	 */
	public final void relocateCodeSegment(final int address) {
		this.relocateCode = true;
		this.codeSegment = address;
	}

	/**
	 * Relocates the data segment to the given address
	 * @param address The address to relocate the data segment to
	 */
	public final void relocateDataSegment(final int address) {
		this.relocateData = true;
		this.dataSegment = address;
	}

	/**
	 * Relocates the bss segment to the given address
	 * @param address The address to relocate the bss segment to
	 */
	public final void relocateBssSegment(final int address) {
		this.relocateBss = true;
		this.bssSegment = address;
	}

	/**
	 * Sets an internal flag, so that the map file will be
	 * generated when the link process is started
	 */
	public final void produceMapFile() {
		this.produceMapFile = true;
	}

	/**
	 * Sets an internal flag, so that the header will
	 * be written when the link process is started
	 */
	public final void writeHeader() {
		this.writeHeader = true;
	}

	/**
	 * Links all input files and produces the executable bianry
	 * file
	 * @throws java.lang.RuntimeException If no input file is provided
	 */
	public final void link() {
		if (!NativeLinker.nativesLoaded) {
			final String architecture = System.getProperty("sun.arch.data.model");
			final String operatingSystem = System.getProperty("os.name").toLowerCase();
			final String arch = architecture.contains("64") ? "_64" : "_32";
			final String type = operatingSystem.contains("win") ? ".dll" : operatingSystem.contains("mac") ? "_osx.so" : ".so";
			final File libPath = JavaUtils.relative("runtime", "natives", "ld" + arch + type);
			System.load(libPath.getAbsolutePath());
			NativeLinker.nativesLoaded = true;
		}
		if (this.inputFiles.isEmpty()) {
			throw new RuntimeException("No input file specified for linkage!");
		} else if (this.outputFile == null) {
			throw new RuntimeException("No output file specified for linkage!");
		} else {
			this.link(this.inputFiles.toArray(new File[0]), this.outputFile, this.codeSegment, this.relocateCode, this.dataSegment, this.relocateData, this.bssSegment, this.relocateBss, this.produceMapFile, this.writeHeader);
		}
	}

	private final native void link(final File[] in, final File out, int relocateCodeSegement, boolean relocateCode, int relocateDataSegement, boolean relocateData, int relocateBssSegement, boolean relocateBss, boolean produceMapFile, boolean withHeader);

}
