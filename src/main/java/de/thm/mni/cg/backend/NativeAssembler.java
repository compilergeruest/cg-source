package de.thm.mni.cg.backend;

import java.io.File;
import java.io.FileNotFoundException;

final public class NativeAssembler {
	private static final Object UtilObject = de.thm.mni.cg.util.package$.MODULE$;
	private static boolean nativesLoaded = false;

	public final void assemble(final File in, final File out) throws Exception {
		if (!NativeAssembler.nativesLoaded) {
			final String architecture = System.getProperty("sun.arch.data.model");
			final String operatingSystem = System.getProperty("os.name").toLowerCase();
			final String arch = architecture.contains("64") ? "_64" : "_32";
			final String type = operatingSystem.contains("win") ? ".dll" : operatingSystem.contains("mac") ? "_osx.so" : ".so";
			final File libPath = JavaUtils.relative("runtime", "natives", "as" + arch + type);
			System.load(libPath.getAbsolutePath());
			NativeAssembler.nativesLoaded = true;
		}
		if (!in.exists()) {
			throw new FileNotFoundException(String.format("The input file '%s' does not exist!", in.getAbsolutePath()));
		} else if (!in.isFile() || in.isDirectory()) {
			throw new IllegalArgumentException(String.format("The input file '%s' seems not to be a *.asm file!", in.getAbsolutePath()));
		}
		if (!out.canWrite() && out.exists()) {
			throw new IllegalArgumentException(String.format("Could not access the output file '%s'!", out.getAbsolutePath()));
		}
		this._assemble(in, out);
	}

	private final native void _assemble(final File in, final File out) throws Exception;

}
