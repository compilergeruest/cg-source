package de.thm.mni.cg.backend;

import java.io.File;
import java.net.URL;

public final class JavaUtils {

	/**
	 * Tries to resolve a relative path, specified by the given path parts, to
	 * the jar
	 * 
	 * @param parts
	 *            The path parts
	 * @return Either the file path relative to the jar or null if an error
	 *         occurred
	 */
	public static File relative(String... parts) {
		try {
			final URL url = JavaUtils.class.getProtectionDomain().getCodeSource().getLocation();
			File result = new File(url.toURI()).getParentFile();
			for (final String part : parts) {
				result = new File(result, part);
			}
			return result;
		} catch (final Throwable t) {
			return null;
		}
	}

}
