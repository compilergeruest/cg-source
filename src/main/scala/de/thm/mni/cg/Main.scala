package de.thm.mni.cg

import java.io.File

import scala.util.Try

import de.thm.mni.cg.compiler.CompilerSettings
import de.thm.mni.cg.language.ErrorMessages._
import de.thm.mni.cg.repl.Repl
import scala.annotation.tailrec

object Main extends AnyRef
  with Compilation
  with Evaluation
  with Usage {

  def main(args: Array[String]): Unit = {
    val settings = CompilerSettings.defaultSettings
    parseArgs(settings, args.toList)
  }

  @tailrec
  private def parseArgs(settings: CompilerSettings, args: List[String]): Unit =
    args match {
      case ("--verbose" | "-v") :: tail =>
        parseArgs(settings.copy(verbose = true), tail)

      case ("--compile" | "-c") :: tail =>
        mainCompile(settings, tail)

      case ("--eval" | "-e") :: tail =>
        mainEval(settings, tail)

      case ("--repl" | "-r") :: _ =>
        Repl.main()

      case _ =>
        usage(unknownArgument(args.headOption))
    }

  object uint {
    private val number = "([0-9]+)".r
    def unapply(s: String): Option[Int] = Option(s).flatMap {
      case number(x) => Try(x.toInt).toOption
      case _         => None
    }
  }
  object file {
    def unapply(s: String): Option[File] = Option(new File(s))
  }
  object filex {
    def unapply(s: String): Option[File] = file.unapply(s).collect {
      case f => if (f.exists()) f else {
        println(inputFileNotExist(f))
        usage()
      }
    }
  }

}
