package de.thm.mni.cg.repl

import scala.collection.mutable.ArrayBuffer
import scala.collection.mutable.{ Set => MSet }
import scala.io.Source
import scala.util._

import de.thm.mni.cg.compiler.Compiler._
import de.thm.mni.cg.compiler.CompilerSettings
import de.thm.mni.cg.compiler.Phase
import de.thm.mni.cg.eval.Evaluator
import de.thm.mni.cg.eval.Pointer
import de.thm.mni.cg.eval.Pointer._
import de.thm.mni.cg.eval.RArray
import de.thm.mni.cg.language.ErrorMessages._
import de.thm.mni.cg.ir
import de.thm.mni.cg.ir._
import de.thm.mni.cg.{ semantic => sem }
import de.thm.mni.cg.semantic._
import de.thm.mni.cg.semantic.Ty._
import de.thm.mni.cg.table._
import de.thm.mni.cg.util._

object Repl extends ReplEvaluator {

  private val welcome = "Welcome to the interactive interpreter!\n" +
    "Type in expressions or statements for evaluation. Or try :help.\n"
  private val Tree = ":ast (.+)".r
  private val Heap = ":heapsize ([0-9]+)".r
  private val MemDump = ":memdump ([0-9]+)".r
  private val Help = ":help"
  private val Gfx = ":gfx"
  private val Paste = ":paste"
  private val End = ":end"
  private val Trace = ":stacktrace"

  private val promt = "> "
  private val InvCmd = ":(.+)".r

  private val commands = Seq(
    ":ast <code>"        -> "\t\tshows the abstract-syntax-tree of <code>",
    ":heapsize <bytes>"  -> "\tsets the size of the heap to <bytes>",
    ":memdump <rows>"    -> "\t\tdumps <rows> rows to the repl",
    Gfx                  -> "\t\t\ttoggle graphics module",
    Help                 -> "\t\t\tshows this help",
    Paste                -> "\t\t\tenable multi-line mode (quit with :end)",
    ":q | :quit | :exit" -> "\texit repl",
    Trace                -> "\t\tshow the stacktrace of exceptions",
    ":clear | :reset"    -> "\t\tclear all definitions of functions, types and variables"
  ).sortBy(first)

  /** The names of all std-lib functions */
  private val std = MSet[String]()

  /** Keeps last execution for rollback on exceptions */
  private var previousScope: ArrayBuffer[Stm] = _
  private var globalScope: Program = _

  private val evaluatedNodes = MSet[Int]()

  private var singleExp: Exp = _

  private var gfxVisible = false
  private var stacktrace = false

  private var line = 0

  // only eval stms, if they are not evaluated already
  protected def shouldEval(ast: Ast): Boolean =
    !(evaluatedNodes contains System.identityHashCode(ast))

  def main(/*args: Array[String] = Array()*/): Unit = {
    initStd()
    println(welcome)
    run()
  }

  private def initStd() = {
    compile("") match {
      case Success(Left(x)) =>
        val table = x.getTable
        for ((Name(name), _) <- table) std += name
      case _ =>
    }
  }

  private def run() = {
    print(promt)
    for (line <- Source.stdin.getLines()) {
      previousScope = ArrayBuffer()
      for ((stms, _) <- body(globalScope)) {
        previousScope ++= stms
      }

      def rollback(): Unit = {
        for ((stms, _) <- body(globalScope)) {
          stms.clear()
          stms ++= previousScope
        }
      }

      try handleLn(line.trim)
      catch {
        case e: RuntimeException =>
          if (stacktrace) e.printStackTrace(System.out)
          val message = Option(e.getMessage).getOrElse("unknown error")
          println(s"Skeleton: $message")
          rollback()
        case e: StackOverflowError =>
          if (stacktrace) e.printStackTrace(System.out)
          println(s"Skeleton: stack overflow exception")
          rollback()
        case t: Throwable =>
          throw t
      }
      print(promt)
    }
  }

  private def replError(s: String) = {
    println(s"internal repl error: $s")
    sys.exit(1)
  }

  private def handleLn(ln: String) = ln match {
    case MemDump(x) => memDump(x.toInt)
    case Heap(x) => setHeapSize(x.toInt)
    case Tree(x) => onSuccess(compile(x))(
      _.fold(show[Stm], show[Exp])
    )
    case ":clear" | ":reset" =>
      for ((stms, _) <- body(globalScope))
        stms.clear()
      globalScope = null
      evaluatedNodes.clear()
      line = 0
      resetNameGenerator()
      println("Cleared all definitions!")
    case Trace =>
      stacktrace = !stacktrace
      val will = if (stacktrace) "will" else "won't"
      println(s"The stacktrace now $will be shown on exceptions!")
    case ":q" | ":quit" | ":exit" => sys.exit()
    case `Help` => printHelp()
    case `Gfx` =>
      gfxVisible = !gfxVisible
      val prefix = if (gfxVisible) "En" else "Dis"
      println(s"${prefix}able graphics module!")
      setVisible(gfxVisible)
    case `Paste` =>
      multiLineMode()
    case InvCmd(x) =>
      println(s"Unknown command '$x'!")
      printHelp()
    case "" => ()
    case s => onSuccess(compile(s))(
      _.fold(x => handleProgram(Program.wrap(NoPosition, x)), handleSingleExpr)
    )
  }

  private def printHelp(): Unit =
    commands.foreach { case (cmd, desc) =>
      print(cmd); println(desc)
    }

  private def multiLineMode(): Unit = {
    println("You have entered the multiline mode!\nEnter your code and quit with :end!\n")
    import scala.util.control.Breaks._
    val buff = new StringBuilder()
    val iter = Source.stdin.getLines()

    breakable {
      while (iter.hasNext) iter.next().trim match {
        case `End` => break
        case line  => buff ++= line += '\n'
      }
    }

    onSuccess(compiles(buff.toString))(handleProgram)
  }

  private def handleProgram(prog: Program) = {
    if (globalScope == null) {
      globalScope = prog
    } else {
      // Merge main-scope across runs of the compiler
      for ((gbody, _) <- body(globalScope); (nbody, _) <- body(prog)) {
        for (stm <- nbody) {
          gbody += stm
        }
      }
    }

    destroyTables()
    semantics(globalScope) match {
      case Success(_) =>
        eval(globalScope)
        body(globalScope).foreach { case (stms, _) =>
          for (stm <- stms) if (shouldEval(stm)) {
            evaluatedNodes += System.identityHashCode(stm)
          }
        }
      case Fail(msg) =>
        println(msg)
        for ((gbody, _) <- body(globalScope); (nbody, _) <- body(prog)) {
          for (stm <- nbody) {
            gbody -= stm
          }
        }
    }
  }

  private def destroyTables(): Unit = {
    if (globalScope == null) {
      globalScope = Program.wrap(HasPosition(0, 0, ""))
    } else {
      globalScope = globalScope.copy(globalTable = SymbolTable(false, None))
    }

    // fix positions
    for ((stms, _) <- body(globalScope)) {
      for (stm <- stms; if shouldEval(stm)) {
        for (sub <- stm.flatten) {
          sub <@ (sub.pos match {
            case HasPosition(ln, cl, s) => HasPosition(line, cl, s)
            case NoPosition             => NoPosition
          })

          def newp(pos: Position): Position = pos match {
            case HasPosition(ln, cl, s) => HasPosition(line, cl, s)
            case NoPosition             => NoPosition
          }

          sub match {
            case nv @ NameVar(n, _)         => n <@ newp(nv.pos)
            case fd @ FieldDef(_, n)        => n <@ newp(fd.pos)
            case fd @ FunDef(n, _, _, _, _) => n <@ newp(fd.pos)
            case td @ TyDef(n, _)           => n <@ newp(td.pos)
            case ts @ NameTypeSymbol(n)     => n <@ newp(ts.pos)
            case cs @ ComplexTypeSymbol((mems, _)) =>
              for (i <- mems.indices) {
                val ir.Component(ts, n) = mems(i)
                mems(i) = ir.Component(ts, n <@ newp(cs.pos))
              }
            case fe @ FunExp(n, _, _)    => n <@ newp(fe.pos)
            case no @ NewObjExp(n, _, _) => n <@ newp(no.pos)
            case av @ AccessVar(_, n, _) => n <@ newp(av.pos)
            case _                       => ()
          }
        }
        line += 1
      }
    }
  }

  private var freshName: Incremental[Name] = _

  private def resetNameGenerator(): Unit = {
    freshName = Incremental(x => Name("res" + x))
  }
  resetNameGenerator()

  protected def wantValueOf(exp: Exp): Boolean =
    singleExp != null && (exp eq singleExp)

  private def handleSingleExpr(exp: Exp): Unit = {
    if (!typecheckSingleExp(exp)) return ()

    val exptys = handleSpecialTypesInSingleExp(exp)
    if (exptys == null) return ()

    if (exptys == List(NullTy)) {
      println("null")
    } else {
      val (vdefsAnyTys, vdefs, init) = buildInfoForSingleExp(exp, exptys)

      addSingleExpToGlobalScope(exp, init)
      destroyTables()
      // check newly patched globalScope
      semantics(globalScope) match {
        case Success(_) =>
        case Fail(_)    => internalError()
      }

      evaluatedNodes += System.identityHashCode(init)

      val result = evalGlobalScopeForSingleExp(exp)

      // print results
      fancyPrintSingleExpResult(exp, init, vdefsAnyTys, result)
    }
  }

  /** returns false, if an error occured */
  private def typecheckSingleExp(exp: Exp): Boolean = {
    if (globalScope == null) {
      globalScope = Program.wrap(exp.pos)
    }

    for ((stms, _) <- body(globalScope)) {
      stms += ExpStm(exp) <@ exp.pos
    }

    destroyTables()
    val checkresult = semantics(globalScope) match {
      case Success(_) =>
        true
      case Fail(msg)  =>
        println(msg)
        false
    }

    for ((stms, _) <- body(globalScope)) {
      stms -= ExpStm(exp) <@ exp.pos
    }

    checkresult
  }

  /** handle special types (null, tuple) */
  private def handleSpecialTypesInSingleExp(exp: Exp): List[Ty] = exp.getTy match {
    case t @ `unitTy` =>
      println("Error: Did you use a statement as an expression?")
      null
    case t @ NullTy   => List(t)
    case TupleTy(tys) => tys
    case ty           => List(ty)
  }

  /** foreach return value, generate vardef with freshName */
  private def buildInfoForSingleExp(exp: Exp, exptys: List[Ty]) = {
    val vdefsAnyTys = exptys map { ty =>
      val name = freshName()
      (VarDef(false, ty2sym(ty), name <@ HasPosition(line, 1, name.name)), ty)
    }
    val vdefs = vdefsAnyTys map first
    val init = InitStm(ArrayBuffer(vdefs: _*), exp) <@ exp.pos

    (vdefsAnyTys, vdefs, init)
  }

  /** add init stm to global scope */
  private def addSingleExpToGlobalScope(exp: Exp, init: InitStm): Unit = {
    if (globalScope == null) {
      globalScope = Program.wrap(exp.pos)
    }
    body(globalScope).foreach { case (stms, _) =>
      stms += init
      evaluatedNodes += System.identityHashCode(init)
    }
  }

  private def evalGlobalScopeForSingleExp(exp: Exp) = {
    singleExp = exp

    // get value of evaluated exp
    val rawRes = try eval(globalScope) catch {
      case e: ExprValue => e.value
      case t: Throwable => throw t
    }

    singleExp = null

    rawRes match {
      case xs: ArrayBuffer[_] => xs.asInstanceOf[ArrayBuffer[Any]]
      case sg                 => ArrayBuffer(sg)
    }
  }

  private def fancyPrintSingleExpResult(exp: Exp, init: InitStm, vdefsAnyTys: List[(VarDef, Ty)], result: ArrayBuffer[Any]) = {
    val concat = vdefsAnyTys zip result
    lazy val error = sys.error("illegal state")
    implicit val tbl = exp.getTable

    def arrRep[A](seq: Seq[A]) =
      seq.mkString("[", ", ", "]")

    def strRep(ty: Ty, res: Any, handleFake: Boolean): String = (res, resolve(ty).getOrElse(error)) match {
      case (n: Int, _)                  => n.toString
      case (b: Boolean, _)              => b.toString
      case (c: Char, _)                 => s"'${escape((c.toInt & 0xFF).toChar, charlit = true)}'"
      case (p: Pointer, _) if p == NULL => "null"
      case (p: Pointer, ComplexTy(_)) =>
        (if (handleFake) "// " else "") + p.toString
      case (p: Pointer, arr @ ArrayTy(atyR)) =>
        val aty = resolve(atyR).getOrElse(error)
        val tySize = aty.stackSize
        val array = wrapRequire[RArray](p, arr)
        val values = for (i <- 0 until array.size) yield array(i)

        if (tySize == 1) {
          val data = aty match {
            case `boolTy` => values.map(_ != 0)
            case `charTy` => values.map(_.toChar)
            case _ => error
          }
          (if (handleFake) "// " else "") + arrRep(data.map(strRep(aty, _, handleFake = false)))
        } else if (tySize == 4) {
          val data = aty match {
            case ComplexTy(_) => values.map(p ! _)
            case ArrayTy(_)   => values.map(p ! _)
            case _            => values
          }
          (if (handleFake) "// " else "") + arrRep(data.map(strRep(aty, _, handleFake = false)))
        } else error

      case (any, _)     => any.toString
    }

    def printSingleLine(vd: VarDef, ty: Ty, res: Any, indent: Boolean = false) = {
      if (indent) print("  ")
      println(s"$ty ${vd.getName.name} = ${strRep(ty, res, handleFake = true)};")
    }

    val onlyOne = containsOnlyOne(vdefsAnyTys)
    if (!onlyOne) {
      init.printCode()
    }
    for (((vd, ty), res) <- concat) {
      printSingleLine(vd, ty, res, indent = !onlyOne)
    }
  }

  private def ty2sym(ty: Ty): TypeSymbol = {
    def transform(c: sem.Component): ir.Component =
      ir.Component(ty2sym(c.ty), Name(c.name))

    ty match {
      case NameTy(name)   => NameTypeSymbol(Name(name))
      case ArrayTy(base)  => ArrayTypeSymbol(ty2sym(base))
      case StructTy(mems) => StructTypeSymbol((mems map transform).to[ArrayBuffer])
      case UnionTy(mems)  => UnionTypeSymbol((mems map transform).to[ArrayBuffer])
      case _              => internalError()
    }
  }

  /*
   * =============================== helper functions ===============================
   */

  private def countDefs(program: Program) = {
    val stms = body(program).map(first)
    stms.map(_.count(_.isInstanceOf[Def])).getOrElse(0)
  }

  private def defOf(entry: TableEntry) =
    entry match {
      case e: FunEntry   => e.getFunDef
      case e: FieldEntry => e.getFieldDef
      case e: TyEntry    => e.getTyDef
    }

  private def show(prog: Program): Unit =
    show(prog.globalScope)

  private def show[T <: Ast](ast: T): Unit = {
    ast.printLispy()
    println()
  }

  private def onSuccess[T](t: Try[T])(f: T => Unit): Unit = t match {
    case Success(x) => f(x)
    case Fail(msg)  => print(msg)
  }

  private def body(prog: Program): Option[(ArrayBuffer[Stm], SymbolTable)] =
    (for {
      program <- Option(prog)
      gscope  <- Option(program.globalScope)
      mainbdy <- Option(gscope.getBody)
    } yield mainbdy) collect {
      case BlockStm(xs, local) => (xs, local)
    }

  /*
   * =========================== repl-compiler interface ============================
   */

  private val settings = CompilerSettings.defaultSettings

  /** left = stms, right = exp */
  private def compile(src: String): Try[Either[Stm, Exp]] = {
    lazy val progp = (statement |> fakeS(namer)).run(settings, src).map(Left(_))
    val exprp = (expression |> fakeE(namer)).run(settings, src).map(Right(_))
    exprp.orElse(progp)
  }

  private def compiles(input: InputSource) =
    syntacticAnalysis.run(settings, input)

  private val expression = parseWith(_.only(_.exp()))
  private val statement  = parseWith(_.only(_.stm()))

  private def semantics(program: Program) =
    semanticAnalysis.run(settings, program)

  private def fakeS(phase: Phase[Program, Program]): Phase[Stm, Stm] = Phase { case (_, stm) =>
    val prog = Program.wrap(NoPosition, stm)
    phase.run(settings, prog).map(_ => stm)
  }

  private def fakeE(phase: Phase[Program, Program]): Phase[Exp, Exp] = Phase { case (_, exp) =>
    val prog = Program.wrap(NoPosition, ExpStm(exp))
    phase.run(settings, prog).map(_ => exp)
  }

}
