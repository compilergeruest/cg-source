package de.thm.mni.cg.repl

import de.thm.mni.cg.eval._
import de.thm.mni.cg.ir._

import scala.collection.mutable.{ Map => MMap }
import scala.collection.mutable.{ Set => MSet }

abstract class ReplEvaluator extends Evaluator {

  protected def shouldEval(ast: Ast): Boolean

  private def alwaysEval(ast: Ast) =
    Option(ast) collect {
      case _: FieldDef  => true
      case _: AssignStm => true
      case _: InitStm   => true
    } getOrElse false

  override protected def eval(s: Stm)(implicit ctx: EvalContext): Unit =
    // only eval stms, if they are not evaluated already
    if (alwaysEval(s) || shouldEval(s)) super.eval(s)(ctx)
    else ()

  protected def wantValueOf(exp: Exp): Boolean

  override protected def eval(exp: Exp, deref: Boolean)(implicit ctx: EvalContext): Any = {
    val result = super.eval(exp, deref = deref)
    if (wantValueOf(exp)) {
      throw new ExprValue(result)
    }
    result
  }

  private val allocated = MMap[Int, Pointer]()

  override protected def stringLit(s: String, deref: Boolean): Pointer =
    allocated.getOrElseUpdate(System.identityHashCode(s), super.stringLit(s, deref))

  override protected def newArrExp(n: NewArrExp)(implicit ctx: EvalContext): Pointer =
    allocated.getOrElseUpdate(System.identityHashCode(n), super.newArrExp(n))

  override protected def newObjExp(n: NewObjExp)(implicit ctx: EvalContext): Pointer =
    allocated.getOrElseUpdate(System.identityHashCode(n), super.newObjExp(n))

  private val freed = MSet[Int]()

  override protected def delStm(s: DelStm)(implicit ctx: EvalContext): Unit = {
    val id = System.identityHashCode(s)
    if (!(freed contains id)) {
      super.delStm(s)
    }
  }

}
