package de.thm.mni.cg.repl

/**
 * Exception-class to take an eval-result out of
 * its runtime context.
 */
private[repl] class ExprValue(val value: Any) extends Exception
