package de.thm.mni.cg.eval

import scala.collection.mutable.ArrayBuffer
import scala.collection.mutable.{ Map => MMap }
import scala.reflect.ClassTag
import scala.util.Try
import de.thm.mni.cg.eval.Pointer._
import de.thm.mni.cg.ir._
import de.thm.mni.cg.{ semantic => sem }
import de.thm.mni.cg.semantic._
import de.thm.mni.cg.semantic.Ty._
import de.thm.mni.cg.table._
import de.thm.mni.cg.util._
import de.thm.mni.cg.util.cache.Caching
import System.{ currentTimeMillis => now }
import java.util.Scanner

// int fib(int n) if (n < 2) return n; else return fib(n - 1) + fib(n - 2); // fib(30) == 832040
// int fac(int n) if (n == 0) return 1; else return n * fac(n - 1);

/*
 * There are the following problems:
 * - better error messages (e.g. heap-overflow)
 */
class Evaluator extends Caching {

  private var mem = new Memory(defaultHeapSizeInEvaluator)

  private def println(x: Any) =
    Predef.println(s"DEBUG: $x")

  private def println() =
    Predef.println()

  def eval(program: Program): Unit = {
    if (startTime == -1) startTime = now()
    val globalctx = new EvalContext()
    val mainFunc = program.globalScope
    eval(mainFunc)(globalctx)
  }

  def setHeapSize(size: Int): Unit = {
    val mem2 = new Memory(size)
    val copyto = size min mem.size

    for (i <- 0 until copyto) {
      mem2.byte(i) = mem.byte(i)
    }
    mem = mem2
  }

  def memDump(rows: Int): Unit =
    mem.dump(rows)

  /*
   * ========================= std-function implementations =========================
   */

  private var startTime: Long = -1L

  private lazy val stdFunction = Map(
    "printc" -> std_printc _,
    "printi" -> std_printi _,
    "printx" -> std_printx _,
    "readc"  -> std_readc _,
    "readi"  -> std_readi _,
    "exit"   -> std_exit _,
    "time"   -> std_time _,

    "clearAll"   -> std_clearAll _,
    "drawLine"   -> std_drawLine _,
    "setPixel"   -> std_setPixel _,
    "drawCircle" -> std_drawCircle _
  )

  /* unit printc(char c); */
  private def std_printc(ctx: EvalContext): Unit =
    ctx("c") match {
      case c: Char => print(c)
      case _       => error
    }

  /* unit printi(int n); */
  private def std_printi(ctx: EvalContext): Unit =
    ctx("n") match {
      case n: Int => print(n)
      case _      => error
    }

  /* unit printx(int n); */
  private def std_printx(ctx: EvalContext): Unit =
    ctx("n") match {
      case n: Int => printf("0x%08X", n)
      case _      => error
    }

  /* char readc(); */
  private def std_readc(ctx: EvalContext): Unit = {
    val c = _readChar()
    throw new EvalReturn(ArrayBuffer(c))
  }

  /* char readi(); */
  private def std_readi(ctx: EvalContext): Unit = {
    val n = _readInt()
    throw new EvalReturn(ArrayBuffer(n))
  }

  /* unit exit(); */
  private def std_exit(ctx: EvalContext): Unit = sys.exit(0)

  /* int time(); */
  private def std_time(ctx: EvalContext): Unit = {
    val time = ((now() - startTime) / 1000).toInt
    throw new EvalReturn(ArrayBuffer(time))
  }

  private val gfx = new EvaluatorGraphics()

  final def setVisible(visible: Boolean) =
    gfx setVisible visible

  def killWindow() =
    gfx.killWindow()

  /* unit clearAll(int color); */
  private def std_clearAll(ctx: EvalContext): Unit =
    ctx("color") match {
      case c: Int => gfx.clearAll(c)
      case _      => error
    }

  /* unit drawLine(int x1, int y1, int x2, int y2, int color); */
  private def std_drawLine(ctx: EvalContext): Unit =
    (ctx("x1"), ctx("y1"), ctx("x2"), ctx("y2"), ctx("color")) match {
      case (x1: Int, y1: Int, x2: Int, y2: Int, color: Int) => gfx.drawLine(x1, y1, x2, y2, color)
      case _ => error
    }

  /* unit setPixel(int x, int y, int color); */
  private def std_setPixel(ctx: EvalContext): Unit =
    (ctx("x"), ctx("y"), ctx("color")) match {
      case (x: Int, y: Int, color: Int) => gfx.drawPixel(x, y, color)
      case _                            => error
    }

  /* unit drawCircle(int x, int y, int radius, int color); */
  private def std_drawCircle(ctx: EvalContext): Unit =
    (ctx("x"), ctx("y"), ctx("radius"), ctx("color")) match {
      case (x: Int, y: Int, radius: Int, color: Int) => gfx.drawCircle(x, y, radius, color)
      case _                                         => error
    }

  private var scannedLine: String = _
  private val scanner = new Scanner(System.in)
  private val number = "-?[0-9]+".r

  private def _readInt(): Int = {
    if (scannedLine != null) {
      number.findPrefixMatchOf(scannedLine) match {
        case Some(mat) =>
          val matched = mat.matched
          scannedLine = scannedLine.substring(matched.length())
          if (scannedLine.isEmpty()) {
            scannedLine = null
          }
          matched.toInt
        case None      =>
          scannedLine = null
          -1
      }
    } else {
      scannedLine = scanner.nextLine()
      _readInt()
    }
  }

  private def _readChar(): Char = {
    if (scannedLine != null) {
      val c = scannedLine(0)
      scannedLine = scannedLine.substring(1)
      if (scannedLine.isEmpty()) {
        scannedLine = null
      }
      c
    } else {
      scannedLine = scanner.nextLine()
      _readChar()
    }
  }

  /*
   * ================================= Definitions ==================================
   */

  protected def eval(d: FunDef)(implicit ctx: EvalContext): Any = {
    val bodyctx = EvalContext(parent = Some(ctx))
    eval(d.getBody)(bodyctx)
    ()
  }

  protected def eval(d: FieldDef)(implicit ctx: EvalContext): Unit = d match {
    case VarDef(_, _, Name(name)) => ctx.insert(name)
    case ParDef(_, Name(name))    => ctx.insert(name)
  }

  /*
   * ================================== Statements ==================================
   */

  protected def eval(s: Stm)(implicit ctx: EvalContext): Unit = s match {
    case f: FieldDef  => eval(f)

    case a: AssignStm => assignStm(a)
    case i: InitStm   => initStm(i)

    case b: BlockStm  => blockStm(b)
    case d: DelStm    => delStm(d)
    case e: ExpStm    => expStm(e)

    case f: ForStm    => forStm(f)
    case i: IfStm     => ifStm(i)
    case w: WhileStm  => whileStm(w)

    case r: ReturnStm => returnStm(r)

    case _            => ()
  }

  protected def expStm(s: ExpStm)(implicit ctx: EvalContext): Unit = {
    val ExpStm(exp) = s
    eval(exp, deref = true)
  }

  protected def initStm(s: InitStm)(implicit ctx: EvalContext): Unit = {
    val InitStm(vdfs, rhsExp) = s

    val rhss = eval(rhsExp, deref = true) match {
      case xs: ArrayBuffer[_] => xs.asInstanceOf[ArrayBuffer[Any]]
      case sg                 => ArrayBuffer(sg)
    }

    for ((v @ VarDef(_,_,Name(name)), rhs) <- vdfs zip rhss) {
      eval(v)(ctx)
      ctx(name) = rhs
    }
  }

  protected def assignStm(s: AssignStm)(implicit ctx: EvalContext): Unit = {
    val AssignStm(lhsExps, rhsExp) = s

    case class Assign(l: Any, r: Any, ty: Ty)

    val rhs = eval(rhsExp, deref = true) match {
      case xs: ArrayBuffer[_] => xs.asInstanceOf[ArrayBuffer[Any]]
      case sg                 => ArrayBuffer(sg)
    }
    implicit val tbl = s.getTable
    val assignments = lhsExps zip rhs map { case (l, r) =>
      val lh = eval(l, deref = false)
      val ty = resolve(l.getTy).getOrElse(error)
      Assign(lh, r, ty)
    }

    for (assign <- assignments) assign match {
      // namevar
      case Assign(Name(name), any, _) =>
        ctx(name) = any
      // complex
      case Assign(p: Pointer, any, ty) =>
        val size = ty.stackSize
        if (size == 1) p.byte = any match {
          case c: Char => c.toByte
          case true    => 1.toByte
          case false   => 0.toByte
          case _       => error
        }
        else if (size == 4) p.word = any match {
          case n: Int     => n
          case p: Pointer => p.addr
          case _          => error
        }
        else error
      case _ =>
        error
    }
  }

  protected def returnStm(s: ReturnStm)(implicit ctx: EvalContext): Unit = {
    val ReturnStm(exps) = s
    val res = exps.map(eval(_, deref = true))
    throw new EvalReturn(res)
  }

  protected def blockStm(s: BlockStm)(implicit ctx: EvalContext): Unit = {
    val BlockStm(stms, _) = s
    val local = EvalContext(parent = Some(ctx))
    stms.foreach(eval(_)(local))
  }

  protected def forStm(s: ForStm)(implicit ctx: EvalContext): Unit = {
    val ForStm(vd, Range(startExp, optStep, endExp), body, _) = s

    val forScope = EvalContext(Some(ctx))
    eval(vd)(forScope)

    val VarDef(_, _, Name(name)) = vd

    val start = eval(startExp, deref = true) match {
      case n: Int => n
      case _      => error
    }
    forScope(vd.getName.name) = start
    val end = eval(endExp, deref = true) match {
      case n: Int => n
      case _      => error
    }
    val step = optStep.map(eval(_, deref = true)) match {
      case Some(next: Int)      => next - start
      case None if start <= end => 1
      case None                 => -1
      case _                    => error
    }

    def evalIdx() =
      forScope(name) match {
        case n: Int => n
        case _      => error
      }

    def hasNext() =
      if (step < 0) evalIdx() > end
      else evalIdx() < end

    while (hasNext()) {
      eval(body)(forScope)
      val counterVal = step + (forScope(name) match {
        case n: Int => n
        case _      => error
      })
      forScope(name) = counterVal
    }
  }

  protected def delStm(s: DelStm)(implicit ctx: EvalContext): Unit = {
    val DelStm(expr) = s
    eval(expr, deref = true) match {
      case p: Pointer => mem.free(p)
      case _          => error
    }
  }

  protected def whileStm(s: WhileStm)(implicit ctx: EvalContext): Unit = {
    val WhileStm(testExp, body) = s
    def evalTest() =
      eval(testExp, deref = true) match {
        case b: Boolean => b
        case _          => error
      }

    while (evalTest()) {
      eval(body)
    }
  }

  protected def ifStm(s: IfStm)(implicit ctx: EvalContext): Unit = {
    val IfStm(testExp, onTrue, optOnFalse) = s
    val test = eval(testExp, deref = true) match {
      case b: Boolean => b
      case _          => error
    }
    if (test) {
      eval(onTrue)
    } else {
      for (onFalse <- optOnFalse) {
        eval(onFalse)
      }
    }
  }

  /*
   * ================================= Expressions ==================================
   */

  protected def eval(exp: Exp, deref: Boolean)(implicit ctx: EvalContext): Any = exp match {
    // literals
    case BoolLit(b, _)        => b
    case CharLit(c, _)        => c
    case IntLit(n, _, _)      => n
    case NullLit(_)           => NULL
    case StringLit(s, _)      => stringLit(s, deref)

    // vars
    case v: NameVar           => nameVar(v, deref)
    case v: AccessVar         => accessVar(v, deref)
    case v: ArrayVar          => arrayVar(v, deref)

    // exps
    case BinExp(op, l, r, _)  => binExp(op, l, r)
    case UnaryExp(op, exp, _) => unExp(op, exp)
    case CastExp(sym, exp, _) => castExp(sym, exp)
    case f: FunExp            => funExp(f)
    case n: NewArrExp         => newArrExp(n)
    case n: NewObjExp         => newObjExp(n)
  }

  protected def wrapPointer(ptr: Pointer, ty: Ty)(implicit table: SymbolTable): Any =
    ty match {
      case s: StructTy => RStruct(ptr, namedMembers(s))
      case u: UnionTy  => RUnion(ptr, namedMembers(u))
      case a: ArrayTy  => RArray(ptr, resolve(a.base).getOrElse(error).stackSize)
      case _           => error
    }

  protected def wrapRequire[T <: Complex](ptr: Pointer, ty: Ty)(implicit table: SymbolTable, ct: ClassTag[T]): T = {
    val resolvedTy = resolve(ty).getOrElse(error)
    Try(wrapPointer(ptr, resolvedTy)).toOption collect {
      case ct(x) => x
    } getOrElse error
  }

  protected def stringLit(s: String, deref: Boolean): Pointer = {
    val lit = mem.malloc(4 + s.length())
    if (lit == NULL) NULL
    else {
      lit.word = s.length()
      for (i <- s.indices) {
        (lit + 4 + i).byte = s(i).toByte
      }
      lit
    }
  }

  protected def funExp(exp: FunExp)(implicit ctx: EvalContext): Any = {
    val FunExp(name, args, _) = exp
    val fentry = exp.getTable[FunEntry](name).get
    val stackFrame = EvalContext(parent = Some(ctx))

    val paramAndValue = fentry.getFunDef.getParams zip args
    for ((p @ ParDef(_, Name(name)), exp) <- paramAndValue) {
      eval(p)(stackFrame)
      stackFrame(name) = eval(exp, true)
    }

    try {
      if (fentry.isStdFunction) stdFunction(name.name)(stackFrame)
      else eval(fentry.getFunDef)(stackFrame)
    } catch {
      case r: EvalReturn => r.value match {
        case xs: ArrayBuffer[_] if xs.size == 1 => xs.head
        case xs: ArrayBuffer[_]                 => xs
        case _: Unit                            => ()
        case _                                  => error
      }
      case t: Throwable => throw t
    }
  }

  protected def nameVar(v: NameVar, deref: Boolean)(implicit ctx: EvalContext): Any = {
    val NameVar(n @ Name(name), _) = v
    if (deref) ctx(name)
    else n
  }

  protected def arrayVar(v: ArrayVar, deref: Boolean)(implicit ctx: EvalContext): Any = {
    val ArrayVar(base, index, _) = v
    val b = eval(base, deref = true)
    val idx = eval(index, deref = true) match {
      case x: Int => x
      case _      => error
    }
    implicit val tbl = v.getTable
    b match {
      case p: Pointer if p != NULL =>
        val view = wrapRequire[RArray](p, base.getTy)
        val addr = view.ptr(idx)
        if (deref) {
          val ty = resolve(v.getTy).getOrElse(error)
          ty match {
            case ComplexTy(_) => p ! addr.word
            case _: ArrayTy   => p ! addr.word
            case `charTy`     => addr.byte.toChar
            case `boolTy`     => addr.byte != 0
            case `intTy`      => addr.word
            case _            => error
          }
        } else {
          addr
        }
      case p: Pointer => npe
      case _          => error
    }
  }

  protected def accessVar(v: AccessVar, deref: Boolean)(implicit ctx: EvalContext): Any = {
    val AccessVar(base, Name(member), _) = v
    val b = eval(base, deref = true)
    implicit val tbl = v.getTable
    b match {
      case p: Pointer if p != NULL =>
        val view = wrapRequire[NamedMembers](p, base.getTy)
        val (off, _) = view.info(member)
        val addr = p + off
        if (deref) {
          val ty = resolve(v.getTy).getOrElse(error)
          ty match {
            case ComplexTy(_) => p ! addr.word
            case _: ArrayTy   => p ! addr.word
            case `charTy`     => addr.byte.toChar
            case `boolTy`     => addr.byte != 0
            case `intTy`      => addr.word
            case _            => error
          }
        } else {
          addr
        }
      case p: Pointer => npe
      case _ => error
    }
  }

  protected def newArrExp(n: NewArrExp)(implicit ctx: EvalContext): Pointer = {
    val NewArrExp(NameTypeSymbol(name), dims, _) = n

    implicit val tbl = n.getTable
    val tentry = tbl[TyEntry](name).getOrElse(error)
    val resolved = resolve(tentry.getTy).getOrElse(error)

    var failed = false
    val allocated = ArrayBuffer[Pointer]()

    def helper(dims: List[Option[Exp]], ty: Ty): Pointer = dims match {
      case Nil                 =>
        error
      case (None :: _)         =>
        NULL
      case (Some(dim) :: Nil) =>
        val size = eval(dim, deref = true) match {
          case n: Int => n
          case _      => error
        }
        if (size <= 0) ias

        val baseSize = resolved.stackSize
        val allocSize = 4 + baseSize * size
        val addr = mem.malloc(allocSize)
        if (addr != NULL) {
          addr.zero(allocSize)
          addr.word = size
        } else {
          failed = true
          allocated += addr
        }
        addr
      case (Some(dim) :: tail) =>
        val size = eval(dim, deref = true) match {
          case n: Int => n
          case _      => error
        }
        if (size <= 0) ias

        val base =
          ty.cast[ArrayTy].map(_.base) match {
            case Some(base: Ty) => base
            case _              => null
          }
        val baseSize = base.stackSize

        val allocSize = 4 + baseSize * size
        val addr = mem.malloc(allocSize)
        if (addr != NULL) {
          addr.zero(allocSize)
          addr.word = size
          val view = wrapRequire[RArray](addr, ty)

          for (i <- 0 until view.size) {
            view(i) =
              if (tail.isEmpty) 0
              else {
                val sub = helper(tail, base)
                if (sub == null) {
                  allocated += addr
                  return NULL
                }
                sub.addr
              }
          }
          addr
        } else {
          failed = true
          allocated += addr
          addr
        }
    }

    val fakety = dims.foldLeft(resolved) {
      case (acc, _) => ArrayTy(acc)
    }
    val result = helper(dims.toList, fakety)

    if (failed) for (alloc <- allocated) {
      mem.free(alloc)
    }

    result
  }

  protected def newObjExp(n: NewObjExp)(implicit ctx: EvalContext): Pointer = {
    val NewObjExp(name, args, _) = n
    implicit val tbl = n.getTable
    val tentry = tbl[TyEntry](name).getOrElse(error)
    val resolved = resolve(tentry.getTy).getOrElse(error)
    val size = resolved.heapSize
    val complex = mem.malloc(size)

    if (complex == NULL) NULL
    else resolved match {
      case s: StructTy => newstruct(complex, s, args)
      case u: UnionTy  => newunion(complex, u, args)
      case _           => error
    }

    complex
  }

  protected def newunion(dst: Pointer, u: UnionTy, args: ArrayBuffer[(Name, Exp)])
                                   (implicit table: SymbolTable, ctx: EvalContext) = {
    val UnionTy(members) = u
    val addr = dst + 0

    for ((Name(name), exp) <- args) {
      val component = members.find {
        case sem.Component(_, nme) => nme == name
      }.getOrElse(error)
      val csize = component.ty.stackSize

      if (csize == 1) {
        addr.byte = eval(exp, true) match {
          case c: Char => c.toByte
          case true    => 1.toByte
          case false   => 0.toByte
          case _       => error
        }
      } else if (csize == 4) {
        addr.word = eval(exp, true) match {
          case n: Int     => n
          case p: Pointer => p.addr
          case _          => error
        }
      } else error
    }
  }

  protected def newstruct(dst: Pointer, s: StructTy, args: ArrayBuffer[(Name, Exp)])
                                     (implicit table: SymbolTable, ctx: EvalContext) = {
    val StructTy(members) = s

    for ((Name(name), exp) <- args) {
      val component = members.find {
        case sem.Component(_, nme) => nme == name
      }.getOrElse(error)
      val csize = component.ty.stackSize
      val off = s.offsetOf(name)
      val addr = dst + off

      if (csize == 1) {
        addr.byte = eval(exp, true) match {
          case c: Char => c.toByte
          case true    => 1.toByte
          case false   => 0.toByte
          case _       => error
        }
      } else if (csize == 4) {
        addr.word = eval(exp, true) match {
          case n: Int     => n
          case p: Pointer => p.addr
          case _          => error
        }
      } else error
    }
  }

  /** invariant: `ty` has to be a struct or a union ty */
  protected def namedMembers(ty: Ty)(implicit table: SymbolTable): Map[String, RComp] = {
    val offsets = MMap[String, RComp]()
    val ComplexTy(members) = ty
    for (sem.Component(cty, name) <- members) {
      val off = ty match {
        case s: StructTy => s.offsetOf(name)
        case u: UnionTy  => 0
        case _           => error
      }
      val size = resolve(cty).getOrElse(error).stackSize
      offsets += ((name, RComp(off, size)))
    }
    offsets.toMap
  }

  protected def castExp(sym: TypeSymbol, exp: Exp)(implicit ctx: EvalContext): Any = {
    val e = eval(exp, true)
    val int: TypeSymbol = NameTypeSymbol(Name("int"))
    val char: TypeSymbol = NameTypeSymbol(Name("char"))
    (sym, e) match {
      case (`int`, x: Char)  => x.toInt & 0xFF
      case (`int`, x: Int)   => x
      case (`int`, _)        => error

      case (`char`, x: Int)  => x.toChar
      case (`char`, x: Char) => (x.toInt & 0xFF).toChar
      case (`char`, _)       => error

      case _                 => error
    }
  }

  protected def binExp(op: BinOp, left: Exp, right: Exp)(implicit ctx: EvalContext): Any = {
    val l = eval(left, true)
    val r = eval(right, true)

    (op, l, r) match {
      /* arithmetic operations */
      case (Add, a: Int, b: Int)         => a + b
      case (Add, a: Char, b: Char)       => (a + b).toChar
      case (Add, _, _)                   => error

      case (Sub, a: Int, b: Int)         => a - b
      case (Sub, a: Char, b: Char)       => (a - b).toChar
      case (Sub, _, _)                   => error

      case (Div, a: Int, b: Int)         => a / b
      case (Div, a: Char, b: Char)       => (a / b).toChar
      case (Div, _, _)                   => error

      case (Mod, a: Int, b: Int)         => a % b
      case (Mod, a: Char, b: Char)       => (a % b).toChar
      case (Mod, _, _)                   => error

      case (Mul, a: Int, b: Int)         => a * b
      case (Mul, a: Char, b: Char)       => (a * b).toChar
      case (Mul, _, _)                   => error

      /* bitwise operations */
      case (Band, a: Int, b: Int)        => a & b
      case (Band, a: Char, b: Char)      => (a & b).toChar
      case (Band, _, _)                  => error

      case (Bor, a: Int, b: Int)         => a | b
      case (Bor, a: Char, b: Char)       => (a | b).toChar
      case (Bor, _, _)                   => error

      case (Sar, a: Int, b: Int)         => a >>> b
      case (Sar, a: Char, b: Char)       => (a >>> b).toChar
      case (Sar, _, _)                   => error

      case (Slr, a: Int, b: Int)         => a >> b
      case (Slr, a: Char, b: Char)       => (a >> b).toChar
      case (Slr, _, _)                   => error

      case (Sll, a: Int, b: Int)         => a << b
      case (Sll, a: Char, b: Char)       => (a << b).toChar
      case (Sll, _, _)                   => error

      case (Xor, a: Int, b: Int)         => a ^ b
      case (Xor, a: Char, b: Char)       => (a ^ b).toChar
      case (Xor, _, _)                   => error

      /* comparison operations */
      case (Eq, a: Pointer, b: Pointer)  => a == b
      case (Eq, a: Any, b: Any)          => a == b

      case (Ne, a: Pointer, b: Pointer)  => a != b
      case (Ne, a: Any, b: Any)          => a != b

      case (And, a: Boolean, b: Boolean) => a && b
      case (And, _, _)                   => error

      case (Or, a: Boolean, b: Boolean)  => a || b
      case (Or, _, _)                    => error

      case (Ge, a: Int, b: Int)          => a >= b
      case (Ge, a: Char, b: Char)        => a >= b
      case (Ge, _, _)                    => error

      case (Gt, a: Int, b: Int)          => a > b
      case (Gt, a: Char, b: Char)        => a > b
      case (Gt, _, _)                    => error

      case (Le, a: Int, b: Int)          => a <= b
      case (Le, a: Char, b: Char)        => a <= b
      case (Le, _, _)                    => error

      case (Lt, a: Int, b: Int)          => a < b
      case (Lt, a: Char, b: Char)        => a < b
      case (Lt, _, _)                    => error
    }
  }

  protected def unExp(op: UnaryOp, exp: Exp)(implicit ctx: EvalContext): Any = {
    val e = eval(exp, true)
    (op, e) match {
      case (Bnot, x: Int)     => ~x
      case (Bnot, x: Char)    => (~x).toChar
      case (Bnot, _)          => error

      case (Minus, x: Int)    => -x
      case (Minus, x: Char)   => (-x).toChar
      case (Minus, _)         => error

      case (Not, b: Boolean)  => !b
      case (Not, _)           => error

      case (Size, p: Pointer) =>
        implicit val tbl = exp.getTable
        wrapRequire[RArray](p, exp.getTy).size
      case (Size, _)          => error
    }
  }

}
