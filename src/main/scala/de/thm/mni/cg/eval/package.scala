package de.thm.mni.cg

package object eval {

  val defaultHeapSizeInEvaluator = 0x00400000

  private[eval] lazy val error =
    sys.error("illegal state")

  private[eval] lazy val npe =
    sys.error("null pointer exception")

  private[eval] lazy val idxe =
    sys.error("array index out of bounds")

  private[eval] lazy val ias =
    sys.error("illegal array size exception")

  private[eval] lazy val segfault =
    sys.error("segmentation fault")

}
