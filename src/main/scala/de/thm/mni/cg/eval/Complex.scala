package de.thm.mni.cg.eval

sealed trait Complex {
  val ptr: Pointer
}

/** The class `Array` represents an array at runtime of the repl. */
case class RArray(ptr: Pointer, baseSize: Int) extends Complex {
  def size = ptr.word
  def size_=(sz: Int) = ptr.word = sz

  def apply(idx: Int) =
    if (baseSize == 1) ptr(idx).byte
    else if (baseSize == 4) ptr(idx).word
    else error

  def update(idx: Int, value: Int): Unit =
    if (baseSize == 1) ptr(idx).byte = value.toByte
    else if (baseSize == 4) ptr(idx).word = value
    else error

  def ptr(idx: Int): Pointer =
    if (idx >= size || idx < 0) idxe
    else (ptr + 4) + (baseSize * idx)
}

/** Generalization of complex types with named members */
sealed trait NamedMembers extends Complex {
  val components: Map[String, RComp]

  def info(member: String) =
    components.get(member) match {
      case Some(RComp(off, size)) => (off, size)
      case None                   => error
    }

  def apply(member: String) = {
    val (off, size) = info(member)
    val addr = ptr + off
    if (size == 1) addr.byte
    else if (size == 4) addr.word
    else error
  }

  def update(member: String, value: Int): Unit = {
    val (off, size) = info(member)
    val addr = ptr + off
    if (size == 1) addr.byte = value.toByte
    else if (size == 4) addr.word = value
    else error
  }
}

/** The class `Struct` represents a struct at runtime of the repl. */
case class RStruct(ptr: Pointer, components: Map[String, RComp]) extends NamedMembers

/** The class `Union` represents a union at runtime of the repl. */
case class RUnion(ptr: Pointer, components: Map[String, RComp]) extends NamedMembers

/**
 * `RComp` is is a helper class for storing runtime information
 * for structs and unions. The name is an abbreviation for
 * `RuntimeComponent`.
 */
case class RComp(offset: Int, size: Int)
