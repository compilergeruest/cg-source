package de.thm.mni.cg.eval

import scala.collection.mutable.{ Map => MMap }

/**
 * This class represents the runtime state of the
 * program.
 */
case class EvalContext(
  private val parent: Option[EvalContext] = None,
  private val variables: MMap[String, Cell] = MMap()) {

  private def lookup(name: String): Cell =
    if (variables contains name) variables(name)
    else parent match {
      case Some(par) => par lookup name
      case None      => error
    }

  def insert(name: String) =
    variables += name -> new Cell(null)

  def apply(name: String) = lookup(name).elem

  def update(name: String, value: Any): Unit =
    lookup(name).elem = value

}

/**
 * This class represents a memory cell.
 */
private[eval] class Cell(var elem: Any)
