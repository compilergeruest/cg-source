package de.thm.mni.cg.eval

import javax.swing.JFrame
import javax.swing.SwingUtilities
import javax.swing.WindowConstants
import javax.swing.JComponent
import java.awt.Dimension
import java.awt.Graphics
import java.awt.Graphics2D
import java.awt.image.BufferedImage
import java.awt.Color
import java.awt.event.WindowAdapter
import java.awt.event.WindowEvent
import java.awt.image.DataBufferInt

/**
 * Reimplementation of graphics module of the eco32-assembler simulator,
 * which was implemented for Prof. Letschert for "Engineering a compiler" WS15/16.
 * Source: https://git.thm.de/jsld22/eco32-asm-runner/blob/master/src/ecosim/gfx/EcoWindow.scala
 */
class EvaluatorGraphics {

  import System.{ currentTimeMillis => now }

  private val w = 640
  private val h = 480

  private val image = new BufferedImage(w, h, BufferedImage.TYPE_INT_RGB)
  private val g = image.createGraphics()

  private val frame = new JFrame("Evaluator Graphics")
  frame.getContentPane.setPreferredSize(new Dimension(w, h))
  frame.setDefaultCloseOperation(WindowConstants.DO_NOTHING_ON_CLOSE)
  frame.setResizable(false)
  frame.pack()
  frame.add(new JComponent {
    override def paint(e: Graphics): Unit = e.drawImage(image, 0, 0, null)
  })

  private def edt(f: => Unit): Unit =
    SwingUtilities.invokeLater(new Runnable {
      def run(): Unit = f
    })

  private def color(c: Int): Color =
    new Color(
      (c & 0xFF0000) >> 16,
      (c &   0xFF00) >>  8,
      (c &     0xFF) >>  0)

  private def refresh(): Unit =
    edt(frame.repaint())

  def setVisible(visible: Boolean): Unit =
    edt(frame setVisible visible)

  def clearAll(c: Int): Unit = {
    g.setColor(color(c))
    g.fillRect(0, 0, w, h)
    refresh()
  }

  def drawLine(x1: Int, y1: Int, x2: Int, y2: Int, c: Int): Unit = {
    g.setColor(color(c))
    g.drawLine(x1, y1, x2, y2)
    refresh()
  }

  def drawPixel(x: Int, y: Int, c: Int): Unit = {
    g.setColor(color(c))
    g.fillRect(x, y, 1, 1)
    refresh()
  }

  def drawCircle(x: Int, y: Int, r: Int, c: Int): Unit = {
    g.setColor(color(c))
    g.drawOval(x - r / 2, y - r / 2, r, r)
    refresh()
  }

  def killWindow() =
    edt(frame.dispose())

}
