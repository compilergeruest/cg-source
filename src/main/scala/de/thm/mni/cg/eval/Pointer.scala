package de.thm.mni.cg.eval

object Pointer {
  val NULL = Pointer(null, 0)
}

case class Pointer(mem: Memory, addr: Int) {
  def +(off: Int) = Pointer(mem, addr + off)
  def -(off: Int) = Pointer(mem, addr - off)

  def <(addr: Int) = this.addr < addr
  def <(ptr: Pointer) = addr < ptr.addr
  def <=(addr: Int) = this.addr <= addr
  def <=(ptr: Pointer) = addr <= ptr.addr

  def >(addr: Int) = this.addr > addr
  def >(ptr: Pointer) = addr > ptr.addr
  def >=(addr: Int) = this.addr >= addr
  def >=(ptr: Pointer) = addr >= ptr.addr

  def !(addr: Int) = Pointer(mem, addr)

  /** Zeros a memory range: from this pointer `len` bytes */
  def zero(len: Int): Unit = {
    var pt = this
    var ct = len

    while (ct >= 4) {
      pt.word = 0
      pt += 4
      ct -= 4
    }

    while (ct >= 1) {
      pt.byte = 0
      pt += 1
      ct -= 1
    }
  }

  final override def equals(any: Any) = any match {
    case p: Pointer => addr == p.addr
    case _          => false
  }

  final override def toString = s"0x${Integer.toHexString(addr).toUpperCase()}"

  def word = mem.int(addr)
  def word_=(value: Int) = mem.int(addr) = value

  def byte = mem.byte(addr)
  def byte_=(value: Byte) = mem.byte(addr) = value
}
