package de.thm.mni.cg.eval

/**
 * Function returns are simulated with exceptions
 */
private[eval] class EvalReturn(val value: Any) extends Exception
