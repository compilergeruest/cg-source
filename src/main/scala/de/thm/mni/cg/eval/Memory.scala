package de.thm.mni.cg.eval

import de.thm.mni.cg.eval.Memory._
import de.thm.mni.cg.eval.Pointer._
import de.thm.mni.cg.util._

object Memory {
  val magicnr = 0x77A5844C
}

/**
 * This class is a reimplementation of the memory and lib package of the eco32-assembler simulator,
 * which was implemented for Prof. Letschert for "Engineering a compiler" WS15/16.
 *
 * Source: https://git.thm.de/njss90/eco32-asm-runner/blob/master/src/ecosim/lib/Heapmanager.scala
 * Source: https://git.thm.de/njss90/eco32-asm-runner/blob/master/src/ecosim/memory/
 */
class Memory(val size: Int) { self =>

  private val data = new Array[Byte](size)

  /** Allocates `size` bytes in this virtual memory */
  def malloc(size: Int): Pointer = {
    if (size >= self.size)
      return NULL

    var i = Pointer(this, 0)

    def skipNonFree() =
      while (i < self.size && i.word == magicnr) {
        i += 4 // skip magicnr
        val size = i.word
        i += 4 // skip size
        i += size // skip data block
      }

    def skipFree(): Boolean = {
      var j = i
      while (j < self.size && (j - i.addr) < size + 8) {
        if (j.word == magicnr) {
          i = j
          return true
        }
        j += 1
      }
      false
    }

    do {
      skipNonFree()
    } while (skipFree())

    if (i + 8 + size >= self.size) {
      NULL
    } else {
      i.word = magicnr
      (i + 4).word = size

      i + 8
    }
  }

  /** Frees the pointer `ptr` */
  def free(ptr: Pointer): Unit = {
    if (ptr == NULL) return ()
    val adrMagicNo = ptr - 8

    if (adrMagicNo.word != magicnr)
      sys.error("segmentation fault")

    adrMagicNo.word = 0
    (adrMagicNo + 4).word = 0
  }

  def dump(rows: Int): Unit = {
    var addr = 0
    val digitcnt = (Math.log(size) / Math.log(16)).ceil.toInt

    def hexAddr() = {
      val rawHexAddr = addr.toHexString.toUpperCase
      "0x" + ("0" * (digitcnt - rawHexAddr.length)) + rawHexAddr
    }

    for (line <- data.grouped(16) take rows) {
      print(hexAddr())
      print(" | ")
      for (b <- line) {
        val bhex = (b & 0xFF).toHexString.toUpperCase
        if (bhex.length() == 1) {
          print("0")
        }
        print(bhex)
        print(" ")
      }
      println()
      addr += 16
    }
  }

  private def idx(addr: Int): Int =
    if (addr < 0 || addr >= size) segfault
    else addr

  private[eval] object int {
    def apply(addr: Int): Int =
      ((byte(idx(addr + 0)).toInt & 0xFF) << 24) |
      ((byte(idx(addr + 1)).toInt & 0xFF) << 16) |
      ((byte(idx(addr + 2)).toInt & 0xFF) <<  8) |
      ((byte(idx(addr + 3)).toInt & 0xFF) <<  0)

    def update(addr: Int, value: Int): Unit = {
      byte(idx(addr + 0)) = ((value & 0xFF000000) >> 24).toByte
      byte(idx(addr + 1)) = ((value & 0xFF0000  ) >> 16).toByte
      byte(idx(addr + 2)) = ((value & 0xFF00    ) >>  8).toByte
      byte(idx(addr + 3)) = ((value & 0xFF      ) >>  0).toByte
    }
  }

  private[eval] object byte {
    def apply(addr: Int): Byte =
      data(idx(addr))

    def update(addr: Int, value: Byte): Unit =
      data(idx(addr)) = value
  }
}
