package de.thm.mni.cg.parser

import java.io.Reader
import java.util.regex.Pattern

import de.thm.mni.cg.language.Constants._
import de.thm.mni.cg.language.ErrorMessages._
import de.thm.mni.cg.language.NumberSystem
import de.thm.mni.cg.util._

import scala.annotation.switch

final object Tokenizer {
  private val EOF = -1
  private val macroPattern = Pattern.compile("^__[^\\s]+__")
  private val stringPattern = Pattern.compile('^' + stringLit.pattern.toString)
  private val identPattern = Pattern.compile('^' + identifier.pattern.toString)
}

final class Tokenizer(reader: Reader) { self =>
  import Tokenizer._
  private val buffer = new StringBuilder()
  private var lineOfCode = ""
  private var column = -1
  private var line = -1

  // buffer initial line
  nextLine()

  private def withPosition(token: Token): Token = {
    token.pos = currentPosition
    token
  }

  /**
   * Skips any white space at the current stream position
   */
  private def skipWhiteSpace(): Unit = {
    var current = readChar()
    if (current == EOF) return ()
    while (current.toChar.isWhitespace) {
      current = readChar()
      if (current == EOF) return ()
    }
    // first non-whitespace char is a slash
    if (current == '/') {
      current = readChar()
      if (current == EOF) {
        // go back before the '/'
        column -= 1
      } else if (current == '/') {
        // next char is also a slash
        current = readChar()
        if (current == EOF) return ()
        // skip all character until new-line character
        while (current != '\n') {
          current = readChar()
          if (current == EOF) return ()
        }
        // skip all trailing white space
        skipWhiteSpace()
      } else if (current == '*') {
        // ignore all characters until a '*/'
        do {
          current = readChar()
          if (current == EOF) return ()
          while (current != '*') {
            current = readChar()
          if (current == EOF) return ()
          }
          current = readChar()
          if (current == EOF) return ()
        } while (current != '/')
        // skip all trailing white space
        skipWhiteSpace()
      } else {
        // second char is not a slash
        // the first slash either follows: a) a line feed, b) an EOF or c) any other character
        // meaning that it is safe to reverse two times
        column -= 2
      }
    } else {
      // buffer seen non-whitespace char
      column -= 1
    }
  }

  /** buffers a line from the input stream */
  private def nextLine(): Unit = {
    buffer.clear
    column = -1
    line += 1
    var idx = 0
    var current = reader.read()

    while (current != EOF && current != '\n') {
      buffer += current.toChar
      current = reader.read()
      idx += 1
    }
    if (current == '\n') {
      buffer += '\n'
      lineOfCode = buffer.toString
    } else {
      lineOfCode = buffer.toString + '\n'
    }
  }

  /**
   * Returns the current source code position of this tokenizer
   */
  private def currentPosition: Position =
    HasPosition(line + 1, column + 1, lineOfCode)

  /**
   * Reads the next character from the stream
   */
  private def readChar(): Int = {
    column += 1
    if (column >= buffer.length) {
      nextLine()
      column = 0
    }
    if (buffer.isEmpty) EOF
    else buffer.charAt(column)
  }

  private def peekChar(n: Int = 1): Int =
    if (buffer.isEmpty || (column + n) >= buffer.length) EOF
    else buffer.charAt(column + n)

  /**
   * Matches the internal buffer against the pattern and returns
   * the matched string. Does not move the column pointer.
   */
  private def prefixMatch(pattern: Pattern): String = {
    val current = buffer.substring(column)
    val matcher = pattern.matcher(current)
    if (matcher.find()) {
      current.substring(matcher.start(), matcher.end())
    } else {
      null
    }
  }

  private def next(): Token = {
    skipWhiteSpace()
    val next = readChar()
    if (next == EOF) withPosition(EofToken())
    else if (next == '(') withPosition(LParenToken())
    else if (next == ')') withPosition(RParenToken())
    else if (next == '[') withPosition(LBrackToken())
    else if (next == ']') withPosition(RBrackToken())
    else if (next == '{') withPosition(LCurlyToken())
    else if (next == '}') withPosition(RCurlyToken())
    else if (next == '+') withPosition(PlusToken())
    else if (next == '-') withPosition(MinusToken())
    else if (next == '*') withPosition(StarToken())
    else if (next == '/') withPosition(SlashToken())
    else if (next == '%') withPosition(PercentToken())
    else if (next == '~') withPosition(TildeToken())
    else if (next == '^') withPosition(XOrToken())
    else if (next == '#') withPosition(HashToken())
    else if (next == ',') withPosition(CommaToken())
    else if (next == ';') withPosition(SemicolonToken())
    else if (next == '=') {
      if (peekChar() == '=') {
        val result = withPosition(EqualsToken())
        column += 1
        result
      } else {
        withPosition(AssignToken())
      }
    } else if (next == '.') {
      if (peekChar() == '.') {
        val result = withPosition(RangeToken())
        column += 1
        result
      } else {
        withPosition(DotToken())
      }
    } else if (next == '|') {
      if (peekChar() == '|') {
        val result = withPosition(OrToken())
        column += 1
        result
      } else {
        withPosition(BOrToken())
      }
    } else if (next == '&') {
      if (peekChar() == '&') {
        val result = withPosition(AndToken())
        column += 1
        result
      } else {
        withPosition(BAndToken())
      }
    } else if (next == '!') {
      if (peekChar() == '=') {
        val result = withPosition(NotEqualsToken())
        column += 1
        result
      } else {
        withPosition(BangToken())
      }
    } else if (next == '<') {
      val next = peekChar()
      if (next == '<') {
        val result = withPosition(SllToken())
        column += 1
        result
      } else if (next == '=') {
        val result = withPosition(LeToken())
        column += 1
        result
      } else if (next == '-') {
        val result = withPosition(LArrowToken())
        column += 1
        result
      } else {
        withPosition(LtToken())
      }
    } else if (next == '>') {
      val next = peekChar()
      if (next == '>') {
        if (peekChar(2) == '>') {
          val result = withPosition(SarToken())
          column += 2
        result
        } else {
          val result = withPosition(SlrToken())
          column += 1
        result
        }
      } else if (next == '=') {
        val result = withPosition(GeToken())
        column += 1
        result
      } else {
        withPosition(GtToken())
      }
    } else if (next == '"') {
      val string = prefixMatch(stringPattern)
      if (string == null) {
        error(currentPosition, illegalStrLit)
      } else {
        val len = string.length - 1
        val rawlit = string.substring(1, len)
        val rawlen = rawlit.length()
        val literal = new StringBuilder()
        var i = 0

        def simpleEscape(): Unit = {
          i += 1
          literal += escapes(rawlit.charAt(i))
        }

        def has(s: String, i: Int, f: Int => Boolean): Boolean =
          i < s.length() && f(s.charAt(i))

        while (i < rawlen) {
          val c = rawlit.charAt(i)
          if (c == '\\') {
            if (has(rawlit, i + 1, _ == 'u')) {
              if (has(rawlit, i + 2, isHexChar) && has(rawlit, i + 3, isHexChar)) {
                val c2 = rawlit.charAt(i + 2).toInt
                val c3 = rawlit.charAt(i + 3).toInt
                val char = hexDigit(c2) << 4 | hexDigit(c3)
                literal += char.toChar
                i += 3
              } else {
                error(currentPosition, illegalUnicodeStringLit)
              }
            } else {
              simpleEscape()
            }
          } else {
            literal += c
          }
          i += 1
        }
        val result = withPosition(StringToken(literal.toString.intern()))
        column += len
        result
      }
    } else if (next == '\'') {
      if (peekChar() == '\\') {
        val p3 = peekChar(3)
        val p4 = peekChar(4)
        if (peekChar(2) == 'u') {
          if (isHexChar(p3) && isHexChar(p4)) {
            if (peekChar(5) != '\'') {
              column += 5
              error(currentPosition, unclosedCharLit)
            } else {
              val char = hexDigit(p3) << 4 | hexDigit(p4)
              val result = withPosition(CharToken(escapes(char.toChar)))
              column += 5
              result
            }
          } else {
            error(currentPosition, illegalUnicodeCharLit)
          }
        } else if (peekChar(3) != '\'') {
          column += 3
          error(currentPosition, unclosedCharLit)
        } else {
          val char = peekChar(2)
          if (char == 't' || char == 'n' || char == 'r' || char == '\'' || char == '\\') {
            val result = withPosition(CharToken(escapes(char.toChar)))
            column += 3
            result
          } else {
            error(currentPosition, illegalEscape(char.toChar))
          }
        }
      } else if (peekChar(2) == '\'') {
        val result = withPosition(CharToken(peekChar().toChar))
        column += 2
        result
      } else {
        column += 2
        error(currentPosition, unclosedCharLit)
      }
    } else if (next == '_') {
      val prefix = prefixMatch(macroPattern)
      if (prefix == null) {
        error(currentPosition, expectedMacro)
      } else if (macros.contains(prefix)) {
        val result = withPosition(MacroToken(prefix))
        column += prefix.length - 1
        result
      } else {
        error(currentPosition, unknownMacro(prefix))
      }
    } else if (next >= 'a' && next <= 'z' || next >= 'A' && next <= 'Z') {
      val ident = prefixMatch(identPattern)
      val result = ident match {
        case "const"  => withPosition(ConstToken())
        case "type"   => withPosition(TypeToken())
        case "del"    => withPosition(DelToken())
        case "new"    => withPosition(NewToken())
        case "false"  => withPosition(FalseToken())
        case "true"   => withPosition(TrueToken())
        case "null"   => withPosition(NullToken())
        case "for"    => withPosition(ForToken())
        case "while"  => withPosition(WhileToken())
        case "if"     => withPosition(IfToken())
        case "else"   => withPosition(ElseToken())
        case "return" => withPosition(ReturnToken())
        case "struct" => withPosition(StructToken())
        case "union"  => withPosition(UnionToken())
        case _        => withPosition(IdentToken(ident.intern()))
      }
      column += ident.length - 1
      result
    } else if (next >= '0' && next <= '9') {
      val n = peekChar()
      if (n == 'x') {
        var num: BigInt = 0
        var len = 2
        val buf = new StringBuilder("0x")
        var cur = peekChar(len)
        while (isHexChar(cur)) {
          buf += cur.toChar
          num *= 16
          num += hexDigit(cur)
          len += 1
          cur = peekChar(len)
        }
        if (num > (1L << 32) - 1) {
          error(currentPosition, hexLitOutOfRange(buf))
        } else {
          val result = withPosition(NumToken(num.toInt, NumberSystem.Hexadecimal, buf.toString))
          column += len - 1
          result
        }
      } else if (n == 'b') {
        var num: BigInt = 0
        var len = 2
        val buf = new StringBuilder("0b")
        var cur = peekChar(len)
        while (cur >= '0' && cur <= '1') {
          buf += cur.toChar
          num = (num << 1) + (cur - '0')
          len += 1
          cur = peekChar(len)
        }
        if (num > (1L << 32) - 1) {
          error(currentPosition, binLitOutOfRange(buf))
        } else {
          val result = withPosition(NumToken(num.toInt, NumberSystem.Binary, buf.toString))
          column += len - 1
          result
        }
      } else {
        var num: BigInt = next - '0'
        var len = 1
        val buf = new StringBuilder(next.toChar.toString)
        var cur = peekChar(len)
        while (cur >= '0' && cur <= '9') {
          buf += cur.toChar
          num = num * 10 + (cur - '0')
          len += 1
          cur = peekChar(len)
        }
        if (!num.isValidInt) {
          error(currentPosition, decLitOutOfRange(buf))
        } else {
          val result = withPosition(NumToken(num.toInt, NumberSystem.Decimal, buf.toString))
          column += len - 1
          result
        }
      }
    } else error(currentPosition, unexpectedInputChar(next.toChar))
  }

  private def isHexChar(x: Int) =
    x >= '0' && x <= '9' || x >= 'A' && x <= 'F' || x >= 'a' && x <= 'f'

  private def hexDigit(x: Int): Int =
    if (x >= '0' && x <= '9') (x - '0')
    else if (x >= 'A' && x <= 'F') (x - 'A') + 10
    else if (x >= 'a' && x <= 'f') (x - 'a') + 10
    else x

  private def hasNext: Boolean =
    buffer.nonEmpty

  def stream: Stream[Token] =
    if (hasNext) next() #:: stream
    else Stream.empty[Token]

  def iterator: Iterator[Token] =
    new Iterator[Token]() {
      def next() = self.next()
      def hasNext = self.hasNext
    }

  def foreach(f: Token => Unit): Unit =
    while (hasNext) f(next())

}
