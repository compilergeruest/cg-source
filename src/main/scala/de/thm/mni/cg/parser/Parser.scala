package de.thm.mni.cg.parser

import scala.collection.mutable.ArrayBuffer
import scala.reflect.ClassTag

import de.thm.mni.cg.ir._
import de.thm.mni.cg.language.Constants._
import de.thm.mni.cg.language.ErrorMessages._
import de.thm.mni.cg.table.SymbolTable
import de.thm.mni.cg.util._

final class Parser(tokenizer: Tokenizer) {

  val bufferdTokenizer = new SmartTokenizer(tokenizer)
  import bufferdTokenizer._

  val dummies = new DummyTokens {}
  import dummies._

  def program(): Program = {
    val stms = parseUntil(stm(), _.isInstanceOf[EofToken])
    accept[EofToken]
    Program.wrap(
      if (stms.isEmpty) HasPosition(1, 1, "")
      else NoPosition,
    stms: _*)
  }

  /*
   * ============================ Constants and Helpers =============================
   */

  private def accept[T <: Token](implicit ev: ClassTag[T], dm: Dummy[T]): Unit =
    expect[T](ev.runtimeClass.asInstanceOf[Class[T]], next())

  private def expect[T <: Token](expect: Class[T], actual: Token)(implicit dm: Dummy[T]): Unit =
    if (actual.getClass != expect) {
      val expectTxt = tokstr(dm.dummy)
      val actualTxt = tokstr(actual)
      error(actual, expected(expectTxt, actualTxt))
    }

  private def expect[T <: Token](actual: Token)(implicit ev: ClassTag[T], dm: Dummy[T]): Unit =
    expect[T](ev.runtimeClass.asInstanceOf[Class[T]], actual)

  private def tokstr(tok: Token) = tok match {
    case t: EofToken => t.readable
    case t           => s"token '${t.readable}'"
  }

  private def withPos[T <: Pos, X <: Pos](from: T, to: X): X =
    withPos(from.pos, to)

  private def withPos[X <: Pos](pos: Position, to: X): X = {
    to.pos = pos
    to
  }

  /** Repeatedly parses `prod`, while the next token satisfies the predicate `p` */
  private def parseWhile[T](prod: => T, p: Token => Boolean): ArrayBuffer[T] = {
    val q = { t: Token => p(t) && !t.isInstanceOf[EofToken] }
    val buffer = ArrayBuffer[T]()
    while (q(peek())) {
      buffer += prod
    }
    buffer
  }

  /** Repeatedly parses `prod`, until the next token satisfies the predicate `p` */
  private def parseUntil[T](prod: => T, p: Token => Boolean): ArrayBuffer[T] = {
    val q = { t: Token => p(t) || t.isInstanceOf[EofToken] }
    val buffer = ArrayBuffer[T]()
    while (!q(peek())) {
      buffer += prod
    }
    buffer
  }

  private def comma[T](prod: () => T) = {
    accept[CommaToken]
    prod()
  }

  /**
   * `f`, <eof>
   */
  def only[T <: Ast](f: Parser => T): T = {
    val result = f(this)
    accept[EofToken]
    result
  }

  private def name(): Name =
    next() match {
      case t @ IdentToken(name) => withPos(t, Name(name))
      case t                    => error(t, expectedIdent(tokstr(t)))
    }

  private def nameAt(n: Int): Name =
    peek(n) match {
      case t @ IdentToken(name) => withPos(t, Name(name))
      case t                    => error(t, expectedIdent(tokstr(t)))
    }

  /**
   * Expression, { ",", Expression }
   */
  private def expList(): ArrayBuffer[Exp] = {
    val first = exp()
    val rest = parseWhile(comma(exp _), _.isInstanceOf[CommaToken])
    first +: rest
  }

  /**
   * VarDef, { ",", VarDef }
   */
  private def varDefList(): ArrayBuffer[VarDef] = {
    val first = varDef()
    first +: parseWhile(comma(varDef _), _.isInstanceOf[CommaToken])
  }

  /**
   * TypeSymbol, Identifier, ";" ;
   */
  private def typeComponent(): Component = {
    val vdef = varDef()
    if (vdef.isConst) {
      error(vdef, memberFieldNoConst(vdef.getName))
    }
    accept[SemicolonToken]
    withPos(vdef, Component(vdef.getTypeSymbol, vdef.getName))
  }

  /*
   * ================================== Statements ==================================
   */

  /**
   *   BlockStm
   * | Definition
   * | InitStm
   * | AssignStm
   * | IfStm
   * | WhileStm
   * | ForStm
   * | ReturnStm
   * | ExpressionStm
   * | DelStm
   * ;
   */
  private[cg] def stm(): Stm =
    peek() match {
      case t: IfToken     =>
        accept[IfToken]
        withPos(t, ifStm())
      case t: WhileToken  =>
        accept[WhileToken]
        withPos(t, whileStm())
      case t: ForToken    =>
        accept[ForToken]
        withPos(t, forStm())
      case t: ReturnToken =>
        accept[ReturnToken]
        withPos(t, returnStm())
      case t: DelToken    =>
        accept[DelToken]
        withPos(t, delStm())
      case t: LCurlyToken =>
        accept[LCurlyToken]
        withPos(t, blockStm())
      case _ =>
        expStmDefAssign()
    }

  /**
   * Definition =
   *   FunctionDef
   * | TypeDef          x
   * | (VarDef, ";")
   * ;
   *
   * VarDef = [ "const" ], TypeSymbol, Identifier ;
   * TypeDef = "type", Identifier, "=", (TypeSymbol | AnonymousTypeSymbol), ";" ;
   * FunctionDef = ReturnType, Identifier, "(", ParamList, ")", Statement ;
   *
   * InitStm = VarDef, { ",", VarDef }, "=", Expression, ";" ;
   * AssignStm = ExpressionList, "=", Expression, ";" ;
   *
   * ExpressionStm = Expression, ";" ;
   */
  private def expStmDefAssign(): Stm =
    peek() match {
      case t: TypeToken =>
        // TypeDef
        accept[TypeToken]
        withPos(t, tyDef())
      case t: ConstToken =>
        // InitStm
        // const int a, const int b = f();
        val defs = varDefList()
        accept[AssignToken]
        val rhs = exp()
        accept[SemicolonToken]
        withPos(t, InitStm(defs, rhs))
      case t: IdentToken =>

        // FunctionDef, VarDef, InitStm, AssignStm, ExpressionStm
        withPos(t, ambiguousIdent())
      case t =>
        val exp = this.exp()
        peek() match {
          case _: AssignToken =>
            accept[AssignToken]
            val rhs = this.exp()
            accept[SemicolonToken]
            withPos(exp, AssignStm(ArrayBuffer(exp), rhs))
          case _: CommaToken =>
            val exps = exp +: parseWhile(comma(this.exp _), _.isInstanceOf[CommaToken])
            accept[AssignToken]
            val rhs = this.exp()
            accept[SemicolonToken]
            withPos(exp, AssignStm(exps, rhs))
          case _ =>
            accept[SemicolonToken]
            withPos(exp, ExpStm(exp))
        }
  }

  private def ambiguousIdent(): Stm = {
    peek(1) match {
      case _: AssignToken => // assign statement
        val name = this.name()
        val nameVar = withPos(name, NameVar(name))
        accept[AssignToken]
        val exp = this.exp()
        accept[SemicolonToken]
        AssignStm(ArrayBuffer(nameVar), exp)
      case _: CommaToken =>
        // assign, function
        ambiguousIdentComma()
      case _: IdentToken =>
        // init stm, vardef, function
        ambiguousIdentIdent()
      case _: LBrackToken =>
        // assign, init, function, vardef
        ambiguosIdentLBrack()
      case _: LParenToken =>
        // assign statement
        ambiguousIdentLParen()
      case _: DotToken =>
        // assignment, exp stm
        ambiguousIdentDot()
      case _ =>
        val exp = this.exp()
        accept[SemicolonToken]
        withPos(exp, ExpStm(exp))
    }
  }

  private def ambiguousIdentDot(): Stm = {
    val exp = this.exp()
    peek() match {
      case _: SemicolonToken =>
        accept[SemicolonToken]
        withPos(exp, ExpStm(exp))
      case _: CommaToken =>
        val lhs = exp +: parseWhile(comma(this.exp _), _.isInstanceOf[CommaToken])
        accept[AssignToken]
        val rhs = this.exp()
        accept[SemicolonToken]
        withPos(exp, AssignStm(lhs, rhs))
      case t =>
        accept[AssignToken]
        val rhs = this.exp()
        accept[SemicolonToken]
        withPos(exp, AssignStm(ArrayBuffer(exp), rhs))
    }
  }

  private def ambiguousIdentIdent(): Stm = {
    peek(2) match {
      case _: LParenToken =>
        // function
        val ident = name()
        val sym = withPos(ident, NameTypeSymbol(ident))
        functionRest(ArrayBuffer(sym))
      case _ =>
        // vardef, init stm
        val ident = this.name()
        val sym = withPos(ident, NameTypeSymbol(ident))
        val name = this.name()
        val first = withPos(sym, VarDef(false, sym, name))
        peek() match {
          case _: CommaToken =>
            // init stm
            val varDefs = parseWhile(comma(varDef _), _.isInstanceOf[CommaToken])
            accept[AssignToken]
            val rhs = exp()
            accept[SemicolonToken]
            withPos(sym, InitStm(first +: varDefs, rhs))
          case _: AssignToken =>
            // init stm
            accept[AssignToken]
            val rhs = exp()
            accept[SemicolonToken]
            withPos(sym, InitStm(ArrayBuffer(first), rhs))
          case _ =>
            // vardef
            accept[SemicolonToken]
            first
        }
    }
  }

  private def ambiguosIdentLBrack(): Stm = {
    // assign, init, function, vardef
    peek(2) match {
      case a: RBrackToken => // <name>[] ...
        val sym = this.sym()
        // int[][][][][] a
        peek() match {
          case _: CommaToken =>
            // function
            val syms = parseWhile(comma(this.sym _), _.isInstanceOf[CommaToken])
            withPos(sym, functionRest(sym +: syms))
          case _ =>
            // function, vardef, initstm
            (nameAt(0), peek(1)) match {
              case (n, _: LParenToken) =>
                functionRest(ArrayBuffer(sym))
              case _ =>
                val name = this.name()
                val first = withPos(sym, VarDef(false, sym, name))
                peek() match {
                  case _: CommaToken =>
                    // init stm
                    val varDefs = parseWhile(comma(varDef _), _.isInstanceOf[CommaToken])
                    accept[AssignToken]
                    val rhs = exp()
                    accept[SemicolonToken]
                    withPos(sym, InitStm(first +: varDefs, rhs))
                  case _: AssignToken =>
                    // init stm
                    accept[AssignToken]
                    val rhs = exp()
                    accept[SemicolonToken]
                    withPos(sym, InitStm(ArrayBuffer(first), rhs))
                  case _ =>
                    // vardef
                    accept[SemicolonToken]
                    first
                }
            }
        }
      case e => // <name>[<exp>] ...
        val exp = this.exp()
        peek() match {
          case _: CommaToken =>
            // assign
            val exps = parseWhile(comma(this.exp _), _.isInstanceOf[CommaToken])
            accept[AssignToken]
            val rhs = this.exp()
            accept[SemicolonToken]
            withPos(exp, AssignStm(exp +: exps, rhs))
          case _: AssignToken =>
            // assign
            accept[AssignToken]
            val rhs = this.exp()
            accept[SemicolonToken]
            withPos(exp, AssignStm(ArrayBuffer(exp), rhs))
          case _ =>
            accept[SemicolonToken]
            withPos(exp, ExpStm(exp))
        }
    }
  }

  private def ambiguousIdentLParen(): Stm = {
    val first = exp()
    peek() match {
      case _: AssignToken =>
        // <exp> = <exp>;
        accept[AssignToken]
        val rhs = exp()
        accept[SemicolonToken]
        withPos(first, AssignStm(ArrayBuffer(first), rhs))
      case _: CommaToken =>
        // <exp>(, <exp>)* = <exp>;
        val exps = parseWhile(comma(exp _), _.isInstanceOf[CommaToken])
        accept[AssignToken]
        val rhs = exp()
        withPos(first, AssignStm(first +: exps, rhs))
      case _ =>
        // <exp>;
        accept[SemicolonToken]
        withPos(first, ExpStm(first))
    }
  }

  private def ambiguousIdentComma(): Stm = {
    def parseIdentList(): (Token, Pos) = {
      accept[CommaToken]

      (nameAt(0), peek(1), peek(2)) match {
        case (_, a: LBrackToken, b: RBrackToken) => (a, sym())
        case (_, a: LBrackToken, _)              => (a, exp())
        case (_, t: DotToken, _)                 => (t, exp())
        case (_, t: LParenToken, _)              => (t, exp())
        case (ident, t, _)                       => accept[IdentToken]; (t, ident)
      }
    }
    val first = (peek(), this.name())
    val list = first +: parseWhile(parseIdentList(), _.isInstanceOf[CommaToken])

    peek() match {
      case t: IdentToken =>
        // Function
        val syms = list map {
          case (_, n: Name)       => withPos(n, NameTypeSymbol(n))
          case (_, s: TypeSymbol) => s
          case (t, _)             => error(t, expectedTypeButExp)
        }
        withPos(first._2, functionRest(syms))
      case t: AssignToken =>
        // Assignment
        val lhs = list map {
          case (_, n: Name) => withPos(n, NameVar(n))
          case (_, e: Exp)  => e
          case (t, _)       => error(t, expectedExpButType)
        }
        accept[AssignToken]
        val rhs = exp()
        accept[SemicolonToken]
        withPos(lhs.head, AssignStm(lhs, rhs))
      case t =>
        error(t, expectedNameOrExpr(tokstr(t)))
    }
  }

  /**
   *
   *  no position
   */
  private def functionRest(syms: ArrayBuffer[TypeSymbol]): Def = {
    val name = this.name()
    accept[LParenToken]

    val paramsAsVarDef = peek() match {
      case _: RParenToken => ArrayBuffer()
      case _              => varDefList()
    }
    val consts = paramsAsVarDef.find(_.isConst)
    for (const <- consts) error(const, constOnParameter(const.getName))

    accept[RParenToken]
    val body = stm()
    val params = paramsAsVarDef map { case p @ VarDef(_, sym, name) =>
      withPos(p, ParDef(sym, name))
    }

    FunDef(name, syms, params, body, null)
  }

  /**
   *  "(", Expression, ")", Statement, [ "else", Statement ] ;
   *
   *  no position
   */
  private def ifStm(): IfStm = {
    accept[LParenToken]
    val test = exp()
    accept[RParenToken]
    val onTrue = stm()
    val onFalse = peek() match {
      case _: ElseToken =>
        accept[ElseToken]
        Some(stm())
      case _            =>
        None
    }
    IfStm(test, onTrue, onFalse)
  }

  /**
   * "(", Expression, ")", Statement ;
   *
   * no position
   */
  private def whileStm(): WhileStm = {
    accept[LParenToken]
    val test = exp()
    accept[RParenToken]
    val onTrue = stm()
    WhileStm(test, onTrue)
  }

  /**
   * "(", TypeSymbol, Identifier, "<-", ForStmRange ")", Statement ;
   *
   * ForStmRange = Expression, [ ",", Expression ], "..", Expression ;
   *
   * no position
   */
  private def forStm(): ForStm = {
    accept[LParenToken]
    val loopvar = varDef()
    if (loopvar.isConst) {
      error(loopvar, constOnForVar(loopvar.getName))
    }
    accept[LArrowToken]

    // Range
    val start = exp()
    val next = peek() match {
      case _: CommaToken =>
        accept[CommaToken]
        Some(exp())
      case _ =>
        None
    }
    accept[RangeToken]
    val end = exp()
    val range = withPos(start, Range(start, next, end))

    accept[RParenToken]
    val body = stm()
    ForStm(loopvar, range, body, null)
  }

  /**
   * ExpressionList ;
   *
   * no position
   */
  private def returnStm(): ReturnStm = {
    val explist = expList()
    accept[SemicolonToken]
    ReturnStm(explist)
  }

  /**
   * Expression, ";"
   *
   * no position
   */
  private def delStm(): DelStm = {
    val exp = this.exp()
    accept[SemicolonToken]
    DelStm(exp)
  }

  /**
   * { Statement }, "}" ;
   *
   * no position
   */
  private def blockStm(): BlockStm = {
    val buffer = parseUntil(stm(), _.isInstanceOf[RCurlyToken])
    accept[RCurlyToken]
    BlockStm(buffer, null)
  }

  /*
   * ================================= Definitions ==================================
   */

  /**
   * [ "const" ], TypeSymbol, Identifier ;
   *
   * with position
   */
  def varDef(): VarDef = {
    val const = peek() match {
      case t: ConstToken => accept[ConstToken] ; t
      case _             => null
    }
    val sym = this.sym()
    val name = this.name()

    val pos =
      if (const != null) const.pos
      else sym.pos
    withPos(pos, VarDef(const != null, sym, name))
  }

  /**
   * Identifier, "=", (TypeSymbol | AnonymousTypeSymbol), ";" ;
   *
   * no position
   */
  private def tyDef(): TyDef = {
    val name = this.name()
    accept[AssignToken]

    val tySym = peek() match {
      case t: StructToken =>
        accept[StructToken]
        withPos(t, anonComplexSym("struct", StructTypeSymbol))
      case t: UnionToken  =>
        accept[UnionToken]
        withPos(t, anonComplexSym("union", UnionTypeSymbol))
      case t              =>
        withPos(t, sym())
    }
    accept[SemicolonToken]

    TyDef(name, tySym)
  }

  /*
   * ================================= Expressions ==================================
   */

  /**
   * Repeatedly parses `prod` delimited by `delim` while `delim` is defined
   * and combines the parsed elements into a `BinExp`
   */
  private def binExp(prod: => Exp)(delim: PartialFunction[Token, BinOp]): Exp = {
    val zero = prod
    def parseStep() = {
      val del = delim(next())
      (del, prod)
    }
    val steps = parseWhile(parseStep(), delim.isDefinedAt)
    steps.foldLeft(zero) { case (acc, (op, next)) =>
      withPos(acc, BinExp(op, acc, next))
    }
  }

  private[cg] def exp(): Exp =
    binExp(exp1()) {
      case _: OrToken => Or
    }

  private def exp1(): Exp =
    binExp(exp2()) {
      case _: AndToken => And
    }

  private def exp2(): Exp =
    binExp(exp3()) {
      case _: BOrToken => Bor
    }

  private def exp3(): Exp =
    binExp(exp4()) {
      case _: XOrToken => Xor
    }

  private def exp4(): Exp =
    binExp(exp5()) {
      case _: BAndToken => Band
    }

  private def exp5(): Exp =
    binExp(exp6()) {
      case _: EqualsToken    => Eq
      case _: NotEqualsToken => Ne
    }

  private def exp6(): Exp =
    binExp(exp7()) {
      case _: LtToken => Lt
      case _: LeToken => Le
      case _: GtToken => Gt
      case _: GeToken => Ge
    }

  private def exp7(): Exp =
    binExp(exp8()) {
      case _: SllToken => Sll
      case _: SarToken => Sar
      case _: SlrToken => Slr
    }

  private def exp8(): Exp =
    binExp(exp9()) {
      case _: PlusToken  => Add
      case _: MinusToken => Sub
    }

  private def exp9(): Exp =
    binExp(prefixExp()) {
      case _: StarToken    => Mul
      case _: SlashToken   => Div
      case _: PercentToken => Mod
    }

  private def isExpIntro(token: Token): Boolean = token match {
    case ( _: IdentToken   | _: NewToken
         | _: StringToken  | _: CharToken
         | _: NumToken     | _: TrueToken
         | _: FalseToken   | _: NullToken
         | _: LParenToken  | _: MacroToken) => true
    case _ => false
  }

  private def ambiguousPrefixLParenIdent() = {
    peek(2) match {
      case _: RParenToken => // (<ident>)() ...
        peek(3) match {
          case tok if isExpIntro(tok) =>
            accept[LParenToken]
            val sym = this.sym()
            accept[RParenToken]
            val exp = postfixExp()
            if (!sym.isInstanceOf[NameTypeSymbol]) {
              // should not happen: internal error
              val arrStr = sym.code().replaceAll("[\r\n]+", "")
              error(sym, illegalCastToArray(arrStr))
            }
            withPos(sym, CastExp(sym.asInstanceOf[NameTypeSymbol], exp))
          case _ =>
            postfixExp()
        }
      case _: LBrackToken => // (<ident>[...
        peek(3) match {
          case _: RBrackToken =>
            accept[LParenToken]
            val sym = this.sym()
            accept[RParenToken]
            val exp = postfixExp()
            if (!sym.isInstanceOf[NameTypeSymbol]) {
              val arrStr = sym.code().replaceAll("[\r\n]+", "")
              error(sym, illegalCastToArray(arrStr))
            }
            withPos(sym, CastExp(sym.asInstanceOf, exp))
          case _ => postfixExp()
        }
      case _ => postfixExp()
    }
  }

  private def prefixExp(): Exp =
    peek() match {
      case t: LParenToken =>
        peek(1) match {
          case _: IdentToken =>
            ambiguousPrefixLParenIdent()
          case _ => postfixExp()
        }
      case t: HashToken =>
        accept[HashToken]
        val exp = postfixExp()
        withPos(t, UnaryExp(Size, exp))
      case t: BangToken =>
        accept[BangToken]
        val exp = postfixExp()
        withPos(t, UnaryExp(Not, exp))
      case t: TildeToken =>
        accept[TildeToken]
        val exp = postfixExp()
        withPos(t, UnaryExp(Bnot, exp))
      case t: MinusToken =>
        accept[MinusToken]
        val exp = postfixExp()
        withPos(t, UnaryExp(Minus, exp))
      case _ => postfixExp()
    }

  private def postfixExp(): Exp = {
    val exp = value()
    if (exp.isInstanceOf[NameVar]) {
      peek() match {
        case _: LParenToken => // function call
          val name = exp.asInstanceOf[NameVar].getName
          peek(1) match {
            case _: RParenToken => // empty function call
              accept[LParenToken]
              accept[RParenToken]
              accessExp(withPos(name, FunExp(name, ArrayBuffer())))
            case _ =>
              accept[LParenToken]
              val args = expList()
              accept[RParenToken]
              accessExp(withPos(name, FunExp(name, args)))
          }
        case _ => accessExp(exp)
      }
    } else {
      accessExp(exp)
    }
  }

  private def accessExp(base: Exp): Exp = {
    var current = base
    while (peek() match {
      case _: LBrackToken =>
        accept[LBrackToken]
        val index = exp()
        accept[RBrackToken]
        current = withPos(current, ArrayVar(current, index))
        true
      case _: DotToken =>
        accept[DotToken]
        val name = this.name()
        current = withPos(current, AccessVar(current, name))
        true
      case _ => false
    }) ()
    current
  }

  private def value(): Exp =
    peek() match {
      case t @ MacroToken(mac) =>
        val result = macros(mac)(t.pos)
        accept[MacroToken]
        result
      case t: NewToken =>
        // NewObject, NewArray
        newExp()
      case t: LParenToken =>
        // "(", Expression, ")"
        accept[LParenToken]
        val exp = this.exp()
        accept[RParenToken]
        exp
      case _: IdentToken =>
        val name = this.name()
        withPos(name, NameVar(name))
      case _ =>
        literal()
    }

  private def newExp(): Exp = {
    val tok = peek()
    accept[NewToken]

    (nameAt(0), peek(1)) match {
      case (_, _: LBrackToken) => withPos(tok, newArrExp())
      case (_, _: LCurlyToken) => withPos(tok, newObjExp())
      case (_, t)              =>
        val expect = s"${tokstr(LBrackToken())} or ${tokstr(LCurlyToken())}"
        error(t, expected(expect, tokstr(t)))
    }
  }

  /**
   * Identifier, "{", InitList, "}" ;
   *
   * no position
   */
  private def newObjExp(): Exp = {
    val name = this.name()
    accept[LCurlyToken]

    def varDefSemicolon(): (Name, Exp) = {
      val name = this.name()
      accept[AssignToken]
      val exp = this.exp()
      accept[SemicolonToken]
      (name, exp)
    }

    val first = varDefSemicolon()
    val inits = first +: parseUntil(varDefSemicolon(), _.isInstanceOf[RCurlyToken])
    accept[RCurlyToken]

    NewObjExp(name, inits)
  }

  /**
   * Identifier, SpecifiedDimension, { "[", "]" } ;
   *
   * no position
   */
  private def newArrExp(): Exp = {
    val name = this.name()
    val nameTy = withPos(name, NameTypeSymbol(name))

    var seenEmptyDim = false

    def arrayDim(): Option[Exp] = {
      accept[LBrackToken]

      val result = peek() match {
        case t: RBrackToken =>
          seenEmptyDim = true
          accept[RBrackToken]; None
        case t =>
          if (seenEmptyDim) {
            error(t, arrayEmptyDimAfterNonEmpty)
          }
          val exp = this.exp()
          accept[RBrackToken]
          Some(exp)
      }
      result
    }

    val dims = parseWhile(arrayDim(), _.isInstanceOf[LBrackToken])

    NewArrExp(nameTy, dims)
  }

  /**
   *   NullLit
   * | IntLit
   * | CharLit
   * | StringLit
   * | BoolLit
   */
  private def literal(): Exp =
    next() match {
      case t: NullToken           => withPos(t, NullLit())
      case t @ NumToken(n, ns, _) => withPos(t, IntLit(n, ns))
      case t @ CharToken(c)       => withPos(t, CharLit(c))
      case t @ StringToken(s)     => withPos(t, StringLit(s))
      case t: FalseToken          => withPos(t, BoolLit(false))
      case t: TrueToken           => withPos(t, BoolLit(true))
      case t                      => error(t, expectedExpr(tokstr(t)))
    }

  /*
   * =================================== Symbols ====================================
   */

  /**
   * Identifier { "[", "]" }
   *
   * with position
   */
  private def sym(): TypeSymbol = {
    val name = this.name()
    def bracketPair(): Unit = {
      accept[LBrackToken]
      accept[RBrackToken]
    }
    val bracks = parseWhile(bracketPair(), _.isInstanceOf[LBrackToken])
    val zero: TypeSymbol = withPos(name, NameTypeSymbol(name))
    bracks.foldLeft(zero) { case (acc, _) => withPos(acc, ArrayTypeSymbol(acc)) }
  }

  /**
   * "{", ComponentList, "}" ;
   */
  private def anonComplexSym(name: String, f: ArrayBuffer[Component] => ComplexTypeSymbol): ComplexTypeSymbol = {
    accept[LCurlyToken]
    val members = parseUntil(typeComponent(), _.isInstanceOf[RCurlyToken])
    if (members.isEmpty) {
      error(peek(), emptyComplex(name))
    }
    accept[RCurlyToken]
    f(members)
  }

}
