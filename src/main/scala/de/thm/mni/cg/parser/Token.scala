package de.thm.mni.cg.parser

import de.thm.mni.cg.language.NumberSystem
import de.thm.mni.cg.util.Pos

trait Token extends Pos {
  val readable: String
}

case class ConstToken() extends Token { val readable = "const" }
case class TypeToken() extends Token { val readable = "type" }
case class DelToken() extends Token { val readable = "del" }
case class NewToken() extends Token { val readable = "new" }
case class FalseToken() extends Token { val readable = "false" }
case class TrueToken() extends Token { val readable = "true" }
case class NullToken() extends Token { val readable = "null" }
case class ForToken() extends Token { val readable = "for" }
case class WhileToken() extends Token { val readable = "while" }
case class IfToken() extends Token { val readable = "if" }
case class ElseToken() extends Token { val readable = "else" }
case class ReturnToken() extends Token { val readable = "return" }
case class StructToken() extends Token { val readable = "struct" }
case class UnionToken() extends Token { val readable = "union" }

case class StringToken(value: String) extends Token { val readable = "\"" + value + "\"" }
case class CharToken(value: Char) extends Token { val readable = "'" + value + "'" }
case class IdentToken(value: String) extends Token { val readable = value }
case class NumToken(value: Int, system: NumberSystem, show: String) extends Token { val readable = show }
case class MacroToken(value: String) extends Token { val readable = value }

case class MinusToken() extends Token { val readable = "-" }
case class StarToken() extends Token { val readable = "*" }
case class SlashToken() extends Token { val readable = "/" }
case class PlusToken() extends Token { val readable = "+" }
case class PercentToken() extends Token { val readable = "%" }

case class HashToken() extends Token { val readable = "#" }
case class BangToken() extends Token { val readable = "!" }
case class TildeToken() extends Token { val readable = "~" }

case class SllToken() extends Token { val readable = ">>" }
case class SlrToken() extends Token { val readable = "<<" }
case class SarToken() extends Token { val readable = ">>>" }

case class AndToken() extends Token { val readable = "&&" }
case class OrToken() extends Token { val readable = "||" }
case class BAndToken() extends Token { val readable = "&" }
case class BOrToken() extends Token { val readable = "|" }
case class XOrToken() extends Token { val readable = "^" }

case class LtToken() extends Token { val readable = "<" }
case class LeToken() extends Token { val readable = "<=" }
case class GtToken() extends Token { val readable = ">" }
case class GeToken() extends Token { val readable = ">=" }

case class AssignToken() extends Token { val readable = "=" }
case class EqualsToken() extends Token { val readable = "==" }
case class NotEqualsToken() extends Token { val readable = "!=" }

case class LParenToken() extends Token { val readable = "(" }
case class RParenToken() extends Token { val readable = ")" }
case class LCurlyToken() extends Token { val readable = "{" }
case class RCurlyToken() extends Token { val readable = "}" }
case class LBrackToken() extends Token { val readable = "[" }
case class RBrackToken() extends Token { val readable = "]" }

case class CommaToken() extends Token { val readable = "," }
case class DotToken() extends Token { val readable = "." }
case class RangeToken() extends Token { val readable = ".." }
case class SemicolonToken() extends Token { val readable = ";" }
case class LArrowToken() extends Token { val readable = "<-" }

case class EofToken() extends Token { val readable = "end of source" }

/**
 * Collection of dummy-instances of all tokens,
 * which are used in error messages in the parser.
 */
trait DummyTokens {
  private object Dummy {
    def apply[T <: Token](value: T): Dummy[T] =
      new Dummy[T] { val dummy = value }
  }

  trait Dummy[T <: Token] {
    val dummy: T
  }

  implicit val dummyAssign = Dummy(AssignToken())
  implicit val dummyEof = Dummy(EofToken())
  implicit val dummyComma = Dummy(CommaToken())
  implicit val dummySemicolon = Dummy(SemicolonToken())
  implicit val dummyIdent = Dummy(IdentToken("identifier"))
  implicit val dummyLArrow = Dummy(LArrowToken())
  implicit val dummyRange = Dummy(RangeToken())
  implicit val dummyMacro = Dummy(MacroToken("macro"))

  implicit val dummyIf = Dummy(IfToken())
  implicit val dummyElse = Dummy(ElseToken())
  implicit val dummyWhile = Dummy(WhileToken())
  implicit val dummyFor = Dummy(ForToken())
  implicit val dummyReturn = Dummy(ReturnToken())
  implicit val dummyDel = Dummy(DelToken())
  implicit val dummyType = Dummy(TypeToken())
  implicit val dummyConst = Dummy(ConstToken())
  implicit val dummyStruct = Dummy(StructToken())
  implicit val dummyUnion = Dummy(UnionToken())
  implicit val dummyDot = Dummy(DotToken())
  implicit val dummyNew = Dummy(NewToken())

  implicit val dummyHash = Dummy(HashToken())
  implicit val dummyBang = Dummy(BangToken())
  implicit val dummyTilde = Dummy(TildeToken())
  implicit val dummyMinus = Dummy(MinusToken())

  implicit val dummyLCurly = Dummy(LCurlyToken())
  implicit val dummyRCurly = Dummy(RCurlyToken())
  implicit val dummyLParen = Dummy(LParenToken())
  implicit val dummyRParen = Dummy(RParenToken())
  implicit val dummyLBrack = Dummy(LBrackToken())
  implicit val dummyRBrack = Dummy(RBrackToken())
}
