package de.thm.mni.cg.parser

import scala.collection.mutable.ArrayBuffer

class SmartTokenizer(in: Tokenizer) {

  private val iter = in.iterator
  private val queue = new ArrayBuffer[Token]()

  def peek(n: Int = 0): Token = {
    if (queue.size <= n) fill(n)
    queue(n)
  }

  def next(): Token = {
    if (queue.size <= 1) fill(1)
    queue.remove(0)
  }

  private def fill(n: Int): Unit = {
    var i = 0
    while (iter.hasNext && i <= n) {
      queue += iter.next()
      i += 1
    }
    while (i <= n) {
      queue += EofToken()
      i += 1
    }
  }

}
