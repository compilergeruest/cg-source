package de.thm.mni.cg

trait Usage {

  private sealed trait CmdLine extends Product with Serializable

  private case class Heading(text: String) extends CmdLine

  private case class SingleOption(command: String, cmd: Option[String], description: String) extends CmdLine

  private object SingleOption {
    def apply(command: String, description: String) =
      new SingleOption(command, Option.empty[String], description)

    def apply(command: String, cmd: String, description: String) =
      new SingleOption(command, Option(cmd), description)
  }

  private case class Section(seq: Seq[CmdLine]) extends CmdLine

  private def options = Section(Seq(
    Heading("options:"),
    SingleOption("--verbose",     "-v",        "print verbose information of the compilation process"),
    SingleOption("--compile",     "-c",        "[compile-options] compiliation of a file"),
    SingleOption("--eval <file>", "-e <file>", "[eval-options] interprets a file"),
    SingleOption("--repl",        "-r",        "starts the interactive interpreter")
  ))

  private def compileOptions = Section(Seq(
    Heading("compile-options:"),
    SingleOption("--input <file>",     "-i <file>", "input file"),
    SingleOption("--output <file>",    "-o <file>", "output binary file (can be left out)"),
    SingleOption("--assembler <file>", "-s <file>", "output assembler-file (can be left out)")
  ))

  private def evalOptions = Section(Seq(
    Heading("eval-options:"),
    SingleOption("--heapsize <size>", "-h <size>", "size of the heap in bytes"),
    SingleOption("--gfx",             "-g",        "enable the graphics module")
  ))

  private def cmdLineOptions = Seq[CmdLine](
    Heading("usage: programname [options]"),
    options,
    compileOptions,
    evalOptions
  )

  private val commandlengthZero = (Int.MinValue, Int.MinValue)

  private def maxCommandLength(cmdLine: CmdLine): (Int, Int) = cmdLine match {
    case Heading(_) =>
      commandlengthZero

    case SingleOption(command, cmd, _) =>
      val commandLength = command.length
      val cmdLength     = cmd map (_.length) getOrElse 0
      (commandLength, cmdLength)

    case Section(xs) => xs.foldLeft(commandlengthZero) {
      case ((accCommandLength, accCmdLength), cmdline) =>
        val (eleCommandLength, eleCmdLength) = maxCommandLength(cmdline)
        val commandLength = accCommandLength max eleCommandLength
        val cmdLength     = accCmdLength max eleCmdLength
        (commandLength, cmdLength)
    }
  }

  private def formatCmdOptions(cmdLine: CmdLine): String = {
    def spaces(n: Int): String = " " * n

    lazy val (maxCommandLen, maxCmdLen) = maxCommandLength(cmdLine)

    def helper(cmdLine: CmdLine, global: Boolean): String =
      cmdLine match {
        case Heading(text) =>
          text

        case SingleOption(command, cmd, desc) =>
          val spaceLongShort = spaces(maxCommandLen - command.length() + 2)
          val longCommand    = spaces(4) + command + spaceLongShort

          val separatorLongShort = "|  "
          val cmdLength = maxCmdLen - cmd.map(_.length).getOrElse(0) + 2

          def formatShortCommand(cmd: String): String =
            separatorLongShort + cmd + spaces(cmdLength)
          def defaultSpaces =
            spaces(cmdLength + separatorLongShort.length)
          val shortCommand = cmd.map(formatShortCommand) getOrElse defaultSpaces

          longCommand + shortCommand + desc

        case Section(xs) =>
          val separator = if (global) "\n\n" else "\n"
          xs.map(helper(_, global = false)).mkString(separator)
      }

    helper(cmdLine, global = true)
  }

  final def usageCmd(command: String, message: Option[String] = Option.empty[String]): Nothing =
    usage(Option(command), message)

  final def usage(message: Option[String] = Option.empty[String]): Nothing =
    usage(Option.empty[String], message)

  private def usage(command: Option[String], message: Option[String]): Nothing = {
    val subMenu = command.map(x => s"Error while parsing arguments of '$x'-command")

    val additionalMsg = (subMenu, message) match {
      case (Some(sub), Some(msg)) => Option(sub + ": " + msg)
      case (None, Some(msg))      => Option(msg)
      case (Some(sub), None)      => Option(sub + ".")
      case (None, None)           => Option.empty[String]
    }

    val clOptions  = additionalMsg.toSeq.map(Heading(_)) ++ cmdLineOptions
    val fmtOptions = formatCmdOptions(Section(clOptions))
    println(fmtOptions)
    sys.exit(1)
  }

  final def unknownArgument(argName: Option[String]): Option[String] =
    argName.map(x => s"Unknown command line argument '$x'!")

}
