package de.thm.mni.cg.util

import java.io.PrintStream
import java.io.OutputStream
import java.io.PrintWriter

class UnixPrintWriter(out: OutputStream) extends PrintWriter(out) {

  final override def println(): Unit = print('\n')

}
