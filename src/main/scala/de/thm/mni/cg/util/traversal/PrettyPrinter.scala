package de.thm.mni.cg.util.traversal

import de.thm.mni.cg.ir._
import de.thm.mni.cg.util._
import scala.Predef.{ print => _, println => _, _ }
import java.io.PrintStream
import scala.collection.mutable.ArrayBuffer

/** `PrettyPrinter` is a traversal through the AST to print it in a lisp-like style */
final class PrettyPrinter(stream: PrintStream) {

  def show(e: Ast): Unit = e match {
    case e: Exp        => show(e)
    case e: Stm        => show(e)
    case e: TypeSymbol => show(e)
  }

  /*
   * ================================= Expressions ==================================
   */

  private def show(e: Exp): Unit = e match {
    case e: Lit       => show(e)
    case e: Var       => show(e)
    case e: BinExp    => show(e)
    case e: CastExp   => show(e)
    case e: FunExp    => show(e)
    case e: NewArrExp => show(e)
    case e: NewObjExp => show(e)
    case e: UnaryExp  => show(e)
  }

  private def show(e: BinExp): Unit =
    lisp(e)(e.getOp, e.getLeft, e.getRight, e.getTy)

  private def show(e: CastExp): Unit =
    lisp(e)(e.getTypeSymbol, e.getExp, e.getTy)

  private def show(e: FunExp): Unit =
    lisp(e)(e.getName, e.getArgs, e.getTy)

  private def show(e: NewArrExp): Unit =
    lisp(e)(e.getTypeSymbol, e.getDimensions, e.getTy)

  private def show(e: NewObjExp): Unit =
    lisp(e)(e.getName, e.getArgs, e.getTy)

  private def show(e: UnaryExp): Unit =
    lisp(e)(e.getOp, e.getExp, e.getTy)

  /*
   * =================================== Literals ===================================
   */

  private def show(e: Lit): Unit = e match {
    case e: BoolLit   => show(e)
    case e: CharLit   => show(e)
    case e: IntLit    => show(e)
    case e: NullLit   => show(e)
    case e: StringLit => show(e)
  }

  private lazy val reverseEscape = Map(
    '\t' -> "\\t",
    '\n' -> "\\n",
    '\r' -> "\\r",
    '\'' -> "\\'",
    '"' -> "\\\"").withDefault(_.toString())

  private def show(e: CharLit): Unit =
    lisp(e)("'" + reverseEscape(e.getValue) + "'")

  private def show(e: StringLit): Unit = {
    val raw = (e.getValue map reverseEscape).mkString("", "", "")
    lisp(e)(raw, e.getTy)
  }

  private def show(e: IntLit): Unit =
    lisp(e)(e.getValue, e.getTy)

  private def show(e: BoolLit): Unit =
    lisp(e)(e.getValue, e.getTy)

  private def show(e: NullLit): Unit =
    lisp(e)(e.getTy)

  /*
   * ================================== Variables ===================================
   */

  private def show(e: Var): Unit = e match {
    case e: AccessVar => show(e)
    case e: ArrayVar  => show(e)
    case e: NameVar   => show(e)
  }

  private def show(e: AccessVar): Unit =
    lisp(e)(e.getBase, e.getMember, e.getTy)

  private def show(e: ArrayVar): Unit =
    lisp(e)(e.getBase, e.getIndex, e.getTy)

  private def show(e: NameVar): Unit =
    lisp(e)(e.getName, e.getTy)

  /*
   * ================================== Statements ==================================
   */

  private def show(e: Stm): Unit = e match {
    case e: Def       => show(e)
    case e: AssignStm => show(e)
    case e: BlockStm  => show(e)
    case e: DelStm    => show(e)
    case e: ExpStm    => show(e)
    case e: ForStm    => show(e)
    case e: IfStm     => show(e)
    case e: InitStm   => show(e)
    case e: ReturnStm => show(e)
    case e: WhileStm  => show(e)
  }

  private def show(e: AssignStm): Unit =
    lisp(e)(e.getLhs, e.getRhs)

  private def show(e: BlockStm): Unit =
    lisp(e)(e.getBody)

  private def show(e: DelStm): Unit =
    lisp(e)(e.getExp)

  private def show(e: ExpStm): Unit =
    lisp(e)(e.getExp)

  private def show(e: ForStm): Unit =
    lisp(e)(e.getVarDef, e.getRange, e.getBody)

  private def show(e: IfStm): Unit =
    lisp(e)(e.getCheck, e.getBody, e.getElseBody)

  private def show(e: InitStm): Unit =
    lisp(e)(e.getDefs, e.getExp)

  private def show(e: ReturnStm): Unit =
    lisp(e)(e.getExps)

  private def show(e: WhileStm): Unit =
    lisp(e)(e.getCheck, e.getBody)

  /*
   * ================================= Definitions ==================================
   */

  private def show(e: Def): Unit = e match {
    case e: FieldDef => show(e)
    case e: FunDef   => show(e)
    case e: TyDef    => show(e)
  }

  private def show(e: FieldDef): Unit = e match {
    case e: ParDef => show(e)
    case e: VarDef => show(e)
  }

  private def show(e: FunDef): Unit =
    lisp(e)(e.getName, e.getOut, e.getParams, e.getBody)

  private def show(e: TyDef): Unit =
    lisp(e)(e.getName, e.getTypeSymbol)

  private def show(e: ParDef): Unit =
    lisp(e)(e.getTypeSymbol, e.getName)

  private def show(e: VarDef): Unit =
    lisp(e)(e.getTypeSymbol, e.getName, e.isConst)

  /*
   * =================================== Symbols ====================================
   */

  private def show(e: TypeSymbol): Unit = e match {
    case e: ComplexTypeSymbol => show(e)
    case e: ArrayTypeSymbol   => show(e)
    case e: NameTypeSymbol    => show(e)
  }

  private def show(e: ComplexTypeSymbol): Unit = e match {
    case e: StructTypeSymbol => show(e)
    case e: UnionTypeSymbol  => show(e)
  }

  private def show(e: ArrayTypeSymbol): Unit =
    lisp(e)(e.getBase)

  private def show(e: NameTypeSymbol): Unit =
    lisp(e)(e.getName)

  private def show(e: StructTypeSymbol): Unit =
    lisp(e)(e.getMembers)

  private def show(e: UnionTypeSymbol): Unit =
    lisp(e)(e.getMembers)

  /*
   * =================================== Helpers ====================================
   */

  private val indentCache = scala.collection.mutable.HashMap[Int, String]()
  private def showIndent(indent: Int): Unit =
    stream.print(indentCache.getOrElseUpdate(indent, "  " * indent))

  private var indent = 0

  private def lisp(instance: AnyRef, open: String = "(",
                   close: String = ")", break: Boolean = false)(elements: Any*): Unit = {
    val name = instance.getClass.getSimpleName
    lispNamed(name, open, close, break)(elements: _*)
  }

  private def lispNamed(name: String, open: String = "(",
                   close: String = ")", break: Boolean = false)(elements: Any*): Unit = {
    stream.print(name)
    stream.print(open)
    lazy val onlyOne = containsOnlyOne(elements)

    if (!onlyOne) indent += 1

    def handleElement: Any => Unit = {
      case null               => stream.print("null")
      case a: Ast             => show(a)
      case n: Name            => lispNamed("Name")(n.name)
      case c: Component       => lispNamed("Component")(c.symbol, c.name)
      case r: Range           => lispNamed("Range")(r.start, r.next, r.end)
      case o: BinOp           => lispNamed("BinOp")(o.token)
      case o: UnaryOp         => lispNamed("UnaryOp")(o.token)
      case xs: ArrayBuffer[_] => lispNamed("List", "(", ")")(xs: _*)
      case Some(any)          => lispNamed("Some")(any)
      case None               => lispNamed("None")()
      case (x, y)             => lispNamed("")(x); stream.print(" -> "); lispNamed("")(y)
      case a                  => stream.print(a.toString)
    }

    if (elements.nonEmpty) {
      if (onlyOne) {
        handleElement(elements.head)
      } else {
        val iter = elements.iterator
        stream.println()
        showIndent(indent)
        handleElement(iter.next())
        while (iter.hasNext) {
          stream.println(",")
          showIndent(indent)
          handleElement(iter.next())
        }
      }
    }

    if (!onlyOne) indent -= 1

    if (break) {
      stream.println()
      showIndent(indent)
    }
    stream.print(close)
  }

}
