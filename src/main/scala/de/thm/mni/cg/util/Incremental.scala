package de.thm.mni.cg.util

sealed trait Incremental[T] {
  def apply(): T
}

object Incremental {
  def apply[T](f: Int => T): Incremental[T] =
    new IncrementalImpl[T](f)

  def curried[I, T](f: Int => I => T): Incremental[I => T] =
    new IncrementalImpl[I => T](f)

  private class IncrementalImpl[T](f: Int => T)
      extends Incremental[T] {

    private var cnt = -1
    def apply(): T = { cnt += 1; f(cnt) }
  }

}
