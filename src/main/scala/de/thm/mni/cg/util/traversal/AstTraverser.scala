package de.thm.mni.cg.util.traversal

import de.thm.mni.cg.ir._
import de.thm.mni.cg.table.SymbolTable
import de.thm.mni.cg.util.traversal.TraverseOrder._

/**
 * `AstTraverser` is a class which makes traversals of the AST very easy.
 * You can think of it as a kind of visitor, which handles the symboltable for you.
 * It is possible to specify a traverse order.
 */
class AstTraverser(order: TraverseOrder) {

  def traverse(e: Ast)(implicit table: SymbolTable): Unit = e match {
    case e: Exp        => _traverse(e)
    case e: Stm        => _traverse(e)
    case e: TypeSymbol => _traverse(e)
  }

/*
 * ================================= Expressions ==================================
 */

  private def _traverse(e: Exp)(implicit table: SymbolTable): Unit = e match {
    case e: Lit       => _traverse(e)
    case e: Var       => _traverse(e)
    case e: BinExp    => _traverse(e)
    case e: CastExp   => _traverse(e)
    case e: FunExp    => _traverse(e)
    case e: NewArrExp => _traverse(e)
    case e: NewObjExp => _traverse(e)
    case e: UnaryExp  => _traverse(e)
  }

  private def _traverse(e: BinExp)(implicit table: SymbolTable): Unit = order match {
    case PreOrder  => traverse(e); _traverse(e.getLeft); _traverse(e.getRight)
    case InOrder   => _traverse(e.getLeft); traverse(e); _traverse(e.getRight)
    case PostOrder => _traverse(e.getLeft); _traverse(e.getRight); traverse(e)
  }
  protected def traverse(e: BinExp)(implicit table: SymbolTable): Unit = ()

  private def _traverse(e: CastExp)(implicit table: SymbolTable): Unit = order match {
    case PreOrder => traverse(e); _traverse(e.getTypeSymbol); _traverse(e.getExp)
    case InOrder => _traverse(e.getTypeSymbol); traverse(e); _traverse(e.getExp)
    case PostOrder => _traverse(e.getTypeSymbol); _traverse(e.getExp); traverse(e)
  }
  protected def traverse(e: CastExp)(implicit table: SymbolTable): Unit = ()

  private def _traverse(e: FunExp)(implicit table: SymbolTable): Unit = order match {
    case PreOrder => traverse(e); e.getArgs.foreach(_traverse)
    case PostOrder | InOrder => e.getArgs.foreach(_traverse); traverse(e)
  }
  protected def traverse(e: FunExp)(implicit table: SymbolTable): Unit = ()

  private def _traverse(e: NewObjExp)(implicit table: SymbolTable): Unit = order match {
    case PreOrder => traverse(e); e.getArgs.foreach { case (name, exp) => _traverse(exp) }
    case PostOrder | InOrder => e.getArgs.foreach { case (name, exp) => _traverse(exp) }; traverse(e)
  }
  protected def traverse(e: NewObjExp)(implicit table: SymbolTable): Unit = ()

  private def _traverse(e: NewArrExp)(implicit table: SymbolTable): Unit = order match {
    case PreOrder => traverse(e); e.getDimensions.foreach(_ foreach _traverse)
    case PostOrder | InOrder => e.getDimensions.foreach(_ foreach _traverse); traverse(e)
  }
  protected def traverse(e: NewArrExp)(implicit table: SymbolTable): Unit = ()

  private def _traverse(e: UnaryExp)(implicit table: SymbolTable): Unit = order match {
    case PreOrder => traverse(e); _traverse(e.getExp)
    case PostOrder | InOrder => _traverse(e.getExp); traverse(e)
  }
  protected def traverse(e: UnaryExp)(implicit table: SymbolTable): Unit = ()

/*
 * =================================== Literals ===================================
 */

  private def _traverse(e: Lit)(implicit table: SymbolTable): Unit = e match {
    case e: BoolLit   => traverse(e)
    case e: CharLit   => traverse(e)
    case e: IntLit    => traverse(e)
    case e: NullLit   => traverse(e)
    case e: StringLit => traverse(e)
  }

  protected def traverse(e: BoolLit)(implicit table: SymbolTable): Unit = ()

  protected def traverse(e: CharLit)(implicit table: SymbolTable): Unit = ()

  protected def traverse(e: IntLit)(implicit table: SymbolTable): Unit = ()

  protected def traverse(e: NullLit)(implicit table: SymbolTable): Unit = ()

  protected def traverse(e: StringLit)(implicit table: SymbolTable): Unit = ()


/*
 * ================================== Variables ===================================
 */

  private def _traverse(e: Var)(implicit table: SymbolTable): Unit = e match {
    case e: AccessVar => _traverse(e)
    case e: ArrayVar  => _traverse(e)
    case e: NameVar   => traverse(e)
  }

  private def _traverse(e: AccessVar)(implicit table: SymbolTable): Unit = order match {
    case PreOrder => traverse(e); _traverse(e.getBase)
    case PostOrder | InOrder => _traverse(e.getBase); traverse(e)
  }
  protected def traverse(e: AccessVar)(implicit table: SymbolTable): Unit = ()

  private def _traverse(e: ArrayVar)(implicit table: SymbolTable): Unit = order match {
    case PreOrder => traverse(e); _traverse(e.getBase); _traverse(e.getIndex)
    case InOrder => _traverse(e.getBase); traverse(e); _traverse(e.getIndex)
    case PostOrder => _traverse(e.getBase); _traverse(e.getIndex); traverse(e)
  }
  protected def traverse(e: ArrayVar)(implicit table: SymbolTable): Unit = ()

  protected def traverse(e: NameVar)(implicit table: SymbolTable): Unit = ()

/*
 * ================================== Statements ==================================
 */

  private def _traverse(e: Stm)(implicit table: SymbolTable): Unit = e match {
    case e: Def       => _traverse(e)
    case e: AssignStm => _traverse(e)
    case e: BlockStm  => _traverse(e)
    case e: DelStm    => _traverse(e)
    case e: ExpStm    => _traverse(e)
    case e: ForStm    => _traverse(e)
    case e: IfStm     => _traverse(e)
    case e: InitStm   => _traverse(e)
    case e: ReturnStm => _traverse(e)
    case e: WhileStm  => _traverse(e)
  }

  private def _traverse(e: AssignStm)(implicit table: SymbolTable): Unit = order match {
    case PreOrder => traverse(e); e.getLhs.foreach(_traverse); _traverse(e.getRhs)
    case InOrder => e.getLhs.foreach(_traverse); traverse(e); _traverse(e.getRhs)
    case PostOrder => e.getLhs.foreach(_traverse); _traverse(e.getRhs); traverse(e)
  }
  protected def traverse(e: AssignStm)(implicit table: SymbolTable): Unit = ()

  private def _traverse(e: BlockStm)(implicit table: SymbolTable): Unit = order match {
    case PreOrder => traverse(e); e.getBody.foreach(_traverse(_)(e.getLocalTable))
    case PostOrder | InOrder => e.getBody.foreach(_traverse(_)(e.getLocalTable)); traverse(e)
  }
  protected def traverse(e: BlockStm)(implicit table: SymbolTable): Unit = ()

  private def _traverse(e: DelStm)(implicit table: SymbolTable): Unit = order match {
    case PreOrder => traverse(e); _traverse(e.getExp)
    case PostOrder | InOrder => _traverse(e.getExp); traverse(e)
  }
  protected def traverse(e: DelStm)(implicit table: SymbolTable): Unit = ()

  private def _traverse(e: ExpStm)(implicit table: SymbolTable): Unit = order match {
    case PreOrder => traverse(e); _traverse(e.getExp)
    case PostOrder | InOrder => _traverse(e.getExp); traverse(e)
  }
  protected def traverse(e: ExpStm)(implicit table: SymbolTable): Unit = ()

  private def _traverse(e: ForStm)(implicit table: SymbolTable): Unit = order match {
    case PreOrder => traverse(e); _traverse(e.getVarDef)(e.getLocalTable); _traverse(e.getBody)(e.getLocalTable)
    case InOrder => _traverse(e.getVarDef)(e.getLocalTable); traverse(e); _traverse(e.getBody)(e.getLocalTable)
    case PostOrder => _traverse(e.getVarDef)(e.getLocalTable); _traverse(e.getBody)(e.getLocalTable); traverse(e)
  }
  protected def traverse(e: ForStm)(implicit table: SymbolTable): Unit = ()

  private def _traverse(e: IfStm)(implicit table: SymbolTable): Unit = order match {
    case PreOrder => traverse(e); _traverse(e.getCheck); _traverse(e.getBody); e.getElseBody.foreach(_traverse)
    case InOrder => _traverse(e.getCheck); traverse(e); _traverse(e.getBody); e.getElseBody.foreach(_traverse)
    case PostOrder => _traverse(e.getCheck); _traverse(e.getBody); e.getElseBody.foreach(_traverse); traverse(e)
  }
  protected def traverse(e: IfStm)(implicit table: SymbolTable): Unit = ()

  private def _traverse(e: InitStm)(implicit table: SymbolTable): Unit = order match {
    case PreOrder => traverse(e); e.getDefs.foreach(_traverse); _traverse(e.getExp)
    case InOrder => e.getDefs.foreach(_traverse); traverse(e); _traverse(e.getExp)
    case PostOrder => e.getDefs.foreach(_traverse); _traverse(e.getExp); traverse(e)
  }
  protected def traverse(e: InitStm)(implicit table: SymbolTable): Unit = ()

  private def _traverse(e: ReturnStm)(implicit table: SymbolTable): Unit = order match {
    case PreOrder => traverse(e); e.getExps.foreach(_traverse)
    case PostOrder | InOrder => e.getExps.foreach(_traverse); traverse(e)
  }
  protected def traverse(e: ReturnStm)(implicit table: SymbolTable): Unit = ()

  private def _traverse(e: WhileStm)(implicit table: SymbolTable): Unit = order match {
    case PreOrder => traverse(e); _traverse(e.getCheck); _traverse(e.getBody)
    case InOrder => _traverse(e.getCheck); traverse(e); _traverse(e.getBody)
    case PostOrder => _traverse(e.getCheck); _traverse(e.getBody); traverse(e)
  }
  protected def traverse(e: WhileStm)(implicit table: SymbolTable): Unit = ()

/*
 * ================================= Definitions ==================================
 */

  private def _traverse(e: Def)(implicit table: SymbolTable): Unit = e match {
    case e: FieldDef => _traverse(e)
    case e: FunDef   => _traverse(e)
    case e: TyDef    => _traverse(e)
  }

  private def _traverse(e: FunDef)(implicit table: SymbolTable): Unit = order match {
    // Pass local table only to ingoing arguments and the function getBody
    case PreOrder => traverse(e); e.getOut.foreach(_traverse); e.getParams.foreach(_traverse(_)(e.getLocalTable)); _traverse(e.getBody)(e.getLocalTable)
    case InOrder => e.getOut.foreach(_traverse); traverse(e); e.getParams.foreach(_traverse(_)(e.getLocalTable)); _traverse(e.getBody)(e.getLocalTable)
    case PostOrder => e.getOut.foreach(_traverse); e.getParams.foreach(_traverse(_)(e.getLocalTable)); _traverse(e.getBody)(e.getLocalTable); traverse(e)
  }
  protected def traverse(e: FunDef)(implicit table: SymbolTable): Unit = ()

  private def _traverse(e: TyDef)(implicit table: SymbolTable): Unit = order match {
    case PreOrder => traverse(e); _traverse(e.getTypeSymbol)
    case PostOrder | InOrder => _traverse(e.getTypeSymbol); traverse(e)
  }
  protected def traverse(e: TyDef)(implicit table: SymbolTable): Unit = ()

  private def _traverse(e: FieldDef)(implicit table: SymbolTable): Unit = e match {
    case e: ParDef => _traverse(e)
    case e: VarDef => _traverse(e)
  }

  private def _traverse(e: ParDef)(implicit table: SymbolTable): Unit = order match {
    case PreOrder => traverse(e); _traverse(e.getTypeSymbol)
    case PostOrder | InOrder => _traverse(e.getTypeSymbol); traverse(e)
  }
  protected def traverse(e: ParDef)(implicit table: SymbolTable): Unit = ()

  private def _traverse(e: VarDef)(implicit table: SymbolTable): Unit = order match {
    case PreOrder => traverse(e); _traverse(e.getTypeSymbol)
    case PostOrder | InOrder => _traverse(e.getTypeSymbol); traverse(e)
  }
  protected def traverse(e: VarDef)(implicit table: SymbolTable): Unit = ()

/*
 * =================================== Symbols ====================================
 */

  private def _traverse(e: TypeSymbol)(implicit table: SymbolTable): Unit = e match {
    case e: ComplexTypeSymbol => _traverse(e)
    case e: ArrayTypeSymbol   => _traverse(e)
    case e: NameTypeSymbol    => traverse(e)
  }

  private def _traverse(e: ArrayTypeSymbol)(implicit table: SymbolTable): Unit = order match {
    case PreOrder            => traverse(e); _traverse(e.getBase)
    case PostOrder | InOrder => _traverse(e.getBase); traverse(e)
  }
  protected def traverse(e: ArrayTypeSymbol)(implicit table: SymbolTable): Unit = ()

  protected def traverse(e: NameTypeSymbol)(implicit table: SymbolTable): Unit = ()

  private def _traverse(e: ComplexTypeSymbol)(implicit table: SymbolTable): Unit = e match {
    case e: StructTypeSymbol => _traverse(e)
    case e: UnionTypeSymbol  => _traverse(e)
  }

  protected def _traverse(e: StructTypeSymbol)(implicit table: SymbolTable): Unit = order match {
    case PreOrder => traverse(e); e.getMembers.foreach { case Component(symbol, _) => _traverse(symbol) }
    case PostOrder | InOrder => e.getMembers.foreach { case Component(symbol, _) => _traverse(symbol) }; traverse(e)
  }
  protected def traverse(e: StructTypeSymbol)(implicit table: SymbolTable): Unit = ()

  protected def _traverse(e: UnionTypeSymbol)(implicit table: SymbolTable): Unit = order match {
    case PreOrder => traverse(e); e.getMembers.foreach { case Component(symbol, _) => _traverse(symbol) }
    case PostOrder | InOrder => e.getMembers.foreach { case Component(symbol, _) => _traverse(symbol) }; traverse(e)
  }
  protected def traverse(e: UnionTypeSymbol)(implicit table: SymbolTable): Unit = ()

}
