package de.thm.mni.cg.util

object Position {

  implicit val PositionOrdering =
    Ordering.fromLessThan[Position] { case (a, b) =>
      a.line < b.line ||
      a.line == b.line && a.column < b.column
    }

}

sealed trait Position {
  val line: Int
  val column: Int
  def longString: String
}

case class HasPosition(line: Int, column: Int, private val lineString: String) extends Position {

  def longString: String = {
    lineString + '\n' + lineString.substring(0, column - 1).replaceAll("[^ \t\n\r\b\f]", " ") + '^'
  }

}

case object NoPosition extends Position {
  val line: Int = -1
  val column: Int = -1
  def longString: String = "<no position>"
}
