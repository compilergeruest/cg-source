package de.thm.mni.cg.util.cache

import de.thm.mni.cg.ir.TypeSymbol
import de.thm.mni.cg.semantic.Ty

/** Mixin `Caching`, which allows caching some predefined conversion */
trait Caching {
  implicit val symbolToTy = Caching.globalSymbolToTy
}

/** Object `Caching`, holds global instances of caches */
object Caching {
  private val globalSymbolToTy = Cache[TypeSymbol, Ty](Ty.fromSymbolUncached)
}
