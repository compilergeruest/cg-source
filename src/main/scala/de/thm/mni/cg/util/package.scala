package de.thm.mni.cg

import java.io._
import java.nio.charset.StandardCharsets
import java.text.SimpleDateFormat
import java.util.Date

import scala.annotation.tailrec
import scala.reflect.ClassTag
import scala.util._

import de.thm.mni.cg.ir.Ast
import de.thm.mni.cg.table.SymbolTable
import java.util.concurrent.TimeUnit

package object util extends InputSourceImplicits {

  /**
   * `Fail` lets you make instances of scalas Failure more easily.
   *
   * @see scala.util.Failure
   */
  object Fail {
    def apply[A](msg: String) =
      Failure[A](new RuntimeException(msg))

    def unapply[A](fail: Failure[A]): Option[String] =
      Option(fail.exception.getMessage)
  }

  /** Creates a error message at a certain position */
  def errorMsg(pos: Position, s: String): String =
    s"Error in line ${pos.line} at column ${pos.column}:\n$s\n${pos.longString}\n"

  /**
   * Creates a error message at a certain position,
   * the position is a member of the given object
   */
  def errorMsg[A <: Pos](ast: A, s: String): String =
    errorMsg(ast.pos, s)

  def error(pos: Position, s: String): Nothing =
    throw new RuntimeException(errorMsg(pos, s))

  def error[A <: Pos](ast: A, s: String): Nothing =
    error(ast.pos, s)

  def ignoreTableInTraversal(consumer: PartialFunction[Ast, Unit]): PartialFunction[(Ast, SymbolTable), Unit] =
    { case (x: Ast, _) if consumer isDefinedAt x => consumer(x) }

  def withEffect[T](t: T)(f: T => Unit): T = { f(t); t }

  /**
   * Tries to resolve a relative path, specified by the given path parts, to the jar
   * @param parts The path parts
   * @return Either the file path relative to the jar or the error which occurred
   */
  def relative(parts: String*): Try[File] = Try {
    val url = getClass.getProtectionDomain().getCodeSource().getLocation()
    val zero = new File(url.toURI).getParentFile
    val res = parts.foldLeft(zero)(new File(_, _))
    if (!res.exists() && !res.canWrite()) {
      throw new FileNotFoundException(res.getAbsolutePath)
    }
    res
  }

  /** Utility function to load files from the compiler jar-file. */
  def asset(resource: String): Try[InputStream] =
    Option(this.getClass.getResourceAsStream("/" + resource)) match {
      case Some(stream) => Success(stream)
      case None         => Fail(s"Resource '$resource' doesn't exist!")
    }

  /**
   * Returns 'None', if one of the Options in 'lst' is 'None',
   * otherwise the elements are collected in a 'Some'.
   */
  def sequence[T](lst: List[Option[T]]): Option[List[T]] =
    lst.foldRight(Option(List.empty[T])) {
      case (ele, acc) => acc.flatMap(lst => ele.map(_ :: lst))
    }

  /**
   * Returns all elements, which occur in the list 'xs' multiple times.
   *
   * Examples:
   * {{{
   * duplicates(List()) == List()
   * duplicates(List(1, 2, 3)) == List()
   * duplicates(List(1, 1, 2)) == List(1)
   * duplicates(List(1, 1, 1)) == List(1, 1)
   * duplicates(List(1, 1, 1, 2, 2)) == List(1, 1, 2)
   * }}}
   */
  def duplicates[A](xs: List[A]): List[A] = {
    @tailrec
    def helper(xs: List[A], acc: List[A], seen: Set[A]): List[A] = xs match {
      case hd :: tl =>
        if (seen contains hd) helper(tl, hd :: acc, seen)
        else helper(tl, acc, seen + hd)
      case _ => acc
    }

    helper(xs, Nil, Set()).reverse
  }

  /**
   * Returns a readable string representation of the list 'xs' and a
   * boolean indicating that 'xs' contains multiple elements
   *
   * Examples:
   * {{{
   * enumerate(List()) == ("", true)
   * enumerate(List(1)) == ("1", false)
   * enumerate(List(1, 2)) == ("1 and 2", true)
   * enumerate(List(1, 2, 3)) == ("1, 2 and 3", true)
   * }}}
   */
  def enumerate[A](xs: List[A]): (String, Boolean) = xs match {
    case Nil          => ("", true)
    case hd :: Nil    => (hd.toString, false)
    case init :+ last => (init.mkString(", ") + " and " + last, true)
  }

  /**
   * Returns 'true', if the list only contains one element, 'false' otherwise.
   * This function doesn't use the O(n) size method on lists.
   */
  def containsOnlyOne[A](xs: List[A]): Boolean = xs match {
    case _ :: Nil => true
    case _        => false
  }

  /**
   * Returns 'true', if the traversable only contains one element, 'false' otherwise.
   * This function doesn't use the O(n) size method on traversables.
   */
  def containsOnlyOne[A](xs: Traversable[A]): Boolean =
    if (xs.isInstanceOf[IndexedSeq[A]]) {
      xs.size == 1
    } else {
      xs.headOption.nonEmpty && xs.tail.isEmpty
    }

  /** Returns the current date formatted as `pattern`. */
  private def formatterForNow(pattern: String) =
    new SimpleDateFormat(pattern).format(new Date())

  /** Returns the current date in the format dd.MM.yyyy as a string. */
  def getDateString: String =
    formatterForNow("dd.MM.yyyy")

  /** Returns the current time in the format HH:mm:ss as a string. */
  def getTimeString: String =
    formatterForNow("HH:mm:ss")

  /**
   * Helpers, that allow to do a 'type-safe-cast' and
   * 'type-safe-instanceof checks'
   */
  implicit class AnyRefSafeTypes(any: AnyRef) {
    def ifInstanceOf[T](f: T => Unit)(implicit ct: ClassTag[T]): Unit =
      cast[T] foreach f

    def cast[T](implicit ct: ClassTag[T]): Option[T] =
      Option(any) collect { case ct(x) => x }
  }

  /** escapes the string `s` by escaping each char */
  def escape(s: String): String =
    s.map(escape(_, false)).mkString

  /** escapes char `c`, if `charlit` is true '\'' will be escaped */
  def escape(c: Char, charlit: Boolean): String = c match {
    case '\n'              => "\\n"
    case '\r'              => "\\r"
    case '\t'              => "\\t"
    case '"'               => "\\\""
    case '\\'              => "\\\\"
    case '\'' if charlit   => "\\'"
    case _ if printable(c) => c.toString
    case _                 => f"\\u${c.toInt & 0xFF}%02X"
  }

  /** set of printable characters */
  val printable = (32 to 126).map(_.toChar).toSet

  /** returns the first element from the tuple `t` */
  @inline
  def first[A, B](t: (A, B)) = t._1

  /** returns the second element from the tuple `t` */
  @inline
  def second[A, B](t: (A, B)) = t._2

  /** size of the buffer for reading input sources */
  val defaultBufferSize = 4096

  /** Returns a reader for an input source */
  def inputSourceToReader(input: InputSource): Try[Reader] =
    Try(input) map {
      case StringSource(string) => new StringReader(string)
      case StreamSource(stream) =>
        val reader = new InputStreamReader(stream, StandardCharsets.UTF_8)
        new BufferedReader(reader, defaultBufferSize)
    }

  /**
   * Returns the basename of a path, or None if the
   * given string doesn't contain a "."
   */
  def basename(str: String): Option[String] =
    (str lastIndexOf ".") match {
      case i if i < 0 => None
      case i          => Some(str.substring(0, i))
    }

  /**
   * Derives a default output file for a input file
   * with a new extension `ext`.
   *
   * Examples
   * {{{
   * defaultFile(new File("/test.asm"), "bin") == new File("/test.bin")
   * defaultFile(new File("/test"), "bin") == new File("/out.bin")
   * }}}
   */
  def defaultFile(input: File, ext: String): File = {
    val name = basename(input.getName).getOrElse("out")
    new File(input.getParentFile, s"$name.$ext")
  }

  /**
   * Prints a duration (given in milliseconds) in a
   * human readable format.
   *
   * Examples
   * {{{
   * showDuration(0)         == "0ms"
   * showDuration(1)         == "1ms"
   * showDuration(1000)      == "1s"
   * showDuration(60000)     == "1min"
   * showDuration(120000)    == "2mins"
   * showDuration(3600000)   == "1h"
   * showDuration(7200000)   == "2hs"
   * showDuration(86400000)  == "1d"
   * showDuration(172800000) == "2ds"
   * }}}
   */
  def showDuration(durationInMillis: Long): String = {
    if (durationInMillis < 0)
      throw new IllegalArgumentException("durationInMillis < 0")

    val timeUnits = Array(
      TimeUnit.DAYS         -> "d",
      TimeUnit.HOURS        -> "h",
      TimeUnit.MINUTES      -> "min",
      TimeUnit.SECONDS      -> "s",
      TimeUnit.MILLISECONDS -> "ms")

    val b     = new StringBuilder()
    var first = true
    var empty = true
    var m     = durationInMillis
    var i     = 0

    while (i < timeUnits.length) {
      val (factor, unit) = timeUnits(i)
      val millis         = factor.toMillis(1)

      if (m >= millis) {
        empty = false
        if (!first) {
          b += ',' += ' '
        } else {
          first = false
        }

        val t = m / millis
        b ++= t.toString
        b ++= unit
        if (t != 1 && i < 3) {
          b += 's'
        }

        m -= (t * millis)
      }

      i += 1
    }

    if (empty) "0ms"
    else b.toString
  }

}
