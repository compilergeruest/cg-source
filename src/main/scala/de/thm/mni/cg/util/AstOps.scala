package de.thm.mni.cg.util

import java.io.ByteArrayOutputStream
import java.io.OutputStream
import java.io.PrintStream
import java.nio.charset.StandardCharsets
import de.thm.mni.cg.ir.Ast
import de.thm.mni.cg.util.traversal._
import scala.collection.mutable.ArrayBuffer
import de.thm.mni.cg.table.SymbolTable
import de.thm.mni.cg.util.traversal.TraverseOrder._

/**
 * AstOps is a mixin, which adds several operations to AST-Nodes.
 */
trait AstOps  { self: Ast =>

  /** Prints this ast-node as source code to the given stream */
  def printCode(stream: OutputStream): Unit =
    printCode(new PrintStream(stream))

  /** Prints this ast-node as source code to the given stream */
  def printCode(stream: PrintStream): Unit =
    new CodePrinter(stream).show(this)

  /** Prints this ast-node as source code to stdout */
  def printCode(): Unit = printCode(System.out)

  /** Returns the source code of the current ast-node as a stream */
  def code(): String = {
    val bytes = new ByteArrayOutputStream()
    printCode(bytes)
    new String(bytes.toByteArray, StandardCharsets.UTF_8)
  }

  /** Replaces all child nodes in the given order on base of the given partial function */
  def replace(order: TraverseOrder = PostOrder)(f: PartialFunction[Ast, Ast]): Unit =
    new AstReplacer(order, f).traverse(self)(getTable)

  /**
   * Traverse this node and all children.
   * Only the nodes specified with the partial function are visited.
   */
  def traverseWithTable(order: TraverseOrder = TraverseOrder.PostOrder)(f: PartialFunction[(Ast, SymbolTable), Unit]): Unit =
    new PartialTraverser(order, f).traverse(this)(getTable)

  /**
   * Traverse this node and all children.
   * Only the nodes specified with the partial function are visited.
   */
  def traverse(order: TraverseOrder = TraverseOrder.PostOrder)(f: PartialFunction[Ast, Unit]): Unit =
    traverseWithTable(order)(ignoreTableInTraversal(f))

  /** Prints this ast-node in a lisp-like style to the given stream */
  def printLispy(stream: OutputStream): Unit =
    printLispy(new PrintStream(stream))

  /** Prints this ast-node in a lisp-like style to the given stream */
  def printLispy(stream: PrintStream): Unit =
    new PrettyPrinter(stream).show(this)

  /** Prints this ast-node in a lisp-like style to stdout */
  def printLispy(): Unit = printLispy(System.out)

  /** Returns lisp-like representation of this ast-node */
  def lispy(): String = {
    val bytes = new ByteArrayOutputStream()
    printLispy(bytes)
    new String(bytes.toByteArray, StandardCharsets.UTF_8)
  }

  /**
   * Returns this Ast-node and all its children, in a flat sequence.
   */
  def flatten: Seq[Ast] = {
    val buffer = ArrayBuffer[Ast]()
    val table = self.getTable match {
      case t: SymbolTable => t
      case _              => SymbolTable(false, None)
    }
    new PartialTraverser(TraverseOrder.PreOrder, {
      case (a: Ast, _) => buffer += a
    }).traverse(this)(table)
    buffer.toSeq
  }

  /**
   * Collects all nodes recursively, by applying a partial function to
   * all child-nodes of this ast-node on which the function is defined.
   */
  def collect[T](f: PartialFunction[Ast, T]): Seq[T] = {
    val buffer = ArrayBuffer[T]()
    val table = self.getTable match {
      case t: SymbolTable => t
      case _              => SymbolTable(false, None)
    }
    new PartialTraverser(TraverseOrder.PreOrder, {
      case (a: Ast, _) if f isDefinedAt a => buffer += f(a)
    }).traverse(this)(table)
    buffer.toSeq
  }

}
