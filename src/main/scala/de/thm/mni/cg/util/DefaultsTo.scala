package de.thm.mni.cg.util

/**
  * Type to default a type parameter to a specific type.
  * source: http://www.cakesolutions.net/teamblogs/default-type-parameters-with-implicits-in-scala (13.04.16)
  */
trait DefaultsTo[T, D]

object DefaultsTo {
  /**
   * Helps scalas type inferencer to constrain the left type on
   * base of the right type (if possible)
   *
   * Example:
   * DefaultsTo[T, Unit] if T is not further constrained (and may
   * be Unit) where it is used, the type inferencer will infer Unit
   * for T.
   */
  implicit def defaultDefaultsTo[T]: DefaultsTo[T, T] = null

  /**
   * If scalas type inferencer could not constrain the left type
   * on base of the right type, do not constrain it in any way
   */
  implicit def fallback[T, D]: DefaultsTo[T, D] = null
}
