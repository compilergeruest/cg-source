package de.thm.mni.cg.util

import System.{ currentTimeMillis => now }

/**
 * Primitive timer implementation for measuring runtime
 */
final class Timer {
  private var started = 0L
  private var acc = 0L

  /** starts to measure the time in milliseconds */
  def start(): Unit = started = now

  /** stops the time measurement and returns the elapsed time in milliseconds */
  def stop(): Long = {
    val time = now - started
    clear()
    time
  }

  /** resets the start time */
  def clear() = {
    started = 0L
    acc = 0L
  }

  /** restarts the time measurement and returns the previous elapsed time in milliseconds */
  def restart() = {
    val time = now - started
    start()
    time
  }

  /** returns the incremental accumulated time of this timer */
  def incremental(): Long = acc

  /** returns the incremental accumulated time of this timer, inclusively the elapsed time of the function `f` */
  def incremental(f: => Unit): Long = {
    val start = now
    f
    val end = now
    acc += end - start
    acc
  }

}
