package de.thm.mni.cg.util

import java.io.File
import java.io.FileInputStream
import java.io.InputStream

import scala.language.implicitConversions

/** InputSource is a class, which is an abstract representation of a source-program */
sealed trait InputSource

case class StringSource(text: String) extends InputSource

case class StreamSource(stream: InputStream) extends InputSource

/**
 * Implicit conversions, which let you use a `String`, an `InputStream` or a `File`
 * as an InputSource without creating an instance explicitly.
 */
trait InputSourceImplicits {
  /** Converts a String implicitly to an InputSource */
  implicit def stringToInputSource(text: String): InputSource =
    StringSource(text)

  /** Converts an InputStream implicitly to an InputSource */
  implicit def streamToInputSource(stream: InputStream): InputSource =
    StreamSource(stream)

  /** Converts a File implicitly to an InputSource */
  implicit def fileToInputSource(file: File): InputSource =
    StreamSource(new FileInputStream(file))
}
