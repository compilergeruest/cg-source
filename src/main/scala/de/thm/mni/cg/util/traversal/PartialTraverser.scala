package de.thm.mni.cg.util.traversal

import de.thm.mni.cg.ir._
import de.thm.mni.cg.table.SymbolTable
import de.thm.mni.cg.util.traversal.TraverseOrder._

/** The `PartialTraverser` lets you select nodes via a partial function. */
class PartialTraverser(order: TraverseOrder, consumer: PartialFunction[(Ast, SymbolTable), Unit])
  extends AstTraverser(order) {

  private def f(ast: Ast, table: SymbolTable): Unit = {
    val pair = (ast, table)
    if (consumer.isDefinedAt(pair)) consumer(pair)
    else ()
  }

/*
 * ================================= Expressions ==================================
 */

  override protected def traverse(e: BinExp)(implicit table: SymbolTable): Unit = f(e, table)
  override protected def traverse(e: CastExp)(implicit table: SymbolTable): Unit = f(e, table)
  override protected def traverse(e: FunExp)(implicit table: SymbolTable): Unit = f(e, table)
  override protected def traverse(e: NewArrExp)(implicit table: SymbolTable): Unit = f(e, table)
  override protected def traverse(e: NewObjExp)(implicit table: SymbolTable): Unit = f(e, table)
  override protected def traverse(e: UnaryExp)(implicit table: SymbolTable): Unit = f(e, table)

/*
 * =================================== Literals ===================================
 */

  override protected def traverse(e: BoolLit)(implicit table: SymbolTable): Unit = f(e, table)
  override protected def traverse(e: CharLit)(implicit table: SymbolTable): Unit = f(e, table)
  override protected def traverse(e: IntLit)(implicit table: SymbolTable): Unit = f(e, table)
  override protected def traverse(e: NullLit)(implicit table: SymbolTable): Unit = f(e, table)
  override protected def traverse(e: StringLit)(implicit table: SymbolTable): Unit = f(e, table)

/*
 * ================================== Variables ===================================
 */

  override protected def traverse(e: AccessVar)(implicit table: SymbolTable): Unit = f(e, table)
  override protected def traverse(e: ArrayVar)(implicit table: SymbolTable): Unit = f(e, table)
  override protected def traverse(e: NameVar)(implicit table: SymbolTable): Unit = f(e, table)

/*
 * ================================== Statements ==================================
 */

  override protected def traverse(e: AssignStm)(implicit table: SymbolTable): Unit = f(e, table)
  override protected def traverse(e: BlockStm)(implicit table: SymbolTable): Unit = f(e, table)
  override protected def traverse(e: DelStm)(implicit table: SymbolTable): Unit = f(e, table)
  override protected def traverse(e: ExpStm)(implicit table: SymbolTable): Unit = f(e, table)
  override protected def traverse(e: ForStm)(implicit table: SymbolTable): Unit = f(e, table)
  override protected def traverse(e: IfStm)(implicit table: SymbolTable): Unit = f(e, table)
  override protected def traverse(e: InitStm)(implicit table: SymbolTable): Unit = f(e, table)
  override protected def traverse(e: ReturnStm)(implicit table: SymbolTable): Unit = f(e, table)
  override protected def traverse(e: WhileStm)(implicit table: SymbolTable): Unit = f(e, table)

/*
 * ================================= Definitions ==================================
 */

  override protected def traverse(e: FunDef)(implicit table: SymbolTable): Unit = f(e, table)
  override protected def traverse(e: TyDef)(implicit table: SymbolTable): Unit = f(e, table)
  override protected def traverse(e: ParDef)(implicit table: SymbolTable): Unit = f(e, table)
  override protected def traverse(e: VarDef)(implicit table: SymbolTable): Unit = f(e, table)

/*
 * =================================== Symbols ====================================
 */

  override protected def traverse(e: ArrayTypeSymbol)(implicit table: SymbolTable): Unit = f(e, table)
  override protected def traverse(e: NameTypeSymbol)(implicit table: SymbolTable): Unit = f(e, table)
  override protected def traverse(e: StructTypeSymbol)(implicit table: SymbolTable): Unit = f(e, table)
  override protected def traverse(e: UnionTypeSymbol)(implicit table: SymbolTable): Unit = f(e, table)

}
