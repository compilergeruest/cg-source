package de.thm.mni.cg.util.traversal

/** A `TraverseOrder` is a Enumeration containing all traverse orders */
object TraverseOrder extends Enumeration {
  type TraverseOrder = Value
  val PreOrder, InOrder, PostOrder = Value
}
