package de.thm.mni.cg.util.traversal

import scala.reflect.ClassTag
import de.thm.mni.cg.table.SymbolTable
import de.thm.mni.cg.util.traversal.TraverseOrder._
import de.thm.mni.cg.ir._
import de.thm.mni.cg.util._

/**
 * An ast traverser which allows to replace (or 'map') ast nodes on base of a partial function
 */
final class AstReplacer(order: TraverseOrder, f: PartialFunction[Ast, Ast]) extends AstTraverser(order) {

  /**
   * Ensures that the element of type T, when mapped on base
   * of f, is still a subtype of U
   */
  private def ensure[T <: Ast, U >: T <: Ast](element: T)(implicit ct: ClassTag[U], df: DefaultsTo[U, T]): U =
    f.lift(element).collect { case ct(r) => r }.getOrElse(element)

/*
 * ================================= Expressions ==================================
 */

  override protected def traverse(e: BinExp)(implicit table: SymbolTable): Unit = {
    e.setLeft(ensure(e.getLeft))
    e.setRight(ensure(e.getRight))
  }
  override protected def traverse(e: CastExp)(implicit table: SymbolTable): Unit = {
    e.setExp(ensure(e.getExp))
  }
  override protected def traverse(e: FunExp)(implicit table: SymbolTable): Unit = {
    for (i <- e.getArgs.indices) {
      e.getArgs(i) = ensure(e.getArgs(i))
    }
  }
  override protected def traverse(e: NewArrExp)(implicit table: SymbolTable): Unit = {
    for (i <- e.getDimensions.indices) {
      e.getDimensions(i) = e.getDimensions(i).map(ensure[Exp, Exp])
    }
  }
  override protected def traverse(e: NewObjExp)(implicit table: SymbolTable): Unit = {
    for (i <- e.getArgs.indices) {
      e.getArgs(i) = (e.getArgs(i)._1, ensure(e.getArgs(i)._2))
    }
  }
  override protected def traverse(e: UnaryExp)(implicit table: SymbolTable): Unit = {
    e.setExp(ensure(e.getExp))
  }

/*
 * =================================== Literals ===================================
 */

  override protected def traverse(e: BoolLit)(implicit table: SymbolTable): Unit = ()
  override protected def traverse(e: CharLit)(implicit table: SymbolTable): Unit = ()
  override protected def traverse(e: IntLit)(implicit table: SymbolTable): Unit = ()
  override protected def traverse(e: NullLit)(implicit table: SymbolTable): Unit = ()
  override protected def traverse(e: StringLit)(implicit table: SymbolTable): Unit = ()

/*
 * ================================== Variables ===================================
 */

  override protected def traverse(e: AccessVar)(implicit table: SymbolTable): Unit = {
    e.setBase(ensure(e.getBase))
  }
  override protected def traverse(e: ArrayVar)(implicit table: SymbolTable): Unit = {
    e.setBase(ensure(e.getBase))
    e.setIndex(ensure(e.getIndex))
  }
  override protected def traverse(e: NameVar)(implicit table: SymbolTable): Unit = ()

/*
 * ================================== Statements ==================================
 */

  override protected def traverse(e: AssignStm)(implicit table: SymbolTable): Unit = {
    for (i <- e.getLhs.indices) {
      e.getLhs(i) = ensure(e.getLhs(i))
    }
    e.setRhs(ensure(e.getRhs))
  }
  override protected def traverse(e: BlockStm)(implicit table: SymbolTable): Unit = {
    for (i <- e.getBody.indices) {
      e.getBody(i) = ensure(e.getBody(i))
    }
  }
  override protected def traverse(e: DelStm)(implicit table: SymbolTable): Unit = {
    e.setExp(ensure(e.getExp))
  }
  override protected def traverse(e: ExpStm)(implicit table: SymbolTable): Unit = {
    e.setExp(ensure(e.getExp))
  }
  override protected def traverse(e: ForStm)(implicit table: SymbolTable): Unit = {
    e.setVarDef(ensure(e.getVarDef))
    e.setRange(Range(ensure(e.getRange.start), e.getRange.next.map(ensure[Exp, Exp]), ensure(e.getRange.end)) <@ e.getRange)
  }
  override protected def traverse(e: IfStm)(implicit table: SymbolTable): Unit = {
    e.setCheck(ensure(e.getCheck))
    e.setBody(ensure(e.getBody))
    e.setElseBody(e.getElseBody.map(ensure[Stm, Stm]))
  }
  override protected def traverse(e: InitStm)(implicit table: SymbolTable): Unit = {
    for (i <- e.getDefs.indices) {
      e.getDefs(i) = ensure(e.getDefs(i))
    }
    e.setExp(ensure(e.getExp))
  }
  override protected def traverse(e: ReturnStm)(implicit table: SymbolTable): Unit = {
    for (i <- e.getExps.indices) {
      e.getExps(i) = ensure(e.getExps(i))
    }
  }
  override protected def traverse(e: WhileStm)(implicit table: SymbolTable): Unit = {
    e.setCheck(ensure(e.getCheck))
    e.setBody(ensure(e.getBody))
  }

/*
 * ================================= Definitions ==================================
 */

  override protected def traverse(e: FunDef)(implicit table: SymbolTable): Unit = {
    for (i <- e.getOut.indices) {
      e.getOut(i) = ensure(e.getOut(i))
    }
    for (i <- e.getParams.indices) {
      e.getParams(i) = ensure(e.getParams(i))
    }
    e.setBody(ensure(e.getBody))
  }
  override protected def traverse(e: TyDef)(implicit table: SymbolTable): Unit = {
    e.setTypeSymbol(ensure(e.getTypeSymbol))
  }
  override protected def traverse(e: ParDef)(implicit table: SymbolTable): Unit = {
    e.setTypeSymbol(ensure(e.getTypeSymbol))
  }
  override protected def traverse(e: VarDef)(implicit table: SymbolTable): Unit = {
    e.setTypeSymbol(ensure(e.getTypeSymbol))
  }

/*
 * =================================== Symbols ====================================
 */

  override protected def traverse(e: ArrayTypeSymbol)(implicit table: SymbolTable): Unit = {
    e.setBase(ensure(e.getBase))
  }
  override protected def traverse(e: NameTypeSymbol)(implicit table: SymbolTable): Unit = ()
  override protected def traverse(e: StructTypeSymbol)(implicit table: SymbolTable): Unit = {
    for (i <- e.getMembers.indices) {
      e.getMembers(i) = Component(ensure(e.getMembers(i).symbol), e.getMembers(i).name) <@ e.getMembers(i)
    }
  }
  override protected def traverse(e: UnionTypeSymbol)(implicit table: SymbolTable): Unit = {
    for (i <- e.getMembers.indices) {
      e.getMembers(i) = Component(ensure(e.getMembers(i).symbol), e.getMembers(i).name) <@ e.getMembers(i)
    }
  }

}
