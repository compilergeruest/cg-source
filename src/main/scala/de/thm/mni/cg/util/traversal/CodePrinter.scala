package de.thm.mni.cg.util.traversal

import java.io.PrintStream
import scala.collection.mutable.ArrayBuffer

import de.thm.mni.cg.ir._
import de.thm.mni.cg.util._

/** `CodePrinter` is a traversal through the AST to print its original source code */
final class CodePrinter(stream: PrintStream) {
  import stream._
  import Op.precedence._

  def show(e: Ast): Unit = e match {
    case e: Exp        => show(e, 0)
    case e: Stm        => show(e, 0)
    case e: TypeSymbol => show(e, 0)
  }

  /*
   * ================================= Expressions ==================================
   */

  private def show(e: Exp, indent: Int): Unit = e match {
    case e: Lit       => show(e, indent)
    case e: Var       => show(e, indent)
    case e: BinExp    => show(e, indent)
    case e: CastExp   => show(e, indent)
    case e: FunExp    => show(e, indent)
    case e: NewArrExp => show(e, indent)
    case e: NewObjExp => show(e, indent)
    case e: UnaryExp  => show(e, indent)
  }

  private def nestedOperators(par: BinExp, child: Exp, indent: Int): Unit = {
    val operator = Option(child) collect {
      case BinExp(op, _, _, _) => op
      case UnaryExp(op, _, _)  => op
    }
    val needsParenthesis = operator exists (_  < par.getOp)

    if (needsParenthesis) print('(')
    show(child, indent)
    if (needsParenthesis) print(')')
  }

  private def show(e: BinExp, indent: Int): Unit = {
    nestedOperators(e, e.getLeft, indent)
    print(" ")
    print(e.getOp.token)
    print(" ")
    nestedOperators(e, e.getRight, indent)
  }

  private def show(e: CastExp, indent: Int): Unit = {
    print("(")
    show(e.getTypeSymbol, indent)
    print(") (")
    show(e.getExp, indent)
    print(")")
  }

  private def show(e: FunExp, indent: Int): Unit = {
    show(e.getName, indent)
    printList(e.getArgs, "(", ", ", ")")(show(_, indent))
  }

  private def show(e: NewArrExp, indent: Int): Unit = {
    print("new ")
    show(e.getTypeSymbol, indent)
    e.getDimensions.foreach {
      case Some(exp) =>
        print("[")
        show(exp, indent)
        print("]")
      case None => print("[]")
    }
  }

  private def show(e: NewObjExp, indent: Int): Unit = {
    print("new ")
    show(e.getName, indent)
    printList(e.getArgs, " { ", "", " }") {
      case (name, exp) =>
        show(name, indent)
        print(" = ")
        show(exp, indent)
        print("; ")
    }
  }

  private def show(e: UnaryExp, indent: Int): Unit = {
    print(e.getOp.token)
    show(e.getExp, indent)
  }

  /*
   * =================================== Literals ===================================
   */

  private def show(e: Lit, indent: Int): Unit = e match {
    case e: BoolLit   => print(e.getValue)
    case e: CharLit   => show(e, indent)
    case e: IntLit    => print(e.getNumberSystem.encode(e.getValue))
    case e: NullLit   => print("null")
    case e: StringLit => show(e, indent)
  }

  private def show(e: CharLit, indent: Int): Unit = {
    print('\'')
    print(escape(e.getValue, true))
    print('\'')
  }

  private def show(e: StringLit, indent: Int): Unit = {
    print('"')
    print(escape(e.getValue))
    print('"')
  }

  /*
   * ================================== Variables ===================================
   */

  private def show(e: Var, indent: Int): Unit = e match {
    case e: AccessVar => show(e, indent)
    case e: ArrayVar  => show(e, indent)
    case e: NameVar   => show(e, indent)
  }

  private def show(e: AccessVar, indent: Int): Unit = {
    show(e.getBase, indent)
    print('.')
    show(e.getMember, indent)
  }

  private def show(e: ArrayVar, indent: Int): Unit = {
    show(e.getBase, indent)
    print('[')
    show(e.getIndex, indent)
    print(']')
  }

  private def show(e: NameVar, indent: Int): Unit = {
    show(e.getName, indent)
  }

  /*
   * ================================== Statements ==================================
   */

  private def show(e: Stm, indent: Int): Unit = e match {
    case e: Def       => show(e, indent)
    case e: AssignStm => show(e, indent)
    case e: BlockStm  => show(e, indent)
    case e: DelStm    => show(e, indent)
    case e: ExpStm    => show(e, indent)
    case e: ForStm    => show(e, indent)
    case e: IfStm     => show(e, indent)
    case e: InitStm   => show(e, indent)
    case e: ReturnStm => show(e, indent)
    case e: WhileStm  => show(e, indent)
  }

  private def show(e: AssignStm, indent: Int): Unit = {
    showIndent(indent)
    printList(e.getLhs, ", ")(show(_, indent))
    print(" = ")
    show(e.getRhs, indent)
    println(";")
  }

  private def show(e: BlockStm, indent: Int): Unit = {
    if (e.getBody.isEmpty)
      println("{}")
    else {
      printList(e.getBody, "{\n", "\n", "") { stm =>
        show(stm, indent + 1)
      }
      showIndent(indent)
      println("}")
    }
  }

  private def show(e: DelStm, indent: Int): Unit = {
    showIndent(indent)
    print("del ")
    show(e.getExp, indent)
    println(";")
  }

  private def show(e: ExpStm, indent: Int): Unit = {
    showIndent(indent)
    show(e.getExp, indent)
    println(";")
  }

  private def show(e: ForStm, indent: Int): Unit = {
    showIndent(indent)
    print("for (")
    show(e.getVarDef.getTypeSymbol, indent)
    print(" ")
    show(e.getVarDef.getName, indent)
    print(" <- ")
    show(e.getRange.start, indent)
    e.getRange.next.foreach { n =>
      print(", ")
      show(n, indent)
    }
    print(" .. ")
    show(e.getRange.end, indent)
    print(")")
    showBodyStm(e.getBody, indent)
  }

  private def show(e: IfStm, indent: Int): Unit = {
    showIndent(indent)
    print("if (")
    show(e.getCheck, indent)
    print(")")
    showBodyStm(e.getBody, indent)
    e.getElseBody.foreach { eTh =>
      showIndent(indent)
      print("else")
      showBodyStm(eTh, indent)
    }
  }

  private def show(e: InitStm, indent: Int): Unit = {
    showIndent(indent)
    printList(e.getDefs, ", ") {
      case VarDef(const, symbol, name) =>
        if (const) {
          print("const ")
        }
        show(symbol, indent)
        print(" ")
        show(name, indent)
    }
    print(" = ")
    show(e.getExp, indent)
    println(";")
  }

  private def show(e: ReturnStm, indent: Int): Unit = {
    showIndent(indent)
    print("return ")
    printList(e.getExps, ", ")(show(_, indent))
    println(";")
  }

  private def show(e: WhileStm, indent: Int): Unit = {
    showIndent(indent)
    print("while (")
    show(e.getCheck, indent)
    print(")")
    showBodyStm(e.getBody, indent)
  }

  /*
   * ================================= Definitions ==================================
   */

  private def show(e: Def, indent: Int): Unit = e match {
    case e: FieldDef => show(e, indent)
    case e: FunDef   => show(e, indent)
    case e: TyDef    => show(e, indent)
  }

  private def show(e: FieldDef, indent: Int): Unit = e match {
    case e: ParDef => show(e, indent)
    case e: VarDef => show(e, indent)
  }

  private def show(e: FunDef, indent: Int): Unit = {
    showIndent(indent)
    printList(e.getOut, ", ")(show(_, 0))
    print(" ")
    show(e.getName, indent)
    printList(e.getParams, "(", ", ", ")")(show(_, 0))
    showBodyStm(e.getBody, indent)
  }

  private def show(e: TyDef, indent: Int): Unit = {
    print("type ")
    show(e.getName, indent)
    print(" = ")
    show(e.getTypeSymbol, indent)
  }

  private def show(e: ParDef, indent: Int): Unit = {
    show(e.getTypeSymbol, indent)
    print(" ")
    show(e.getName, indent)
  }

  private def show(e: VarDef, indent: Int): Unit = {
    showIndent(indent)
    if (e.isConst) print("const ")
    show(e.getTypeSymbol, indent)
    print(" ")
    show(e.getName, indent)
    println(";")
  }

  /*
   * =================================== Symbols ====================================
   */

  private def show(e: TypeSymbol, indent: Int): Unit = e match {
    case e: ComplexTypeSymbol => show(e, indent)
    case e: ArrayTypeSymbol   => show(e, indent)
    case e: NameTypeSymbol    => show(e, indent)
  }

  private def show(e: ComplexTypeSymbol, indent: Int): Unit = e match {
    case e: StructTypeSymbol => show(e, indent)
    case e: UnionTypeSymbol  => show(e, indent)
  }

  private def show(e: ArrayTypeSymbol, indent: Int): Unit = {
    show(e.getBase, indent)
    print("[]")
  }

  private def show(e: NameTypeSymbol, indent: Int): Unit =
    show(e.getName, indent)

  private def show(e: StructTypeSymbol, indent: Int): Unit =
    showComplex("struct", e.getMembers, indent)

  private def show(e: UnionTypeSymbol, indent: Int): Unit =
    showComplex("union", e.getMembers, indent)

  private def showComplex(name: String, xs: ArrayBuffer[Component], indent: Int): Unit = {
    print(name)
    println("{ ")
    printList(xs, "") { c =>
      showIndent(indent + 1)
      show(c.symbol, indent)
      print(" ")
      show(c.name, indent)
      println(";")
    }
    showIndent(indent + 1)
    println("};")
  }

  /*
   * =================================== Helpers ====================================
   */

  private val indentCache = scala.collection.mutable.HashMap[Int, String]()
  private def showIndent(indent: Int): Unit =
    print(indentCache.getOrElseUpdate(indent, "  " * indent))

  private def show(name: Name, indent: Int) =
    print(name.name)

  private def printList[A](xs: ArrayBuffer[A], delim: String)(f: A => Unit): Unit =
    printList(xs, "", delim, "")(f)

  private def printList[A](xs: ArrayBuffer[A], start: String, delim: String, end: String)(f: A => Unit): Unit = {
    print(start)
    val iter = xs.iterator
    while (iter.hasNext) {
      f(iter.next())
      if (iter.hasNext) {
        print(delim)
      }
    }
    print(end)
  }

  private def showBodyStm(stm: Stm, indent: Int): Unit = {
    if (!stm.isInstanceOf[BlockStm]) {
      println()
      show(stm, indent + 1)
    } else {
      print(" ")
      show(stm, indent)
    }
  }

}
