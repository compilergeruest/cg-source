package de.thm.mni.cg.util

/**
 * `Pos` is the base trait of all ast-nodes,
 * types and helper classes, which contain a position
 */
trait Pos extends Product with Serializable {
  var pos: Position = NoPosition

  /** Use position of `elem` for this element and return this */
  def usePos(elem: Pos): this.type = this <@ elem

  /** Use position `pos` for this element and return this */
  def usePos(pos: Position): this.type = this <@ pos

  /** Use position of `elem` for this element and return this */
  def <@(elem: Pos): this.type = this <@ elem.pos

  /** Use position `pos` for this element and return this */
  def <@(pos: Position): this.type = {
    this.pos = pos
    this
  }

}
