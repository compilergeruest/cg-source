package de.thm.mni.cg.util.traversal

import de.thm.mni.cg.ir._
import de.thm.mni.cg.util.traversal.TraverseOrder._

class DfgTraverser(order: TraverseOrder) {

  /**
   * Accumulates sequences of StmDfg and ScopeDfg, reducing
   * recursion depth during dfg traversal
   *
   * invariant: `dfg` has to be StmDfg or ScopeDfg
   */
  private def accumulate(dfg: Dfg): (Traversable[Dfg], Dfg) =
    if (order == PreOrder) {
      var buffer = Vector[Dfg](dfg)
      while (buffer.last match {
        case e: StmDfg =>
          buffer :+= e.getNext
          true
        case e: ScopeDfg =>
          buffer :+= e.getNext
          true
        case _ => false
      })()
      (buffer.init, buffer.last)
    } else { // InOrder || PostOrder
      var buffer = List[Dfg](dfg)
      while (buffer.head match {
        case e: StmDfg =>
          buffer = e.getNext :: buffer
          true
        case e: ScopeDfg =>
          buffer = e.getNext :: buffer
          true
        case _ => false
      })()
      (buffer.tail, buffer.head)
    }

  /**
   * Traverses accumulated sequences of StmDfg and ScopeDfg, reducing
   * recursion depth during dfg traversal
   *
   * invariant: `dfg` has to be StmDfg or ScopeDfg
   */
  private def traverseSeq(dfg: Dfg, seen: Set[Dfg]): Unit = {
    val (seq, next) = accumulate(dfg)
    val nseen = scala.collection.mutable.Set[Dfg]()
    nseen ++= seen
    if (order == PreOrder) {
      for (e <- seq; if !(nseen contains e)) e match {
        case e: StmDfg => traverse(e); nseen += e
        case e: ScopeDfg => traverse(e); nseen += e
        case _ => sys.error("internal error - maybe traverseSeq was called with a wrong dfg?")
      }
      _traverse(next, nseen.toSet)
    } else {
      _traverse(next, seen)
      for (e <- seq; if !(nseen contains e)) e match {
        case e: StmDfg => traverse(e); nseen += e
        case e: ScopeDfg => traverse(e); nseen += e
        case _ => sys.error("internal error - maybe traverseSeq was called with a wrong dfg?")
      }
    }
  }

  private def _traverse(dfg: Dfg, seen: Set[Dfg]): Unit = {
    if (!(seen contains dfg)) {
      val nseen = seen + dfg
      dfg match {
        case e: StmDfg => traverseSeq(e, seen)
        case e: ScopeDfg => traverseSeq(e, seen)
        case e: ExitDfg => traverse(e)
        case e: BranchDfg =>
          if (order == PreOrder) {
            traverse(e)
            _traverse(e.getOnTrue, nseen)
            _traverse(e.getOnFalse, nseen)
          } else if (order == InOrder) {
            _traverse(e.getOnTrue, nseen)
            traverse(e)
            _traverse(e.getOnFalse, nseen)
          } else if (order == PostOrder) {
            _traverse(e.getOnTrue, nseen)
            _traverse(e.getOnFalse, nseen)
            traverse(e)
          }
      }
    }
  }

  final def traverse(dfg: Dfg): Unit = _traverse(dfg, Set())

  protected def traverse(e: ExitDfg): Unit = ()
  protected def traverse(e: ScopeDfg): Unit = ()
  protected def traverse(e: StmDfg): Unit = ()
  protected def traverse(e: BranchDfg): Unit = ()

}
