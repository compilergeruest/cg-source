package de.thm.mni.cg.util.traversal

import de.thm.mni.cg.ir._
import de.thm.mni.cg.util.traversal.TraverseOrder._

class PartialDfgTraverser(order: TraverseOrder, f: PartialFunction[Dfg, Unit]) extends DfgTraverser(order) {

  private def total(dfg: Dfg): Unit =
    if (f.isDefinedAt(dfg)) f(dfg)
    else ()

  protected override def traverse(dfg: ExitDfg): Unit = total(dfg)
  protected override def traverse(dfg: ScopeDfg): Unit = total(dfg)
  protected override def traverse(dfg: StmDfg): Unit = total(dfg)
  protected override def traverse(dfg: BranchDfg): Unit = total(dfg)

}
