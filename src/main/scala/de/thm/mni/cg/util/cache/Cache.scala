package de.thm.mni.cg.util.cache

import scala.collection.mutable.HashMap

/** `Cache` is a memoized function. */
case class Cache[F, T](f: F => T) extends (F => T) {
  private val map = HashMap.empty[F, T]

  def apply(from: F): T =
    map.getOrElseUpdate(from, f(from))

  def apply(from: F, default: => T): T =
    map.getOrElseUpdate(from, default)

}
