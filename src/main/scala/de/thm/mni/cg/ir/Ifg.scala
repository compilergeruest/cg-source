package de.thm.mni.cg.ir

import scala.collection.mutable.{ Map => MMap }
import scala.collection.mutable.{ Set => MSet }
import scala.collection.mutable.BitSet
import scala.collection.mutable.ArrayBuffer
import scala.collection.mutable.Stack
import scala.collection.mutable.Queue

import java.util.Comparator
import scala.util.Sorting

final object Ifg {
  /** A representation of cliques in an interference graph */
  final type Clique = ArrayBuffer[Temporary]

  /**
   * Constructs an interference graph of the given ssa-form
   * program on base of liveness analysis
   * post-requisite: The produced interference graph is chordal
   */
  def apply(ssa: Ssa): Ifg = {
    sealed trait LiveAnalysis
    case class LiveOutAtBlock(n: Ssa, v: Temporary, m: MSet[Ssa]) extends LiveAnalysis
    case class LiveOutAtStatement(sIdx: Int, v: Temporary, m: MSet[Ssa], sBlock: BasicBlock) extends LiveAnalysis
    case class LiveInAtStatement(sIdx: Int, v: Temporary, m: MSet[Ssa], sBlock: BasicBlock) extends LiveAnalysis

    val seen = MSet[Ssa]()
    val allBlocks = Ssa.collectAllNodes(ssa)
    type LiveUsages = MSet[(Int, BasicBlock)]
    val notSharable = MSet[(Temporary, Temporary)]()
    val worklist = Stack[Queue[LiveAnalysis]]()
    livenessAnalysis(ssa)

    def collectAll(): (MSet[Temporary], MMap[Temporary, (LiveUsages, LiveUsages)]) = {
      val variables = MSet[Temporary]()
      val usages = MMap[Temporary, (LiveUsages, LiveUsages)]()

      for (block <- allBlocks) {
        val bb = block.asInstanceOf[BasicBlock]

        @inline
        def collect(vars: Set[Temporary]) =
          for (vari <- vars) {
            val (usagesOfP, usagesOfV) = usages.getOrElseUpdate(vari, (MSet(), MSet()))
            // cache all usages of `vari` in phi functions
            for (phi <- bb.phis) {
              val idx = phi.src.indexOf(vari)
              if (idx > -1) {
                usagesOfP += ((idx, bb))
              }
            }
            // cache all usages of `vari` in the three address code
            val instr = bb.instructions
            for (i <- instr.indices) {
              if (instr(i).getUsages.contains(vari)) {
                usagesOfV += ((i + bb.phis.size, bb))
              }
            }
            variables += vari
          }
        // collect all variables from the three address code
        for (instr <- bb.instructions) {
          collect(instr.getKills)
          collect(instr.getUsages)
        }
        // collect all variables from the phi functions
        for (phi <- bb.phis) {
          collect(phi.src.toSet)
          collect(Set(phi.getDestination))
        }
      }

      (variables, usages)
    }

    def evalLiveOutAtBlock(l: LiveOutAtBlock): Unit = {
      val LiveOutAtBlock(n, v, m) = l
      n.getLiveOutSet += v
      if (!m.contains(n)) {
        m += n
        val bb = n.asInstanceOf[BasicBlock]
        bb.instructions.lastOption.orElse(bb.phis.lastOption) match {
          case Some(s: PhiFunction) => worklist push Queue(LiveOutAtStatement(bb.wholeSize - 1, v, m, bb))
          case Some(s: Tac)         => worklist push Queue(LiveOutAtStatement(bb.wholeSize - 1, v, m, bb))
          case _                    =>
        }
      }
    }

    def evalLiveOutAtStatement(l: LiveOutAtStatement): Unit = {
      val LiveOutAtStatement(sIdx, v, m, sBlock) = l
      // `v` is live-out at `s`
      val s = sBlock.instrOrPhiAt(sIdx)
      val W = (s match {
        case Left(phi)  => MSet(phi.getDestination)
        case Right(tac) => tac.getKills.to[MSet]
      })

      for (w <- W - v) {
        // v and w can not share the same register -> edge in interference graph
        if (!notSharable.contains(w -> v) && !notSharable.contains(v -> w)) {
          notSharable += ((v, w))
        }
      }

      if (!W.contains(v)) {
        worklist push Queue(LiveInAtStatement(sIdx, v, m, sBlock))
      }
    }

    def evalLiveInAtStatement(l: LiveInAtStatement): Unit = {
      val LiveInAtStatement(sIdx, v, m, sBlock) = l
      // `v` is live-in at `s` (== sIdx)
      val s = sBlock.instrOrPhiAt(sIdx)
      if (sIdx == 0) {
        // `v` is live-in at `n`
        sBlock.getLiveInSet += v

        worklist push sBlock.getPredeccessors.map(LiveOutAtBlock(_, v, m): LiveAnalysis).to[Queue]
      } else {
        val sPredIdx = sIdx - 1
        if (sPredIdx >= 0 && sPredIdx <= sBlock.wholeSize) {
          worklist push Queue(LiveOutAtStatement(sIdx - 1, v, m, sBlock))
        }
      }
    }

    def evalWorklist(): Unit = {
      while (worklist.nonEmpty) {
        val tos = worklist.top
        if (tos.nonEmpty) {
          val head = tos.dequeue()
          head match {
            case l: LiveOutAtBlock     => evalLiveOutAtBlock(l)
            case l: LiveOutAtStatement => evalLiveOutAtStatement(l)
            case l: LiveInAtStatement  => evalLiveInAtStatement(l)
          }
        } else {
          worklist.pop()
        }
      }
    }

    /**
     * algorithm from Modern compiler implementation in Java, by Appel p. 429
     */
    def livenessAnalysis(ssa: Ssa): MSet[Temporary] = {
      val (variables, usages) = collectAll()

      // for each variable v
      for (v <- variables) {
        val m = MSet[Ssa]()
        val (phiuse, tacuse) = usages(v)
        // for each site-of-use s
        // if s is a phi function
        for ((sIdx, sBlock) <- phiuse) {
          // if v is the i-th argument of the phi function
          // get the i-th predecessor of the block which contains s
          val p = sBlock.getPredeccessors(sIdx)
          // liveOutAtBlock(p, v)
          worklist push Queue(LiveOutAtBlock(p, v, m))
          evalWorklist()
        }
        // if s is not a phi function
        for ((sIdx, sBlock) <- tacuse) {
          // liveInAtStatement(s, v)
          worklist push Queue(LiveInAtStatement(sIdx, v, m, sBlock))
          evalWorklist()
        }
      }

      variables
    }

    new Ifg(notSharable.toSet)
  }

}

/**
 * An interference graph with util functions for chordal, as
 * well as non-perfect graphs.
 */
final class Ifg(edges: Set[(Temporary, Temporary)]) {
  import Ifg._
  private val emptySet = Set[Temporary]()
  private val indexMapping = MMap[Temporary, Int]()
  private val adjacent = MMap[Temporary, Set[Temporary]]()
  private val vertices_ = MSet[Temporary]()

  private val amount = {
    var cnt = -1
    for ((v, w) <- edges) {
      indexMapping.getOrElseUpdate(v, { cnt += 1; cnt })
      indexMapping.getOrElseUpdate(w, { cnt += 1; cnt })
    }
    cnt + 1
  }
  private val indices = 0 until amount
  private val adjacency = new BitSet(amount * amount)

  for ((v, w) <- edges) {
    if (!adjacent.contains(v)) adjacent(v) = emptySet
    adjacent(v) += w
    if (!adjacent.contains(w)) adjacent(w) = emptySet
    adjacent(w) += v
    val vIdx = indexMapping(v)
    val wIdx = indexMapping(w)
    adjacency(adjacencyIndex(vIdx, wIdx)) = true
    adjacency(adjacencyIndex(wIdx, vIdx)) = true
    vertices_ += v
    vertices_ += w
  }

  /**
   * Returns true if `a` and `b` interfere, false otherwise
   */
  def interfers(a: Temporary, b: Temporary): Boolean =
    if ((indexMapping contains a) && (indexMapping contains b))
      adjacency(adjacencyIndex(indexMapping(a), indexMapping(b)))
    else
      false

  /**
   * Returns all temporaries which interfere with `t`
   */
  def neighbours(t: Temporary): Set[Temporary] =
    adjacent.getOrElse(t, emptySet)

  /** Calculates the 1-dimensional index of the adjacency matrix */
  @inline
  private def adjacencyIndex(x: Int, y: Int): Int =
    y * amount + x

  /** Returns the set of all vertices of this interference graph */
  def vertices = vertices_

  override final def toString: String = {
    val res = new StringBuilder()
    val maxLen = indexMapping.map(_._1.toString.length + 2).max
    res ++= " " * maxLen
    for ((t, _) <- indexMapping.toList.sortBy(_._2)) {
      val str = t.toString
      res ++= str
      res ++= " " * (maxLen - str.length)
    }
    res += '\n'
    for (y <- indices) {
      indexMapping.find(_._2 == y).foreach { case (t, _) =>
        val str = t.toString
        res ++= str
        res ++= " " * (maxLen - str.length)
      }
      for (x <- indices) {
        res ++= " " * ((maxLen - 1) / 2)
        res += (if (adjacency(adjacencyIndex(x, y))) '1' else '0')
        res ++= " " * Math.ceil((maxLen - 1f) / 2).toInt
      }
      res += '\n'
    }
    res.toString
  }

  /**
   * Calculates a perfect elimination ordering of this interference
   * graph on base of the maximum cardinality search. Be aware that
   * this method may behave undefined for interference graphs which
   * are not chordal.
   *
   * pre-requisite: This interference graph is chordal
   *
   * Source: http://citeseerx.ist.psu.edu/viewdoc/download?doi=10.1.1.528.9182&rep=rep1&type=pdf p. 4
   */
  lazy val perfectEliminationOrdering: ElimOrder = {
    /*
     * Pseudo-Code:
     * for all vertices v in this graph -> initialize weight(v) to 0
     * for vertices.size - 1 downto 0
     *    choose a vertex z with the maximum weight which is still unnumbered
     *      elimination ordering (z) := i
     *      z is not unnumbered anymore
     *    for all unnumbered vertices y in neighbours(z) -> increment weight(y)
      */
    val ordering = new ElimOrder(vertices.size)
    val buckets = MMap[Int, MSet[Temporary]]() // maps weight to the set of temporaries with that weight
    val weight = MMap[Temporary, Int]() // weight of temporaries
    var maximalWeight = 0

    @inline def setWeight(t: Temporary, w: Int) = {
      buckets.getOrElseUpdate(w, MSet()) += t
      weight(t) = w
      maximalWeight = w max maximalWeight
    }

    @inline def removeMaximal(): Temporary = {
      val result = buckets(maximalWeight).head
      removeTemporary(result, maximalWeight)
      weight -= result
      result
    }

    @inline def removeTemporary(t: Temporary, w: Int): Unit = {
      buckets(w) -= t
      if (buckets(w).isEmpty) {
        buckets -= w
        if (w == maximalWeight) {
          if (buckets.isEmpty) {
            maximalWeight = -1
          } else {
            maximalWeight = buckets.keySet.max
          }
        }
      }
    }

    for (vertex <- vertices) {
      setWeight(vertex, 0)
    }

    for (i <- vertices.size - 1 to 0 by -1) {
      val z = removeMaximal()
      ordering(i) = z
      for (y <- neighbours(z)) {
        if (weight.contains(y)) { // y is unnumbered
          val wy = weight(y)
          removeTemporary(y, wy)
          setWeight(y, wy + 1)
        }
      }
    }
    ordering
  }

  /**
   * Calculates the maximal cliques of this interference graph and filters
   * them by the given threshold `k` (where `k` is usually the amount of
   * available registers). Be aware that this function works on base of a
   * perfect elimination ordering and may behave undefined on interference
   * graphs which are not chordal.
   *
   * pre-requisite: This interference graph is chordal
   *
   * @return All maximal cliques with more than `k` nodes
   * Source: http://www2.cs.uni-paderborn.de/cs/ag-madh/vorl/GraphAlgorithmsI02/l3.pdf p.6
   */
  def maximalCliques(k: Int): ArrayBuffer[Clique] = {
    val mclique = MMap[Temporary, Boolean]()
    val A = MMap[Temporary, MSet[Temporary]]()
    val σ = perfectEliminationOrdering
    val X = new ArrayBuffer[MSet[Temporary]](σ.size)

    for (i <- σ.indices) {
      val vi = σ(i)
      mclique(vi) = true
      A(vi) = MSet()

      val neigborsOfViAfteri = MSet[Temporary]()
      val neigborsOfVi = neighbours(vi)
      for (n <- neigborsOfVi) {
        val k = σ.indexOf(n)
        if (i < k) neigborsOfViAfteri += n
      }

      X += neigborsOfViAfteri
    }

    for (i <- σ.indices) {
      val Xi = X(i)
      if (Xi.nonEmpty) {
        var j = Int.MaxValue
        for (vk <- Xi) {
          j = Math.min(j, σ.indexOf(vk))
        }
        val Xj = X(j)
        val vj = σ(j)
        if (Xi.size == Xj.size + 1) {
          mclique(vj) = false
        }
        for (xi <- Xi) {
          if (xi != vj) A(vj) += xi
        }
      }
    }

    val result = ArrayBuffer[Clique]()
    for (i <- σ.indices) {
      val vi = σ(i)
      val Xi = X(i)
      if (mclique(vi) && Xi.size + 1 > k) {
        val clique = ArrayBuffer[Temporary]()
        clique ++= Xi
        clique += vi
        result += clique
      }
    }
    result
  }

  /**
   * Returns a graph coloring for this interference graph based on
   * a greedy coloring algorithm. Be aware that this function uses
   * the perfect elimination order for a polynomial runtime, therefore
   * this interference graph needs to be chordal. Also note, that the
   * result maps each node of this graph to its color.
   *
   * pre-requisite: This interference graph is chordal
   *
   * Source: http://laure.gonnord.org/pro/research/ER03_2015/SSABasedRA.pdf p. 38
   */
  lazy val graphColoring: MMap[Temporary, Int] = {
    val colored = MMap[Temporary, Int]()
    val σ = perfectEliminationOrdering

    var color = -1
    def freshColor() = { color += 1; color }

    for (v <- σ) {
      val neighbors = neighbours(v)

      // find a color, which is not used from the neighbors
      val nsWithColor = neighbors.filter(colored contains _)
      val colorsOfNs = nsWithColor.map(colored)

      val allColors = (0 to color).toSet
      val availColorForV = allColors -- colorsOfNs

      colored(v) =
        if (availColorForV.isEmpty) { // if no color is available, use a fresh one
          freshColor()
        } else { // else reuse a color
          availColorForV.head
        }
    }

    colored
  }

}
