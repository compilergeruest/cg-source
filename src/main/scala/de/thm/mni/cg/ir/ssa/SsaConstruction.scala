package de.thm.mni.cg.ir.ssa

import de.thm.mni.cg.ir._
import de.thm.mni.cg.table.FunEntry
import de.thm.mni.cg.util._

import scala.collection.mutable.{ Map => MMap }
import scala.collection.mutable.ArrayBuffer

private[ir] trait SsaConstruction {

  /**
   * Generates the basic Ssa-structure by splitting the `function` into its basic blocks.
   * Note, no phi functions are generated and no variables are renamed.
   */
  def generateSsa(function: Tac.TacFunction) = {
    val program = function.code
    val (leaders, targets) = findLeaders(program, function.funEntry)

    var leaderIdx = 0
    while (leaderIdx < leaders.size) {
      val (leader, block) = leaders(leaderIdx)
      var i = leader + 1

      val nextLeader = leaders.lift(leaderIdx + 1).map(_._1).getOrElse(Int.MaxValue)
      while (i < program.size && nextLeader != i) {
        block.instructions += program(i)
        i += 1
      }

      i -= 1
      val optLabel = targetLabel(program(i))

      def target = for {
        label <- optLabel
        start <- targets.get(label)
        (_, block) <- leaders.find(_._1 == start)
      } yield block

      target.foreach { target =>
        block.addSuccessor(target)
      }

      program(i) match {
        case t if t.isJump => ()
        case _ if (leaderIdx + 1 < leaders.size) =>
          block.addSuccessor(leaders(leaderIdx + 1)._2)
        case _ =>
      }
      leaderIdx += 1
    }

    second(leaders.head)
  }

  /**
   * Returns the target label-name of a jump
   */
  @inline
  private def targetLabel(tac: Tac): Option[String] =
    Option(tac) collect {
      case Tac.Beq(_, _, _, LabelAddress(x))  => x
      case Tac.Bge(_, _, _, LabelAddress(x))  => x
      case Tac.Bgeu(_, _, _, LabelAddress(x)) => x
      case Tac.Bgt(_, _, _, LabelAddress(x))  => x
      case Tac.Bgtu(_, _, _, LabelAddress(x)) => x
      case Tac.Ble(_, _, _, LabelAddress(x))  => x
      case Tac.Bleu(_, _, _, LabelAddress(x)) => x
      case Tac.Blt(_, _, _, LabelAddress(x))  => x
      case Tac.Bltu(_, _, _, LabelAddress(x)) => x
      case Tac.Bne(_, _, _, LabelAddress(x))  => x
      case Tac.Jmp(_, LabelAddress(x))        => x
    }

  /**
   * Returns all leaders (index and its basic block) and
   * the positions of all labels in `program`
   */
  private def findLeaders(program: IndexedSeq[Tac], funEntry: FunEntry): (IndexedSeq[(Int, BasicBlock)], MMap[String, Int]) = {
    val labels = MMap[String, Int]()
    val result = ArrayBuffer[(Int, BasicBlock)]()
    result += ((0, BasicBlock(funEntry, ArrayBuffer(), ArrayBuffer() ++ program.lift(0))))

    @inline
    def addLabelAt(idx: Int) =
      program.lift(idx).foreach {
        case lbl: Tac.Label => labels += ((lbl.name, idx))
        case _              => ()
      }

    addLabelAt(0)
    val len =  program.size
    var i = 1
    while (i < len) {
      program(i) match {
        case label: Tac.Label                =>
          result += ((i, BasicBlock(funEntry, ArrayBuffer(), ArrayBuffer(label))))
          addLabelAt(i)
        case t: Tac if t.isBranchOrLabel && !t.isCall =>
          val maybeNext = program.lift(i + 1)
          result += ((i + 1, BasicBlock(funEntry, ArrayBuffer(), ArrayBuffer() ++ maybeNext)))
          addLabelAt(i + 1)
          if (maybeNext.map(_.isInstanceOf[Tac.Label]).getOrElse(false)) {
            i += 1
          }
        case _  => ()
      }
      i += 1
    }

    (result, labels)
  }

}
