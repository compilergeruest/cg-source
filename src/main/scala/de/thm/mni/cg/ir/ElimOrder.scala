package de.thm.mni.cg.ir

import scala.collection.mutable.{ Map => MMap }

/**
 * `ElimOrder` represents the perfect elimination order calculated by
 * the ssa-based register allocator.
 * It keeps a reverse mapping for a fast indexOf check.
 *
 * @see [[http://laure.gonnord.org/pro/research/ER03_2015/SSABasedRA.pdf]]
 */
case class ElimOrder(val size: Int) {
  private val order = new Array[Temporary](size)
  private val reverse: MMap[Temporary, Int] = MMap()

  def update(index: Int, t: Temporary): Unit = {
    order(index) = t
    reverse(t) = index
  }

  def foreach(f: Temporary => Unit): Unit = order.foreach(f)

  def apply(index: Int): Temporary = order(index)

  def indexOf(t: Temporary) = reverse(t)

  def contains(t: Temporary): Boolean = reverse.contains(t)

  override def toString = s"σ := ${order.mkString("{", ", ", "}" )}"

  def indices = 0 until size

}
