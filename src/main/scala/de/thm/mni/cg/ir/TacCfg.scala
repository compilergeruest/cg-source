package de.thm.mni.cg.ir

import scala.collection.mutable.ArrayBuffer
import de.thm.mni.cg.table.FunEntry
import collection.mutable.{ Map => MMap }
import collection.mutable.{ Set => MSet }
import de.thm.mni.cg.util._
import scala.annotation.tailrec
import java.io.FileOutputStream
import java.io.File

object TacCfg {

  def apply(function: Tac.TacFunction): TacCfg = {
    val program = function.code
    val (leaders, targets) = findLeaders(program, function.funEntry)

    var leaderIdx = 0
    while (leaderIdx < leaders.size) {
      val leader = leaders(leaderIdx)
//      println(("=" * 20) + "CFG-NODE" + ("=" * 20))
//      for (i <- 0 until leader.size) {
//        println(leader(i))
//      }
      //      var i = leader.start.index + 1
//      val nextLeader = leaders.lift(leaderIdx + 1).map(_.start.index).getOrElse(Int.MaxValue)
//      while (i < program.size && nextLeader != i) {
//        i += 1
//      }
//      leader.end = i
//
//      i -= 1
      val lastIns = leader(leader.size - 1)
      val optLabel = targetLabel(lastIns)

      def target = for {
        label <- optLabel
        start <- targets.get(label)
        block <- leaders.find(_.start.index == start)
      } yield block

      target.foreach { target =>
        leader.addSuccessor(target)
      }

      lastIns match {
        case t if t.isJump => ()
        case _ if (leaderIdx + 1 < leaders.size) =>
          leader.addSuccessor(leaders(leaderIdx + 1))
        case _ =>
      }
      leaderIdx += 1
    }
    leaders.head
  }

  /**
   * Returns the target label-name of a jump
   */
  @inline
  private def targetLabel(tac: Tac): Option[String] =
    Option(tac) collect {
      case Tac.Beq(_, _, _, LabelAddress(x))  => x
      case Tac.Bge(_, _, _, LabelAddress(x))  => x
      case Tac.Bgeu(_, _, _, LabelAddress(x)) => x
      case Tac.Bgt(_, _, _, LabelAddress(x))  => x
      case Tac.Bgtu(_, _, _, LabelAddress(x)) => x
      case Tac.Ble(_, _, _, LabelAddress(x))  => x
      case Tac.Bleu(_, _, _, LabelAddress(x)) => x
      case Tac.Blt(_, _, _, LabelAddress(x))  => x
      case Tac.Bltu(_, _, _, LabelAddress(x)) => x
      case Tac.Bne(_, _, _, LabelAddress(x))  => x
      case Tac.Jmp(_, LabelAddress(x))        => x
    }

  /**
   * Returns all leaders and
   * the positions of all labels in `program`
   */
  private def findLeaders(program: ArrayBuffer[Tac], funEntry: FunEntry): (IndexedSeq[TacBasicBlock], MMap[String, Int]) = {
    val labels = MMap[String, Int]()
    val result = ArrayBuffer[TacBasicBlock]()
    var curOff = new NextOffset(StartOffset, 0)
    result += TacBasicBlock(funEntry, program, StartOffset, curOff)

    @inline
    def addLabelAt(idx: Int) =
      if (idx >= 0 && idx < program.length && program(idx).isInstanceOf[Tac.Label]) {
        labels += ((program(idx).asInstanceOf[Tac.Label].name, idx))
      }

    addLabelAt(0)
    val len =  program.size
    var i = 1
    while (i < len) {
      program(i) match {
        case label: Tac.Label                =>
          curOff.add(i - curOff.index)
          val nextOff = new NextOffset(curOff, 0)
          result += TacBasicBlock(funEntry, program, curOff, nextOff)
          curOff = nextOff
          addLabelAt(i)
        case t: Tac if t.isBranchOrLabel && !t.isCall =>
          val maybeNext = program.lift(i + 1)
          curOff.add((i + 1) - curOff.index)
          val nextOff = new NextOffset(curOff, 0)
          result += TacBasicBlock(funEntry, program, curOff, nextOff)
          curOff = nextOff
          addLabelAt(i + 1)
          if (maybeNext.map(_.isInstanceOf[Tac.Label]).getOrElse(false)) {
            i += 1
          }
        case _  => ()
      }
      i += 1
    }
    curOff.add(len - curOff.index)

    (result, labels)
  }

}

sealed trait TacCfg {

  val funEntry: FunEntry
  //  val instructions: IndexedSeq[Tac]

  /**
   * Removes an instruction at index `index`. The removal is also
   * applied to the underlying program
   */
  def remove(index: Int): Tac

  /**
   * Sets the instruction at index `index` to `tac`, where null
   * indicates the removal of the old value
   */
  def update(index: Int, tac: Tac): Unit

  /** Returns the instruction at the given index */
  def apply(index: Int): Tac

  /** Returns whether the instruction at `index` is non-null */
  def isPresent(index: Int): Boolean

  /** Returns the size of all instructions */
  def size: Int

  /** Iterates over all instructions of this node (the function may not mutate this node) */
  def foreach(f: Tac => Unit): Unit

  def reverseForeach(f: (Tac, Int) => Unit): Unit

  /** Inserts the given tac at the specified index, all following elements will be shifted */
  def insert(index: Int, tac: Tac): Unit

  private val successors = ArrayBuffer[TacCfg]()
  private val predecessors = ArrayBuffer[TacCfg]()

  private val liveOut = MSet[Temporary]()
  private val liveIn = MSet[Temporary]()

  /**
   * Clears the live-in and live-out sets
   */
  def clearLiveness() = {
    liveIn.clear()
    liveOut.clear()
  }

  /**
   * Returns the set of all variables that are live out at this node.
   */
  def getLiveOut = liveOut

  /**
   * Returns the set of all variables that are live in at this node.
   */
  def getLiveIn = liveIn

  /**
   * Returns the predecessors of this node
   */
  def getPredeccessors: IndexedSeq[TacCfg] = predecessors

  /**
   * Returns the successors of this node
   */
  def getSuccessors: IndexedSeq[TacCfg] = successors

  /**
   * Adds the successors `cfg`, and adds `this` to cfg's predecessors
   */
  def addSuccessor(cfg: TacCfg): Unit = {
    successors += cfg
    cfg.predecessors += this
  }

  /**
   * Adds the predecessor `cfg`, and adds `this` to cfg's successors
   */
  def addPredecessors(cfg: TacCfg): Unit = {
    predecessors += cfg
    cfg.successors += this
  }

  /**
   * Removes the predecessor `cfg`, and removes this as successor of `cfg`
   */
  def removePredecessor(cfg: TacCfg): Unit = {
    predecessors -= cfg
    cfg.successors -= this
  }

  /**
   * Removes the successor `cfg`, and removes this as predecessor of `cfg`
   */
  def removeSuccessor(cfg: TacCfg): Unit = {
    successors -= cfg
    cfg.predecessors -= this
  }

  private[ir] lazy val id =
    "0x" + Integer.toHexString(hashCode)

  override final def hashCode: Int = System.identityHashCode(this)

  override final def equals(x: Any): Boolean = x match {
    case x: TacCfg => id == x.id
    case _         => false
  }

}

case class TacBasicBlock(override val funEntry: FunEntry, private val program: ArrayBuffer[Tac], private[ir] val start: ViewOffset, private[ir] val end: NextOffset) extends TacCfg {

  def remove(index: Int): Tac = {
    require(index >= 0 && index < size)
    val tac = program.remove(start.index + index)
    end.decrement()
    tac
  }

  def update(index: Int, tac: Tac): Unit = {
    require(index >= 0 && index < size)
    program(start.index + index) = tac
  }

  def apply(index: Int): Tac = {
    require(index >= 0 && index < size)
    program(start.index + index)
  }

  /** Whether or not the instruction at the given index is present (i.e. not removed) */
  def isPresent(index: Int): Boolean = {
    require(index >= 0 && index < size)
    program(start.index + index) != null
  }

  def size = end.index - start.index

  def foreach(f: Tac => Unit): Unit = {
    var i = start.index
    val e = end.index
    while (i < e) {
      f(program(i))
      i += 1
    }
  }

  def reverseForeach(f: (Tac, Int) => Unit): Unit = {
    var i = end.index - 1
    val s = start.index
    while (i >= s) {
      f(program(i), i - s)
      i -= 1
    }
  }

  def insert(index: Int, tac: Tac): Unit = {
    require(index >= 0 && index <= size)
    program.insert(index + start.index, tac)
    end.increment()
  }

}

private[ir] sealed trait ViewOffset {
  private[ir] var next: NextOffset = null
  def index: Int
}

private[ir] object StartOffset extends ViewOffset {
  val index = 0
}

private[ir] class NextOffset(val previous: ViewOffset, private var skip: Int) extends ViewOffset {
  private var _index = previous.index + skip
  previous.next = this

  @tailrec
  final def add(size: Int): Unit = {
    skip += size
    _index += size
    if (next ne null) {
      next.add(size)
    }
  }

  @tailrec
  final def sub(size: Int): Unit = {
    skip -= size
    _index -= size
    if (next ne null) {
      next.sub(size)
    }
  }

  def increment(): Unit = add(1)

  def decrement(): Unit = sub(1)

  def index = _index

}
