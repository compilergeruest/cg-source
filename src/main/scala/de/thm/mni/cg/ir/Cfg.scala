package de.thm.mni.cg.ir

import de.thm.mni.cg.util._
import de.thm.mni.cg.language.ErrorMessages._

object Cfg {

  /**
   * Creates a control-flow-graph from a function definition.
   * @param merge Should consecutive BasicBlocks be merged?
   */
  def apply(fdef: FunDef, merge: Boolean): Cfg = {
    def helper(stm: Stm, next: Cfg): Cfg = stm match {
      case IfStm(_, th, optElse) =>
        val onFalse = optElse.map(helper(_, next)).getOrElse(next)
        val onTrue = helper(th, next)
        val reaching = reaches(next) _
        if (!(reaching(onFalse) || reaching(onTrue))) {
          reportAtFirst(next, unreachableCode)
        }
        BranchCfg(stm, onTrue, onFalse)
      case WhileStm(_, th) =>
        val res = withEffect(BranchCfg(stm, null, next)) {
          cfg => cfg._onTrue = helper(th, cfg)
        }
        val reaching = reaches(next) _
        if (!(reaching(res.getOnFalse) || reaching(res.getOnTrue))) {
          reportAtFirst(next, unreachableCode)
        }
        res
      case ForStm(_, _, stm, _) =>
        val res = withEffect(BranchCfg(stm, null, next)) {
          cfg => cfg._onTrue = helper(stm, cfg)
        }
        val reaching = reaches(next) _
        if (!(reaching(res.getOnFalse) || reaching(res.getOnTrue))) {
          reportAtFirst(next, unreachableCode)
        }
        res
      case BlockStm(stms, _) =>
        if (stms.isEmpty) {
          BasicBlockCfg(Nil, next)
        } else {
          def mergeSeqs(n: Stm, acc: Cfg) =
            helper(n, acc) match {
              case BasicBlockCfg(hd, next) if (acc != next && reportAtFirst(acc, unreachableCode)) =>
                internalError()
              case BasicBlockCfg(hd, BasicBlockCfg(tl, next)) if merge =>
                BasicBlockCfg(hd ::: tl, next)
              case cfg =>
                cfg
            }
          stms.init.foldRight(helper(stms.last, next)) { mergeSeqs }
        }
      case stm: ReturnStm =>
        BasicBlockCfg(List(stm), ExitCfg)
      case _ =>
        BasicBlockCfg(List(stm), next)
    }
    helper(fdef.getBody, ExitCfg)
  }

  /** Tries to find a statement in the given cfg and reports the error at it, otherwise false is returned */
  private def reportAtFirst(cfg: Cfg, msg: String): Boolean = cfg match {
    case BasicBlockCfg(hd :: _, _) => error(hd, msg)
    case BasicBlockCfg(Nil, next)  => reportAtFirst(next, msg)
    case ExitCfg                   => false
    case BranchCfg(stm, _, _)      => error(stm, msg)
  }

  /** Tests whether the given start control-flow-node reaches the given end */
  private def reaches(end: Cfg)(start: Cfg): Boolean = {
    val seen = scala.collection.mutable.Set[Cfg]()
    def helper(start: Cfg): Boolean =
      if (!seen.contains(start)) {
        seen += start
        start match {
          case `end`                         => true
          case BasicBlockCfg(_, next)        => helper(next)
          case BranchCfg(_, onTrue, onFalse) => helper(onTrue) || helper(onFalse)
          case ExitCfg                       => false
        }
      } else {
        false
      }
    helper(start)
  }

}

/**
 * Base trait for nodes of the control-flow-graph.
 *
 * @see [[de.thm.mni.cg.ir.BranchCfg]]
 * @see [[de.thm.mni.cg.ir.BasicBlockCfg]]
 * @see [[de.thm.mni.cg.ir.ExitCfg]]
 */
sealed trait Cfg {
  private val liveOut = collection.mutable.Set[Name]()
  private val liveIn = collection.mutable.Set[Name]()
  private val kill = collection.mutable.Set[Name]()
  private val gen = collection.mutable.Set[Name]()

  def getLiveOutSet = liveOut
  def getLiveInSet = liveIn
  def getGenSet = gen
  def getKillSet = kill

  private[ir] lazy val id =
    "0x" + Integer.toHexString(hashCode)

  private lazy val show: String = {
    val b = new StringBuilder()

    def helper(cfg: Cfg, seen: Set[Cfg]): Unit = cfg match {
      case BranchCfg(stm, onTrue, onFalse) =>
        b ++= s"BranchCfg[${cfg.id}]("
        b ++= stm.describe
        b ++= ", "
        helper(onTrue, seen + cfg)
        b ++= ", "
        helper(onFalse, seen + cfg)
        b ++= ")"
      case BasicBlockCfg(stms, next) =>
        b ++= s"BasicBlockCfg[${cfg.id}]("
        b ++= "count: "
        b ++= stms.size.toString
        b ++= ", "
        if (seen contains next) {
          b ++= s"<- ${next.id}"
        } else {
          helper(next, seen + cfg)
        }
        b ++= ")"
      case ExitCfg => b ++= "ExitCfg"
    }

    helper(this, Set())
    b.toString
  }

  override final def toString: String = show

  override final def hashCode: Int = System.identityHashCode(this)

  override final def equals(x: Any): Boolean = x match {
    case x: Cfg => id == x.id
    case _      => false
  }

}

/**
 * A branch of the control-flow-graph.
 * The constructor arguments' meaning differ depending on the type of branch (If, While, For).
 *
 * - IfStm:
 *     - onTrue contains the then-part of the IfStm.
 *     - onFalse contains either the else-part of the IfStm, or the next node of the control-flow-graph, if the else-part of the IfStm is empty.
 *
 * - WhileStm:
 *     - onTrue contains the body of the while loop.
 *     - onFalse contains the the next node of the control-flow-graph.
 *
 * - ForStm:
 *     - onTrue contains the body of the for loop.
 *     - onFalse contains the the next node of the control-flow-graph.
 */
case class BranchCfg(private val stm: Stm, private[ir] var _onTrue: Cfg, private val onFalse: Cfg) extends Cfg {
  def getOnTrue = _onTrue
  def getOnFalse = onFalse
  def getStm = stm
}

/**
 * BasicBlockCfg contains a sequence of statements,
 * which are collapsed to one node of the control-flow-graph.
 */
case class BasicBlockCfg(private val stms: List[Stm], private val next: Cfg) extends Cfg {
  def getStms = stms
  def getNext = next
}

/** Explicit exit of the control-flow-graph. */
case object ExitCfg extends Cfg
