package de.thm.mni.cg.ir

import de.thm.mni.cg.table.SymbolTable
import de.thm.mni.cg.util._
import scala.collection.mutable.{Set => MSet}

object Dfg {

  /**
   * Creates a dataflow-graph on base of the given function definition
   */
  def apply(fdef: FunDef): Dfg = {
    def helper(stm: Stm, next: Dfg): Dfg = stm match {
      case IfStm(_, th, optElse) =>
        val onFalse = optElse.map(helper(_, next)).getOrElse(next)
        BranchDfg(stm, helper(th, next), onFalse, next)
      case WhileStm(_, th) =>
        withEffect(BranchDfg(stm, null, next, next)) {
          dfg => dfg._onTrue = helper(th, dfg)
        }
      case f @ ForStm(_, _, stm, _) =>
        withEffect(BranchDfg(f, null, next, next)) {
          dfg => dfg._onTrue = helper(stm, dfg)
        }
      case BlockStm(stms, table) =>
        if (stms.isEmpty) {
          ScopeDfg(ExitDfg(), next, table)
        } else {
          val zero = helper(stms.last, ExitDfg())
          val inner = stms.init.foldRight(zero)(helper)
          ScopeDfg(inner, next, table)
        }
      case ReturnStm(_) =>
        StmDfg(stm, ExitDfg())
      case _ =>
        StmDfg(stm, next)
    }
    helper(fdef.getBody, ExitDfg())
  }

}

/**
 * Base trait of the dataflow-graph
 * The main difference between the controlflow-graph and the dataflow-graph
 * are the live-, kill- and gen-sets, where:
 * a) the live-set contains all variables which are live at program point p
 * b) the kill-set contains all variables which are killed (assigned) at program point p
 * c) the gen-set contains all variables which are used before killed at program point p
 */
sealed trait Dfg {
  private val live = MSet[Name]()
  private val kill = MSet[Name]()
  private val gen = MSet[Name]()
  protected val next: Dfg

  /** Returns true as long as there is still one sibling node */
  def hasNext = !this.isInstanceOf[ExitDfg]
  /** Returns the next sibling node */
  def getNext = next
  /** Returns the live set */
  def getLiveSet = live
  /** Returns the gen set */
  def getGenSet = gen
  /** Returns the kill set */
  def getKillSet = kill

  private[ir] lazy val id =
    "0x" + Integer.toHexString(hashCode)

  private[ir] lazy val show = {
    val b = new StringBuilder()
    @inline
    def ref(next: Dfg, seen: Set[Dfg], self: Dfg) =
      if (seen contains next) {
          b ++= s"<- ${next.id}"
        } else {
          helper(next, seen + self)
        }

    def helper(dfg: Dfg, seen: Set[Dfg]): Unit = this match {
      case StmDfg(stm, next) =>
        b ++= s"StmDfg[${dfg.id}]("
        b ++= stm.describe
        b ++= ", "
        ref(next, seen, dfg)
        b ++= ")"
      case BranchDfg(stm, onTrue, onFalse, next)=>
        b ++= s"BranchDfg[${dfg.id}]("
        b ++= stm.describe
        b ++= ", "
        ref(onTrue, seen, dfg)
        b ++= ", "
        ref(onFalse, seen, dfg)
        b ++= ", "
        ref(next, seen, dfg)
        b ++= ")"
      case ScopeDfg(inner, next, _) =>
        b ++= s"ScopeDfg[${dfg.id}]("
        ref(inner, seen, dfg)
        b ++= ")"
      case ExitDfg() => b ++= "ExitDfg"
    }
    helper(this, Set())
    b.toString()
  }

  override final def toString: String = show

  override final def hashCode: Int = System.identityHashCode(this)

  override final def equals(x: Any): Boolean = x match {
    case x: Dfg => id == x.id
    case _      => false
  }

}

/** A class representing a single program point p in the dataflow-graph */
case class StmDfg(private val stm: Stm, protected val next: Dfg) extends Dfg {
  /** Returns the statement which is associated with this dataflow-node */
  def getStm = stm
}

/** A class representing a control-flow branch (i.e. if, while, etc.) */
case class BranchDfg(private val stm: Stm, private[ir] var _onTrue: Dfg, private val onFalse: Dfg, protected val next: Dfg) extends Dfg {
  /** Returns the dataflow-graph for the left (true) branch */
  def getOnTrue = _onTrue
  /** Returns the dataflow-graph for the right (false) branch (which may also be the following sibling node of this one) */
  def getOnFalse = onFalse
  /** Returns the statement which is associated with this dataflow-node (i.e. an if-else-statement) */
  def getStm = stm
}

/** A class representing a deeper lexical scope in the source program */
case class ScopeDfg(private val inner: Dfg, protected val next: Dfg, private val table: SymbolTable) extends Dfg {
  /** The inner dataflow-graph of this scope */
  def getInner = inner
  /** The symbol table which is associated with the BlockStm */
  def getTable = table
}

/** Explicit exit of the data-flow-graph. */
case class ExitDfg() extends Dfg {
  protected override val next = this
}
