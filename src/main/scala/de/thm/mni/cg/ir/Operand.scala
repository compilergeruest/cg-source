package de.thm.mni.cg.ir

import scala.language.implicitConversions
import de.thm.mni.cg.runtime.Register

/**
 * An `Operand` denotes an abstract implementation of an immediate value
 * (represented by class `Immediate`) or a variable in the three-address-code
 * (represented by class `Temporary`).
 *
 * @see [[de.thm.mni.cg.ir.Tac]]
 * @see [[de.thm.mni.cg.ir.Immediate]]
 * @see [[de.thm.mni.cg.ir.Temporary]]
 */
sealed trait Operand

/**
 * A variable in the three-address-code.
 *
 * @see [[de.thm.mni.cg.ir.Tac]]
 * @see [[de.thm.mni.cg.ir.Operand]]
 */
case class Temporary(prefix: String, n: Int) extends Operand {
  override def toString() = prefix + n + (if (hasRegister) " @ " + getRegister.codeRepresentation else "")

  override def equals(any: Any): Boolean = any match {
    case _: Temporary with Subscription if (!this.isInstanceOf[Temporary with Subscription]) => false
    case t: Temporary                   => t.prefix == prefix && t.n == n
    case _                              => false
  }

  override def hashCode(): Int =
    31 * prefix.hashCode() + n

  private var register: Register = _

  /**
   * Returns true, if a register was assigned to this temporary
   */
  def hasRegister = register != null

  /**
   * Returns the register, which was assigned to this temporary
   */
  def getRegister: Register = register

  /**
   * Assigns register `reg` with this temporary
   */
  def setRegister(reg: Register) = register = reg
}

/**
 * An immediate value in the three-address-code is either a
 * constant number or a labelname.
 *
 * @see [[de.thm.mni.cg.ir.ConstNumber]]
 * @see [[de.thm.mni.cg.ir.LabelAddress]]
 */
sealed trait Immediate extends Operand

object Immediate {
  def apply(name: String): Immediate =
    LabelAddress(name)
  def apply(n: Int): Immediate =
    ConstNumber(n)

  implicit def numToImmediate(n: Int) =
    ConstNumber(n)

  implicit def strToImmediate(s: String) =
    LabelAddress(s)

  implicit def lblToImmediate(l: Tac.Label) =
    LabelAddress(l.name)

}

/**
 * A label name in the three-address-code.
 *
 * @see [[de.thm.mni.cg.ir.Tac]]
 */
case class LabelAddress(name: String) extends Immediate {
  override def toString() = name
}

/**
 * A constant number in the three-address-code.
 *
 * @see [[de.thm.mni.cg.ir.Tac]]
 */
case class ConstNumber(n: Int) extends Immediate {
  override def toString() = n.toString()
}
