package de.thm.mni.cg.ir

import scala.collection.mutable.ArrayBuffer
import de.thm.mni.cg.language.NumberSystem
import de.thm.mni.cg.semantic.Ty
import de.thm.mni.cg.table.SymbolTable
import de.thm.mni.cg.util.AstOps
import de.thm.mni.cg.util.Pos
import de.thm.mni.cg.util.escape
import de.thm.mni.cg.util.HasPosition
import de.thm.mni.cg.util.Position
import de.thm.mni.cg.util.NoPosition

/**
 * `Ast` is the base trait of the class hierarchy representing the nodes
 * of the abstract syntax-tree of the language. Every AST-Node also has
 * an position.
 *
 * The hierarchy is structured as follows:
 *
 * {{{
 * Ast
 * |
 * +--> Stm
 * |    |
 * |    +--> AssignStm
 * |    |
 * |    +--> BlockStm
 * |    |
 * |    +--> DelStm
 * |    |
 * |    +--> ExpStm
 * |    |
 * |    +--> ForStm
 * |    |
 * |    +--> IfStm
 * |    |
 * |    +--> InitStm
 * |    |
 * |    +--> ReturnStm
 * |    |
 * |    +--> WhileStm
 * |    |
 * |    +--> Def
 * |         |
 * |         +--> FieldDef
 * |         |    |
 * |         |    +--> ParDef
 * |         |    |
 * |         |    +--> VarDef
 * |         |
 * |         +--> FunDef
 * |         |
 * |         +--> TyDef
 * |
 * +--> TypeSymbol
 * |    |
 * |    +--> ArrayTypeSymbol
 * |    |
 * |    +--> NameTypeSymbol
 * |    |
 * |    +--> ComplexTypeSymbol
 * |         |
 * |         +--> StructTypeSymbol
 * |         |
 * |         +--> UnionTypeSymbol
 * |
 * +--> Exp
 *      |
 *      +--> BinExp
 *      |
 *      +--> CastExp
 *      |
 *      +--> FunExp
 *      |
 *      +--> NewArrExp
 *      |
 *      +--> NewObjExp
 *      |
 *      +--> UnaryExp
 *      |
 *      +--> Lit
 *      |    |
 *      |    +--> BoolLit
 *      |    |
 *      |    +--> CharLit
 *      |    |
 *      |    +--> IntLit
 *      |    |
 *      |    +--> NullLit
 *      |    |
 *      |    +--> StringLit
 *      |
 *      +--> Var
 *           |
 *           +--> AccessVar
 *           |
 *           +--> ArrayVar
 *           |
 *           +--> NameVar
 * }}}
 *
 * @see [[de.thm.mni.cg.ir.Stm]]
 * @see [[de.thm.mni.cg.ir.TypeSymbol]]
 * @see [[de.thm.mni.cg.ir.Exp]]
 */
sealed trait Ast extends Pos with AstOps {
  private var table: SymbolTable = null
  /** returns the symbol table of the enclosing scope */
  def getTable = table
  def setTable(table: SymbolTable) = this.table = table
}

/*
 * ============================ Constants and Helpers =============================
 */

/** `Name` is a wrapper class for Names in the language, it also contains a position */
case class Name(name: String) extends Pos

object Program {
  /**
   * Wraps a sequence of statements into a program. This program might not be valid,
   * it needs to be semantically analyzed.
   *
   * Every synthetic function (e.g. global "main"-function) gets the position `position`
   * assigned, if `NoPosition` is specified, the position of the first statement from
   * `stms` is taken, if it exists.
   */
  def wrap(position: Position, stms: Stm*): Program = {
    val pos = position match {
      case p @ NoPosition if stms.nonEmpty => stms.head.pos
      case p                               => p
    }
    val mainFun = {
      val mainName = Name("main") <@ pos
      val mainTy = NameTypeSymbol(Name("unit") <@ pos) <@ pos
      val body = BlockStm(stms.to[ArrayBuffer], null) <@ pos
      FunDef(mainName, ArrayBuffer(mainTy), ArrayBuffer(), body, null) <@ pos
    }
    Program(mainFun, SymbolTable(false, None)) <@ pos
  }
}

/** `Program` is a class collecting the global main function and the global symbol table */
case class Program(globalScope: FunDef, globalTable: SymbolTable) extends Pos {

  private var maxLexicalFunDepth: Int = 0
  def getMaxLexicalFunDepth = maxLexicalFunDepth
  def setMaxLexicalFunDepth(depth: Int) = this.maxLexicalFunDepth = depth

}

/*
 * ================================== Statements ==================================
 */

/**
 * `Stm` is the base trait of all statements in the language.
 *
 * @see [[de.thm.mni.cg.ir.Def]]
 * @see [[de.thm.mni.cg.ir.AssignStm]]
 * @see [[de.thm.mni.cg.ir.BlockStm]]
 * @see [[de.thm.mni.cg.ir.DelStm]]
 * @see [[de.thm.mni.cg.ir.ExpStm]]
 * @see [[de.thm.mni.cg.ir.ForStm]]
 * @see [[de.thm.mni.cg.ir.IfStm]]
 * @see [[de.thm.mni.cg.ir.InitStm]]
 * @see [[de.thm.mni.cg.ir.ReturnStm]]
 * @see [[de.thm.mni.cg.ir.WhileStm]]
 */
sealed trait Stm extends Ast {
  /**
   * Returns a short text, describing this statement.
   *
   * e.g.:
   * {{{
   * IfStm(...).describe == "if"
   * WhileStm(...).describe == "while"
   * ...
   * }}}
   */
  final def describe: String = description

  private lazy val description: String =
    this match {
      case _: Def    => "definition"
      case _: ExpStm => "exp-stm"
      case stm =>
        val stmClassName = stm.getClass.getSimpleName
        val stmName = stmClassName.replaceFirst("Stm", "")
        stmName.toLowerCase()
    }
}

/**
 * `AssignStm` represents an assignment in the language
 *
 * Examples:
 * {{{
 * n = 6;
 * a[0] = 7;
 * p.x = 8;
 * f().c = 9;
 * }}}
 */
case class AssignStm(private val lhs: ArrayBuffer[Exp], private var rhs: Exp) extends Stm {
  def getLhs = lhs
  def getRhs = rhs
  def setRhs(exp: Exp) = rhs = exp
}

/**
 * `BlockStm` represents a block statement in the language
 *
 * Examples:
 * {{{
 * {
 *   ...
 * }
 * }}}
 */
case class BlockStm(private val body: ArrayBuffer[Stm], private var local: SymbolTable) extends Stm {
  def getBody = body
  def setLocalTable(table: SymbolTable) = local = table
  def getLocalTable = local
}

/**
 * `DelStm` represents the deallocation of ram
 *
 * Example:
 * {{{
 * del p;
 * }}}
 */
case class DelStm(private var exp: Exp) extends Stm {
  def getExp = exp
  def setExp(exp: Exp) = this.exp = exp
}

/**
 * `ExpStm` represents an expression in statement position
 *
 * Example:
 * {{{
 * 5;
 * 6 < 0;
 * }}}
 */
case class ExpStm(private var exp: Exp) extends Stm {
  def getExp = exp
  def setExp(exp: Exp) = this.exp = exp
}

/**
 * `ForStm` represents a for statement in the language
 *
 * Example:
 * {{{
 * for (int i <- 0, 2 .. 10) {}
 * }}}
 */
case class ForStm(private var varDef: VarDef, private var range: Range, private var body: Stm, private var local: SymbolTable) extends Stm {
  def getVarDef = varDef
  def setVarDef(varDef: VarDef) = this.varDef = varDef
  def getRange = range
  def setRange(range: Range) = this.range = range
  def getBody = body
  def setBody(body: Stm) = this.body = body
  def setLocalTable(table: SymbolTable) = local = table
  def getLocalTable = local
}

/**
 *  `Range` is a helper class used in the `ForSt`
 *
 *  @see [[de.thm.mni.cg.ir.ForStm]]
 */
case class Range(start: Exp, next: Option[Exp], end: Exp) extends Pos {
  def getStart = start
  def getNext = next
  def getEnd = end
}

/**
 * `IfStm` represents an if statement in the language
 *
 * Example:
 * {{{
 * if (!false) {}
 * }}}
 */
case class IfStm(private var check: Exp, private var body: Stm, private var elseBody: Option[Stm]) extends Stm {
  def getCheck = check
  def setCheck(check: Exp) = this.check = check
  def getBody = body
  def setBody(body: Stm) = this.body = body
  def getElseBody = elseBody
  def setElseBody(elseBody: Stm) = this.elseBody = Option(elseBody)
  def setElseBody(elseBody: Option[Stm]) = this.elseBody = elseBody
}

/**
 * `InitStm` represents an declaration and initialization of a variable in the language
 *
 * Example:
 * {{{
 * int n = 6;
 * }}}
 */
case class InitStm(private val defs: ArrayBuffer[VarDef], private var exp: Exp) extends Stm {
  def getDefs = defs
  def getExp = exp
  def setExp(exp: Exp) = this.exp = exp
}

/**
 * `InitStm` represents the return statement in the language
 *
 * Example:
 * {{{
 * return 8;
 * }}}
 */
case class ReturnStm(private val exps: ArrayBuffer[Exp]) extends Stm {
  def getExps = exps
}

/**
 * `WhileStm` represents an while statement in the language
 *
 * Example:
 * {{{
 * while (true) {}
 * }}}
 */
case class WhileStm(private var check: Exp, private var body: Stm) extends Stm {
  def getCheck = check
  def setCheck(check: Exp) = this.check = check
  def getBody = body
  def setBody(body: Stm) = this.body = body
}

/*
 * ================================= Definitions ==================================
 */

object Def {
  def unapply(d: Def): Option[Name] = d match {
    case FieldDef(_, name) => Some(name)
    case f: FunDef         => Some(f.getName)
    case t: TyDef          => Some(t.getName)
  }
}

/**
 * `Def` is the base trait of all definitions in the language.
 *
 * @see [[de.thm.mni.cg.ir.FieldDef]]
 * @see [[de.thm.mni.cg.ir.FunDef]]
 * @see [[de.thm.mni.cg.ir.TyDef]]
 */
sealed trait Def extends Stm

object FieldDef {
  def unapply(d: FieldDef): Option[(TypeSymbol, Name)] = d match {
    case ParDef(symbol, name)    => Some((symbol, name))
    case VarDef(_, symbol, name) => Some((symbol, name))
  }
}

/**
 * `FieldDef` is the base trait of all field-definitions
 * (function parameter or variables) in the language.
 *
 * @see [[de.thm.mni.cg.ir.ParDef]]
 * @see [[de.thm.mni.cg.ir.VarDef]]
 */
sealed trait FieldDef extends Def {
  protected var symbol: TypeSymbol
  protected var name: Name

  def getTypeSymbol = symbol
  def setTypeSymbol(symbol: TypeSymbol) = this.symbol = symbol
  def getName = name
  def setName(name: Name) = this.name = name
}

/** `ParDef` represents the definition of a function-parameter  in the language */
case class ParDef(override protected var symbol: TypeSymbol, override protected var name: Name) extends FieldDef

/**
 * `VarDef` represents the definition of a variable in the language
 *
 * Examples:
 * {{{
 * const int n = 100;
 * int i = 0;
 * }}}
 */
case class VarDef(private var const: Boolean, override protected var symbol: TypeSymbol, override protected var name: Name) extends FieldDef {
  def isConst = const
  def setConst(const: Boolean) = this.const = const
}

/**
 * `FunDef` represents the definition of a function in the language
 *
 * Examples:
 * {{{
 * int factorial(int n)
 *     if (n <= 1) return 1;
 *     else n * factorial(n - 1);
 *
 * int, int divMod(int a, int b) {
 *     return a / b, a % b;
 * }
 * }}}
 */
case class FunDef(private var name: Name, private val out: ArrayBuffer[TypeSymbol], private val params: ArrayBuffer[ParDef], private var body: Stm, private var local: SymbolTable) extends Def {
  def getName = name
  def setName(name: Name) = this.name = name
  def getOut = out
  def getParams = params
  def getBody = body
  def setBody(body: Stm) = this.body = body
  def getLocalTable = local
  def setLocalTable(table: SymbolTable) = local = table
}

/**
 * `TyDef` represents the definition of a type in the language
 *
 * Examples:
 * {{{
 * type void = unit; // typealias void for unit
 *
 * type Point = struct { int x; int y; };
 *
 * type IntMatrix = int[][];
 * }}}
 */
case class TyDef(private var name: Name, private var symbol: TypeSymbol) extends Def {
  def getName = name
  def setName(name: Name) = this.name = name
  def getTypeSymbol = symbol
  def setTypeSymbol(symbol: TypeSymbol) = this.symbol = symbol
}

/*
 * =================================== Symbols ====================================
 */

/**
 * `TypeSymbol` is the base trait of all symbols representing types in the language.
 *
 * @see [[de.thm.mni.cg.ir.ArrayTypeSymbol]]
 * @see [[de.thm.mni.cg.ir.NameTypeSymbol]]
 * @see [[de.thm.mni.cg.ir.ComplexTypeSymbol]]
 */
sealed trait TypeSymbol extends Ast

/**
 * `ArrayTypeSymbol` represents an array symbol in the language
 *
 * Examples:
 * {{{
 * type IntMatrix = int[][];
 *                  ^^^^^^
 * }}}
 */
case class ArrayTypeSymbol(private var base: TypeSymbol) extends TypeSymbol {
  def getBase = base
  def setBase(base: TypeSymbol) = this.base = base
}

/**
 * `NameTypeSymbol` represents a named type in the language
 *
 * Examples:
 * {{{
 * type Number = int;
 *               ^^^
 * }}}
 */
case class NameTypeSymbol(private var name: Name) extends TypeSymbol {
  def getName = name
  def setName(name: Name) = this.name = name
}

object ComplexTypeSymbol {
  /** returns the name, the components and if the complex symbol is a union (true) or a struct (false) */
  def unapply(d: ComplexTypeSymbol): Option[(ArrayBuffer[Component], Boolean)] = d match {
    case UnionTypeSymbol(components)  => Some((components, true))
    case StructTypeSymbol(components) => Some((components, false))
  }
}

/**
 * `ComplexTypeSymbol` is the base trait of all symbols representing complex types
 * (structs and unions) in the language.
 *
 * @see [[de.thm.mni.cg.ir.StructTypeSymbol]]
 * @see [[de.thm.mni.cg.ir.UnionTypeSymbol]]
 */
sealed trait ComplexTypeSymbol extends TypeSymbol {
  protected val members: ArrayBuffer[Component]

  def getMembers: ArrayBuffer[Component] = members
}

/**
 * `Component` is a helper class used in `ComplexTypeSymbol` to descibe a
 * member of a struct or a union
 *
 * @see [[de.thm.mni.cg.ir.StructTypeSymbol]]
 * @see [[de.thm.mni.cg.ir.UnionTypeSymbol]]
 */
case class Component(symbol: TypeSymbol, name: Name) extends Pos

/**
 * `StructTypeSymbol` represents a struct in the language
 *
 * Examples:
 * {{{
 * type Point = struct { int x; int y; };
 *              ^^^^^^^^^^^^^^^^^^^^^^^
 * }}}
 */
case class StructTypeSymbol(protected val members: ArrayBuffer[Component]) extends ComplexTypeSymbol

/**
 * `UnionTypeSymbol` represents a union in the language
 *
 * Examples:
 * {{{
 * type Either = union { int x; int y; };
 *               ^^^^^^^^^^^^^^^^^^^^^^
 * }}}
 */
case class UnionTypeSymbol(protected val members: ArrayBuffer[Component]) extends ComplexTypeSymbol

/*
 * ================================= Expressions ==================================
 */

/**
 * `Exp` is the base trait of all expressions in the language.
 *
 * @see [[de.thm.mni.cg.ir.BinExp]]
 * @see [[de.thm.mni.cg.ir.CastExp]]
 * @see [[de.thm.mni.cg.ir.FunExp]]
 * @see [[de.thm.mni.cg.ir.NewArrExp]]
 * @see [[de.thm.mni.cg.ir.NewObjExp]]
 * @see [[de.thm.mni.cg.ir.UnaryExp]]
 */
sealed trait Exp extends Ast {
  protected var ty: Ty
  def getTy = ty
  def setTy(ty: Ty) = this.ty = ty
}

/**
 * `BinExp` represents a binary expression in the language
 *
 * Examples:
 * {{{
 * 8 < 9
 * true != false
 * }}}
 */
case class BinExp(private var op: BinOp, private var left: Exp, private var right: Exp, protected var ty: Ty = null) extends Exp {
  def getOp = op
  def setOp(op: BinOp) = this.op = op
  def getLeft = left
  def setLeft(left: Exp) = this.left = left
  def getRight = right
  def setRight(right: Exp) = this.right = right
}

/**
 * `CastExp` represents a type cast in the language
 *
 * Examples:
 * {{{
 * (int) 'a'
 * (char) 97
 * }}}
 */
case class CastExp(private var symbol: NameTypeSymbol, private var exp: Exp, protected var ty: Ty = null) extends Exp {
  def getTypeSymbol = symbol
  def setTypeSymbol(symbol: NameTypeSymbol) = this.symbol = symbol
  def getExp = exp
  def setExp(exp: Exp) = this.exp = exp
}

/**
 * `FunExp` represents a function call in the language
 *
 * Examples:
 * {{{
 * test();
 * factorial(1 + 2 + 3);
 * }}}
 */
case class FunExp(private var name: Name, private val args: ArrayBuffer[Exp], protected var ty: Ty = null) extends Exp {
  def getName = name
  def setName(name: Name) = this.name = name
  def getArgs = args
}

/**
 * `FunExp` represents a allocation of a new, empty array in the language
 *
 * Examples:
 * {{{
 * new int[50]
 * new char[1]
 * }}}
 */
case class NewArrExp(private var symbol: NameTypeSymbol, private val dimensions: ArrayBuffer[Option[Exp]], protected var ty: Ty = null) extends Exp {
  def getTypeSymbol = symbol
  def setTypeSymbol(symbol: NameTypeSymbol) = this.symbol = symbol
  def getDimensions = dimensions
}

/**
 * `NewObjExp` represents a allocation of a new struct or union instance in the language
 *
 * Examples:
 * {{{
 * new Point { x = 0; y = 8; };
 * new Either { x = 0; };
 * }}}
 */
case class NewObjExp(private var name: Name, private val args: ArrayBuffer[(Name, Exp)], protected var ty: Ty = null) extends Exp {
  def getName = name
  def setName(name: Name) = this.name = name
  def getArgs = args
}

/**
 * `UnaryExp` represents a unary expression in the language
 *
 * Examples:
 * {{{
 * -7
 * #arr
 * }}}
 */
case class UnaryExp(private var op: UnaryOp, private var exp: Exp, protected var ty: Ty = null) extends Exp {
  def getOp = op
  def setOp(op: UnaryOp) = this.op = op
  def getExp = exp
  def setExp(exp: Exp) = this.exp = exp
}

/*
 * =================================== Literals ===================================
 */

/**
 * `Lit` is the base trait of all literals in the language.
 *
 * @see [[de.thm.mni.cg.ir.BoolLit]]
 * @see [[de.thm.mni.cg.ir.CharLit]]
 * @see [[de.thm.mni.cg.ir.IntLit]]
 * @see [[de.thm.mni.cg.ir.NullLit]]
 * @see [[de.thm.mni.cg.ir.StringLit]]
 */
sealed trait Lit extends Exp

/** `BoolLit` represents boolean literals (false, true) */
case class BoolLit(private val value: Boolean, protected var ty: Ty = Ty.boolTy) extends Lit {
  def getValue = value
}

/**
 * `CharLit` represents character literals
 * Examples:
 * {{{
 * 'a'
 * '\''
 * '!'
 * }}}
 */
case class CharLit(private val value: Char, protected var ty: Ty = Ty.charTy) extends Lit {
  def getValue = value
  override final def toString = s"CharLit('${escape(value, true)}', $ty)"
}

/**
 * `IntLit` represents integer literals
 *
 * Examples:
 * {{{
 * 0
 * 42
 * 100
 * 0xFF
 * 0b101
 * }}}
 */
case class IntLit(private val value: Int, private val numberSystem: NumberSystem = NumberSystem.Decimal, protected var ty: Ty = Ty.intTy) extends Lit {
  def getValue = value
  def getNumberSystem = numberSystem
}

/** `NullLit` represents the null reference */
case class NullLit(protected var ty: Ty = Ty.nullTy) extends Lit

/**
 * `StringLit` represents string literals
 * Example:
 * {{{
 * "Hello, World!"
 * }}}
 */
case class StringLit(private val value: String, protected var ty: Ty = Ty.stringTy) extends Lit {
  def getValue = value
  override final def toString = s"""StringLit("${escape(value)}", $ty)"""
}

/*
 * ================================== Variables ===================================
 */

/**
 * `Var` is the base trait of all references in the language.
 *
 * @see [[de.thm.mni.cg.ir.AccessVar]]
 * @see [[de.thm.mni.cg.ir.ArrayVar]]
 * @see [[de.thm.mni.cg.ir.NameVar]]
 */
sealed trait Var extends Exp

/**
 * `AccessVar` represents the implicit reference denoted by the access of a struct/union member
 *
 * Examples:
 * {{{
 * p.x
 * list.head
 * }}}
 */
case class AccessVar(private var base: Exp, private var member: Name, protected var ty: Ty = null) extends Var {
  def getBase = base
  def setBase(base: Exp) = this.base = base
  def getMember = member
  def setMember(member: Name) = this.member = member
}

/**
 * `ArrayVar` represents the implicit reference denoted by the access of an index of a array
 *
 * Examples:
 * {{{
 * arr[0]
 * points[1]
 * }}}
 */
case class ArrayVar(private var base: Exp, private var index: Exp, protected var ty: Ty = null) extends Var {
  def getBase = base
  def setBase(base: Exp) = this.base = base
  def getIndex = index
  def setIndex(index: Exp) = this.index = index
}

/**
 * `NameVar` represents the implicit reference denoted by a variable
 *
 * Examples:
 * {{{
 * a
 * b
 * c
 * }}}
 */
case class NameVar(private var name: Name, protected var ty: Ty = null) extends Var {
  def getName = name
  def setName(name: Name) = this.name = name
}

/*
 * ================================== Operators ===================================
 */

/**
 * `Op` is the general superclass for binary and unary operators.
 *
 * @see [[de.thm.mni.cg.ir.BinOp]]
 * @see [[de.thm.mni.cg.ir.UnaryOp]]
 */
sealed abstract class Op

object Op {

  /**
   * Definition of an instance of [[scala.math.Ordering]] (a special subtype of
   * [[java.util.Comparator]]), which defines the precedence for the operators.
   *
   * see Language-Specification, chapter 5
   */
  lazy val precedence: Ordering[Op] = Ordering by {
    case _: UnaryOp        => 10
    case Mul | Div | Mod   => 9
    case Add | Sub         => 8
    case Sll | Slr | Sar   => 7
    case Lt | Le | Gt | Ge => 6
    case Eq | Ne           => 5
    case Band              => 4
    case Xor               => 3
    case Bor               => 2
    case And               => 1
    case Or                => 0
  }

}

/**
 * `BinOp` represents a binary operator in the language. Used in BinExp.
 *
 * @see [[de.thm.mni.cg.ir.BinExp]]
 */
sealed abstract class BinOp(val token: String) extends Op
case object Add extends BinOp("+")
case object Div extends BinOp("/")
case object Mod extends BinOp("%")
case object Mul extends BinOp("*")
case object Sub extends BinOp("-")

case object Band extends BinOp("&")
case object Bor extends BinOp("|")
case object Sar extends BinOp(">>>")
case object Sll extends BinOp("<<")
case object Slr extends BinOp(">>")
case object Xor extends BinOp("^")

case object And extends BinOp("&&")
case object Or extends BinOp("||")

case object Eq extends BinOp("==")
case object Ge extends BinOp(">=")
case object Gt extends BinOp(">")
case object Le extends BinOp("<=")
case object Lt extends BinOp("<")
case object Ne extends BinOp("!=")

/**
 * `BinOp` represents a unary operator in the language. Used in UnaryExp.
 *
 * @see [[de.thm.mni.cg.ir.UnaryExp]]
 */
sealed abstract class UnaryOp(val token: String) extends Op
case object Bnot extends UnaryOp("~")
case object Minus extends UnaryOp("-")
case object Not extends UnaryOp("!")
case object Size extends UnaryOp("#")
