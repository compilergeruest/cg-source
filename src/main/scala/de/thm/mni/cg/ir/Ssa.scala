package de.thm.mni.cg.ir

import scala.collection.mutable.ArrayBuffer
import scala.collection.mutable.Queue
import scala.collection.mutable.{ Map => MMap }
import scala.collection.mutable.{ Set => MSet }
import de.thm.mni.cg.table.FunEntry
import de.thm.mni.cg.util._
import de.thm.mni.cg.ir.ssa._

object Ssa extends AnyRef
    with SsaConstruction
    with SsaDominance
    with SsaRenaming
    with SsaReconstruction {

  /**
   * Constructs a semi-pruned ssa form of the given program.
   * The produced ssa form already calculated all dominance
   * sets.
   */
  def apply(function: Tac.TacFunction): Ssa = {
    val result = generateSsa(function)
    val allNodes = collectAllNodes(result)
    calculateDominators(result)
    calculateDominanceFrontiers(result, allNodes)
    insertPhiFunctions(result, allNodes)
    rename(result)
    result
  }

    /**
   * Collect all basic blocks from `ssa`, which are reachable via successors, into a mutable set
   * invariant: `ssa` has to be the entry node of the Ssa-graph
   */
  def collectAllNodes(ssa: Ssa): MSet[Ssa] = {
    val result = MSet[Ssa]()
    val worklist = Queue(ssa)
    while (worklist.nonEmpty) {
      val current = worklist.dequeue()
      result += current
      worklist ++= current.getSuccessors.filterNot(result.contains)
    }
    result
  }

  /**
   * Collects all variables that occur in the given set of basic blocks, where
   * phi-functions will be ignored. Furthermore each kill will be mapped to the
   * basic block where the kill occurred.
   */
  private def collectAllVariables(allNodes: MSet[Ssa]): (MSet[Temporary], MMap[Temporary, MSet[Ssa]]) = {
    val result = MSet[Temporary]()
    val mapping = MMap[Temporary, MSet[Ssa]]()
    for (node <- allNodes) {
      val block = node.asInstanceOf[BasicBlock]
      for (i <- block.instructions) {
        val kills = i.getKills
        for (kill <- kills) {
          result += kill
          if (!mapping.contains(kill)) {
            mapping(kill) = MSet(block)
          } else {
            mapping(kill) += block
          }
        }
        result ++= i.getUsages
      }
    }
    (result, mapping)
  }

  /**
   * Insert phi functions into `ssa` and return the global names
   * algorithm from Engineering a compiler, by Cooper and Torczon p. 501
   * invariant: `ssa` has to be the entry node of the Ssa-graph
   */
  private def insertPhiFunctions(ssa: Ssa, allNodes: MSet[Ssa]): Unit = {
    val globals = MSet[Temporary]()
    val blocks = MMap[Temporary, MSet[Ssa]]() // contains all blocks which defines `Temporary`

    for (block <- allNodes) {
      val kill = MSet[Temporary]()
      for (instr <- block.asInstanceOf[BasicBlock].instructions) {
        val kills = instr.getKills
        globals ++= instr.getUsages.filterNot(kill.contains)
        kill ++= kills
        for (k <- kills) {
          if (!blocks.contains(k)) {
            blocks(k) = MSet()
          }
          blocks(k) += block
        }
      }
    }

    for (name <- globals) if (blocks.contains(name)) {
      val dfplus = Ssa.getIteratedDominanceFrontier(blocks(name))
      for (dfblock <- dfplus) {
        val block = dfblock.asInstanceOf[BasicBlock]
        if (!block.phiFunctionExists(name)) {
          val phi = PhiFunction(name, ArrayBuffer.fill(block.getPredeccessors.size)(name))
          block.addPhiFunction(phi)
        }
      }
    }
  }

}

sealed trait Ssa {

  private val dominanceFrontier = MSet[Ssa]()
  private val dominatorTreeSuccessors = MSet[Ssa]()
  private var idom: Option[Ssa] = None

  private val successors = ArrayBuffer[Ssa]()
  private val predecessors = ArrayBuffer[Ssa]()

  private val liveIn = MSet[Temporary]()
  /** the live-in set of this Ssa node */
  def getLiveInSet = liveIn

  private val liveOut = MSet[Temporary]()
  /** the live-out set of this Ssa node */
  def getLiveOutSet = liveOut

  def getImmediateDominator = idom
  def setImmediateDominator(idom: Ssa) = this.idom = Option(idom)
  def setImmediateDominator(idom: Option[Ssa]) = this.idom = idom

  def getDominanceFrontiers = dominanceFrontier

  protected val funEntry: FunEntry

  def getAssociatedFunEntry = funEntry

  def getDominatorTreeSuccessors: MSet[Ssa] = dominatorTreeSuccessors

  /**
   * Returns the predecessors of this node
   */
  def getPredeccessors: IndexedSeq[Ssa] = predecessors

  /**
   * Returns the successors of this node
   */
  def getSuccessors: IndexedSeq[Ssa] = successors

  /**
   * Adds the successors `ssa`, and adds `this` to ssa's predecessors
   */
  def addSuccessor(ssa: Ssa): Unit = {
    successors += ssa
    ssa.predecessors += this
  }

  /**
   * Adds the predecessor `ssa`, and adds `this` to ssa's successors
   */
  def addPredecessors(ssa: Ssa): Unit = {
    predecessors += ssa
    ssa.successors += this
  }

  /**
   * Removes the predecessor `ssa`, and removes this as successor of `ssa`
   */
  def removePredecessor(ssa: Ssa): Unit = {
    predecessors -= ssa
    ssa.successors -= this
  }

  /**
   * Removes the successor `ssa`, and removes this as predecessor of `ssa`
   */
  def removeSuccessor(ssa: Ssa): Unit = {
    successors -= ssa
    ssa.predecessors -= this
  }

  private[ir] lazy val id =
    "0x" + Integer.toHexString(hashCode)

  override final def hashCode: Int = System.identityHashCode(this)

  override final def equals(x: Any): Boolean = x match {
    case s: Ssa => id == s.id
    case _      => false
  }

  /**
   * Returns an iterator over the dominator tree
   */
  def domTreeNodes: Iterator[BasicBlock] = new Iterator[BasicBlock] {
    private val worklist = Queue(Ssa.this)
    override def hasNext = worklist.nonEmpty
    override def next = {
      val theNext = worklist.dequeue().asInstanceOf[BasicBlock]
      worklist ++= theNext.getDominatorTreeSuccessors
      theNext
    }
  }

}

case class BasicBlock(funEntry: FunEntry, phis: ArrayBuffer[PhiFunction], instructions: ArrayBuffer[Tac]) extends Ssa {

  def instrOrPhiAt(index: Int): Either[PhiFunction, Tac] =
    if (index >= phis.size) {
      Right(instructions(index - phis.size))
    } else {
      Left(phis(index))
    }

  lazy val phiMatrix: PhiMatrix = PhiMatrix(phis)

  private val phiSet = MSet[Temporary]()
  def addPhiFunction(phi: PhiFunction): Unit = {
    phis += phi
    phiSet += phi.dst
  }

  def phiFunctionExists(temporary: Temporary) =
    phiSet.contains(temporary)

  def wholeSize = phis.size + instructions.size

  def phiInstrs: ArrayBuffer[Either[PhiFunction, Tac]] =
    (0 until wholeSize).map(instrOrPhiAt).to[ArrayBuffer]

}

object Subscription {
  def apply(temporary: Temporary, subscript: Int): Temporary with Subscription =
    new Temporary(temporary.prefix, temporary.n) with Subscription {
      val subscription = subscript
    }
}
sealed trait Subscription extends AnyRef { self: Temporary =>
  val subscription: Int

  override final def equals(any: Any): Boolean =
    any match {
      case s: Temporary with Subscription => s.subscription == subscription && super.equals(any)
      case _                              => false
    }

  override final def toString(): String =
    prefix + n + "." + subscription + (if (hasRegister) " @ " + getRegister.codeRepresentation else "")

  override final def hashCode(): Int =
    subscription * 31 + super.hashCode()

  def dropSubscription: Temporary =
    Temporary(prefix, n)
}

/**
 * Representation of a phi-matrix described by Sebastian Hack
 * Source: Register Allocation for Programs in SSA Form - S. Hack http://d-nb.info/986273813/34
 * Example:
 * {{{
 *                       x11 x12 ... x1m
 * (y1, y2, ..., yn) =  ...          ...
 *                      xn1 xn2 ... xnm
 * }}}
 * Where each row `n` on the right side represents the tuple of a parallel
 * assignment to the left side. Each column represents the argument-tuple of
 * phi-function `m`. It is required that the assignment to the left-hand-side
 * is executed in parallel to avoid the lost-copy-problem.
 */
case class PhiMatrix(private val phis: IndexedSeq[PhiFunction]) {

  def getAssignmentForPredecessor(predecessor: Int): IndexedSeq[Temporary] =
    phis.map(_.src(predecessor))

  def getOperationForPredecessor(predecessor: Int): (IndexedSeq[Temporary], IndexedSeq[Temporary]) =
    (getLeftHandSide, getAssignmentForPredecessor(predecessor))

  def getLeftHandSide = phis.map(_.dst)

  def isSpilled =
    phis.forall(_.spilled)

  def setSpilled(spilled: Boolean) =
    phis.foreach(_.spilled = spilled)

}

object PhiFunction {
  def apply(dst: Temporary, src: Temporary*) = new PhiFunction(dst, src: _*)
}

case class PhiFunction(private[ir] var dst: Temporary, src: ArrayBuffer[Temporary]) {
  def this(dst: Temporary, src: Temporary*) = this(dst, src.to[ArrayBuffer])

  private[ir] var spilled = false
  /** Whether or not this phi function resides completely in memory */
  def isPhiSpilled = spilled

  def renameArgument(index: Int, name: Temporary): Unit =
    src(index) = name

  def replaceDefinitions(old: Temporary, n: Temporary): Unit =
    if (dst == old) dst = n

  def getDestination = dst

  final override def toString = s"$dst <- Φ${src.mkString("(", ", ", ")")}"
}
