package de.thm.mni.cg.ir.ssa

import de.thm.mni.cg.ir.Ssa

import scala.collection.mutable.{ Map => MMap }
import scala.collection.mutable.{ Set => MSet }
import scala.collection.mutable.Stack
import scala.collection.mutable.Queue

private[ir] trait SsaDominance {

  /**
   * Calculate the dominance sets of all nodes
   * Lengauer-Tarjan algorithm from Modern compiler implementation in Java, by Appel p. 414
   *
   * invariant: `ssa` has to be the entry node of the Ssa-graph
   */
  def calculateDominators(ssa: Ssa): Unit = {
    val vertex = MMap[Int, Ssa]()
    val bucket = MMap[Ssa, MSet[Ssa]]()
    val dfnum = MMap[Ssa, Int]()
    val semi = MMap[Ssa, Ssa]()
    val ancestor = MMap[Ssa, Ssa]()
    val idom = MMap[Ssa, Ssa]()
    val samedom = MMap[Ssa, Ssa]()
    val best = MMap[Ssa, Ssa]()
    val parent = MMap[Ssa, Ssa]()
    var N = 0

    val dfswl = Stack[Queue[(Ssa, Ssa)]]()
    val dfsseen = MSet[Ssa]()

    def dfs(p: Ssa, n: Ssa): Unit = if (!dfsseen.contains(n)) {
      dfsseen += n
      if (!(dfnum contains n)) dfnum(n) = 0
      if (dfnum(n) == 0) {
        dfnum(n) = N
        vertex(N) = n
        parent(n) = p
        N += 1

        val xs = n.getSuccessors.map(w => (n, w))
        // simulate recursive call, by pushing queue of actions onto stack `dfswl`
        dfswl push xs.to[Queue]
      }
    }

    def ancestorWithLowestSemi(v : Ssa): Ssa = {
      if (!(ancestor contains v)) ancestor(v) = null
      var a = ancestor(v)
      if (a != null && (ancestor contains a)) {
        var b = ancestorWithLowestSemi(a)
        if (!ancestor.contains(a)) ancestor(a) = null
        ancestor(v) = ancestor(a)
        if (!semi.contains(b)) semi(b) = null
        val semib = semi(b)
        if (!best.contains(v)) best(v) = null
        if (!semi.contains(best(v))) semi(best(v)) = null
        val semibv = semi(best(v))
        if (!dfnum.contains(semib)) dfnum(semib) = 0
        if (!dfnum.contains(semibv)) dfnum(semibv) = 0
        if (dfnum(semi(b)) < dfnum(semibv)) {
          best(v) = b
        }
      }
      best(v)
    }

    def link(p: Ssa, n: Ssa): Unit = {
      ancestor(n) = p
      best(n) = n
    }

    // "initial function call of simulated recursion"
    dfswl push Queue((null, ssa))

    // "recursive" dfs
    /*
     * if dfs stack is not empty
     *  if n has successor
     *    calculate step (which may push new stack frame)
     *  else
     *    destroy stackframe
     */
    /*
     * only tos is stepped in the calculation
     */
    while (dfswl.nonEmpty) {
      val children = dfswl.top
      if (children.nonEmpty) {
        val (p, n) = children.dequeue()
        dfs(p, n)
      } else {
        dfswl.pop()
      }
    }

    for (i <- N - 1 until 0 by -1) {
      val n = vertex(i)
      val p = parent(n)
      var s = p
      var s2: Ssa = null
      for (v <- n.getPredeccessors) {
        if (!dfnum.contains(v)) dfnum(v) = 0
        if (!dfnum.contains(n)) dfnum(n) = 0
        if (dfnum(v) <= dfnum(n)) {
          s2 = v
        } else {
          val ancestor = ancestorWithLowestSemi(v)
          if (!semi.contains(ancestor)) semi(ancestor) = null
          s2 = semi(ancestor)
        }
        if (!dfnum.contains(s2)) dfnum(s2) = 0
        if (!dfnum.contains(s)) dfnum(s) = 0
        if (dfnum(s2) < dfnum(s)) {
          s = s2
        }
      }
      if (!semi.contains(n)) semi(n) = null
      semi(n) = s
      if (!bucket.contains(s)) bucket(s) = MSet()
      bucket(s) += n
      link(p, n)
      if (!bucket.contains(p)) bucket(p) = MSet()
      for (v <- bucket(p)) {
        val y = ancestorWithLowestSemi(v)
        if (!semi.contains(y)) semi(y) = null
        if (!semi.contains(v)) semi(v) = null
        if (semi(y) == semi(v)) {
          idom(v) = p
          v.setImmediateDominator(Option(p))
          v.getImmediateDominator.foreach { domPred =>
            domPred.getDominatorTreeSuccessors += v
          }
        } else {
          samedom(v) = y
        }
      }
      bucket(p) = MSet()
    }
    for (i <- 1 until N) {
      val n = vertex(i)
      if (!samedom.contains(n)) samedom(n) = null
      val sdn = samedom(n)
      if (sdn != null) {
        if (!idom.contains(sdn)) idom(sdn) = null
        idom(n) = idom(sdn)
        n.setImmediateDominator(Option(idom(sdn)))
        n.getImmediateDominator.foreach { domPred =>
          domPred.getDominatorTreeSuccessors += n
        }
      }
    }

  }

  /**
   * Calculates the iterated dominance frontier (DF+) for the given set of
   * blocks that kill a temporary `t`. The iterated dominance frontier set
   * is the set which contains all blocks which need to define a phi-function
   * for `t`.
   *
   * Source: http://www.cse.iitd.ernet.in/~nvkrishna/courses/winter07/ssa.pdf p. 10
   *
   * prerequisite: The dominance frontier set is already calculated
   */
  def getIteratedDominanceFrontier(blocks: Traversable[Ssa]): MSet[Ssa] = {
    val worklist = Queue[Ssa]()
    val dfplus = MSet[Ssa]()
    worklist ++= blocks
    while (worklist.nonEmpty) {
      val ssa = worklist.dequeue()
      for (df <- ssa.getDominanceFrontiers) {
        if (!dfplus.contains(df)) {
          dfplus += df
          worklist += df
        }
      }
    }
    dfplus
  }

  /**
   * Calculate the dominance frontiers of `ssa`
   * algorithm from Engineering a compiler, by Cooper and Torczon p. 499
   *
   * invariant: `ssa` has to be the entry node of the Ssa-graph
   * prerequisite: the dominators have to be calculated already
   */
  def calculateDominanceFrontiers(ssa: Ssa, allNodes: MSet[Ssa]): Unit = {
    for (node <- allNodes) {
      node.getDominanceFrontiers.clear()
    }
    for (node <- allNodes) {
      if (node.getPredeccessors.size > 1) {
        for (pred <- node.getPredeccessors) {
          var runner = pred
          while (runner != node.getImmediateDominator.getOrElse(null) && runner != null) {
            runner.getDominanceFrontiers += node
            runner = runner.getImmediateDominator.getOrElse(null)
          }
        }
      }
    }
  }

}
