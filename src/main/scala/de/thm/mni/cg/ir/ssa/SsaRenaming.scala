package de.thm.mni.cg.ir.ssa

import java.util.{ Stack => JStack }
import scala.collection.mutable.{ Map => MMap }

import de.thm.mni.cg.ir._

private[ir] trait SsaRenaming {

  /**
   * algorithm from Modern Compiler Implementation in Java 2nd Edition p. 409, by Appel
   *
   * invariant: `ssa` has to be the entry node of the Ssa-graph
   *
   * prerequisite: the dominators and the dominance frontier have to be calculated already
   */
  def rename(ssa: Ssa): Unit = {
    val count = MMap[Temporary, Int]()
    val stack = MMap[Temporary, JStack[Int]]()

    // An algebraic data type for the simulated stack behaviour
    sealed trait RenamingTask
    case class CleanupBlock(block: BasicBlock) extends RenamingTask
    case class RenameBlock(block: BasicBlock) extends RenamingTask

    // simulated stack to prevent stack overflow when compiling large programs
    val worklist = new JStack[RenamingTask]()
    worklist.push(RenameBlock(ssa.asInstanceOf[BasicBlock]))
    while (!worklist.isEmpty()) {
      worklist.pop() match {

        case CleanupBlock(n) =>
          var i = 0
          while (i < n.wholeSize) {
            n.instrOrPhiAt(i).fold(
              phi => stack.getOrElseUpdate(phi.getDestination, Stack(0)).pop(),
              ins => ins.getKills.foreach(stack.getOrElseUpdate(_, Stack(0)).pop()))
            i += 1
          }

        case RenameBlock(n) =>
          // for each statement `s` in block `n`
          var i = 0
          while (i < n.wholeSize) {
            val instruction = n.instrOrPhiAt(i)
            val phis = n.phis
            // if `s` is not a phi function
            if (instruction.isRight) {
              val ins = instruction.right.get
              val uses = ins.getUsages
              // for each use of some variable `x` in `s`
              for (use <- uses) {
                val subscript = stack.getOrElseUpdate(use, Stack(0)).peek()
                val subscription = Subscription(use, subscript)
                // replace use of `x` with `xi` in `s`
                ins.replaceUsages(use, subscription)
              }
            }

            // for each definition of some variable `a` in `s`
            val defs = instruction.fold(phi => Set(phi.getDestination), _.getKills)
            for (a <- defs) {
              val subscript = count.getOrElseUpdate(a, 0) + 1
              count(a) += 1
              val subscription = Subscription(a, subscript)
              stack.getOrElseUpdate(a, Stack(0)) push subscript
              // replace the definition of `a` with `ai` in `s`
              instruction.fold(_.replaceDefinitions(a, subscription), _.replaceDefinitions(a, subscription))
            }
            i += 1
          }

          // for each successor `y` of `n`
          for (successor <- n.getSuccessors) {
            val y = successor.asInstanceOf[BasicBlock]
            // suppose `n` is the `j`th predecessor of `y`
            val j = y.getPredeccessors.indexWhere(_ eq n)
            // for each phi function in `y`
            for (phi <- y.phis) {
              // suppose the `j`th operand of this phi function is `phij`
              val phij = phi.src(j)
              val subscript = stack.getOrElseUpdate(phij, Stack(0)).peek()
              val subscription = Subscription(phij, subscript)
              phi.src(j) = subscription
            }
          }
          // for each statement `s` in block `n` and for each definition of `a` in `s` push clean-up task
          worklist push CleanupBlock(n)

          // for each child `x` of `n` push rename task
          val dominatorRenameTasks = n.getDominatorTreeSuccessors.map(block => RenameBlock(block.asInstanceOf[BasicBlock]))
          for (x <- dominatorRenameTasks) {
            worklist.push(x)
          }
      }
    }

  }

  /** Create a singleton stack with element `n` */
  private def Stack(n: Int): JStack[Int] = {
    val stack = new JStack[Int]()
    stack.push(n)
    stack
  }

}
