package de.thm.mni.cg.ir.ssa

import scala.collection.mutable.{ Map => MMap }
import scala.collection.mutable.{ Set => MSet }
import scala.collection.mutable.ArrayBuffer
import de.thm.mni.cg.ir._
import de.thm.mni.cg.util.Incremental
import de.thm.mni.cg.language.ErrorMessages._

/**
 * Source: SSA-based Compiler Design p. 64
 */
private[ir] trait SsaReconstruction {
  /**
   * Type for a single instruction, where the left value is the index
   * of the instruction and where the right value is the block where
   * the instruction resides.
   */
  protected final type Instruction = (Int, BasicBlock)

  /** A set of instructions */
  protected final type InstructionSet = MSet[Instruction]
  /** Constructor for an empty InstructionSet */
  protected final def InstructionSet(): InstructionSet = MSet()

  /** A sequence of instructions */
  protected final type InstructionSeq = ArrayBuffer[Instruction]
  /** Constructor for an empty InstructionSeq */
  protected final def InstructionSeq(): InstructionSeq = ArrayBuffer()

  /**
   * A map, where each temporary that breaks ssa-property to all
   * the definitions in the ssa-program.
   */
  protected final type VarDefMap = MMap[Temporary, InstructionSet]
  /** Constructor for an empty VarDefMap */
  protected final def VarDefMap(): VarDefMap = MMap()

  /**
   * A map, where each temporary that breaks ssa-property is mapped
   * to all its uses in the ssa-form program
   */
  protected final type VarUseMap = MMap[Temporary, InstructionSet]
  /** Constructor for an empty VarUseMap */
  protected final def VarUseMap(): VarUseMap = MMap()

  private implicit val reverseInstructionOrdering = new Ordering[Instruction] {
    def compare(a: Instruction, b: Instruction): Int = b._1 - a._1
  }

  private def insOrPhi[T](fa: PhiFunction => T, fb: Tac => T)(ins: Instruction): T = {
    val (idx, block) = ins
    block.instrOrPhiAt(idx).fold(fa, fb)
  }

  private def isBefore(a: Instruction, b: Instruction): Boolean =
    a._1 < b._1 && (a._2 eq b._2)

  /**
   * Reconstructs the given ssa-form program, where `vdefs` maps each variable
   * which breaks the ssa-property to its definitions in the whole program and
   * where `vuses` maps each variable which breaks the ssa-property to its uses
   * in the whole program. A definition or use is simply the tuple, consisting of
   * the index of the instruction and the block, where the instruction resides.
   *
   * @param ssa The program in ssa-form to reconstruct a tac from
   * @param vdefs variable which breaks the ssa-property mapped to its definitions in the program
   * @param vuses variable which breaks the ssa-property mapped to its usages in the program
   * @param nameProvider name provider, which generates temporary variable names with subscription
   */
  def reconstruct(ssa: Ssa, vdefs: VarDefMap, vuses: VarUseMap, nameProvider: Incremental[Temporary with Subscription]): Unit = {
    def temporary(): Temporary = nameProvider()
    val variables = vdefs.keySet ++ vuses.keySet
    for (v <- variables) {
      driver(v, ssa, vdefs.getOrElse(v, MSet()), vuses.getOrElse(v, MSet()))
    }

    def findDefFromBottom(v: Temporary, b: BasicBlock, vdefs: InstructionSet, vuses: InstructionSet, bdefs: InstructionSeq, rewritten: MSet[Temporary]): Instruction = {
      val bdef = bdefs.filter(_._2 eq b)
      if (bdef.nonEmpty) {
        bdef.last
      } else {
        findDefFromTop(v, b, vuses, vuses, bdefs, rewritten)
      }
    }

    def findDefFromTop(v: Temporary, b: BasicBlock, vdefs: InstructionSet, vuses: InstructionSet, bdefs: InstructionSeq, rewritten: MSet[Temporary]): Instruction = {
      val dfplus = Ssa.getIteratedDominanceFrontier(vdefs.map(_._2))
      if (dfplus contains b) {
        val vs = temporary()
        val d = PhiFunction(vs, ArrayBuffer.fill(b.getPredeccessors.size)(vs))
        val instr: Instruction = ((b.phis.size, b))
        bdefs += instr
        b.addPhiFunction(d)
        var i = 0
        for (p <- b.getPredeccessors) {
          val o = findDefFromBottom(v, p.asInstanceOf[BasicBlock], vdefs, vuses, bdefs, rewritten)
          // v' version of `v` defined by `o`
          val defs = insOrPhi(phi => MSet(phi.getDestination), _.getKills)(o)
          val args = defs.filter(rewritten)
          require(args.size == 1)
          // set argument of phi function at index i
          d.src(i) = args.head
          i += 1
        }
        instr
      } else {
        val idom = b.getImmediateDominator.getOrElse(variableIsLiveInAtRootSsaError(v))
        findDefFromBottom(v, idom.asInstanceOf[BasicBlock], vdefs, vuses, bdefs, rewritten)
      }
    }

    def driver(v: Temporary, ssa: Ssa, vdefs: InstructionSet, vuses: InstructionSet): Unit = {
      val bdefs = InstructionSeq()
      val rewritten = MSet[Temporary]()

      for (instruction <- vdefs) {
        val vs = temporary()
        insOrPhi(_.replaceDefinitions(v, vs), _.replaceDefinitions(v, vs))(instruction)
        rewritten += vs
        bdefs += instruction
      }

      for (instruction @ (index, block) <- vuses) {
        def phi(phi: PhiFunction): Instruction = {
          val index = phi.src.indexOf(v)
          val p = block.getPredeccessors(index)
          findDefFromBottom(v, p.asInstanceOf[BasicBlock], vdefs, vuses, bdefs, rewritten)
        }
        def ins(ins: Tac): Instruction = {
          var d: Instruction = null
          val iterator = bdefs.sorted.iterator
          while (iterator.hasNext && d.eq(null)) {
            val l = iterator.next()
            if (isBefore(l, instruction)) {
              d = l
            }
          }
          if (d eq null) {
            d = findDefFromTop(v, block, vdefs, vuses, bdefs, rewritten)
          }
          d
        }
        val d = insOrPhi(phi, ins)(instruction)
        // v' version of `v` defined by `d`
        // 1. fetch all definitions of the instruction `d`
        val defs = insOrPhi(phi => MSet(phi.getDestination), _.getKills)(d)
        // 2. filter these definitions by all those which were actually rewritten
        val vs = defs.filter(rewritten)
        for (vse <- vs) {
          // 3. replace every use
          insOrPhi({ phi =>
            val index = phi.src.indexOf(v)
            require(index >= 0, phiUsageWithoutUsage)
            phi.src(index) = vse
          }, _.replaceUsages(v, vse))(instruction)
        }
      }
    }
  }

}
