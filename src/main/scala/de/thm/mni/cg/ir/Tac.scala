package de.thm.mni.cg.ir

import scala.collection.mutable.ArrayBuffer
import scala.collection.mutable.{ Set => MSet }
import scala.language.implicitConversions

import de.thm.mni.cg.runtime.Offset
import de.thm.mni.cg.table.FunEntry
import de.thm.mni.cg.table.SymbolTable
import de.thm.mni.cg.util._
import de.thm.mni.cg.compiler.Compiler.{ registerProvider => regs }

/**
 * `Tac` is the base class for all instructions of the
 * three address code.
 */
sealed trait Tac extends Product with Serializable {
  protected val ast: Ast
  def getCorrespondingAst = ast

  private var comment: Option[String] = None
  /** gets the comment corresponding to this tac-node */
  def getComment = comment
  /** sets the comment corresponding to this tac-node */
  def --(comment: String): this.type = { this.comment = Option(comment); this }
  /** sets the comment corresponding to this tac-node */
  def setComment(comment: String): this.type = { this.comment = Option(comment); this }

  /**
   * Return true, if this Tac is either a branch or a label
   * @see [[de.thm.mni.cg.ir.Tac.Beq]]
   * @see [[de.thm.mni.cg.ir.Tac.Bge]]
   * @see [[de.thm.mni.cg.ir.Tac.Bgeu]]
   * @see [[de.thm.mni.cg.ir.Tac.Bgt]]
   * @see [[de.thm.mni.cg.ir.Tac.Bgtu]]
   * @see [[de.thm.mni.cg.ir.Tac.Ble]]
   * @see [[de.thm.mni.cg.ir.Tac.Bleu]]
   * @see [[de.thm.mni.cg.ir.Tac.Blt]]
   * @see [[de.thm.mni.cg.ir.Tac.Bltu]]
   * @see [[de.thm.mni.cg.ir.Tac.Bne]]
   * @see [[de.thm.mni.cg.ir.Tac.Jal]]
   * @see [[de.thm.mni.cg.ir.Tac.Jalr]]
   * @see [[de.thm.mni.cg.ir.Tac.Jmp]]
   * @see [[de.thm.mni.cg.ir.Tac.Jmpr]]
   * @see [[de.thm.mni.cg.ir.Tac.Label]]
   */
  def isBranchOrLabel: Boolean =
    isInstanceOf[Tac.Label] ||
    isInstanceOf[Tac.Beq]   ||
    isInstanceOf[Tac.Bne]   ||
    isInstanceOf[Tac.Bgt]   ||
    isInstanceOf[Tac.Bge]   ||
    isInstanceOf[Tac.Blt]   ||
    isInstanceOf[Tac.Ble]   ||
    isInstanceOf[Tac.Bgtu]  ||
    isInstanceOf[Tac.Bgeu]  ||
    isInstanceOf[Tac.Bltu]  ||
    isInstanceOf[Tac.Bleu]  ||
    isInstanceOf[Tac.Jmp]   ||
    isInstanceOf[Tac.Jmpr]  ||
    isInstanceOf[Tac.Jal]   ||
    isInstanceOf[Tac.Jalr]

  /**
   * Return true, if this Tac is a call
   * @see [[de.thm.mni.cg.ir.Tac.Jal]]
   * @see [[de.thm.mni.cg.ir.Tac.Jalr]]
   */
  def isCall: Boolean =
    isInstanceOf[Tac.Jal] ||
    isInstanceOf[Tac.Jalr]

  /**
   * Return true, if this Tac is a jump
   * @see [[de.thm.mni.cg.ir.Tac.Jmp]]
   * @see [[de.thm.mni.cg.ir.Tac.Jmpr]]
   */
  def isJump: Boolean =
    isInstanceOf[Tac.Jmp] ||
    isInstanceOf[Tac.Jmpr]

  /**
   * Return true, if this Tac is a pseudo instruction
   * Pseudo instruction will only be used by the compiler,
   * they won't be included in the output-program.
   * @see [[de.thm.mni.cg.ir.Tac.Def]]
   * @see [[de.thm.mni.cg.ir.Tac.Nop]]
   * @see [[de.thm.mni.cg.ir.Tac.Use]]
   */
  def isPseudoInstruction: Boolean =
    isInstanceOf[Tac.Def] ||
    isInstanceOf[Tac.Nop] ||
    isInstanceOf[Tac.Use]

  /**
   * Returns a set of names, which this Tac kills
   */
  def getKills: Set[Temporary] = Set()

  /**
   * Returns a set of names, which this Tac gens
   */
  def getUsages: Set[Temporary] = Set()

  /** Replaces all usages of `old` with `n` in this instruction */
  def replaceUsages(old: Temporary, n: Temporary): Unit =
    this match {
      case e: Tac.Swap =>
        if (e.a == old) e.a = n
        if (e.b == old) e.b = n
      case e: Tac.ArithTac =>
        if (e.a == old) e.a = n
        if (e.b == old) e.b = n
      case e: Tac.Use =>
         if (e.tmp == old) e.tmp = n

      case e: Tac.CondBranch =>
        if (e.a == old) e.a = n
        if (e.b == old) e.b = n
      case e: Tac.Jmpr =>
        if (e.a == old) e.a = n
      case e: Tac.Jalr =>
        if (e.a == old) e.a = n

      case e: Tac.LoadStaticAddress =>
        if (e.staticLink == old) e.staticLink = n
      case e: Tac.StoreRegister =>
        if (e.src == old) e.src = n
      case e: Tac.Mov =>
        if (e.src == old) e.src = n

      case e: Tac.MemLoad =>
        if (e.addr == old) e.addr = n
      case e: Tac.MemStore =>
        if (e.addr == old) e.addr = n
        if (e.src == old) e.src = n

      case e =>
    }

  /** Replaces all definitions (kills) of `old` with `n` in this instruction */
  def replaceDefinitions(old: Temporary, n: Temporary): Unit =
    this match {
      case e: Tac.Swap =>
        if (e.a == old) e.a = n
        if (e.b == old) e.b = n
      case e: Tac.ArithTac =>
        if (e.dst == old) e.dst = n
      case e: Tac.Def =>
         if (e.tmp == old) e.tmp = n
      case e: Tac.CalleeReturnAddress =>
        if (e.dst == old) e.dst = n
      case e: Tac.CallerReturnAddress =>
        if (e.dst == old) e.dst = n
      case e: Tac.CalleeArgumentAddress =>
        if (e.dst == old) e.dst = n
      case e: Tac.CallerArgumentAddress =>
        if (e.dst == old) e.dst = n
      case e: Tac.LoadRegister =>
        if (e.dst == old) e.dst = n
      case e: Tac.LoadLocalAddress =>
        if (e.dst == old) e.dst = n
      case e: Tac.LoadStaticAddress =>
        if (e.dst == old) e.dst = n
      case e: Tac.StoreDisplayEntry =>
        if (e.dstA == old) e.dstA = n
        if (e.dstB == old) e.dstB = n
      case e: Tac.LoadDisplayEntry =>
        if (e.dstA == old) e.dstA = n
        if (e.dstB == old) e.dstB = n

      case e: Tac.Mov =>
        if (e.dst == old) e.dst = n
      case e: Tac.MemLoad =>
        if (e.dst == old) e.dst = n

      case e =>
  }

}

object Tac {
  val zeroTemporary = withEffect(Temporary("$", 0))(_.setRegister(regs.zero))
  require(zeroTemporary.toString == "$0 @ $0")

  val assemblerReserved = withEffect(Temporary("$", 1))(_.setRegister(regs.forbiddenRegisters(0)))
  require(assemblerReserved.toString == "$1 @ $1")

  val returnTemporary1 = withEffect(Temporary("$", 2))(_.setRegister(regs.returnValueRegisters(0)))
  require(returnTemporary1.toString == "$2 @ $2")
  val returnTemporary2 = withEffect(Temporary("$", 3))(_.setRegister(regs.returnValueRegisters(1)))
  require(returnTemporary2.toString == "$3 @ $3")

  val argTemporary1 = withEffect(Temporary("$", 4))(_.setRegister(regs.argumentRegisters(0)))
  require(argTemporary1.toString == "$4 @ $4")
  val argTemporary2 = withEffect(Temporary("$", 5))(_.setRegister(regs.argumentRegisters(1)))
  require(argTemporary2.toString == "$5 @ $5")
  val argTemporary3 = withEffect(Temporary("$", 6))(_.setRegister(regs.argumentRegisters(2)))
  require(argTemporary3.toString == "$6 @ $6")
  val argTemporary4 = withEffect(Temporary("$", 7))(_.setRegister(regs.argumentRegisters(3)))
  require(argTemporary4.toString == "$7 @ $7")

  val callerSaved1 = withEffect(Temporary("$", 8))(_.setRegister(regs.temporaryRegisters(0)))
  require(callerSaved1.toString == "$8 @ $8")
  val callerSaved2 = withEffect(Temporary("$", 9))(_.setRegister(regs.temporaryRegisters(1)))
  require(callerSaved2.toString == "$9 @ $9")
  val callerSaved3 = withEffect(Temporary("$", 10))(_.setRegister(regs.temporaryRegisters(2)))
  require(callerSaved3.toString == "$10 @ $10")
  val callerSaved4 = withEffect(Temporary("$", 11))(_.setRegister(regs.temporaryRegisters(3)))
  require(callerSaved4.toString == "$11 @ $11")
  val callerSaved5 = withEffect(Temporary("$", 12))(_.setRegister(regs.temporaryRegisters(4)))
  require(callerSaved5.toString == "$12 @ $12")
  val callerSaved6 = withEffect(Temporary("$", 13))(_.setRegister(regs.temporaryRegisters(5)))
  require(callerSaved6.toString == "$13 @ $13")
  val callerSaved7 = withEffect(Temporary("$", 14))(_.setRegister(regs.temporaryRegisters(6)))
  require(callerSaved7.toString == "$14 @ $14")
  val callerSaved8 = withEffect(Temporary("$", 15))(_.setRegister(regs.temporaryRegisters(7)))
  require(callerSaved8.toString == "$15 @ $15")
  val callerSaved9 = withEffect(Temporary("$", 24))(_.setRegister(regs.temporaryRegisters(8)))
  require(callerSaved9.toString == "$24 @ $24")
  val callerSaved10 = withEffect(Temporary("$", 25))(_.setRegister(regs.temporaryRegisters(9)))
  require(callerSaved10.toString == "$25 @ $25")

  val calleeSaved1 = withEffect(Temporary("$", 16))(_.setRegister(regs.registerVariables(0)))
  require(calleeSaved1.toString == "$16 @ $16")
  val calleeSaved2 = withEffect(Temporary("$", 17))(_.setRegister(regs.registerVariables(1)))
  require(calleeSaved2.toString == "$17 @ $17")
  val calleeSaved3 = withEffect(Temporary("$", 18))(_.setRegister(regs.registerVariables(2)))
  require(calleeSaved3.toString == "$18 @ $18")
  val calleeSaved4 = withEffect(Temporary("$", 19))(_.setRegister(regs.registerVariables(3)))
  require(calleeSaved4.toString == "$19 @ $19")
  val calleeSaved5 = withEffect(Temporary("$", 20))(_.setRegister(regs.registerVariables(4)))
  require(calleeSaved5.toString == "$20 @ $20")
  val calleeSaved6 = withEffect(Temporary("$", 21))(_.setRegister(regs.registerVariables(5)))
  require(calleeSaved6.toString == "$21 @ $21")
  val calleeSaved7 = withEffect(Temporary("$", 22))(_.setRegister(regs.registerVariables(6)))
  require(calleeSaved7.toString == "$22 @ $22")
  val calleeSaved8 = withEffect(Temporary("$", 23))(_.setRegister(regs.registerVariables(7)))
  require(calleeSaved8.toString == "$23 @ $23")

  val kernelReserved1 = withEffect(Temporary("$", 26))(_.setRegister(regs.forbiddenRegisters(1)))
  require(kernelReserved1.toString == "$26 @ $26")
  val kernelReserved2 = withEffect(Temporary("$", 27))(_.setRegister(regs.forbiddenRegisters(2)))
  require(kernelReserved2.toString == "$27 @ $27")
  val kernelReserved3 = withEffect(Temporary("$", 28))(_.setRegister(regs.forbiddenRegisters(3)))
  require(kernelReserved3.toString == "$28 @ $28")

  val spTemporary = withEffect(Temporary("$", 29))(_.setRegister(regs.sp))
  require(spTemporary.toString == "$29 @ $29")
  val irTemporary = withEffect(Temporary("$", 30))(_.setRegister(regs.ir))
  require(irTemporary.toString == "$30 @ $30")
  val rrTemporary = withEffect(Temporary("$", 31))(_.setRegister(regs.rr))
  require(rrTemporary.toString == "$31 @ $31")

  /** The product of a single three address code function and its corresponding function entry */
  case class TacFunction(code: ArrayBuffer[Tac], funEntry: FunEntry)

  /** The product of all functions and their global symbol table */
  case class TacProgram(functions: ArrayBuffer[TacFunction], global: SymbolTable)

  /** Mixin for additional subroutine-call informations or static linking */
  trait FunctionMixin { self: Tac => }

  /** May be used to mix a function entry into a three address code */
  trait Function extends FunctionMixin { self: Tac =>
    val function: FunEntry
  }

  /** May be used to mark a three address code that it uses the internal malloc function */
  trait Malloc extends FunctionMixin { self: Tac => }

  /** May be used to mark a three address code that it uses the internal free function */
  trait Free extends FunctionMixin { self: Tac => }


  @inline
  private def tempsIn(a: Operand): Set[Temporary] = a match {
    case x: Temporary => Set(x)
    case _            => Set()
  }

  @inline
  private def tempsIn(a: Operand, b: Operand): Set[Temporary] = (a, b) match {
    case (x: Temporary, y: Temporary) => Set(x, y)
    case (x: Temporary, _)            => Set(x)
    case (_, y: Temporary)            => Set(y)
    case _                            => Set()
  }

  def comment(tac: Tac): String =
    tac.getComment.map("  ; " + _).getOrElse("")

  private def describe(ast: Ast): String = ast match {
    case null          => "<no-ast>"
    case s: Stm        => s.describe
    case e: Exp        => e.code().replaceAll("[\n\r]+", "")
    case s: TypeSymbol => "<type>"
  }

  def --(comment: String) = Nop().setComment(comment)
  case class Nop(override protected val ast: Ast = null) extends Tac {}

  /**
   * Swaps the given temporaries
   *
   * This is equal to:
   * {{{
   * Xor(ast, a, a, b)
   * Xor(ast, b, b, a)
   * Xor(ast, a, a, b)
   * }}}
   * pre-requisite: `a` and `b` do not share the same memory location
   */
  case class Swap(override protected val ast: Ast, var a: Temporary, var b: Temporary) extends Tac {
    require(a != b)
    override def getUsages: Set[Temporary] = Set(a, b)
    override def getKills: Set[Temporary] = Set(a, b)
  }

  /**
   * Dummy instruction to mark the specified temporary as defined at a program point
   * Usually this instruction is used to mark callee-save registers on entry of a subroutine call
   */
  case class Def(override protected val ast: Ast, var tmp: Temporary) extends Tac {
    override def getKills = Set(tmp)
    override final def toString = s"Def(${describe(ast)}, $tmp)${comment(this)}"
  }

  /**
   * Dummy instruction to mark the specified temporary as used at a program point
   * Usually this instruction is used to mark callee-save registers on exit of a subroutine call
   */
  case class Use(override protected val ast: Ast, var tmp: Temporary) extends Tac {
    override def getUsages = Set(tmp)
    override final def toString = s"Use(${describe(ast)}, $tmp)${comment(this)}"
  }

  /**
   * Jumps back to the caller.
   *
   * Technically this means that a jump to the content of the
   * return register is executed.
   */
  case class Return(override protected val ast: Ast) extends Tac {
    override final def toString = s"Return(${describe(ast)})${comment(this)}"

    override final def getUsages = Set(rrTemporary)
  }

  /**
   * Loads the stack address of a return slot relative to the stack pointer.
   *
   * This is equal to:
   * {{{
   * Add(ast, dst, SP, <offset>)
   * }}}
   */
  case class CallerReturnAddress(override protected val ast: Ast, var dst: Temporary, var offset: Offset) extends Tac {
    override final def toString = s"CallerReturnAddress(${describe(ast)}, $dst, $offset)${comment(this)}"

    override final def getKills: Set[Temporary] = Set(dst)
    override final def getUsages = Set(spTemporary)
  }
  // t1, t2 <- StoreDisplayEntry

  /**
   * Loads the stack address of a return slot relative to the frame pointer.
   *
   * This is equal to:
   * {{{
   * Add(ast, dst, FP, <offset>)
   * }}}
   */
  case class CalleeReturnAddress(override protected val ast: Ast, var dst: Temporary, var offset: Offset) extends Tac {
    override final def toString = s"CalleeReturnAddress(${describe(ast)}, $dst, $offset)${comment(this)}"

    override final def getKills: Set[Temporary] = Set(dst)
    // there is no real frame-pointer, the fp is obtained by adding the framesize to the sp
    override final def getUsages = Set(spTemporary)
  }

  /**
   * Loads the stack address of an argument relative to the stack pointer.
   *
   * This is equal to:
   * {{{
   * Add(ast, dst, SP, <offset>)
   * }}}
   *
   * Usually this instruction is called by the caller of a subroutine to
   * store an argument.
   */
  case class CallerArgumentAddress(override protected val ast: Ast, var dst: Temporary, var offset: Offset) extends Tac {
    override final def toString = s"CallerArgumentAddress(${describe(ast)}, $dst, $offset)${comment(this)}"

    override def getKills: Set[Temporary] = Set(dst)
    override final def getUsages = Set(spTemporary)
  }

  /**
   * Loads the stack address of an argument relative to the frame pointer.
   *
   * This is equal to:
   * {{{
   * Add(ast, dst, FP, <offset>)
   * }}}
   *
   * Usually this instruction is called by the callee to load an argument.
   */
  case class CalleeArgumentAddress(override protected val ast: Ast, var dst: Temporary, var offset: Offset) extends Tac {
    override final def toString = s"CalleeArgumentAddress(${describe(ast)}, $dst, $offset)${comment(this)}"

    override def getKills: Set[Temporary] = Set(dst)
    // there is no real frame-pointer, the fp is obtained by adding the framesize to the sp
    override final def getUsages = Set(spTemporary)
  }

  /**
   * Loads the address of a local variable from the current stack frame.
   *
   * This is equal to:
   * {{{
   * Add(ast, dst, FP, <offset>)
   * }}}
   */
  case class LoadLocalAddress(override protected val ast: Ast, var dst: Temporary, var offset: Offset) extends Tac {
    override final def toString = s"LoadLocalAddress(${describe(ast)}, $dst, $offset)${comment(this)}"

    override final def getKills: Set[Temporary] = Set(dst)
    // there is no real frame-pointer, the fp is obtained by adding the framesize to the sp
    override final def getUsages = Set(spTemporary)
  }

  /**
   * Loads the address of a local variable via static link from another stack frame.
   *
   * This is equal to:
   * {{{
   * Add(ast, dst, staticLink, <offset>)
   * }}}
   */
  case class LoadStaticAddress(override protected val ast: Ast, var dst: Temporary, var staticLink: Temporary, var offset: Offset) extends Tac {
    override final def toString = s"LoadStaticAddress(${describe(ast)}, $dst, $staticLink, $offset)${comment(this)}"

    override final def getKills: Set[Temporary] = Set(dst)

    override final def getUsages: Set[Temporary] = Set(staticLink)
  }

  /**
   * Stores the return register.
   *
   * @define ast
   * This is equal to:
   * {{{
   * Stw(ast, $31, FP, <offset>)
   * }}}
   *
   * Be aware that this instruction should only be called before the instruction `AllocStackFrame`
   */
  case class StoreReturnRegister(override protected val ast: Ast) extends Tac {
    override final def toString = s"StoreReturnRegister(${describe(ast)})${comment(this)}"
    // there is no real frame-pointer, the fp is obtained by adding the framesize to the sp
    override final def getUsages = Set(spTemporary)
  }

  /**
   * Loads the saved return register.
   *
   * @define ast
   * This is equal to:
   * {{{
   * Ldw(ast, $31, FP, <offset>)
   * }}}
   *
   * Be aware that this instruction should only be called after the instruction `DeallocStackFrame`
   */
  case class LoadReturnRegister(override protected val ast: Ast) extends Tac {
    override final def toString = s"LoadReturnRegister(${describe(ast)})${comment(this)}"
    // there is no real frame-pointer, the fp is obtained by adding the framesize to the sp
    override final def getUsages = Set(spTemporary)
  }

  /**
   * Stores the display entry at the current lexical depth into the current stack frame.
   *
   * This is equal to:
   * {{{
   * Mov(ast, b, lexicalDepth)   // byte offset of current lexical depth
   * Ldw(ast, a, b, display)     // load display entry
   * Stw(ast, a, FP, <offset>)   // store display entry
   * Mov(ast, a, FP)            // store the current fp
   * Stw(ast, a, b, display)    // store current fp as new display entry
   * }}}
   *
   * Be aware that this instruction should only be called before the instruction `AllocStackFrame`
   */
  case class StoreDisplayEntry(override protected val ast: Ast, var dstA: Temporary, var dstB: Temporary, var lexicalDepth: Int) extends Tac {
    override final def toString = s"StoreDisplayEntry(${describe(ast)}, $lexicalDepth)${comment(this)}"
    require(dstA != dstB, "StoreDisplayEntry requires to have two distinct temporaries!")

    // although `a` and `b` are used in the code above, atomically they are not used but only killed
    override final def getKills = Set(dstA, dstB)
    // there is no real frame-pointer, the fp is obtained by adding the framesize to the sp
    override final def getUsages = Set(spTemporary)
  }

  /**
   * Loads the display entry from the current stack frame and stores it back into the display.
   *
   * This is equal to:
   * {{{
   * Ldw(ast, a, FP, <offset>)   // load display entry from stack frame
   * Mov(ast, b, lexicalDepth)   // byte offset of current lexical depth
   * Stw(ast, a, b, display)     // store display entry
   * }}}
   *
   * Be aware that this instruction should only be called after the instruction `DeallocStackFrame`
   */
  case class LoadDisplayEntry(override protected val ast: FunDef, var dstA: Temporary, var dstB: Temporary, var lexicalDepth: Int) extends Tac {
    override final def toString = s"LoadDisplayEntry(${describe(ast)}, $lexicalDepth)${comment(this)}"
    require(dstA != dstB, "StoreDisplayEntry requires to have two distinct temporaries!")

    // although `a` and `b` are used in the code above, atomically they are not used but only killed
    override final def getKills = Set(dstA, dstB)
    // there is no real frame-pointer, the fp is obtained by adding the framesize to the sp
    override final def getUsages = Set(spTemporary)
  }

  /**
   * Stores the given register at the specified offset in the stack frame
   *
   * This is equivalent to:
   * {{{
   * Stw(ast, src, FP, <offset>)
   * }}}
   */
  case class StoreRegister(override protected val ast: Ast, var src: Temporary, offset: Offset) extends Tac {
    override final def toString = s"StoreRegister(${describe(ast)}, $src, $offset)${comment(this)}"

    // there is no real frame-pointer, the fp is obtained by adding the framesize to the sp
    override final def getUsages = Set(src, spTemporary)
  }

  /**
   * Loads a value from the specified offset in the stack frame into a given register
   *
   * This is equivalent to:
   * {{{
   * Ldw(ast, dst, FP, <offset>)
   * }}}
   */
  case class LoadRegister(override protected val ast: Ast, var dst: Temporary, var offset: Offset) extends Tac {
    override final def toString = s"LoadRegister(${describe(ast)}, $dst, $offset)${comment(this)}"

    // there is no real frame-pointer, the fp is obtained by adding the framesize to the sp
    override final def getKills = Set(dst, spTemporary)
  }

  /**
   * Initiates the allocation of a new stack frame.
   *
   * Technically this means that the stack pointer will be decremented by the
   * frame size of the given corresponding `ast`, which itself is always a `FunDef`
   * for this instruction.
   */
  case class AllocStackFrame(override protected val ast: FunDef) extends Tac {
    override final def toString = s"AllocStackFrame(${describe(ast)})${comment(this)}"
    override final def getKills = Set(spTemporary, callerSaved1, callerSaved2)
    override final def getUsages = Set(spTemporary)
  }

  /**
   * Initiates the deallocation of the current stack frame.
   *
   * Technically this means that the stack pointer will be incremented by the
   * frame size of the given corresponding `ast`, which itself is always a `FunDef`
   * for this instruction.
   */
  case class DeallocStackFrame(override protected val ast: FunDef) extends Tac {
    override final def toString = s"DeallocStackFrame(${describe(ast)})${comment(this)}"
    override final def getKills = Set(spTemporary)
    override final def getUsages = Set(spTemporary)
  }

  /** Moves value `src` into variable `dst` */
  case class Mov(override protected val ast: Ast, var dst: Temporary, var src: Operand) extends Tac {
    override final def toString = s"Mov(${describe(ast)}, $dst, $src)${comment(this)}"

    override final def getKills: Set[Temporary] = Set(dst)

    override final def getUsages: Set[Temporary] = tempsIn(src)

    override final def hashCode(): Int = System.identityHashCode(this)

    override final def equals(other: Any): Boolean = other match {
      case other: Tac.Mov => other eq this
      case _ => false
    }
  }

  /**
   * Represents an operation of the form `dst` <- `a` <op> `b`
   * in the three-address-code.
   *
   * @see [[de.thm.mni.cg.ir.Tac.Add]]
   * @see [[de.thm.mni.cg.ir.Tac.Sub]]
   * @see [[de.thm.mni.cg.ir.Tac.Mul]]
   * @see [[de.thm.mni.cg.ir.Tac.Mulu]]
   * @see [[de.thm.mni.cg.ir.Tac.Div]]
   * @see [[de.thm.mni.cg.ir.Tac.Divu]]
   * @see [[de.thm.mni.cg.ir.Tac.Rem]]
   * @see [[de.thm.mni.cg.ir.Tac.Remu]]
   * @see [[de.thm.mni.cg.ir.Tac.And]]
   * @see [[de.thm.mni.cg.ir.Tac.Or]]
   * @see [[de.thm.mni.cg.ir.Tac.Xor]]
   * @see [[de.thm.mni.cg.ir.Tac.Xnor]]
   * @see [[de.thm.mni.cg.ir.Tac.Sar]]
   * @see [[de.thm.mni.cg.ir.Tac.Sll]]
   * @see [[de.thm.mni.cg.ir.Tac.Slr]]
   */
  sealed trait ArithTac extends Tac {
    var dst: Temporary
    var a: Operand
    var b: Operand

    override final def getKills: Set[Temporary] = Set(dst)

    override final def getUsages: Set[Temporary] = tempsIn(a, b)
  }

  /** Represents the arithmetic operation `dst` <- `a` + `b` in the three-address-code */
  case class Add(override protected val ast: Ast, var dst: Temporary, var a: Operand, var b: Operand) extends ArithTac {
    override final def toString = s"Add(${describe(ast)}, $dst, $a, $b)${comment(this)}"
  }

  /** Represents the arithmetic operation `dst` <- `a` - `b` in the three-address-code */
  case class Sub(override protected val ast: Ast, var dst: Temporary, var a: Operand, var b: Operand) extends ArithTac {
    override final def toString = s"Sub(${describe(ast)}, $dst, $a, $b)${comment(this)}"
  }

  /** Represents the arithmetic operation `dst` <- `a` * `b` in the three-address-code */
  case class Mul(override protected val ast: Ast, var dst: Temporary, var a: Operand, var b: Operand) extends ArithTac {
    override final def toString = s"Mul(${describe(ast)}, $dst, $a, $b)${comment(this)}"
  }
  /** Represents the arithmetic operation `dst` <- `a` * `b` in the three-address-code */
  case class Mulu(override protected val ast: Ast, var dst: Temporary, var a: Operand, var b: Operand) extends ArithTac {
    override final def toString = s"Mulu(${describe(ast)}, $dst, $a, $b)${comment(this)}"
  }

  /** Represents the arithmetic operation `dst` <- `a` / `b` in the three-address-code */
  case class Div(override protected val ast: Ast, var dst: Temporary, var a: Operand, var b: Operand) extends ArithTac {
    override final def toString = s"Div(${describe(ast)}, $dst, $a, $b)${comment(this)}"
  }
  /** Represents the arithmetic operation `dst` <- `a` / `b` in the three-address-code */
  case class Divu(override protected val ast: Ast, var dst: Temporary, var a: Operand, var b: Operand) extends ArithTac {
    override final def toString = s"Divu(${describe(ast)}, $dst, $a, $b)${comment(this)}"
  }

  /** Represents the arithmetic operation `dst` <- `a` % `b` in the three-address-code */
  case class Rem(override protected val ast: Ast, var dst: Temporary, var a: Operand, var b: Operand) extends ArithTac {
    override final def toString = s"Rem(${describe(ast)}, $dst, $a, $b)${comment(this)}"
  }
  /** Represents the arithmetic operation `dst` <- `a` % `b` in the three-address-code */
  case class Remu(override protected val ast: Ast, var dst: Temporary, var a: Operand, var b: Operand) extends ArithTac {
    override final def toString = s"Remu(${describe(ast)}, $dst, $a, $b)${comment(this)}"
  }

  /** Represents the logic operation `dst` <- `a` && `b` in the three-address-code */
  case class And(override protected val ast: Ast, var dst: Temporary, var a: Operand, var b: Operand) extends ArithTac {
    override final def toString = s"And(${describe(ast)}, $dst, $a, $b)${comment(this)}"
  }
  /** Represents the logic operation `dst` <- `a` || `b` in the three-address-code */
  case class Or(override protected val ast: Ast, var dst: Temporary, var a: Operand, var b: Operand) extends ArithTac {
    override final def toString = s"Or(${describe(ast)}, $dst, $a, $b)${comment(this)}"
  }
  /** Represents the bitwise operation `dst` <- `a` ^ `b` in the three-address-code */
  case class Xor(override protected val ast: Ast, var dst: Temporary, var a: Operand, var b: Operand) extends ArithTac {
    override final def toString = s"Xor(${describe(ast)}, $dst, $a, $b)${comment(this)}"
  }
  /** Represents the bitwise operation `dst` <- `a` <xnor> `b` in the three-address-code */
  case class Xnor(override protected val ast: Ast, var dst: Temporary, var a: Operand, var b: Operand) extends ArithTac {
    override final def toString = s"Xnor(${describe(ast)}, $dst, $a, $b)${comment(this)}"
  }

  /** Represents the bitwise operation `dst` <- `a` << `b` in the three-address-code */
  case class Sll(override protected val ast: Ast, var dst: Temporary, var a: Operand, var b: Operand) extends ArithTac {
    override final def toString = s"Sll(${describe(ast)}, $dst, $a, $b)${comment(this)}"
  }
  /** Represents the bitwise operation `dst` <- `a` >> `b` in the three-address-code */
  case class Slr(override protected val ast: Ast, var dst: Temporary, var a: Operand, var b: Operand) extends ArithTac {
    override final def toString = s"Slr(${describe(ast)}, $dst, $a, $b)${comment(this)}"
  }
  /** Represents the bitwise operation `dst` <- `a` >>> `b` in the three-address-code */
  case class Sar(override protected val ast: Ast, var dst: Temporary, var a: Operand, var b: Operand) extends ArithTac {
    override final def toString = s"Sar(${describe(ast)}, $dst, $a, $b)${comment(this)}"
  }

  /** Representation of a label in the three-address-code */
  case class Label(override protected val ast: Ast, var name: String) extends Tac {
    override final def toString = s"Label(${describe(ast)}, $name:)${comment(this)}"
  }

  /**
   * Represents a conditional jump in the three-address-code.
   *
   * For example:
   * {{{
   * beq \$8, \$0, _nullpointerException
   * }}}
   *
   * @see [[de.thm.mni.cg.ir.Tac.Beq]]
   * @see [[de.thm.mni.cg.ir.Tac.Bne]]
   * @see [[de.thm.mni.cg.ir.Tac.Ble]]
   * @see [[de.thm.mni.cg.ir.Tac.Bleu]]
   * @see [[de.thm.mni.cg.ir.Tac.Blt]]
   * @see [[de.thm.mni.cg.ir.Tac.Bltu]]
   * @see [[de.thm.mni.cg.ir.Tac.Bge]]
   * @see [[de.thm.mni.cg.ir.Tac.Bgeu]]
   * @see [[de.thm.mni.cg.ir.Tac.Bgt]]
   * @see [[de.thm.mni.cg.ir.Tac.Bgtu]]
   */
  sealed trait CondBranch extends Tac {
    var a: Temporary
    var b: Temporary
    var label: LabelAddress

    override final def getUsages: Set[Temporary] = tempsIn(a, b)
  }

  /**
   * Represents a conditional jump in the three-address-code,
   * which to `label`, if `a` == `b`.
   */
  case class Beq(override protected val ast: Ast, var a: Temporary, var b: Temporary, var label: LabelAddress) extends CondBranch {
    override final def toString = s"Beq(${describe(ast)}, $a, $b, ${label.name})${comment(this)}"
  }
  /**
   * Represents a conditional jump in the three-address-code,
   * which to `label`, if `a` != `b`.
   */
  case class Bne(override protected val ast: Ast, var a: Temporary, var b: Temporary, var label: LabelAddress) extends CondBranch {
    override final def toString = s"Bne(${describe(ast)}, $a, $b, ${label.name})${comment(this)}"
  }

  /**
   * Represents a conditional jump in the three-address-code,
   * which to `label`, if `a` <= `b`.
   */
  case class Ble(override protected val ast: Ast, var a: Temporary, var b: Temporary, var label: LabelAddress) extends CondBranch {
    override final def toString = s"Ble(${describe(ast)}, $a, $b, ${label.name})${comment(this)}"
  }
  /**
   * Represents a conditional jump in the three-address-code,
   * which to `label`, if `a` <= `b`.
   */
  case class Bleu(override protected val ast: Ast, var a: Temporary, var b: Temporary, var label: LabelAddress) extends CondBranch {
    override final def toString = s"Bleu(${describe(ast)}, $a, $b, ${label.name})${comment(this)}"
  }

  /**
   * Represents a conditional jump in the three-address-code,
   * which to `label`, if `a` < `b`.
   */
  case class Blt(override protected val ast: Ast, var a: Temporary, var b: Temporary, var label: LabelAddress) extends CondBranch {
    override final def toString = s"Blt(${describe(ast)}, $a, $b, ${label.name})${comment(this)}"
  }
  /**
   * Represents a conditional jump in the three-address-code,
   * which to `label`, if `a` < `b`.
   */
  case class Bltu(override protected val ast: Ast, var a: Temporary, var b: Temporary, var label: LabelAddress) extends CondBranch {
    override final def toString = s"Bltu(${describe(ast)}, $a, $b, ${label.name})${comment(this)}"
  }

  /**
   * Represents a conditional jump in the three-address-code,
   * which to `label`, if `a` >= `b`.
   */
  case class Bge(override protected val ast: Ast, var a: Temporary, var b: Temporary, var label: LabelAddress) extends CondBranch {
    override final def toString = s"Bge(${describe(ast)}, $a, $b, ${label.name})${comment(this)}"
  }
  /**
   * Represents a conditional jump in the three-address-code,
   * which to `label`, if `a` >= `b`.
   */
  case class Bgeu(override protected val ast: Ast, var a: Temporary, var b: Temporary, var label: LabelAddress) extends CondBranch {
    override final def toString = s"Bgeu(${describe(ast)}, $a, $b, ${label.name})${comment(this)}"
  }

  /**
   * Represents a conditional jump in the three-address-code,
   * which to `label`, if `a` > `b`.
   */
  case class Bgt(override protected val ast: Ast, var a: Temporary, var b: Temporary, var label: LabelAddress) extends CondBranch {
    override final def toString = s"Bgt(${describe(ast)}, $a, $b, ${label.name})${comment(this)}"
  }
  /**
   * Represents a conditional jump in the three-address-code,
   * which to `label`, if `a` > `b`.
   */
  case class Bgtu(override protected val ast: Ast, var a: Temporary, var b: Temporary, var label: LabelAddress) extends CondBranch {
    override final def toString = s"Bgtu(${describe(ast)}, $a, $b, ${label.name})${comment(this)}"
  }

  /** Represents a unconditional jump to `label` */
  case class Jmp(override protected val ast: Ast, var label: LabelAddress) extends Tac {
    override final def toString = s"Jmp(${describe(ast)}, ${label.name})${comment(this)}"
  }
  /** Represents a unconditional jump to `a` */
  case class Jmpr(override protected val ast: Ast, var a: Temporary) extends Tac {
    override final def toString = s"Jmpr(${describe(ast)}, $a)${comment(this)}"

    override final def getUsages: Set[Temporary] = Set(a)
  }
  /** Represents a unconditional call to `label`. (jal = jump and link) */
  case class Jal(override protected val ast: Ast, var label: LabelAddress) extends Tac {
    override final def toString = s"Jal(${describe(ast)}, ${label.name})${comment(this)}"
    override final def getKills = Set(
      /*rrTemporary,*/
      callerSaved1,
      callerSaved2,
      callerSaved3,
      callerSaved4,
      callerSaved5,
      callerSaved6,
      callerSaved7,
      callerSaved8,
      callerSaved9,
      callerSaved10
    )
  }
  /** Represents a unconditional call to `a`. (jal = jump and link) */
  case class Jalr(override protected val ast: Ast, var a: Temporary) extends Tac {
    override final def toString = s"Jalr(${describe(ast)}, $a)${comment(this)}"

    override final def getUsages: Set[Temporary] = Set(a)
    override final def getKills = Set(
      /*rrTemporary,*/
      callerSaved1,
      callerSaved2,
      callerSaved3,
      callerSaved4,
      callerSaved5,
      callerSaved6,
      callerSaved7,
      callerSaved8,
      callerSaved9,
      callerSaved10
    )
  }

  /**
   * Superclass for all read accesses from the memory.
   * (but not accessing the stackframe)
   *
   * Every subclass of this class, performs the load
   * `dst` <- memory[`addr` + `offset`]
   *
   * @see [[de.thm.mni.cg.ir.Tac.Ldw]]
   * @see [[de.thm.mni.cg.ir.Tac.Ldh]]
   * @see [[de.thm.mni.cg.ir.Tac.Ldhu]]
   * @see [[de.thm.mni.cg.ir.Tac.Ldb]]
   * @see [[de.thm.mni.cg.ir.Tac.Ldbu]]
   */
  sealed trait MemLoad extends Tac {
    var dst: Temporary
    var addr: Temporary
    var offset: Immediate

    override final def getUsages: Set[Temporary] = Set(addr)

    override final def getKills: Set[Temporary] = Set(dst)
  }

  /** Loads a word from memory[`addr` + `offset`] to `dst` */
  case class Ldw(override protected val ast: Ast, var dst: Temporary, var addr: Temporary, var offset: Immediate) extends MemLoad {
    override final def toString = s"Ldw(${describe(ast)}, $dst, $addr, $offset)${comment(this)}"
  }
  /** Loads a half word from memory[`addr` + `offset`] to `dst` */
  case class Ldh(override protected val ast: Ast, var dst: Temporary, var addr: Temporary, var offset: Immediate) extends MemLoad {
    override final def toString = s"Ldh(${describe(ast)}, $dst, $addr, $offset)${comment(this)}"
  }
  /** Loads a half word from memory[`addr` + `offset`] to `dst` */
  case class Ldhu(override protected val ast: Ast, var dst: Temporary, var addr: Temporary, var offset: Immediate) extends MemLoad {
    override final def toString = s"Ldhu(${describe(ast)}, $dst, $addr, $offset)${comment(this)}"
  }
  /** Loads a byte from memory[`addr` + `offset`] to `dst` */
  case class Ldb(override protected val ast: Ast, var dst: Temporary, var addr: Temporary, var offset: Immediate) extends MemLoad {
    override final def toString = s"Ldb(${describe(ast)}, $dst, $addr, $offset)${comment(this)}"
  }
  /** Loads a byte from memory[`addr` + `offset`] to `dst` */
  case class Ldbu(override protected val ast: Ast, var dst: Temporary, var addr: Temporary, var offset: Immediate) extends MemLoad {
    override final def toString = s"Ldbu(${describe(ast)}, $dst, $addr, $offset)${comment(this)}"
  }

  /**
   * Superclass for all write accesses to the memory.
   * (but not accessing the stackframe)
   *
   * Every subclass of this class, performs the load
   * `src` -> memory[`addr` + `offset`]
   *
   * @see [[de.thm.mni.cg.ir.Tac.Stw]]
   * @see [[de.thm.mni.cg.ir.Tac.Sth]]
   * @see [[de.thm.mni.cg.ir.Tac.Stb]]
   */
  sealed trait MemStore extends Tac {
    var src: Temporary
    var addr: Temporary
    var offset: Immediate

    override final def getUsages: Set[Temporary] = tempsIn(src, addr)
  }

  /** Stores a word from `src` to memory[`addr` + `offset`] */
  case class Stw(override protected val ast: Ast, var src: Temporary, var addr: Temporary, var offset: Immediate) extends MemStore {
    override final def toString = s"Stw(${describe(ast)}, $src, $addr, $offset)${comment(this)}"
  }
  /** Stores a half word from `src` to memory[`addr` + `offset`] */
  case class Sth(override protected val ast: Ast, var src: Temporary, var addr: Temporary, var offset: Immediate) extends MemStore {
    override final def toString = s"Sth(${describe(ast)}, $src, $addr, $offset)${comment(this)}"
  }
  /** Stores a byte from `src` to memory[`addr` + `offset`] */
  case class Stb(override protected val ast: Ast, var src: Temporary, var addr: Temporary, var offset: Immediate) extends MemStore {
    override final def toString = s"Stb(${describe(ast)}, $src, $addr, $offset)${comment(this)}"
  }

}
