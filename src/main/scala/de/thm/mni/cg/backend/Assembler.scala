package de.thm.mni.cg.backend

import java.io.File
import scala.util.Try

/**
 * Assembler for eco32 assembly files
 */
final class Assembler() {
  private val nativeAssembler = new NativeAssembler()

  /** assembles the given input file and produces a *.obj file with the same name */
  def assemble(file: File): Try[File] = {
    val fileName = if (file.getName.isEmpty) "out.asm" else file.getName
    val ending = fileName.lastIndexOf(".")
    val outName = if (ending < 0) fileName + ".obj" else fileName.substring(0, ending) + ".obj"
    val out = new File(file.getParentFile, outName)
    assemble(file, out)
  }

  /** assembles all input files and returns a list of the succeeded or failed output files */
  def assemble(file: File*): List[Try[File]] =
    file.toList.map(assemble)

  /** assembles the given input file and produces a *.obj file with given output filename */
  def assemble(file: File, out: File): Try[File] = Try {
    nativeAssembler.assemble(file, out)
    out
  }

}
