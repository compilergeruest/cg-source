package de.thm.mni.cg.backend

import java.io.OutputStream

import de.thm.mni.cg.ir._
import de.thm.mni.cg.language._
import de.thm.mni.cg.language.ErrorMessages._
import de.thm.mni.cg.runtime.RegisterProvider
import de.thm.mni.cg.table._
import de.thm.mni.cg.util.AnyRefSafeTypes
import de.thm.mni.cg.util.UnixPrintWriter

final class CodeGenerator(stream: OutputStream, maximalLexicalDepth: Int, regProvider: RegisterProvider) {
  val printer = new UnixPrintWriter(stream)
  import printer._
  import regProvider._

  def run(program: Tac.TacProgram): Unit = {
    initialCode(program.global)
    for {
      function    <- program.functions
      instruction <- function.code
    } generate(instruction, function.funEntry)
    // otherwise the program is empty, which should technically never happen

    printer.flush()
  }

  private def initialCode(global: SymbolTable): Unit = {
    println(".import _malloc")
    println(".import _free")
    println(".import _illegalArraySize")
    println(".import _nullPointerException")
    println(".import _stackOverflowException")
    println(".import _indexError")
    println(".import stack_size")
    println(".import heap_size")
    println(".import heap_gap")
    println(".import heap_start")
    val main = global.get[FunEntry](Name("main")).getOrElse(missingMainError())
    val outerScope = main.getFunDef.getBody.asInstanceOf[BlockStm].getLocalTable
    for ((Name(name), entry) <- outerScope) {
      entry.ifInstanceOf[StdFunEntry] { stdfun =>
        println(s".import $name")
      }
    }
    println()
    println(".export _main")
    println()
    println(".data")
    println(".align 4")
    println()
    println("display:")
    for (i <- 0 until maximalLexicalDepth) {
      println(".word 0")
    }
    println()
    println(".code")
    println(".align 4")
    println()
  }

  private def operand(operand: Operand): String = operand match {
    case t: Temporary if (t.getRegister ne null) => t.getRegister.toString
    case l: LabelAddress                         => l.name
    case i: ConstNumber                          => i.n.toString
    case _                                       => ""
  }

  private def generate(tac: Tac, entry: FunEntry): Unit = {
    tac match {
      case Tac.LoadLocalAddress(ast, dst, offset) =>
        print(s"add ${dst.getRegister}, $sp, ${offset.alignedByteOffset + entry.getArgumentSize + entry.getReturnSize}")
      case Tac.LoadStaticAddress(ast, dst, staticLink, offset) =>
        if (!tac.isInstanceOf[Tac.LoadStaticAddress with Tac.Function]) {
          missingRuntimeInfosError("Tac.LoadStaticAddress", "Tac.Function")
        } else {
          val entry = tac.asInstanceOf[Tac.LoadStaticAddress with Tac.Function].function
          val off = if (entry.isCalling) Constants.refSize * 2 else Constants.refSize
          print(s"add ${dst.getRegister}, ${staticLink.getRegister}, ${-(off + entry.getRescuedRegisterSize + entry.getLocalSize) + offset.alignedByteOffset}")
        }
      case Tac.Return(ast) =>
        print(s"jr $rr")

      case tac: Tac.CallerReturnAddress =>
        tac match {
          case tac: Tac.CallerReturnAddress with Tac.Function =>
            print(s"add ${tac.dst.getRegister}, $sp, ${tac.offset.alignedByteOffset + tac.function.getParameterSize}")
          case tac: Tac.CallerReturnAddress with Tac.Free =>
            // sp + 0 := arg
            freeDoesntReturnAThingError()
          case tac: Tac.CallerReturnAddress with Tac.Malloc =>
            // sp + 0 := arg, sp + 4 := ret
            print(s"add ${tac.dst.getRegister}, $sp, 4")
          case _ =>
            missingRuntimeInfosError("Tac.CallerReturnAddress", "Tac.FunctionMixin")
        }
      case Tac.CalleeReturnAddress(ast, dst, offset) =>
        print(s"add ${dst.getRegister}, $sp, ${offset.alignedByteOffset + entry.getFrameSize + entry.getParameterSize}")

      case Tac.CalleeArgumentAddress(ast, dst, offset) =>
        // an argument always starts at FP + FrameSize + Relative Offset
        print(s"add ${dst.getRegister}, $sp, ${offset.alignedByteOffset + entry.getFrameSize}")
      case Tac.CallerArgumentAddress(ast, dst, offset) =>
        // an argument always starts at SP + Relative Offset
        print(s"add ${dst.getRegister}, $sp, ${offset.alignedByteOffset}")

      case Tac.LoadDisplayEntry(ast, a, b, lexicalDepth) =>
        val optAssoc = ast match {
          case f: FunDef => f.getLocalTable.associatedFunEntry
          case ast => ast.getTable.associatedFunEntry
        }
        val fSize = optAssoc.getOrElse(noFunEntryIsAssociatedWithError("Tac.LoadDisplayEntry")).getFrameSize
        println("; restore the display entry")
        println(s"ldw ${a.getRegister}, $sp, ${fSize - 4}")
        println(s"add ${b.getRegister}, $zero, $lexicalDepth")
        print(s"stw ${a.getRegister}, ${b.getRegister}, display")
      case Tac.StoreDisplayEntry(ast, a, b, lexicalDepth) =>
        println("; save the old display entry and store the frame pointer as new display entry")
        println(s"add ${b.getRegister}, $zero, $lexicalDepth")
        println(s"ldw ${a.getRegister}, ${b.getRegister}, display")
        val optAssoc = ast match {
          case f: FunDef => f.getLocalTable.associatedFunEntry
          case ast => ast.getTable.associatedFunEntry
        }
        val fSize = optAssoc.getOrElse(noFunEntryIsAssociatedWithError("Tac.StoreDisplayEntry")).getFrameSize
        println(s"stw ${a.getRegister}, $sp, ${fSize - 4}")
        println(s"add ${a.getRegister}, $sp, ${fSize}")
        print(s"stw ${a.getRegister}, ${b.getRegister}, display")

      case Tac.AllocStackFrame(ast) =>
        val fentry =
          ast.getLocalTable
            .associatedFunEntry
            .getOrElse(noFunEntryIsAssociatedWithError("Tac.AllocStackFrame"))
        println("; check for stack overflow exception")
        println("ldw $8, $0, heap_start")
        println("ldw $9, $0, heap_size")
        println("add $8, $8, $9")
        println("ldw $9, $0, heap_gap")
        println("add $8, $8, $9")
        println("add $9, $0, " + sp)
        println("sub $9, $9, " + fentry.getFrameSize)
        println("bltu $9, $8, _stackOverflowException")
        print("add " + sp + ", $0, $9")
      case Tac.DeallocStackFrame(ast) =>
        val fentry =
          ast.getLocalTable
            .associatedFunEntry
            .getOrElse(noFunEntryIsAssociatedWithError("Tac.DeallocStackFrame"))
        print(s"add $sp, $sp, ${fentry.getFrameSize}  ; release stack frame for function '${ast.getName.name}'")

      case Tac.StoreReturnRegister(ast) =>
        val optAssoc = ast match {
          case f: FunDef => f.getLocalTable.associatedFunEntry
          case ast => ast.getTable.associatedFunEntry
        }
        val fSize = optAssoc.getOrElse(noFunEntryIsAssociatedWithError("Tac.StoreReturnRegister")).getFrameSize
        print(s"stw $rr, $sp, ${fSize - 8}  ; store return register")
      case Tac.LoadReturnRegister(ast) =>
        val optAssoc = ast match {
          case f: FunDef => f.getLocalTable.associatedFunEntry
          case ast => ast.getTable.associatedFunEntry
        }
        val fSize = optAssoc.getOrElse(noFunEntryIsAssociatedWithError("Tac.LoadReturnRegister")).getFrameSize
        print(s"ldw $rr, $sp, ${fSize - 8}  ; load return register")

      case Tac.Label(ast, name) =>
        print(s"$name:")

      case Tac.Mov(ast, dst, src) =>
        print(s"add ${dst.getRegister}, $zero, ${operand(src)}")

      case Tac.Add(ast, dst, t: Temporary, b) =>
        print(s"add ${dst.getRegister}, ${t.getRegister}, ${operand(b)}")
      case Tac.Add(ast, dst, ConstNumber(0), b) =>
        // first operand is 0, infer zero register
        print(s"add ${dst.getRegister}, $zero, ${operand(b)}")
      case Tac.Add(ast, dst, a, b) =>
        // first operand is not a register, infer move instruction
        print(s"add ${dst.getRegister}, $zero, ${operand(a)}")
        print(s"add ${dst.getRegister}, ${dst.getRegister}, ${operand(b)}")

      case Tac.Sub(ast, dst, t: Temporary, b) =>
        print(s"sub ${dst.getRegister}, ${t.getRegister}, ${operand(b)}")
      case Tac.Sub(ast, dst, ConstNumber(0), b) =>
        // first operand is 0, infer zero register
        print(s"sub ${dst.getRegister}, $zero, ${operand(b)}")
      case Tac.Sub(ast, dst, a, b) =>
        // first operand is not a register, infer move instruction
        print(s"add ${dst.getRegister}, $zero, ${operand(a)}")
        print(s"sub ${dst.getRegister}, ${dst.getRegister}, ${operand(b)}")

      case Tac.Mul(ast, dst, t: Temporary, b) =>
        print(s"mul ${dst.getRegister}, ${t.getRegister}, ${operand(b)}")
      case Tac.Mul(ast, dst, ConstNumber(0), b) =>
        // first operand is 0, infer zero register
        print(s"mul ${dst.getRegister}, $zero, ${operand(b)}")
      case Tac.Mul(ast, dst, a, b) =>
        // first operand is not a register, infer move instruction
        print(s"add ${dst.getRegister}, $zero, ${operand(a)}")
        print(s"mul ${dst.getRegister}, ${dst.getRegister}, ${operand(b)}")

      case Tac.Div(ast, dst, t: Temporary, b) =>
        print(s"div ${dst.getRegister}, ${t.getRegister}, ${operand(b)}")
      case Tac.Div(ast, dst, ConstNumber(0), b) =>
        // first operand is 0, infer zero register
        print(s"div ${dst.getRegister}, $zero, ${operand(b)}")
      case Tac.Div(ast, dst, a, b) =>
        // first operand is not a register, infer move instruction
        print(s"add ${dst.getRegister}, $zero, ${operand(a)}")
        print(s"div ${dst.getRegister}, ${dst.getRegister}, ${operand(b)}")

      case Tac.Rem(ast, dst, t: Temporary, b) =>
        print(s"rem ${dst.getRegister}, ${t.getRegister}, ${operand(b)}")
      case Tac.Rem(ast, dst, ConstNumber(0), b) =>
        // first operand is 0, infer zero register
        print(s"rem ${dst.getRegister}, $zero, ${operand(b)}")
      case Tac.Rem(ast, dst, a, b) =>
        // first operand is not a register, infer move instruction
        print(s"add ${dst.getRegister}, $zero, ${operand(a)}")
        print(s"rem ${dst.getRegister}, ${dst.getRegister}, ${operand(b)}")

      case Tac.Mulu(ast, dst, t: Temporary, b) =>
        print(s"mulu ${dst.getRegister}, ${t.getRegister}, ${operand(b)}")
      case Tac.Mulu(ast, dst, ConstNumber(0), b) =>
        // first operand is 0, infer zero register
        print(s"mulu ${dst.getRegister}, $zero, ${operand(b)}")
      case Tac.Mulu(ast, dst, a, b) =>
        // first operand is not a register, infer move instruction
        print(s"add ${dst.getRegister}, $zero, ${operand(a)}")
        print(s"mulu ${dst.getRegister}, ${dst.getRegister}, ${operand(b)}")

      case Tac.Divu(ast, dst, t: Temporary, b) =>
        print(s"divu ${dst.getRegister}, ${t.getRegister}, ${operand(b)}")
      case Tac.Divu(ast, dst, ConstNumber(0), b) =>
        // first operand is 0, infer zero register
        print(s"divu ${dst.getRegister}, $zero, ${operand(b)}")
      case Tac.Divu(ast, dst, a, b) =>
        // first operand is not a register, infer move instruction
        print(s"add ${dst.getRegister}, $zero, ${operand(a)}")
        print(s"divu ${dst.getRegister}, ${dst.getRegister}, ${operand(b)}")

      case Tac.Remu(ast, dst, t: Temporary, b) =>
        print(s"remu ${dst.getRegister}, ${t.getRegister}, ${operand(b)}")
      case Tac.Remu(ast, dst, ConstNumber(0), b) =>
        // first operand is 0, infer zero register
        print(s"remu ${dst.getRegister}, $zero, ${operand(b)}")
      case Tac.Remu(ast, dst, a, b) =>
        // first operand is not a register, infer move instruction
        print(s"add ${dst.getRegister}, $zero, ${operand(a)}")
        print(s"remu ${dst.getRegister}, ${dst.getRegister}, ${operand(b)}")

      case Tac.And(ast, dst, t: Temporary, b) =>
        print(s"and ${dst.getRegister}, ${t.getRegister}, ${operand(b)}")
      case Tac.And(ast, dst, ConstNumber(0), b) =>
        // first operand is 0, infer zero register
        print(s"and ${dst.getRegister}, $zero, ${operand(b)}")
      case Tac.And(ast, dst, a, b) =>
        // first operand is not a register, infer move instruction
        print(s"add ${dst.getRegister}, $zero, ${operand(a)}")
        print(s"and ${dst.getRegister}, ${dst.getRegister}, ${operand(b)}")

      case Tac.Or(ast, dst, t: Temporary, b) =>
        print(s"or ${dst.getRegister}, ${t.getRegister}, ${operand(b)}")
      case Tac.Or(ast, dst, ConstNumber(0), b) =>
        // first operand is 0, infer zero register
        print(s"or ${dst.getRegister}, $zero, ${operand(b)}")
      case Tac.Or(ast, dst, a, b) =>
        // first operand is not a register, infer move instruction
        print(s"add ${dst.getRegister}, $zero, ${operand(a)}")
        print(s"or ${dst.getRegister}, ${dst.getRegister}, ${operand(b)}")

      case Tac.Xor(ast, dst, t: Temporary, b) =>
        print(s"xor ${dst.getRegister}, ${t.getRegister}, ${operand(b)}")
      case Tac.Xor(ast, dst, ConstNumber(0), b) =>
        // first operand is 0, infer zero register
        print(s"xor ${dst.getRegister}, $zero, ${operand(b)}")
      case Tac.Xor(ast, dst, a, b) =>
        // first operand is not a register, infer move instruction
        print(s"add ${dst.getRegister}, $zero, ${operand(a)}")
        print(s"xor ${dst.getRegister}, ${dst.getRegister}, ${operand(b)}")

      case Tac.Xnor(ast, dst, t: Temporary, b) =>
        print(s"xnor ${dst.getRegister}, ${t.getRegister}, ${operand(b)}")
      case Tac.Xnor(ast, dst, ConstNumber(0), b) =>
        // first operand is 0, infer zero register
        print(s"xnor ${dst.getRegister}, $zero, ${operand(b)}")
      case Tac.Xnor(ast, dst, a, b) =>
        // first operand is not a register, infer move instruction
        print(s"add ${dst.getRegister}, $zero, ${operand(a)}")
        print(s"xnor ${dst.getRegister}, ${dst.getRegister}, ${operand(b)}")

      case Tac.Sll(ast, dst, t: Temporary, b) =>
        print(s"sll ${dst.getRegister}, ${t.getRegister}, ${operand(b)}")
      case Tac.Sll(ast, dst, ConstNumber(0), b) =>
        // first operand is 0, infer zero register
        print(s"sll ${dst.getRegister}, $zero, ${operand(b)}")
      case Tac.Sll(ast, dst, a, b) =>
        // first operand is not a register, infer move instruction
        print(s"add ${dst.getRegister}, $zero, ${operand(a)}")
        print(s"sll ${dst.getRegister}, ${dst.getRegister}, ${operand(b)}")

      case Tac.Slr(ast, dst, t: Temporary, b) =>
        print(s"slr ${dst.getRegister}, ${t.getRegister}, ${operand(b)}")
      case Tac.Slr(ast, dst, ConstNumber(0), b) =>
        // first operand is 0, infer zero register
        print(s"slr ${dst.getRegister}, $zero, ${operand(b)}")
      case Tac.Slr(ast, dst, a, b) =>
        // first operand is not a register, infer move instruction
        print(s"add ${dst.getRegister}, $zero, ${operand(a)}")
        print(s"slr ${dst.getRegister}, ${dst.getRegister}, ${operand(b)}")

      case Tac.Sar(ast, dst, t: Temporary, b) =>
        print(s"sar ${dst.getRegister}, ${t.getRegister}, ${operand(b)}")
      case Tac.Sar(ast, dst, ConstNumber(0), b) =>
        // first operand is 0, infer zero register
        print(s"sar ${dst.getRegister}, $zero, ${operand(b)}")
      case Tac.Sar(ast, dst, a, b) =>
        // first operand is not a register, infer move instruction
        print(s"add ${dst.getRegister}, $zero, ${operand(a)}")
        print(s"sar ${dst.getRegister}, ${dst.getRegister}, ${operand(b)}")

      case Tac.Beq(ast, a: Temporary, b: Temporary, LabelAddress(label)) =>
        print(s"beq ${a.getRegister}, ${b.getRegister}, $label")

      case Tac.Bne(ast, a: Temporary, b: Temporary, LabelAddress(label)) =>
        print(s"bne ${a.getRegister}, ${b.getRegister}, $label")

      case Tac.Blt(ast, a: Temporary, b: Temporary, LabelAddress(label)) =>
        print(s"blt ${a.getRegister}, ${b.getRegister}, $label")

      case Tac.Bltu(ast, a: Temporary, b: Temporary, LabelAddress(label)) =>
        print(s"bltu ${a.getRegister}, ${b.getRegister}, $label")

      case Tac.Ble(ast, a: Temporary, b: Temporary, LabelAddress(label)) =>
        print(s"ble ${a.getRegister}, ${b.getRegister}, $label")

      case Tac.Bleu(ast, a: Temporary, b: Temporary, LabelAddress(label)) =>
        print(s"bleu ${a.getRegister}, ${b.getRegister}, $label")

      case Tac.Bgt(ast, a: Temporary, b: Temporary, LabelAddress(label)) =>
        print(s"bgt ${a.getRegister}, ${b.getRegister}, $label")

      case Tac.Bgtu(ast, a: Temporary, b: Temporary, LabelAddress(label)) =>
        print(s"bgtu ${a.getRegister}, ${b.getRegister}, $label")

      case Tac.Bge(ast, a: Temporary, b: Temporary, LabelAddress(label)) =>
        print(s"bge ${a.getRegister}, ${b.getRegister}, $label")

      case Tac.Bgeu(ast, a: Temporary, b: Temporary, LabelAddress(label)) =>
        print(s"bgeu ${a.getRegister}, ${b.getRegister}, $label")

      case Tac.Jmp(ast, LabelAddress(label)) =>
        print(s"j $label")
      case Tac.Jmpr(ast, t) =>
        print(s"jr ${t.getRegister}")
      case Tac.Jal(ast, LabelAddress(label)) =>
        print(s"jal $label")
      case Tac.Jalr(ast, t) =>
        print(s"jalr ${t.getRegister}")

      case Tac.Ldw(ast, dst, addr, offset) =>
        print(s"ldw ${dst.getRegister}, ${addr.getRegister}, ${operand(offset)}")
      case Tac.Ldh(ast, dst, addr, offset) =>
        print(s"ldh ${dst.getRegister}, ${addr.getRegister}, ${operand(offset)}")
      case Tac.Ldhu(ast, dst, addr, offset) =>
        print(s"ldhu ${dst.getRegister}, ${addr.getRegister}, ${operand(offset)}")
      case Tac.Ldb(ast, dst, addr, offset) =>
        print(s"ldb ${dst.getRegister}, ${addr.getRegister}, ${operand(offset)}")
      case Tac.Ldbu(ast, dst, addr, offset) =>
        print(s"ldbu ${dst.getRegister}, ${addr.getRegister}, ${operand(offset)}")

      case Tac.Stw(ast, src, addr, offset) =>
        print(s"stw ${src.getRegister}, ${addr.getRegister}, ${operand(offset)}")

      case Tac.Sth(ast, src, addr, offset) =>
        print(s"sth ${src.getRegister}, ${addr.getRegister}, ${operand(offset)}")

      case Tac.Stb(ast, src, addr, offset) =>
        print(s"stb ${src.getRegister}, ${addr.getRegister}, ${operand(offset)}")

      case _: Tac.Def | _: Tac.Use =>

      case Tac.LoadRegister(ast, dst, offset) =>
        val off = entry.getFrameSize - Constants.refSize - (if (entry.isCalling) Constants.refSize else 0)
        print(s"ldw ${dst.getRegister}, $sp, ${off - offset.alignedByteOffset - offset.byteSize}")
      case Tac.StoreRegister(ast, src, offset) =>
        val off = entry.getFrameSize - Constants.refSize - (if (entry.isCalling) Constants.refSize else 0)
        print(s"stw ${src.getRegister}, $sp, ${off - offset.alignedByteOffset - offset.byteSize}")

      case Tac.Nop(_) =>

      case Tac.Swap(ast, a, b) =>
        println(s"xor ${a.getRegister}, ${a.getRegister}, ${b.getRegister}")
        println(s"xor ${b.getRegister}, ${b.getRegister}, ${a.getRegister}")
        print(s"xor ${a.getRegister}, ${a.getRegister}, ${b.getRegister}")

      case e =>
        unknownTacInstructionError(e)
    }

    if (!tac.isPseudoInstruction || tac.getComment.isDefined) {
      /*
       * print comment (or newline)
       * - of all non-pseudo instructions
       * - of pseudo instruction, if existing
       */
      println(Tac.comment(tac))
    }
  }

}
