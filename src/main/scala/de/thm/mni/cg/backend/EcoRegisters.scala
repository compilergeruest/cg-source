package de.thm.mni.cg.backend

import de.thm.mni.cg.runtime.RegisterProvider
import de.thm.mni.cg.runtime.Register
import scala.collection.mutable.{ Set => MSet }
import scala.language.implicitConversions
import scala.collection.mutable.ArrayBuffer

object EcoRegisters extends RegisterProvider {

  private case class EcoRegister(n: Int) extends Register {
    lazy val codeRepresentation: String = "$" + n
  }

  override val allRegisters = regs(0 to 31)

  override val forbiddenRegisters = regs(1, 26 to 28)

  override val zero: Register = EcoRegister(0)
  override val sp: Register = EcoRegister(29)
  override val rr: Register = EcoRegister(31)

  override val ir: Register = EcoRegister(30)

  override val returnValueRegisters: IndexedSeq[Register] = regs(2, 3)
  override val argumentRegisters: IndexedSeq[Register] = regs(4 to 7)
  override val temporaryRegisters: IndexedSeq[Register] = regs(8 to 15, 24, 25)
  override val registerVariables: IndexedSeq[Register] = regs(16 to 23)

  /*
   * ================= helper functions to build sets of registers ==================
   */

  private implicit def intToRight(n: Int): Either[Range, Int] = Right(n)

  private implicit def rangeToLeft(r: Range): Either[Range, Int] = Left(r)

  private def reg(n: Int): Register = EcoRegister(n)

  private def regs(elements: Either[Range, Int]*): IndexedSeq[Register] = {
    val buffer = ArrayBuffer[Register]()
    elements.foreach {
      case Left(range) => for (n <- range) buffer += reg(n)
      case Right(n)    => buffer += reg(n)
    }
    buffer
  }

}
