package de.thm.mni.cg.language

object NumberSystem {
  /** Number systems whose literals are supported by the language. */
  def all: Set[NumberSystem] = Set(Binary, Decimal, Hexadecimal)

  case object Binary extends NumberSystem("Binary", 2) {
    def encode(n: Int): String = "0b" + n.toBinaryString
  }

  case object Decimal extends NumberSystem("Decimal", 10) {
    def encode(n: Int): String = n.toString
  }

  case object Hexadecimal extends NumberSystem("Hexadecimal", 16) {
    def encode(n: Int): String = "0x" + (n.toHexString.toUpperCase)
  }
}

/** Instances of this class represent a number system.*/
sealed abstract class NumberSystem(name: String, base: Int) extends Product with Serializable {
  final override def toString = s"NumberSystem($name, base: $base)"

  def encode(n: Int): String
}
