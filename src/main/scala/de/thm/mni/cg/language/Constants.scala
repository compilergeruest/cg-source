package de.thm.mni.cg.language

import de.thm.mni.cg.ir._
import de.thm.mni.cg.util._

object Constants {

  /** A sequence containing all keywords of the language */
  val keywords = Seq(
    "const", "type", "del", "new",
    "false", "true", "null",
    "for", "while", "if", "else",
    "return",
    "struct", "union")

  /** A sequence containing all primitive types of the language */
  val primitives = Seq("int", "bool", "unit", "char")

  /** All available predefined macros */
  val macros = Map[String, Position => Exp](
    "__DATE__" -> (pos => StringLit(getDateString)),
    "__TIME__" -> (pos => StringLit(getTimeString)),
    "__INT_SIZE__" -> (pos => IntLit(intSize)),
    "__CHAR_SIZE__" -> (pos => IntLit(charSize)),
    "__BOOL_SIZE__" -> (pos => IntLit(boolSize)),
    "__REF_SIZE__" -> (pos => IntLit(refSize)),
    "__LINE__" -> (pos => IntLit(pos.line)),
    "__COLUMN__" -> (pos => IntLit(pos.column))
  )

  /** Regular Expression of a hexadecimal integer literal */
  val hexIntLit = "0x[A-Fa-f0-9]+".r
  /** Regular Expression of a binary integer literal */
  val binIntLit = "0b[0-1]+".r
  /** Regular Expression of a decimal integer literal */
  val decIntLit = "[0-9]+".r

  /** Regular Expression of a character literal, escape sequences excluded */
  val charLit = """'(?!('|\\))[\x20-\x7F]'""".r
  /** Regular Expression of a character literal, only escape sequences */
  val escapeLit = """'\\[ntr'\\]'""".r

  /** Regular Expression of a string literal */
  val stringLit = ("\""+"""([^"\p{Cntrl}\\]|\\[\\"nrt]|\\u)*+"""+"\"").r

  /** Regular Expression of an identifier */
  val identifier = "[a-zA-Z][a-zA-Z0-9_]*".r

  /** escapes sequences */
  val escapes = Map(
    'n' -> '\n',
    't' -> '\t',
    'r' -> '\r'
  ) withDefault (identity)

  /** Size in bytes of an integer */
  val intSize = 4

  /** Size in bytes of a character */
  val charSize = 1

  /** Size in bytes of a boolean */
  val boolSize = 1

  /** Size in bytes of a reference size */
  val refSize = 4

}
