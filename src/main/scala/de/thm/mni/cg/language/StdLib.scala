package de.thm.mni.cg.language

import de.thm.mni.cg.table._
import de.thm.mni.cg.table.StdFunEntry._
import de.thm.mni.cg.ir._
import de.thm.mni.cg.meta._
import de.thm.mni.cg.semantic._
import Constants._

object StdLib {

  private def ty(name: String): TypeSymbol = NameTypeSymbol(Name(name))

  private def arr(name: String, dims: Int): TypeSymbol =
    (0 until (dims max 1)).foldLeft(ty(name)) {
      case (acc, _) => ArrayTypeSymbol(acc)
    }

  private val string = arr("char", 1)
  private val char = ty("char")
  private val int = ty("int")
  private val unit = ty("unit")

  val functions = Seq(
    stdFunction("printc", List(unit), List(char -> "c"), ".import printc") _,
    stdFunction("printx", List(unit), List(int -> "n"), ".import printx") _,
    stdFunction("printi", List(unit), List(int -> "n"), ".import printi") _,
    stdFunction("readc", List(char), List(), ".import readc") _,
    stdFunction("readi", List(int), List(), ".import readi") _,
    stdFunction("exit", List(unit), List(), ".import exit") _,
    stdFunction("time", List(int), List(), ".import time") _,
    stdFunction("clearAll", List(unit), List(int -> "color"), ".import clearAll") _,
    stdFunction("drawLine", List(unit), List(int -> "x1", int -> "y1", int -> "x2", int -> "y2", int -> "color"), ".import drawLine") _,
    stdFunction("setPixel", List(unit), List(int -> "x", int -> "y", int -> "color"), "") _,
    stdFunction("drawCircle", List(unit), List(int -> "x", int -> "y", int -> "radius", int -> "color"), ".import drawCircle") _
  )

  def insertPrimitives(table: SymbolTable) =
    for (prim <- primitives) {
      val entry = TyEntry(TyDef(Name(prim), NameTypeSymbol(Name(prim))))
      entry.setTy(NameTy(prim))
      table(Name(prim)) = entry
    }

  def insertStdLibFunctions(table: SymbolTable) =
    functions.foreach { function =>
      val stdFunction = function(table)
      table(stdFunction.getFunDef.getName) = stdFunction
    }

}
