package de.thm.mni.cg.language

import java.io.File
import de.thm.mni.cg.ir._
import de.thm.mni.cg.semantic
import de.thm.mni.cg.semantic.Ty
import de.thm.mni.cg.semantic.Ty._
import de.thm.mni.cg.util._
import scala.collection.mutable.ArrayBuffer

object ErrorMessages extends AnyRef
  with TokenizerErrors
  with ParserErrors
  with SemanticErrors
  with InternalErrorMessages {

  def inputFileNotExist(f: File): String =
    s"The specified input file '$f' doesn't exist!\n"

  lazy val noInputFile: String =
    "No input file was specified!\n"

  lazy val phiUsageWithoutUsage: String =
    "Usage in phi function found without an actual use!"

  def interpolationTypeMismatch(actual: Class[_], expect: Class[_]): String =
    s"The interpolated parse result (of type '${actual.getName}') is not an instance of type '${expect.getName}'!"

  def interpolationTypeMismatchError(actual: Class[_], expect: Class[_]): Nothing =
    sys.error(interpolationTypeMismatch(actual, expect))

  def expected(expect: String, but: String): String =
    s"Expected $expect, but got $but!"


}

trait SemanticErrors { self: ErrorMessages.type =>

  def missingReturn(func: Name, ty: String): String =
    s"Missing return statement, the function '${func.name}' must return a value of type '$ty'!"

  def variableMayNotHaveBeenInitialized(name: Name): String =
    s"Variable '${name.name}' may not have been initialized!"

  lazy val unreachableCode: String =
    "Unreachable code!"

  def duplicateComponentsDefs(components: List[Component], name: Name): Unit = {
    val duplicateNames = duplicates(components.map(_.name).toList)
    if (duplicateNames.nonEmpty) {
      val (nameStr, plural) = enumerate(duplicateNames.map("'" + _.name + "'"))
      val plComp = if (plural) "components" else "component"
      error(duplicateNames.head, s"Duplicate definition of the $plComp $nameStr in type '${name.name}'!")
    }
  }

  def duplicateComponentsInit(names: ArrayBuffer[Name], name: Name): Unit = {
    val duplicateNames = duplicates(names.toList)
    if (duplicateNames.nonEmpty) {
      val (nameStr, plural) = enumerate(duplicateNames.map("'" + _.name + "'"))
      val plComp = if (plural) "components" else "component"
      error(duplicateNames.head, s"Duplicate initialization of the $plComp $nameStr in type '${name.name}'!")
    }
  }

  lazy val onlySyntheticNamesForFuncs: String =
    "Can only generate synthetic name for functions!"

  lazy val aBlockShouldHaveAParent: String =
    "A block statement should have at least one parent (the function, in which it is defined)"

  def xIsNotAY(x: String, y: String): String =
    s"'$x' is not a type, but a $y!"


  def arithBinExpExpects(op: String, but: Ty): String =
    s"Arithmetic binary operation ('$op') expects expression of type '$intTy' or '$charTy', but got expression of type '$but'!"

  def logicalBinExpExpects(op: String, but: Ty): String =
    s"Logical binary operation ('$op') expects expression of type '$boolTy', but got expression of type '$but'!"

  def cannotCast(from: Ty, to: Ty, info: String): String =
    s"Can not cast expression of type '$from' to type '$to'$info!"

  def toFewManyArgsFunc(name: Name, many: String): String =
    s"Too $many arguments given for function with name '${name.name}'!"

  def expectedExprOfTypeForParam(dty: Ty, parName: Name, funcName: Name, but: Ty): String =
    s"Expected expression of type '$dty' for parameter '${parName.name}' of function '${funcName.name}', but got expression of type '$but'!"

  def invalidBaseTyForArray(ty: Ty): String =
    s"Can not create an instance of an array with the base type '$ty'!"

  lazy val arrayEmptyDimAfterNonEmpty: String =
    "Cannot specify an array dimension after an empty dimension!"

  lazy val arrayWithoutDimension: String =
    "Cannot create an array without specifying an array dimension!"

  def canNotFreeMemOfPrimitive(ty: Ty) =
    s"Can not free memory of primitive value with type '$ty'!"

  lazy val canNotFreeMemOfNull =
    s"Can not free memory of null reference!"

  def iterationExpectsInt(ty: Ty) =
    s"Iteration on values of type '$ty' is not supported, expected type '$intTy'!"

  def recursiveUse(name: Name) =
    s"Recursive use of variable with name '${name.name}'!"

  def illegalReturnType(out: Ty, actual: Ty) =
    s"Function requires return value to have type '$out', but a value of type '$actual' is returned!"

  lazy val canNotDefineParameterOfUnit =
    s"Can not define a parameter of type '$unitTy'!"

  lazy val canNotDefineVariableOfUnit =
    s"Can not define a variable of type '$unitTy'!"

  def expectedComplex(but: Ty): String =
    s"Expected complex type, but got '$but'!"

  def cyclicDefinitionOf(name: Name) =
    s"Cyclic definition of type '${name.name}'!"

  def redefinitionOfPrimitive(name: Name) =
    s"Redefinition of primitive type '${name.name}'!"

  lazy val canNotMixReturnTypeWithUnit =
    s"Can not mix '$unitTy' return type with multiple return values!"

  lazy val canNotDefineArrayBaseOfUnit =
    s"Can not define an array of base type '$unitTy'!"

  def canNotDefineStructMemberOfUnit(name: Name): String =
    s"Can not define the struct member '${name.name}' of type '$unitTy'!"

  def canNotDefineUnionMemberOfUnit(name: Name): String =
    s"Can not define the union member '${name.name}' of type '$unitTy'!"

  def unknownTypeWithName(name: Name): String =
    s"Unknown type with name '${name.name}'!"

  def structInit(name: Name, notInit: List[semantic.Component]): Unit = {
    if (notInit.nonEmpty) {
      val nNotInit = notInit.map("'" + _.name + "'")
      val (fields, plural) = enumerate(nNotInit)
      val (fieldName, isName) =
        if (plural) ("fields", "are")
        else ("field", "is")
      error(name, s"The $fieldName $fields of the complex type '${name.name}' $isName not initialized!")
    }
  }

  def unionInit(name: Name, names: ArrayBuffer[Name]): Unit = {
    if (!containsOnlyOne(names)) {
      val (nameStr, _) = enumerate(names.map("'" + _.name + "'").toList)
      val pos = names.headOption.map(_.pos).getOrElse(name.pos)
      error(pos, s"A union type must only be initialized with one argument ($nameStr are initialized here)!")
    }
  }

  def unknownComponentInComplexType(cname: Name, in: Name): String =
    s"Unknown component with name '${cname.name}' in complex type '${in.name}'!"

  def unknownComponent(cname: Name): String =
    s"Unknown component with name '${cname.name}'!"

  def unknownComponentError(cname: Name): Nothing =
    sys.error(unknownComponent(cname))

  def arithBitwiseUnaryExpects(arith: Boolean, op: String, but: Ty): String = {
    val description = if (arith) "Arithmetic" else "Bitwise"
    s"$description unary operation ('$op') expects expression of type '$intTy' or '$charTy', but got expression of type '$but'!"
  }

  def logicalUnaryExpects(op: String, but: Ty): String = {
    s"Logical unary operation ('$op') expects expression of type '$boolTy', but got expression of type '$but'!"
  }

  def sizeUnaryExpects(op: String, but: Ty): String = {
    s"Size operation ('$op') expects expression of an array type, but got expression of type '$but'!"
  }

  def reassignmentToX(desc: String, name: Name): String =
    s"Reassignment to $desc with name '${name.name}'!"

  lazy val lhsOfAssignMustBeVar: String =
    "The left-hand side of an assignment must be a variable!"

  def expectedExprOfType(expect: Ty, act: String): String =
    expected(s"expression of type '$expect'", act)

  def expectedExprOfType(expect: Ty, extra: String, act: String): String =
    expected(s"expression of type '$expect'$extra", act)

  def xIsAlreadyDefinedInScope(x: String, name: Name): String =
    s"$x '${name.name}' is already defined in scope!"

}

trait TokenizerErrors { self: ErrorMessages.type =>

  lazy val illegalStrLit: String =
    "Illegal string literal!"

  lazy val unclosedCharLit: String =
    "Unclosed character literal!"

  lazy val illegalUnicodeCharLit: String =
    "Illegal unicode character literal!"

  lazy val illegalUnicodeStringLit: String =
    "Illegal unicode escape in string literal!"

  def illegalEscape(c: Char): String =
    s"Illegal escape sequence '\\$c'!"

  lazy val expectedMacro: String =
    expected("macro", "'_'")

  def unknownMacro(name: String): String =
    s"Unknown macro '$name'!"

  def litOutOfRange(desc: String, cseq: CharSequence): String =
    s"$desc integer literal '$cseq' is out of range!"

  def hexLitOutOfRange(cseq: CharSequence): String =
    litOutOfRange("Hexadecimal", cseq)

  def binLitOutOfRange(cseq: CharSequence): String =
    litOutOfRange("Binary", cseq)

  def decLitOutOfRange(cseq: CharSequence): String =
    litOutOfRange("Decimal", cseq)

  def unexpectedInputChar(c: Char): String =
    s"Unexpected input character '$c'!"

}

trait ParserErrors { self: ErrorMessages.type =>

  def expectedExpr(but: String): String =
    expected("expression", but)

  def expectedIdent(but: String): String =
    expected("identifier", but)

  def expectedNameOrExpr(but: String): String =
    expected("name or assignment", but)

  lazy val expectedTypeButExp: String =
    expected("type", "expression")

  lazy val expectedExpButType: String =
    expected("expression", "type")

  def memberFieldNoConst(name: Name): String =
    s"Member field '${name.name}' may not be constant!"

  def constOnParameter(name: Name): String =
    s"Illegal modifier 'const' for parameter '${name.name}'!"

  def constOnForVar(name: Name): String =
    s"Counter variable '${name.name}' may not be constant!"

  def illegalCastToArray(ty: String): String =
    s"Can not cast to array type '$ty'!"

  def emptyComplex(name: String): String =
    s"Definition of empty ${name}s is not allowed!"

}

trait InternalErrorMessages { self: ErrorMessages.type =>

  def internalError(): Nothing =
    sys.error(prefix.init)

  lazy val prefix = "internal error:"

  lazy val missingMain: String =
    s"$prefix Missing main function!"

  def missingMainError(): Nothing =
    sys.error(missingMain)

  def missingRuntimeInfos(x: String, y: String): String =
    s"$prefix Missing runtime information! Expected $x with $y!"

  def missingRuntimeInfosError(x: String, y: String): Nothing =
    sys.error(missingRuntimeInfos(x, y))

  lazy val freeDoesntReturnAThing: String =
    s"$prefix A subroutine call to free does not return anything!"

  def freeDoesntReturnAThingError(): Nothing =
    sys.error(freeDoesntReturnAThing)

  def noFunEntryIsAssociatedWith(instr: String): String =
    s"$prefix Could not emit assembler code, there is no function entry associated with the instruction `$instr`!"

  def noFunEntryIsAssociatedWithError(instr: String): Nothing =
    sys.error(noFunEntryIsAssociatedWith(instr))

  def unknownTacInstruction(instr: Tac): String =
    s"$prefix Unknown three address code instruction (class '${instr.getClass.getSimpleName}')!"

  def unknownTacInstructionError(instr: Tac): Nothing =
    sys.error(unknownTacInstruction(instr))

  def variableIsLiveInAtRootSsa(v: Temporary): String =
    s"$prefix Variable $v is live-in at the root node of the given ssa-form program!"

  def variableIsLiveInAtRootSsaError(v: Temporary): Nothing =
    sys.error(variableIsLiveInAtRootSsa(v))

  def typeNotExist(name: Name): String =
    s"$prefix The type with name '${name.name}' doesn't exist!"

  def typeNotExistError(name: Name): Nothing =
    sys.error(typeNotExist(name))

  def functionNotExist(name: Name): String =
    s"$prefix The function with name '${name.name}' doesn't exist!"

  def functionNotExistError(name: Name): Nothing =
    sys.error(functionNotExist(name))

  def parameterNotExist(name: Name, func: Name): String =
    s"$prefix The parameter with name '${name.name}' doesn't exist in function '${func.name}'!"

  def parameterNotExistError(name: Name, func: Name): Nothing =
    sys.error(parameterNotExist(name, func))

  def variableNotExist(name: Name): String =
    s"$prefix The variable with name '${name.name}' doesn't exist!"

  def variableNotExistError(name: Name): Nothing =
    sys.error(variableNotExist(name))

  def definitionNotExist(name: Name): String =
    s"$prefix Couldn't find definition of field with name '${name.name}'!"

  def definitionNotExistError(name: Name): Nothing =
    sys.error(definitionNotExist(name))

  def fieldNotExist(name: Name): String =
    s"$prefix Couldn't find field of field with name '${name.name}'!"

  def fieldNotExistError(name: Name): Nothing =
    sys.error(fieldNotExist(name))

  def unknownByteSize(n: Int): String =
    s"$prefix Unknown byte size $n!"

  def unknownByteSizeError(n: Int): Nothing =
    sys.error(unknownByteSize(n))

  def illegalBaseTyOfArray(base: Ty): String =
    s"$prefix Base type '$base' was not an array type!"

  def illegalBaseTyOfArrayError(base: Ty): Nothing =
    sys.error(illegalBaseTyOfArray(base))

  def illegalBaseTyOfArrayAccess(base: Ty): String =
    s"$prefix The base ty ('$base') of an array access was not an array, maybe the type check is missing?"

  def illegalBaseTyOfArrayAccessError(base: Ty): Nothing =
    sys.error(illegalBaseTyOfArray(base))

  lazy val baseTyOfArrayNoRef: String =
    s"$prefix Base type size was not expected reference size of ${Constants.refSize}!"

  lazy val allocationOfEmptyDim: String =
    s"$prefix Unexpected array allocation of empty array dimension!"

  def allocationOfEmptyDimError(): Nothing =
    sys.error(allocationOfEmptyDim)

  lazy val returnedTempNotEqualToFst: String =
    s"$prefix Returned temporary is not equal to the first one!"

  def invalidTypeOfExp[T <: Exp](expr: T): String =
    invalidType(expr.getTy)

  def invalidTypeOfExpError[T <: Exp](expr: T): Nothing =
    sys.error(invalidTypeOfExp[T](expr))

  def invalidType(ty: Ty): String =
    s"$prefix The type '$ty' is invalid!"

  def invalidTypeError(ty: Ty): Nothing =
    sys.error(invalidType(ty))

  def tyIsNotComplex(ty: Ty): String =
    s"$prefix The type '$ty' is not a complex type!"

  def tyIsNotComplexError(ty: Ty): Nothing =
    sys.error(tyIsNotComplex(ty))

  def xIsNotArrayTy(ty: Ty): String =
    s"$prefix The type '$ty' is not an array type!"

  def xIsNotArrayTyError(ty: Ty): Nothing =
    sys.error(xIsNotArrayTy(ty))

  def emptyLhsNotLegal(): String =
    s"$prefix Empty left-hand-side of assignment is not legal!"

  def emptyLhsNotLegalError(): Nothing =
    sys.error(emptyLhsNotLegal())

  lazy val assignToNonScalarWithoutFunCall: String =
    s"$prefix Assignment to non-scalar result without function call on the right-hand-side!"

  def assignToNonScalarWithoutFunCallError(): Nothing =
    sys.error(assignToNonScalarWithoutFunCall)

  def xWithoutEnclosingFunc(x: String): String =
    s"$prefix Found a $x without enclosing function!"

  def xWithoutEnclosingFuncError(x: String): Nothing =
    sys.error(xWithoutEnclosingFunc(x))

  def foundXWithoutAssociatedFunEntry(x: String): String =
    s"$prefix Found return statement without associated function entry!"

  def foundXWithoutAssociatedFunEntryError(x: String): Nothing =
    sys.error(foundXWithoutAssociatedFunEntry(x))

  lazy val illegalEmptyReturnList: String =
    s"$prefix Empty return list is not legal!"

  def illegalEmptyReturnListError(): Nothing =
    sys.error(illegalEmptyReturnList)

  def typecheckShouldHaveCheckedFunc(name: Name): String =
    s"$prefix The Typecheck should have validated the return type of the function '${name.name}'!"

  def typecheckShouldHaveCheckedFuncError(name: Name): Nothing =
    sys.error(typecheckShouldHaveCheckedFunc(name))

  lazy val illegalCacheUse: String =
    s"$prefix Wrong usage of internal cache"

  def illegalCacheUseError(): Nothing =
    sys.error(illegalCacheUse)

  def calcStackSize(ty: String): String =
    s"$prefix Can not calculate stack size of a $ty!"

  def calcStackSizeError(ty: String): Nothing =
    sys.error(calcStackSize(ty))

  def calcHeapSize(ty: String): String =
    s"$prefix Can not calculate heap size of a $ty!"

  def calcHeapSizeError(ty: String): Nothing =
    sys.error(calcHeapSize(ty))

  lazy val internalErrorAtReturn: String =
    s"$prefix At return statement"

  def internalErrorAtReturnError(): Nothing =
    sys.error(internalErrorAtReturn)

  def illegalMember(member: String): String =
    s"Illegal member '$member'!"

  def illegalMemberError(member: String): Nothing =
    sys.error(illegalMember(member))

}
