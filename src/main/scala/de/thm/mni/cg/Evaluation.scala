package de.thm.mni.cg

import java.io.File
import java.io.FileInputStream

import scala.util.Success

import de.thm.mni.cg.eval._
import de.thm.mni.cg.compiler.Compiler
import de.thm.mni.cg.compiler.CompilerSettings
import de.thm.mni.cg.util._

trait Evaluation { self: Main.type =>

  def mainEval(settings: CompilerSettings, args: List[String]): Unit = {
    val (path, rest) = args match {
      case file(f) :: tl => (f, tl)
      case _             => usage()
    }

    var heapSize = defaultHeapSizeInEvaluator
    var graphics = false

    var argz = rest
    while (argz.nonEmpty) argz match {
      case ("--heapsize" | "-h") :: uint(size) :: tl =>
        heapSize = size
        argz = tl

      case ("--gfx" | "-g") :: tl =>
        graphics = true
        argz = tl

      case hd :: _ =>
        usageCmd("-eval", unknownArgument(Option(hd)))
    }

    eval(settings, path, heapSize, graphics)
  }

  private def eval(settings: CompilerSettings, f: File, size: Int, graphics: Boolean): Unit = {
    val fis = new FileInputStream(f)
    val result = Compiler.runtimeInformation(settings, fis)
    fis.close

    result match {
      case Success(program) =>
        val evaluator = new Evaluator()
        evaluator.setHeapSize(size)
        evaluator.setVisible(graphics)
        evaluator.eval(program)
        evaluator.setVisible(false)
        evaluator.killWindow()
      case Fail(msg) =>
        println(msg)
        sys.exit(1)
    }
  }

}
