package de.thm.mni.cg

import java.io.File

import scala.util.Success

import de.thm.mni.cg.compiler.Compiler
import de.thm.mni.cg.compiler.CompilerSettings
import de.thm.mni.cg.language.ErrorMessages._
import de.thm.mni.cg.util._

trait Compilation { self: Main.type =>

  def mainCompile(settings: CompilerSettings, args: List[String]): Unit = {
    var in: File = null
    var asm: File = null
    var out: File = null

    var argz = args
    while (argz.nonEmpty) argz match {
      case ("--input" | "-i") :: filex(f) :: xs =>
        in = f
        argz = xs

      case ("--output" | "-o") :: file(f) :: xs =>
        out = f
        argz = xs

      case ("--assembler" | "-s") :: file(f) :: xs =>
        asm = f
        argz = xs

      case hd :: _ =>
        usageCmd("-compile", unknownArgument(Option(hd)))
    }

    if (in eq null) {
      println(noInputFile)
      usage()
    }

    if (out eq null) {
      out = defaultFile(in, "bin")
    }
    if (asm eq null) {
      asm = defaultFile(in, "asm")
    }

    Compiler.compile(settings, in, asm, out) match {
      case Success(_) => ()
      case Fail(msg)  =>
        println(msg)
        sys.exit(1)
    }
  }

}
