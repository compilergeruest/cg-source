package de.thm.mni.cg.table

import de.thm.mni.cg.ir._
import de.thm.mni.cg.semantic.Ty
import scala.collection.mutable.ArrayBuffer
import de.thm.mni.cg.util.cache.Caching
import de.thm.mni.cg.runtime.Offset
import de.thm.mni.cg.runtime.NoOffset
import de.thm.mni.cg.language.Constants

/** Represents a single entry for symbol table */
sealed trait TableEntry {
  /** returns a short description of this entry */
  val describe: String
}

object FunEntry {
  def apply(funDef: FunDef) = new FunEntry(funDef)
  def unapply(x: Any) = x match {
    case f: FunEntry => Some(f.funDef)
    case _           => None
  }
}

/** Represents an entry for a function definition */
sealed class FunEntry(private val funDef: FunDef) extends TableEntry {
  /** Returns whether this function entry represents a standard library function or not */
  val isStdFunction = false

  def getFunDef = funDef
  def getLocalTable = funDef.getLocalTable

  private var synthetic: List[String] = Nil
  /**
   * A list of the path from the root of the program (the main function)
   * to this function (i.e. List(main, f), meaning that f is a function
   * inside the main function)
   */
  def getSynthetic = synthetic
  /**
   * Sets the synthetic name list
   *
   * The synthetic is a list of the path from the root of the program (the
   * main function) to this function (i.e. List(main, f), meaning that f is
   * a function inside the main function)
   */
  def setSynthetic(name: List[String]) = {
    this.synthetic = name
    this.lexicalDepth = synthetic.size
  }

  /** returns the synthetic name of the function */
  def syntheticName =
    if (isStdFunction) getSynthetic.head
    else getSynthetic.mkString("_", "_", "")

  /**
   * returns the synthetic end label for each function
   * this label marks the frame deallocation code
   */
  def syntheticEndLabel =
    "_lend" + syntheticName

  private var outTys: List[Ty] = Nil
  def getOutTys = outTys
  def setOutTys(out: List[Ty]) = this.outTys = out

  /** offset to return value at index `n` (left to right := 0 .. n) from fp */
  private lazy val returnOffsets = new Array[Offset](outTys.size)
  /** return offset of return value at index `n` (left to right := 0 .. n) from fp */
  def getReturnOffset(n: Int): Offset = returnOffsets(n)
  /** sets the offset of return value at index `n` (left to right := 0 .. n) from fp */
  def setReturnOffset(n: Int, offset: Offset) = returnOffsets(n) = offset

  /**
   * Returns the size of this function's stackframe
   * The stackframe size is determined by the following values:
   * - display entry (always 4 bytes)
   * - rescued return address (if this function calls another function)
   * - size of the rescued registers
   * - size of the local variables
   * - size of the return values
   * - size of the arguments
   */
  def getFrameSize = {
    val displayEntrySize = 4
    val rescuedReturn = if (isCalling) 4 else 0
    displayEntrySize + rescuedReturn + getRescuedRegisterSize + getLocalSize + getReturnSize + getArgumentSize
  }

  private var returnSize = 0
  /** Sets the size (int bytes) of the function's return values */
  def setReturnSize(returnSize: Int): Unit = this.returnSize = returnSize
  /** Returns the size (in bytes) of the function's return value(s) */
  def getReturnSize: Int = returnSize

  private var argumentSize = 0
  /** Sets the size (in bytes) of the function's arguments */
  def setArgumentSize(argumentSize: Int): Unit = this.argumentSize = argumentSize
  /** Returns the size (in bytes) of the function's arguments */
  def getArgumentSize: Int = argumentSize

  /** Returns the size (in bytes) of this function's incoming parameters (previous stackframe) */
  def getParameterSize: Int =
    getFunDef.getParams.map { param =>
      val name = param.getName
      val entry = getLocalTable[ParEntry](name).getOrElse(sys.error(s"internal error: Could not calculate the parameter size of an unknown parameter with name '${name.name}'!"))
      Offset.alignToWordSize(entry.getOffset.alignedByteOffset + entry.getOffset.byteSize)
    } match {
      case ArrayBuffer() => 0
      case lst => lst.max
    }

  private val caching = new Caching {}
  /** Returns the size (in bytes) of this function's outgoing return values (previous stackframe) */
  def getOutgoingSize: Int =
    getOutTys.map { out =>
      out.stackSize(getFunDef.getTable, caching.symbolToTy)
    } match {
      case Nil => sys.error("internal error: Empty return type list!")
      case lst => lst.max
    }

  private val registerOffsets = ArrayBuffer[Offset]()
  def getRegisterOffset(index: Int): Offset = registerOffsets(index)
  def getRegisterCount = registerOffsets.size
  def addRegisterOffset(offset: Offset): Unit = registerOffsets += offset
  def setRegisterOffset(index: Int, offset: Offset): Unit = registerOffsets.insert(index, offset)
  /** Returns the offset where the next register would be (relative to the other rescued registers) in the stack frame */
  def nextRegisterOffset: Offset =
    if (registerOffsets.nonEmpty) {
      registerOffsets.last + Constants.refSize
    } else {
      NoOffset(Constants.refSize)
    }

  /** Returns the size in bytes which is necessary to store registers into the stack frame */
  def getRescuedRegisterSize = {
    val res = registerOffsets.lastOption.map { last =>
      Offset.alignToWordSize(last.alignedByteOffset + last.byteSize)
    }.getOrElse(0)
    res
  }

  private var localSize = 0
  /** returns the size of all local variables */
  def getLocalSize = localSize
  /** sets the size of all local variables */
  def setLocalSize(localSize: Int) = this.localSize = localSize

  private var calling = false
  /** returns true, if this function calls another function */
  def isCalling = calling
  /** sets whether this function calls another function */
  def setCalling(calling: Boolean) = this.calling = calling

  /***/
  private var lexicalDepth: Int = 0
  /** returns the lexical depth of this function */
  def getLexicalDepth = lexicalDepth

  override val describe: String = "function"
}

object StdFunEntry {

  private val caching = new Caching {}

  def stdFunction
    (name: String, out: List[TypeSymbol], in: List[(TypeSymbol, String)], binary: Any)
    (table: SymbolTable): StdFunEntry = {
      val params = in.map {
        case (symbol, name) => ParDef(symbol, Name(name))
      }.to[ArrayBuffer]
      import caching._
      val localTable = SymbolTable(true, Some(table))
      var offset: Offset = null
      for ((sym, name) <- in) {
        val parDef = new ParDef(sym, Name(name))
        val entry = new ParEntry(parDef)
        entry.setTy(Ty.fromSymbol(sym))
        if (offset == null) offset = Offset(entry.getTy.stackSize(localTable, symbolToTy))
        else offset += entry.getTy.stackSize(localTable, symbolToTy)
        entry.setOffset(offset)
        localTable(Name(name)) = entry
      }
      val body = BlockStm(ArrayBuffer.empty, SymbolTable(false, Some(localTable)))
      val fentry = StdFunEntry(
        FunDef(Name(name), out.to[ArrayBuffer], params, body, localTable),
        binary)
      fentry.setSynthetic(List(name))
      val outs = out.map(ty => Ty.fromSymbol(ty))
      fentry.setOutTys(outs)
      var retOff = Offset(4)
      for (i <- 0 until outs.size) {
        fentry.setReturnOffset(i, retOff)
        retOff = Offset(outs(i).stackSize(localTable, caching.symbolToTy), retOff)
      }
      fentry
    }

}

case class StdFunEntry(private val fdef: FunDef, private val binary: Any) extends FunEntry(fdef) {
  def getBinary = binary
  override val isStdFunction = true
}

/** Represents the general entry for any kind of fields (parameters and variables) */
sealed trait FieldEntry extends TableEntry {
  protected val fieldDef: FieldDef

  def isParDef = fieldDef.isInstanceOf[ParDef]
  def asParDef = fieldDef.asInstanceOf[ParDef]

  def isVarDef = fieldDef.isInstanceOf[VarDef]
  def asVarDef = fieldDef.asInstanceOf[VarDef]

  def getFieldDef = fieldDef

  private var ty: Ty = null
  def getTy = ty
  def setTy(ty: Ty) = this.ty = ty

  private var offset: Offset = null
  def getOffset: Offset = offset
  def setOffset(offset: Offset) = this.offset = offset

  /** A field is a constant, if it is a paramter or a constant variable */
  def isConst = isParDef || asVarDef.isConst
}

/** The specialized field entry for variables */
case class VarEntry(private val varDef: VarDef) extends FieldEntry {
  protected val fieldDef = varDef
  def getVarDef = varDef

  override val describe: String = "variable"
}

/** The specialized field entry for parameters */
case class ParEntry(private val parDef: ParDef) extends FieldEntry {
  protected val fieldDef = parDef
  def getParDef = parDef

  override val describe: String = "parameter"
}

/** Represents the entry for type aliases */
case class TyEntry(private val tyDef: TyDef) extends TableEntry {
  def getTyDef = tyDef

  private var heapSize: Int = 0
  /** returns the size of this type, if the type is a complex type, 0 otherwise */
  def getHeapSize = heapSize
  /** sets the size of this type */
  def setHeapSize(byteSize: Int) = this.heapSize = byteSize

  private var stackSize: Int = 0
  /** returns the size in bytes this type consumes on the stack */
  def getStackSize = stackSize
  /** sets the size in bytes this type consumes on the stack */
  def setStackSize(stackSize: Int) = this.stackSize = stackSize

  private var ty: Ty = null
  def getTy = ty
  def setTy(ty: Ty) = this.ty = ty

  override val describe: String = "type"
}
