package de.thm.mni.cg.table

import scala.collection.mutable.HashMap
import scala.reflect.ClassTag
import de.thm.mni.cg.ir.Name
import de.thm.mni.cg.language.ErrorMessages._
import de.thm.mni.cg.util.{ DefaultsTo => ~> }
import de.thm.mni.cg.util.error
import de.thm.mni.cg.util.Position.PositionOrdering._
import de.thm.mni.cg.util.NoPosition

/**
 * `SymbolTable` is the symboltable used in the compiler.
 */
final case class SymbolTable(private var function: Boolean, private var parent: Option[SymbolTable])
  extends Iterable[(Name, TableEntry)] {

  private val map = HashMap.empty[Name, TableEntry]

  def isFunction = function
  def setFunction(function: Boolean) = this.function = function

  def setParent(parent: SymbolTable) = this.parent = Option(parent)
  def setParent(parent: Option[SymbolTable]) = this.parent = parent
  def getParent = parent

  private def only[T <: TableEntry]
    (implicit ct: ClassTag[T]): PartialFunction[TableEntry, T] =
    { case ct(x) => x }

  private def declaredBefore(ref: Name, ignore: Boolean)(te: TableEntry): Option[TableEntry] = {
    import ref._
    te match {
      case _ if ignore => Some(te)
      case _ if ref.pos == NoPosition => Some(te)
      case VarEntry(vd) =>
        if (pos < vd.pos) {
          None
        } else {
          Some(te)
        }
      case _ => Some(te)
    }
  }

  /**
   * Lets you get a table entry by name and by type:
   * If no type is specified it is defaulted to `TableEntry`,
   * if a type is specified, it is also checked during the search of table entries.
   */
  def apply[T <: TableEntry](name: Name)
    (implicit ct: ClassTag[T], ev: T ~> TableEntry): Option[T] = get[T](name)

  /**
   * Lets you get a table entry by name and by type:
   * If no type is specified it is defaulted to `TableEntry`,
   * if a type is specified, it is also checked during the search of table entries.
   */
  def get[T <: TableEntry](name: Name)
    (implicit ct: ClassTag[T], ev: T ~> TableEntry): Option[T] =
    map.get(name) flatMap declaredBefore(name, ignore = false) collect only[T] orElse (parent.flatMap(_.get[T](name)))

  /** Insert a table entry into the table */
  def update[T <: TableEntry](name: Name, entry: T): Unit =
    put[T](name, entry)

  /** Insert a table entry into the table */
  def put[T <: TableEntry](name: Name, entry: T): Unit =
    map += name -> entry

  /** Checks if a name exists, will not check in lexically higher functions */
  def isDefinedInFunction[T <: TableEntry](name: Name)
    (implicit ct: ClassTag[T], ev: T ~> TableEntry): Boolean = {
      val here = isDefinedInScope[T](name)
      if (function) {
        here
      } else {
        here || parent.map(_.isDefinedInFunction[T](name)).getOrElse(false)
      }
    }

  /** Checks if a name exists, will not check lexically higher scopes */
  def isDefinedInScope[T <: TableEntry](name: Name, ignore: Boolean = false)
    (implicit ct: ClassTag[T], ev: T ~> TableEntry): Boolean =
    (map.get(name) flatMap declaredBefore(name, ignore) collect only[T]).isDefined

  /**
   * Executes the call-by-name parameter 'f' when the given name is not
   * associated with a symbol entry. If it is already associated, the given
   * prefix 'x' and the name is prepended to an error message.
   *
   * Example:
   *
   * If the variable 'i' already exists, the following code will produce the
   * error message "Variable 'i' is already defined in scope!"
   * {{{
   * onUndefined(Name("i"), "Variable") { ... }
   * }}}
   */
  def onUndefined(name: Name, x: String)(f: => Unit) =
    if (this.isDefinedInScope(name, ignore = true)) error(name, xIsAlreadyDefinedInScope(x, name))
    else f

  /** Checks if a name exists, will also check lexically higher scopes */
  def exists[T <: TableEntry](name: Name)
    (implicit ct: ClassTag[T], ev: T ~> TableEntry): Boolean =
      get[T](name).isDefined

  /**
   * Returns the associated function entry, meaning that for a function 'f'
   * the function entry of 'f' will be returned. Otherwise, for any non-
   * function statement, the enclosing function entry is returned.
   *
   * Example:
   *
   * When calling 'associatedFunEntry' on the symbol table of function 'f',
   * the function entry of 'f' is returned
   * {{{
   * unit g() {
   *   unit f() {
   *
   *   }
   * }
   * }}}
   *
   * When calling 'associatedFunEntry' on the innermost block statement, the
   * function entry of 'f' is returned
   * {{{
   * unit g() {
   *   unit f() {
   *     {
   *
   *     }
   *   }
   * }
   * }}}
   *
   * @see [[de.thm.mni.cg.table.SymbolTable.enclosingFunEntry]]
   */
  def associatedFunEntry: Option[FunEntry] =
    if (function) searchInParent(this)
    else enclosingFunEntry

  private def searchInParent(needle: SymbolTable) =
    needle.parent.flatMap { parentTable =>
      val funEntries = parentTable.map.collect { case (_, fe: FunEntry) => fe }
      funEntries.find(_.getLocalTable eq needle)
    }

  /**
   * Returns the enclosing function entry, meaning that for a function 'f'
   * which is encapsuled by a function 'g', the entry of 'g' is returned. For
   * an if statement or any other non-function statements, which are encapsuled
   * by a function 'f', the entry of 'f' is returned.
   *
   * Example:
   *
   * Will return the function entry of 'g' when calling 'enclosingFunEntry' on
   * the symbol table of 'f'.
   * {{{
   * unit g() {
   *   unit f() {
   *
   *  }
   * }
   * }}}
   *
   * Will return the function entry of 'f' when calling 'enclosingFunEntry' on
   * the symbol table of the innermost block statement.
   * {{{
   * unit g() {
   *  unit f() {
   *   {
   *
   *   }
   *  }
   * }
   * }}}
   *
   * @see [[de.thm.mni.cg.table.SymbolTable.associatedFunEntry]]
   */
  def enclosingFunEntry: Option[FunEntry] = {
    def helper(table: SymbolTable): Option[FunEntry] =
      if (table.function) searchInParent(table)
      else table.parent.flatMap(helper)

    parent.flatMap(helper)
  }

  /** Returns the iterator for each entry in the table */
  def iterator = map.iterator

}
