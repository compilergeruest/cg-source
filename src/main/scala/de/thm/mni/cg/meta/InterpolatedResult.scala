package de.thm.mni.cg.meta

import de.thm.mni.cg.ir.Ast
import de.thm.mni.cg.language.ErrorMessages._
import scala.reflect.ClassTag
import scala.util.Try

/**
 * `InterpolatedResult` is a wrapper class to provide a more or less typesafe way,
 * to cast the result of an string-interpolation to the desired type.
 */
case class InterpolatedResult[T <: Ast](ast: Ast) {

  def as[U <: Ast]: InterpolatedResult[U] = InterpolatedResult[U](ast)

  def toAst(implicit ct: ClassTag[T]): T =
    ast match {
      case ct(x) => x
      case x     => interpolationTypeMismatchError(x.getClass, ct.runtimeClass)
    }

  def safeToAst(implicit ct: ClassTag[T]) = Try(toAst)
}
