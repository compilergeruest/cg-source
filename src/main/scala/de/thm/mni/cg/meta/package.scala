package de.thm.mni.cg

import java.io.StringReader

import scala.language.implicitConversions
import scala.reflect.ClassTag
import scala.util.Success
import scala.util.Try

import de.thm.mni.cg.ir._
import de.thm.mni.cg.meta.InterpolatedResult
import de.thm.mni.cg.parser._
import de.thm.mni.cg.util.Fail

package object meta {

  /**
   * Enables the cg-interpolator on Strings, which lets you interpolate
   * strings and ast-nodes into other ast-nodes.
   *
   *  Examples:
   *  {{{
   *  val tree: Ast = stm" int main() {} "
   *  }}}
   *
   *  {{{
   *  val test: Exp = exp"1 < 7"
   *  val whilestm: Stm = stm"while (\$test) println(9);"
   *  }}}
   */
  implicit class CGInterpolator(val ctx: StringContext) {

    private def assembleProgram(args: Seq[Any]): String = {
      val pi = ctx.parts.iterator
      val ai = args.iterator
      val bldr = new java.lang.StringBuilder(pi.next())
      while (ai.hasNext) {
        val part = ai.next match {
          case x: Exp    => s"(${x.code()})"
          case x: Ast    => x.code()
          case x         => x.toString
        }
        bldr.append(part)
        bldr.append(pi.next())
      }
      bldr.toString
    }

    /**
     * Returns a parser, which parses `in`, if no error occurred
     */
    private def parser(in: String): Try[Parser] =
      for {
        tokenizer <- Try(new Tokenizer(new StringReader(in)))
        parser <- Try(new Parser(tokenizer))
      } yield parser

    /**
     * Parses the assembled program, starting with production `f`
     */
    private def parseWith[T <: Ast](args: Seq[Any], f: Parser => T): InterpolatedResult[T] = {
      ctx.checkLengths(args)
      val program = assembleProgram(args)
      parser(program).flatMap(p => Try(f(p))) match {
        case Success(result) => InterpolatedResult(result)
        case Fail(msg)       => sys.error(msg)
      }
    }

    /**
     * Interpolator for a single statement. Creates an untypechecked ast.
     */
    def stm(args: Any*): InterpolatedResult[Stm] = parseWith(args, _.stm())

    /**
     * Interpolator for a single expression. Creates an untypechecked ast.
     */
    def exp(args: Any*): InterpolatedResult[Exp] = parseWith(args, _.exp())

  }

  implicit def toAst[T <: Ast: ClassTag](interpolated: InterpolatedResult[T]): T =
    interpolated.toAst

}
