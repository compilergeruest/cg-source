package de.thm.mni.cg.runtime

import de.thm.mni.cg.ir._
import de.thm.mni.cg.semantic._
import de.thm.mni.cg.table.SymbolTable
import de.thm.mni.cg.table.TyEntry
import de.thm.mni.cg.util.cache.Caching
import de.thm.mni.cg.util.traversal._
import de.thm.mni.cg.util.traversal.TraverseOrder._
import de.thm.mni.cg.table._
import de.thm.mni.cg.language.Constants
import de.thm.mni.cg.language.ErrorMessages._

final class RuntimeOrganisation extends Caching {
  private var maxLexicalFunDepth = 0

  private val debug = false

  def organizeTypeSize(program: Program) = {
    maxLexicalFunDepth = 0
    new PartialTraverser(PostOrder, typeRun).traverse(program.globalScope)(program.globalTable)
    program.setMaxLexicalFunDepth(maxLexicalFunDepth)
  }

  private lazy val typeRun: PartialFunction[(Ast, SymbolTable), Unit] = {
    case (TyDef(name, _), table) =>
      val entry = table[TyEntry](name).getOrElse(typeNotExistError(name))
      val stackSize = entry.getTy.stackSize(table, symbolToTy)
      val heapSize = entry.getTy.heapSize(table, symbolToTy)
      entry.setStackSize(stackSize)
      entry.setHeapSize(heapSize)
    case (FunDef(name, _, _, body, local), table) =>
      val entry = table[FunEntry](name).getOrElse(functionNotExistError(name))
      maxLexicalFunDepth = maxLexicalFunDepth max entry.getLexicalDepth

      val paramSizes = entry.getFunDef.getParams.map {
        case ParDef(symbol, paramName) => (paramName, Ty.fromSymbol(symbol).stackSize(table, symbolToTy))
      }

      var offset: Offset = null
      for ((paramName, size) <- paramSizes) {
        if (debug) println("function " + name.name)
        if (offset == null) offset = NoOffset(size)
        else offset += size
        val parEntry = local[ParEntry](paramName).getOrElse(parameterNotExistError(paramName, name))
        parEntry.setOffset(offset)
      }

      offset = null
      val outgoingSizes = entry.getOutTys.map(_.stackSize(table, symbolToTy)).zipWithIndex
      for ((size, idx) <- outgoingSizes) {
        if (offset == null) offset = NoOffset(size)
        else offset += size
        entry.setReturnOffset(idx, offset)
      }

      /*
      DISPLAY:
      0: STATIC LINK
      1: STATIC LINK
      n - 1: STATIC LINK
      */
      val (returnValueSize, argumentSize, isCalling) = calculateCallingSize(body)(local)
      // calculateLocalsOffset returns the offset which theoretically points to the next local variable n + 1
      // so the relativeByteOffset is the size for all variables up to n
      val localSize = Option(calculateLocalsOffset(null, body)(local)).map {
        off => Offset.alignToWordSize(off.alignedByteOffset + off.byteSize)
      }.getOrElse(0)
      entry.setLocalSize(localSize)
      entry.setCalling(isCalling)
      entry.setReturnSize(returnValueSize)
      entry.setArgumentSize(argumentSize)
      if (debug) {
        println()
        println(s"function $name needs ${entry.getReturnSize + entry.getArgumentSize} bytes for result values and arguments")
        println(s"function $name calls another procedure? ${if (isCalling) "yes" else "no"}")
        println(s"size of local variables of function $name: $localSize")
        println(s"frame size of function $name: ${entry.getFrameSize}")
        println()
      }
  }

  private lazy val zeroOutgoing = (0, 0, false)
  private def combineOutgoing(a: (Int, Int, Boolean), b: (Int, Int, Boolean)): (Int, Int, Boolean) = {
    val (asize, asize2, acalling) = a
    val (bsize, bsize2, bcalling) = b
    (asize max bsize, asize2 max bsize2, acalling || bcalling)
  }

  private def callsIn(exp: Exp) =
    exp.collect { case f: FunExp => f }

  private lazy val mallocOutgoing = (0, 0, true)   // argument and return are passed via register
  private lazy val freeOutgoing = (0, 0, true)     // argument is passed via register

  private def mallocCalls(exp: Exp): (Int, Int, Boolean) =
    exp.collect {
      case n: NewObjExp => mallocOutgoing
      case n: NewArrExp => mallocOutgoing
      case l: StringLit => mallocOutgoing
    }.headOption.getOrElse(zeroOutgoing)

  private def calculateCallingSize(exp: Exp)(implicit table: SymbolTable): (Int, Int, Boolean) = {
    val callSize = callsIn(exp).foldLeft(zeroOutgoing) { case (acc, FunExp(name @ Name(n), _, _)) =>
      val fentry = table[FunEntry](name).getOrElse(functionNotExistError(name))
      val local = fentry.getLocalTable
      val paramSize =
        if (fentry.isStdFunction && n == "printc") {
          /*
           * Hack to enable char to int conversion between our language and spl:
           * The assembly procedure "printc" accepts an integer (4 byte), but our
           * printc accepts an expression of type char (which is 1 byte).
           * The default runtime organization would return 1 byte for the
           * parameter size, but the assembly procedure would load 4 bytes,
           * therefore it would access more than one byte.
           */
          4
        } else {
          val paramSizes = fentry.getFunDef.getParams.map {
            case ParDef(symbol, paramName) => (paramName, Ty.fromSymbol(symbol).stackSize(table, symbolToTy))
          }
          var offset: Offset = null
          for ((paramName, size) <- paramSizes) {
            val parEntry = local[ParEntry](paramName).getOrElse(parameterNotExistError(paramName, name))
            if (offset == null) offset = NoOffset(size)
            else offset += size
            if (parEntry.getOffset == null) {
              parEntry.setOffset(offset)
            }
          }
          if (offset == null) {
            0
          } else {
            Offset.alignToWordSize(offset.alignedByteOffset + offset.byteSize)
          }
        }

      val outgoingSize =
        if (fentry.isStdFunction && n == "readc") {
          /*
           * Hack to enable char to int conversion between our language and spl:
           * The assembly procedure "readc" returns an integer (4 byte), but our
           * readc returns an expression of type char (which is 1 byte).
           * The default runtime organization would return 1 byte for the
           * resulting size. To write the resulting value, the assembly procedure
           * would write more than 1 byte into memory.
           */
          4
        } else {
          var offset: Offset = null
          val outgoingSizes = fentry.getOutTys.map(_.stackSize(table, symbolToTy)).zipWithIndex
          for ((size, idx) <- outgoingSizes) {
            if (offset == null) offset = NoOffset(size)
            else offset += size
            if (fentry.getReturnOffset(idx) == null) {
              fentry.setReturnOffset(idx, offset)
            }
          }
          Offset.alignToWordSize(offset.alignedByteOffset + offset.byteSize)
        }

      combineOutgoing(acc, (outgoingSize, paramSize, true))
    }
    combineOutgoing(callSize, mallocCalls(exp))
  }

  /**
   * Returns the maximal size in bytes the statement needs to call all
   * its functions and whether or not the statement calls a function
   */
  private def calculateCallingSize(stm: Stm)(implicit table: SymbolTable): (Int, Int, Boolean) = stm match {
    case _: Def =>
      zeroOutgoing
    case BlockStm(stms, local) =>
      stms.map(calculateCallingSize(_)(local)).foldLeft(zeroOutgoing)(combineOutgoing)
    case DelStm(exp) =>
      combineOutgoing(calculateCallingSize(exp), freeOutgoing)
    case IfStm(test, onTrue, onFalse) =>
      val testSize = calculateCallingSize(test)
      val testOnTrueSize = combineOutgoing(testSize, calculateCallingSize(onTrue))
      combineOutgoing(testOnTrueSize, onFalse.map(calculateCallingSize).getOrElse(zeroOutgoing))
    case WhileStm(test, body) =>
      val testSize = calculateCallingSize(test)
      combineOutgoing(testSize, calculateCallingSize(body))
    case ForStm(_, range, body, local) =>
      val startSize = calculateCallingSize(range.start)
      val nextSize = range.next.map(calculateCallingSize).getOrElse(zeroOutgoing)
      val endSize = calculateCallingSize(range.end)
      val bodySize = calculateCallingSize(body)(local)
      combineOutgoing(startSize, combineOutgoing(nextSize, combineOutgoing(endSize, bodySize)))
    case _ =>
      val exps = (stm.collect { case e: Exp => e }).map(calculateCallingSize)
      exps.foldLeft(zeroOutgoing)(combineOutgoing)
  }

  private def calculateLocalsOffset(offset: Offset, stm: Stm)(implicit table: SymbolTable): Offset =
    stm match {
      case VarDef(_, _, name) =>
        val entry = table[VarEntry](name).getOrElse(variableNotExistError(name))
        val size = entry.getTy.stackSize
        val off =
          if (offset == null) Offset(size)
          else offset + size
        entry.setOffset(off)
        off
      case BlockStm(stms, local) =>
        stms.foldLeft(offset)(calculateLocalsOffset(_, _)(local))
      case ForStm(vdef, _, body, local) =>
        val voff = calculateLocalsOffset(offset, vdef)(local)
        calculateLocalsOffset(voff, body)(local)
      case WhileStm(_, body) =>
        calculateLocalsOffset(offset, body)
      case IfStm(_, onTrue, onFalse) =>
        val trueOffsets = calculateLocalsOffset(offset, onTrue)
        val optFalseOffsets = onFalse.map(calculateLocalsOffset(offset, _))
        val falseOffsets = optFalseOffsets.getOrElse(null)
        val res = if (falseOffsets != null && trueOffsets != null && falseOffsets.relativeByteOffset > trueOffsets.relativeByteOffset) {
          falseOffsets
        } else if (trueOffsets != null) {
          trueOffsets
        } else {
          falseOffsets
        }
        res
      case InitStm(vdefs, _) =>
        vdefs.foldLeft(offset)(calculateLocalsOffset)
      case _ =>
        offset
    }

}
