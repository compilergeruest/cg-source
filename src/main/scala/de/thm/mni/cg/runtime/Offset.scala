package de.thm.mni.cg.runtime

import de.thm.mni.cg.runtime.Offset._
import scala.annotation.tailrec
import de.thm.mni.cg.language.Constants

object Offset {

  def apply(byteSize: Int): Offset =
    NoOffset(byteSize)

  def apply(byteSize: Int, previous: Offset): Offset =
    RelativeOffset(byteSize, previous)

  def alignTo(n: Int, size: Int): Int =
    if (n % size != 0) {
      n + (size - n % size)
    } else {
      n
    }

  def alignToWordSize(n: Int): Int =
    alignTo(n, Constants.refSize)

}

/**
 * Represents the relative offset between stack-slots,
 * meaning that the first argument (or return slot or
 * local variable) may have an offset of 0 bytes (because
 * there is no argument before it). For any other argument i,
 * the offset is defined by the offset of the previous
 * argument i - 1 and the byte size the previous argument
 * i - 1 needs on the stack.
 */
sealed trait Offset {
  val byteSize: Int

  /**
   * Calculates the relative byte offset for this one. Note that the resulting
   * byte offset is not word-aligned
   */
  def relativeByteOffset: Int = {
    @tailrec
    def helper(off: Offset, acc: Int): Int = off match {
      case _: NoOffset             => acc
      case RelativeOffset(_, prev) => helper(prev, acc + prev.byteSize)
    }
    helper(this, 0)
  }

  /**
   * Calculates the aligned byte offset
   *
   * Technically this means, that every word-size offset is aligned at a address divisible
   * by the word-size. Sequential bytes are merged together, meaning that, given three consecutive
   * bytes, their offset will be n, n + 1, n + 2. A following word may be at offset n + 4.
   */
  def alignedByteOffset: Int = this match {
    case RelativeOffset(size, previous) =>
      alignTo(previous.alignedByteOffset + previous.byteSize, size)
    case NoOffset(size) => 0
  }

  /**
   * Whether or not this offset has a previous element
   */
  def hasPreviousOffset: Boolean = this.isInstanceOf[RelativeOffset]

  /** Returns a relative offset to this one with the byte size for the new offset */
  def +(byteSize: Int): Offset =
    RelativeOffset(byteSize, this)

  override final def toString = s"Offset($relativeByteOffset)"

}

/** Represents the base case for offset, meaning that this class always represents the offset 0 */
case class NoOffset(byteSize: Int) extends Offset

/** Represents the induction case, meaning that this class represents the offset of the previous offset plus the size of the previous one */
case class RelativeOffset(byteSize: Int, previous: Offset) extends Offset
