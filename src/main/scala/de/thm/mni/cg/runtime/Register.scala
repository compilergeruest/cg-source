package de.thm.mni.cg.runtime

/**
 * Abstract definition of a register of the target machine
 */
trait Register {

  /**
   * Returns the textual representation of this register
   */
  def codeRepresentation: String

  override final def toString = codeRepresentation

}
