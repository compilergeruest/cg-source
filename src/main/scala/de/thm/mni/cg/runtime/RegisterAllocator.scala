package de.thm.mni.cg.runtime

import scala.annotation.tailrec
import scala.collection.mutable.{ Map => MMap }
import scala.collection.mutable.Queue
import scala.collection.mutable.{ Set => MSet }
import scala.collection.mutable.Stack
import de.thm.mni.cg.ir.Tac
import de.thm.mni.cg.ir.TacCfg
import de.thm.mni.cg.ir.Temporary
import de.thm.mni.cg.ir.Operand
import de.thm.mni.cg.language.Constants
import de.thm.mni.cg.util.Incremental
import scala.collection.mutable.ArrayBuffer

/**
 * Graph-Coloring based register allocator by Modern Compiler Implementation
 * in Java by Appel p. 232
 */
final class RegisterAllocator(provider: RegisterProvider) {

  // 1. Stack-Instructions: Add predefined and constant temporary for all prereserved registers (for example SP)
  // 2. Stack-Instructions: Add prereserved temporaries to kill and/or gen set
  // 3. Register-Allocator: Allow the use of all registers, but enter precolored registers (calling convention: arguments, returns, $0, SP, FP, etc.)
  private class LocalAllocator(function: Tac.TacFunction) {
    private type Instruction = (Int, TacCfg)

    private val temp = Incremental(Temporary(function.funEntry.syntheticName + "_s", _))
    private val calleeTemp = Incremental(Temporary(function.funEntry.syntheticName + "_r", _))
    private val funEntry = function.funEntry
    private val funDef = funEntry.getFunDef

    private val cfg = TacCfg(function)

    private val postOrder = {
      sealed trait Task
      /** Append `cfg` to the set which contains all nodes (`all`)  */
      case class AppendTask(cfg: TacCfg) extends Task
      /** Push all successors of `cfg` onto the stack */
      case class PushTask(cfg: TacCfg) extends Task

      // 1.: collect all cfg-nodes in depth-first-order
      val all = ArrayBuffer[TacCfg]()
      val seen = MSet[TacCfg]()
      val stack = Stack[Task]()
      stack push AppendTask(cfg)
      stack push PushTask(cfg)

      while (stack.nonEmpty) {
        stack.pop match {
          case PushTask(cfg) =>
            seen += cfg
            for (succ <- cfg.getSuccessors) {
              if (!seen.contains(succ)) {
                stack push AppendTask(succ)
                stack push PushTask(succ)
              }
            }
          case AppendTask(cfg) =>
            all += cfg
        }
      }
      all
    }

    // All available registers
    private val K = provider.allRegistersCount

    private val activeMoves = MSet[Tac.Mov]()
    private val moveList = MMap[Temporary, MSet[Tac.Mov]]()
    private val worklistMoves = MSet[Tac.Mov]()

    private val simplifyWorklist = Queue[Temporary]()
    private val freezeWorklist = MSet[Temporary]()
    private val spillWorklist = MSet[Temporary]()
    private val spilledNodes = MSet[Temporary]()
    private val initial = Queue[Temporary]()

    private val alias = MMap[Temporary, Temporary]()
    private val coalescedNodes = Queue[Temporary]()
    private val coalescedMoves = MSet[Tac.Mov]()
    private val frozenMoves = MSet[Tac.Mov]()
    private val constrainedMoves = MSet[Tac.Mov]()

    private val adjSet = MSet[(Temporary, Temporary)]()
    private val adjList = MMap[Temporary, MSet[Temporary]]()
    private val degree = MMap[Temporary, Int]()
    private val precolored = MSet[Temporary](
      Tac.zeroTemporary,
      Tac.assemblerReserved,
      Tac.returnTemporary1,
      Tac.returnTemporary2,
      Tac.argTemporary1,
      Tac.argTemporary2,
      Tac.argTemporary3,
      Tac.argTemporary4,
      Tac.callerSaved1,
      Tac.callerSaved2,
      Tac.callerSaved3,
      Tac.callerSaved4,
      Tac.callerSaved5,
      Tac.callerSaved6,
      Tac.callerSaved7,
      Tac.callerSaved8,
      Tac.calleeSaved1,
      Tac.calleeSaved2,
      Tac.calleeSaved3,
      Tac.calleeSaved4,
      Tac.calleeSaved5,
      Tac.calleeSaved6,
      Tac.calleeSaved7,
      Tac.calleeSaved8,
      Tac.callerSaved9,
      Tac.callerSaved10,
      Tac.kernelReserved1,
      Tac.kernelReserved2,
      Tac.kernelReserved3,
      Tac.spTemporary,
      Tac.irTemporary,
      Tac.rrTemporary
    )
    private val coloredNodes = MSet[Temporary]()

    private val selectStack = Stack[Temporary]()
    private val color = MMap[Temporary, Int](
      Tac.zeroTemporary -> 0,
      Tac.assemblerReserved -> 1,
      Tac.returnTemporary1 -> 2,
      Tac.returnTemporary2 -> 3,
      Tac.argTemporary1 -> 4,
      Tac.argTemporary2 -> 5,
      Tac.argTemporary3 -> 6,
      Tac.argTemporary4 -> 7,
      Tac.callerSaved1 -> 8,
      Tac.callerSaved2 -> 9,
      Tac.callerSaved3 -> 10,
      Tac.callerSaved4 -> 11,
      Tac.callerSaved5 -> 12,
      Tac.callerSaved6 -> 13,
      Tac.callerSaved7 -> 14,
      Tac.callerSaved8 -> 15,
      Tac.calleeSaved1 -> 16,
      Tac.calleeSaved2 -> 17,
      Tac.calleeSaved3 -> 18,
      Tac.calleeSaved4 -> 19,
      Tac.calleeSaved5 -> 20,
      Tac.calleeSaved6 -> 21,
      Tac.calleeSaved7 -> 22,
      Tac.calleeSaved8 -> 23,
      Tac.callerSaved9 -> 24,
      Tac.callerSaved10 -> 25,
      Tac.kernelReserved1 -> 26,
      Tac.kernelReserved2 -> 27,
      Tac.kernelReserved3 -> 28,
      Tac.spTemporary -> 29,
      Tac.irTemporary -> 30,
      Tac.rrTemporary -> 31
    )

    require(precolored.size == color.size)

    def allocate(): Unit = {
      for (nodes <- postOrder) {
        for (instr <- nodes) {
          initial ++= instr.getKills.filterNot(precolored)
          initial ++= instr.getUsages.filterNot(precolored)
        }
      }
      for (x <- precolored) {
        degree(x) = Int.MaxValue
      }
      require(!initial.exists(_ == precolored))
      main()
      postOrder.foreach(_.foreach(assignRegisters))
      // coalesce

      val zero = Tac.zeroTemporary
      for (node <- postOrder) {
        var i = 0
        while (i < node.size) {
          if (node(i).isInstanceOf[Tac.Mov] && coalescedMoves.contains(node(i).asInstanceOf[Tac.Mov])) {
            node(i) = null
          }
          i += 1
        }
      }

      for (node <- postOrder) {
        var i = 0
        while (i < node.size) {
          if (node(i) == null) {
            node.remove(i)
          } else {
            i += 1
          }
        }
      }
    }

    @inline
    private def assignRegisters(tac: Tac): Unit = {
      @inline
      def set(os: Operand*): Unit = os.foreach {
        case t: Temporary if (color contains t) => t.setRegister(provider.allRegisters(color(t)))
        case t: Temporary => sys.error(s"$t is not contained by: ${color.mkString("\n", "\n", "")}")
        case _            => ()
      }

      tac match {
        case e: Tac.Swap                  => set(e.a, e.b)
        case e: Tac.Use                   => set(e.tmp)
        case e: Tac.Def                   => set(e.tmp)
        case e: Tac.CallerReturnAddress   => set(e.dst)
        case e: Tac.CalleeReturnAddress   => set(e.dst)
        case e: Tac.CallerArgumentAddress => set(e.dst)
        case e: Tac.CalleeArgumentAddress => set(e.dst)
        case e: Tac.LoadLocalAddress      => set(e.dst)
        case e: Tac.LoadStaticAddress     => set(e.dst, e.staticLink)
        case e: Tac.StoreDisplayEntry     => set(e.dstA, e.dstB)
        case e: Tac.LoadDisplayEntry      => set(e.dstA, e.dstB)
        case e: Tac.StoreRegister         => set(e.src)
        case e: Tac.LoadRegister          => set(e.dst)
        case e: Tac.Mov                   => set(e.dst, e.src)
        case e: Tac.ArithTac              => set(e.dst, e.a, e.b)
        case e: Tac.CondBranch            => set(e.a, e.b)
        case e: Tac.Jmpr                  => set(e.a)
        case e: Tac.Jalr                  => set(e.a)
        case e: Tac.MemLoad               => set(e.dst, e.addr)
        case e: Tac.MemStore              => set(e.src, e.addr)
        case _                            => ()
      }
    }

    @tailrec
    final def main(): Unit = {
      livenessAnalysis()
      build()
      makeWorklist()

      do {
        if (simplifyWorklist.nonEmpty) {
          simplify()
        } else if (worklistMoves.nonEmpty) {
          coalesce()
        } else if (freezeWorklist.nonEmpty) {
          freeze()
        } else if (spillWorklist.nonEmpty) {
          selectSpill()
        }
      } while (simplifyWorklist.nonEmpty || worklistMoves.nonEmpty || freezeWorklist.nonEmpty || spillWorklist.nonEmpty)
      assignColors()
      if (spilledNodes.nonEmpty) {
        rewriteProgram()
        main()
      }
    }

    private def checkInvariantsAfterBuild(): Unit = {
      def degreeInvariant(): Unit = {
        for (u <- (simplifyWorklist ++ freezeWorklist ++ spillWorklist)) {
          val acc = adjList(u) intersect (precolored ++ simplifyWorklist ++ freezeWorklist ++ spillWorklist)
          require(degree(u) == acc.size)
        }
      }

      def simplifyWorklistInvariant(): Unit = {
        for (u <- simplifyWorklist) {
          require(degree(u) < K)
          val acc = moveList(u) intersect (activeMoves ++ worklistMoves)
          require(acc.isEmpty)
        }
      }

      def freezeWorklistInvariant(): Unit = {
        for (u <- freezeWorklist) {
          require(degree(u) < K)
          val acc = moveList(u) intersect (activeMoves ++ worklistMoves)
          require(acc.nonEmpty)
        }
      }

      def spillWorklistInvariant(): Unit = {
        for (u <- spillWorklist) {
          require(degree(u) <= K)
        }
      }

      degreeInvariant()
      simplifyWorklistInvariant()
      freezeWorklistInvariant()
      spillWorklistInvariant()
    }

    private def addWorklist(u: Temporary): Unit = {
      if (!precolored.contains(u) && !moveRelated(u) && degree(u) < K) {
        freezeWorklist -= u
        simplifyWorklist += u
      }
    }

    private def ok(t: Temporary, r: Temporary): Boolean =
      degree(t) < K || (precolored contains t) || (adjSet contains t -> r)

    private def conservative(nodes: MSet[Temporary]): Boolean = {
      var k = 0
      for (n <- nodes) {
        if (degree(n) >= K) {
          k += 1
        }
      }
      k < K
    }

    private def rewriteProgram(): Unit = {
      val newTemps = MSet[Temporary]()
      val offsets = MMap[Temporary, Offset]()
      for (v <- spilledNodes) {
        val offset =
          if (funEntry.getRegisterCount == 0) Offset(Constants.refSize)
          else Offset(Constants.refSize, funEntry.getRegisterOffset(funEntry.getRegisterCount - 1))
        funEntry.addRegisterOffset(offset)
        offsets += v -> offset
      }
      for (node <- postOrder) {
        var i = 0
        while (i < node.size) {
          val instruction = node(i)
          var skip = 0
          for (k <- instruction.getKills) {
            if (spilledNodes.contains(k)) {
              val t = temp()
              newTemps += t
              val store = Tac.StoreRegister(funDef, t, offsets(k)) -- s"spill register"
              instruction.replaceDefinitions(k, t)
              node.insert(i + 1, store)


              skip += 1
            }
          }
          for (u <- instruction.getUsages) {
            if (spilledNodes.contains(u)) {
              val t = temp()
              newTemps += t
              val load = Tac.LoadRegister(funDef, t, offsets(u)) -- s"reload register"
              instruction.replaceUsages(u, t)
              node.insert(i, load)
              i += 1
            }
          }
          i += 1 + skip
        }
      }
      spilledNodes.clear()
      initial.clear()
      initial ++= coloredNodes
      initial ++= coalescedNodes
      initial ++= newTemps
      coloredNodes.clear()
      coalescedNodes.clear()
    }

    private def assignColors(): Unit = {
      while (selectStack.nonEmpty) {
        val n = selectStack.pop()
        val okColors = (0 until K).to[MSet]
        for (w <- adjList(n)) {
          val aliW = getAlias(w)
          if ((precolored contains aliW) || (coloredNodes contains aliW)) {
            okColors -= color(aliW)
          }
        }
        if (okColors.isEmpty) {
          spilledNodes += n
        } else {
          coloredNodes += n
          val c = okColors.head
          color(n) = c
        }
      }
      for (n <- coalescedNodes) {
        color(n) = color(getAlias(n))
      }
    }

    private def freeze(): Unit = {
      val u = freezeWorklist.head
      freezeWorklist -= u
      simplifyWorklist += u
      freezeMoves(u)
    }

    private def freezeMoves(u: Temporary): Unit = {
      for (m <- nodeMoves(u)) {
        val x = m.dst
        val y = m.src.asInstanceOf[Temporary]
        val v =
          if (getAlias(y) == getAlias(u)) {
            getAlias(x)
          } else {
            getAlias(y)
          }
        activeMoves -= m
        frozenMoves += m
        if (freezeWorklist.contains(v) && nodeMoves(v).isEmpty) {
          freezeWorklist -= v
          simplifyWorklist += v
        }
      }
    }

    private def selectSpill(): Unit = {
      // Use favourite node
      val m = spillWorklist.head
      spillWorklist -= m
      simplifyWorklist += m
      freezeMoves(m)
    }

    private def coalesce(): Unit = {
      val m = worklistMoves.head
      val x = getAlias(m.dst)
      val y = getAlias(m.src.asInstanceOf[Temporary])
      val (u, v) =
        if (precolored.contains(y)) {
          (y, x)
        } else {
          (x, y)
        }
      worklistMoves -= m
      if (u == v) {
        coalescedMoves += m
        addWorklist(u)
      } else if (precolored.contains(v) || adjSet.contains(u -> v)) {
        constrainedMoves += m
        addWorklist(u)
        addWorklist(v)
      } else if ((precolored.contains(u) && adjacent(v).forall(ok(_, u))) ||
                (!precolored.contains(u) && conservative(adjacent(u) ++ adjacent(v)))) {
        coalescedMoves += m
        combine(u, v)
        addWorklist(u)
      } else {
        activeMoves += m
      }
    }

    @tailrec
    private def getAlias(n: Temporary): Temporary = {
      if (coalescedNodes.contains(n)) {
        getAlias(alias(n))
      } else {
        n
      }
    }

    private def combine(u: Temporary, v: Temporary): Unit = {
      if (freezeWorklist contains v) {
        freezeWorklist -= v
      } else {
        spillWorklist -= v
      }
      coalescedNodes += v
      alias(v) = u
      moveList(u) ++= moveList(v)
      enableMoves(MSet(v))
      for (t <- adjacent(v)) {
        addEdge(t, u)
        decrementDegree(t)
      }
      if (degree(u) >= K && (freezeWorklist contains u)) {
        freezeWorklist -= u
        spillWorklist += u
      }
    }

    private def simplify(): Unit = {
      val n = simplifyWorklist.dequeue()
      selectStack push n
      for (m <- adjacent(n)) {
        decrementDegree(m)
      }
    }

    private def decrementDegree(m: Temporary): Unit = {
      val d = degree(m)
      degree(m) = d - 1
      if (d == K) {
        enableMoves(adjacent(m) += m)
        spillWorklist -= m
        if (moveRelated(m)) {
          freezeWorklist += m
        } else {
          simplifyWorklist += m
        }
      }
    }

    private def enableMoves(nodes: MSet[Temporary]): Unit = {
      for (n <- nodes) {
        for (m <- nodeMoves(n)) {
          if (activeMoves contains m) {
            activeMoves -= m
            worklistMoves += m
          }
        }
      }
    }

    private def makeWorklist(): Unit = {
      while (initial.nonEmpty) {
        val n = initial.dequeue()
        if (degree(n) >= K) {
          spillWorklist += n
        } else if (moveRelated(n)) {
          freezeWorklist += n
        } else {
          simplifyWorklist += n
        }
      }
    }

    private def adjacent(n: Temporary) = {
      // adjList[n] \ (selectStack U coalescedNodes)
      val result = adjList.getOrElseUpdate(n, MSet()).to[MSet]
      result --= selectStack
      result --= coalescedNodes
      result
    }

    private def nodeMoves(n: Temporary) = {
      // moveList[n] intersect (activeMoves U worklistMoves)
      val result = MSet[Tac.Mov]()
      val moveN = moveList.getOrElseUpdate(n, MSet())
      result ++= activeMoves
      result ++= worklistMoves
      result intersect moveN
    }

    private def moveRelated(n: Temporary) =
      nodeMoves(n).nonEmpty

    private def addEdge(u: Temporary, v: Temporary): Unit = {
      if (!adjSet.contains(u -> v) && u != v) {
        adjSet += (u -> v)
        adjSet += (v -> u)
        if (!(precolored contains u)) {
          adjList.getOrElseUpdate(u, MSet()) += v
          degree.getOrElseUpdate(u, 0)
          degree(u) += 1
        }
        if (!(precolored contains v)) {
          adjList.getOrElseUpdate(v, MSet()) += u
          degree.getOrElseUpdate(v, 0)
          degree(v) += 1
        }
      }
    }

    private def build(): Unit = {
      for (block <- postOrder) {
        val live = block.getLiveOut.to[MSet]
        var i = block.size - 1
        while (i >= 0) {
          val instr = block(i)
          val kills = instr.getKills
          val uses = instr.getUsages
          if (instr.isInstanceOf[Tac.Mov]) {
            val moveInstr = instr.asInstanceOf[Tac.Mov]
            if (moveInstr.src.isInstanceOf[Temporary]) {
              live --= uses
              for (k <- kills ++ uses) {
                moveList.getOrElseUpdate(k, MSet()) += moveInstr
              }
              worklistMoves += moveInstr
            }
          }
          live ++= kills
          for (k <- kills) {
            for (l <- live) {
              addEdge(l, k)
            }
          }
          // live = use U (live \ def)
          live --= kills
          live ++= uses
          i -= 1
        }
      }
    }

    private def livenessAnalysis(): Unit = {
      for (node <- postOrder) {
        node.clearLiveness()
      }
      var changed = false
      do {
        changed = false
        for (node <- postOrder) {
          val liveOutSize = node.getLiveOut.size
          val liveInSize = node.getLiveIn.size
          val liveOut = node.getLiveOut
          val liveIn = node.getLiveIn
          for (succ <- node.getSuccessors) {
            liveOut ++= succ.getLiveIn
          }
          liveIn ++= liveOut
          val last = node.size - 1
          var i = last
          while (i >= 0) {
            val instruction = node(i)
            liveIn --= instruction.getKills
            liveIn ++= instruction.getUsages
            i -= 1
          }
          changed ||= liveOutSize != liveOut.size || liveInSize != liveIn.size
        }
      } while (changed)
    }
  }

  /**
   * Graph-coloring based register allocation as described in
   * Modern Compiler Implementation in Java by Appel p. 236
   */
  def allocate(program: Tac.TacProgram): Unit = {
    program.functions./*par.*/foreach(new LocalAllocator(_).allocate())
  }

}
