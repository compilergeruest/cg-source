package de.thm.mni.cg.runtime

import scala.collection.mutable.ArrayBuffer
import scala.collection.mutable.ListBuffer
import de.thm.mni.cg.ir._
import de.thm.mni.cg.ir.Immediate._
import de.thm.mni.cg.ir.Tac
import de.thm.mni.cg.semantic._
import de.thm.mni.cg.semantic.Ty
import de.thm.mni.cg.table._
import de.thm.mni.cg.util._
import de.thm.mni.cg.util.cache.Caching
import de.thm.mni.cg.language.Constants
import de.thm.mni.cg.language.ErrorMessages._
import de.thm.mni.cg.ir.Tac.CallerArgumentAddress

// http://web.cs.ucdavis.edu/~pandey/Teaching/ECS142/Lects/runtime.pdf
final class TacGenerator() extends Caching {

  def generate(program: Program): Tac.TacProgram =
    generate(program.globalScope)(program.globalTable)

  private def collectChildFunction(stm: Stm, table: SymbolTable): ListBuffer[(FunDef, SymbolTable)] = {
    val buffer = ListBuffer[(FunDef, SymbolTable)]()
    def helper(stm: Stm, table: SymbolTable): Unit = stm match {
      case e: FunDef =>
        buffer += ((e, table))
      case BlockStm(stms, local) =>
        stms.foreach(helper(_, local))
      case IfStm(_, onTrue, onFalse) =>
        helper(onTrue, table)
        onFalse.foreach(helper(_, table))
      case WhileStm(_, body) =>
        helper(body, table)
      case ForStm(_, _, body, local) =>
        helper(body, local)
      case _ =>
    }
    helper(stm, table)
    buffer
  }

  private val labels = Incremental.curried(i => Tac.Label(_: Ast, "l" + i))
  private def syntheticLabel(ast: Ast) = labels()(ast)

  private val temporary = Incremental(Temporary("t", _))

  private val display = Immediate("display")

  private def generateFrameAllocation(entry: FunEntry)(implicit table: SymbolTable, buffer: ListBuffer[Tac]): Unit = {
    val fun = entry.getFunDef
    val lexicalDepth = entry.getLexicalDepth
    // Step 1.: Load Display Content at lexical depth of main
    // Step 2.: Store Display Content of Step 1. in stack frame of main
    // Step 3.: Store current FP in Display at lexical depth of main
    buffer += Tac.StoreDisplayEntry(fun, temporary(), temporary(), (lexicalDepth - 1) * 4)
    if (entry.isCalling) {
      buffer += Tac.StoreReturnRegister(fun)
    }
  }

  private def generateFrameDeallocation(entry: FunEntry)(implicit table: SymbolTable, buffer: ListBuffer[Tac]): Unit = {
    val fun = entry.getFunDef
    val lexicalDepth = entry.getLexicalDepth
    // Step 1.: Load Display Content from stack frame of main
    // Step 2.: Store Display Content of 1 into Display at lexical depth of main
    buffer += Tac.Label(fun, entry.syntheticEndLabel)
    buffer += Tac.LoadDisplayEntry(fun, temporary(), temporary(), (lexicalDepth - 1) * 4)
    buffer += Tac.Use(fun, Tac.rrTemporary)
    if (entry.isCalling) {
      buffer += Tac.LoadReturnRegister(fun)
    }
  }

  private def generate(fun: FunDef)(implicit table: SymbolTable): Tac.TacProgram = {
    val FunDef(name, out, in, body, local) = fun
    val entry = table[FunEntry](name).getOrElse(functionNotExistError(name))

    val buffer = ListBuffer[Tac]()
    buffer += Tac.Label(fun, entry.syntheticName) -- s"function ${fun.getName.name}"
    buffer += Tac.AllocStackFrame(fun)

    val calleeSaves = (16 to 23).map(_ => temporary())
    // define all forbidden registers (to prevent register-reassignment)
    buffer += Tac.Def(fun, Tac.zeroTemporary)
    buffer += Tac.Def(fun, Tac.assemblerReserved)
    buffer += Tac.Def(fun, Tac.kernelReserved1)
    buffer += Tac.Def(fun, Tac.kernelReserved2)
    buffer += Tac.Def(fun, Tac.kernelReserved3)

    // do not use the return and/or the argument reserved registers
    buffer += Tac.Def(fun, Tac.returnTemporary1)
    buffer += Tac.Def(fun, Tac.returnTemporary2)
    buffer += Tac.Def(fun, Tac.argTemporary1)
    buffer += Tac.Def(fun, Tac.argTemporary2)
    buffer += Tac.Def(fun, Tac.argTemporary3)
    buffer += Tac.Def(fun, Tac.argTemporary4)

    // define all callee-save registers
    buffer += Tac.Def(fun, Tac.calleeSaved1)
    buffer += Tac.Def(fun, Tac.calleeSaved2)
    buffer += Tac.Def(fun, Tac.calleeSaved3)
    buffer += Tac.Def(fun, Tac.calleeSaved4)
    buffer += Tac.Def(fun, Tac.calleeSaved5)
    buffer += Tac.Def(fun, Tac.calleeSaved6)
    buffer += Tac.Def(fun, Tac.calleeSaved7)
    buffer += Tac.Def(fun, Tac.calleeSaved8)

    // define $29, $30 and $31
    buffer += Tac.Def(fun, Tac.spTemporary)
    buffer += Tac.Def(fun, Tac.irTemporary)
    buffer += Tac.Def(fun, Tac.rrTemporary)

    buffer += Tac.Mov(fun, calleeSaves(0), Tac.calleeSaved1)
    buffer += Tac.Mov(fun, calleeSaves(1), Tac.calleeSaved2)
    buffer += Tac.Mov(fun, calleeSaves(2), Tac.calleeSaved3)
    buffer += Tac.Mov(fun, calleeSaves(3), Tac.calleeSaved4)
    buffer += Tac.Mov(fun, calleeSaves(4), Tac.calleeSaved5)
    buffer += Tac.Mov(fun, calleeSaves(5), Tac.calleeSaved6)
    buffer += Tac.Mov(fun, calleeSaves(6), Tac.calleeSaved7)
    buffer += Tac.Mov(fun, calleeSaves(7), Tac.calleeSaved8)

//   Source: Apple, Modern Compiler Implementation in Java p. 228
//    def(r 7 )
//    t 231 ← r 7
//    .
//    .
//    r 7 ← t 231
//    exit: use(r 7 )

    generateFrameAllocation(entry)(local, buffer)
    generate(body)(local, buffer)
    generateFrameDeallocation(entry)(local, buffer)

    // "use" all callee-save registers
    buffer += Tac.Mov(fun, Tac.calleeSaved1, calleeSaves(0))
    buffer += Tac.Mov(fun, Tac.calleeSaved2, calleeSaves(1))
    buffer += Tac.Mov(fun, Tac.calleeSaved3, calleeSaves(2))
    buffer += Tac.Mov(fun, Tac.calleeSaved4, calleeSaves(3))
    buffer += Tac.Mov(fun, Tac.calleeSaved5, calleeSaves(4))
    buffer += Tac.Mov(fun, Tac.calleeSaved6, calleeSaves(5))
    buffer += Tac.Mov(fun, Tac.calleeSaved7, calleeSaves(6))
    buffer += Tac.Mov(fun, Tac.calleeSaved8, calleeSaves(7))

    buffer += Tac.Use(fun, Tac.calleeSaved8)
    buffer += Tac.Use(fun, Tac.calleeSaved7)
    buffer += Tac.Use(fun, Tac.calleeSaved6)
    buffer += Tac.Use(fun, Tac.calleeSaved5)
    buffer += Tac.Use(fun, Tac.calleeSaved4)
    buffer += Tac.Use(fun, Tac.calleeSaved3)
    buffer += Tac.Use(fun, Tac.calleeSaved2)
    buffer += Tac.Use(fun, Tac.calleeSaved1)

    // use $29, $30 and $31 for interference
    buffer += Tac.Use(fun, Tac.spTemporary)
    buffer += Tac.Use(fun, Tac.irTemporary)

    // do not use the return and/or the argument reserved registers
    buffer += Tac.Use(fun, Tac.returnTemporary1)
    buffer += Tac.Use(fun, Tac.returnTemporary2)
    buffer += Tac.Use(fun, Tac.argTemporary1)
    buffer += Tac.Use(fun, Tac.argTemporary2)
    buffer += Tac.Use(fun, Tac.argTemporary3)
    buffer += Tac.Use(fun, Tac.argTemporary4)

    // use all forbidden registers, so that they interfere with any other temporaries
    buffer += Tac.Use(fun, Tac.zeroTemporary)
    buffer += Tac.Use(fun, Tac.assemblerReserved)
    buffer += Tac.Use(fun, Tac.kernelReserved1)
    buffer += Tac.Use(fun, Tac.kernelReserved2)
    buffer += Tac.Use(fun, Tac.kernelReserved3)

    buffer += Tac.DeallocStackFrame(fun)
    buffer += Tac.Return(fun)

    val functions = ListBuffer(Tac.TacFunction(buffer.to[ArrayBuffer], entry))

    val childFunctions = collectChildFunction(body, local)
    // deeper nested functions
    childFunctions.foreach {
      case (function, childTable) =>
        functions ++= generate(function)(childTable).functions
    }

    Tac.TacProgram(functions.to[ArrayBuffer], table)
  }

  private def storeInstr(exp: Exp, src: Temporary, addr: Temporary, offset: Immediate)(implicit table: SymbolTable): Tac =
    storeInstr(exp, exp.getTy, src, addr, offset)

  private def storeInstr(ast: Ast, ty: Ty, src: Temporary, addr: Temporary, offset: Immediate)(implicit table: SymbolTable): Tac =
    ty.stackSize match {
      case 1 => Tac.Stb(ast, src, addr, offset)
      case 4 => Tac.Stw(ast, src, addr, offset)
      case n => unknownByteSizeError(n)
    }

  private def generate(expr: NewArrExp)(implicit table: SymbolTable, buffer: ListBuffer[Tac]): Temporary = {
    val skip = syntheticLabel(expr)
    var first: Temporary = null
    val previouslyAllocated = ListBuffer[(Temporary, Temporary)]()

    def helper(dimensions: List[Option[Exp]], baseType: Ty): Temporary = dimensions match {
      case Some(exp) :: tl =>
        val size = generate(exp, deref = true)
        val byteSize = temporary()
        // check if given array size is greater than 0
        buffer += Tac.Ble(expr, size, Tac.zeroTemporary, "_illegalArraySize")
        // multiply by base size if necessary
        baseType.stackSize match {
          case 1 =>
            buffer += Tac.Add(expr, byteSize, size, 4)
          case 4 =>
            buffer += Tac.Sll(expr, byteSize, size, 2)
            buffer += Tac.Add(expr, byteSize, byteSize, 4)
          case n =>
            unknownByteSizeError(n)
        }
        // call malloc
        val argument = temporary()
        val returnValue = temporary()
        val result = temporary()
        if (first eq null) {
          first = result
        }
        buffer += Tac.Add(expr, Tac.argTemporary1, byteSize, 0)
        buffer += Tac.Jal(expr, "_malloc")
        buffer += Tac.Add(expr, result, Tac.returnTemporary1, 0)
        // initialize if not null
        val initialize = syntheticLabel(expr)
        buffer += Tac.Bne(expr, result, Tac.zeroTemporary, initialize)
        // generate code for free
        for ((previousStart, previousIterator) <- previouslyAllocated.reverse) {
          val ploop = syntheticLabel(expr)
          val pcheck = syntheticLabel(expr)
          val pargument = temporary()
          val paddress = temporary()
          // subtract reference size of failed element
          buffer += Tac.Sub(expr, previousIterator, previousIterator, Constants.refSize)
          // jump to loop-check
          buffer += Tac.Jmp(expr, pcheck)
          // loop body
          buffer += ploop
          buffer += Tac.Ldw(expr, paddress, previousIterator, 0)
          buffer += Tac.Add(expr, Tac.argTemporary1, paddress, 0)
          buffer += Tac.Jal(expr, "_free")
          buffer += Tac.Sub(expr, previousIterator, previousIterator, Constants.refSize)
          // loop check
          buffer += pcheck
          buffer += Tac.Bgtu(expr, previousIterator, previousStart, ploop)
        }
        buffer += Tac.Mov(expr, first, 0)
        // skip initialization
        buffer += Tac.Jmp(expr, skip)
        // initialize
        buffer += initialize
        // store array size
        buffer += Tac.Stw(expr, size, result, 0)

        // initialize everything else
        val end = temporary()
        val iterator = temporary()
        val loop = syntheticLabel(expr)
        val check = syntheticLabel(expr)
        // add iterator and start address
        previouslyAllocated += ((result, iterator))
        // initialize end address
        buffer += Tac.Add(expr, end, result, byteSize)
        // initialize start address
        buffer += Tac.Add(expr, iterator, result, 4)
        // jump to check
        buffer += Tac.Jmp(expr, check)
        // loop-label
        buffer += loop

        if (tl.headOption.flatten.isDefined) {
          // allocate sub-array
          val sub = helper(tl, baseType.cast[ArrayTy].getOrElse(illegalBaseTyOfArrayError(baseType)).base)
          require(baseType.stackSize == Constants.refSize, baseTyOfArrayNoRef)
          buffer += Tac.Stw(expr, sub, iterator, 0)
        } else {
          // store null / zero at the current address
          buffer += storeInstr(expr, baseType, Tac.zeroTemporary, iterator, 0)
        }

        // increment address by byte offset of one element
        buffer += Tac.Add(expr, iterator, iterator, baseType.stackSize)
        // check of loop-condition
        buffer += check
        buffer += Tac.Blt(expr, iterator, end, loop)

        result
      case _ =>
        allocationOfEmptyDimError()
    }
    val startType = Ty.resolve(expr.getTy).getOrElse(invalidTypeOfExpError(expr))
    val baseType = startType.cast[ArrayTy].getOrElse(xIsNotArrayTyError(startType)).base
    val result = helper(expr.getDimensions.toList, baseType)
    require(result eq first, returnedTempNotEqualToFst)
    buffer += skip
    result
  }

  private def generate(stm: Stm)(implicit table: SymbolTable, buffer: ListBuffer[Tac]): Unit = stm match {
    case _: Def => ()
    case AssignStm(lhs, rhs) =>
      val right = generate(rhs, deref = true)
      if (lhs.isEmpty) {
        emptyLhsNotLegalError()
      } else {
        val left = generate(lhs.head, deref = false)
        buffer += storeInstr(stm, lhs.head.getTy, right, left, 0)
        // when more than one destination specified -> right-hand-side has to be a function
        if (lhs.size > 1) {
          rhs match {
            case f: FunExp =>
              val entry = table[FunEntry](f.getName).getOrElse(functionNotExistError(f.getName))
              var offset = Offset(entry.getOutTys(0).stackSize)
              for (i <- 1 until lhs.size) {
                val tmpAddr = temporary()
                val tmp = temporary()
                val stackSize = entry.getOutTys(i).stackSize
                offset += stackSize
                buffer += new Tac.CallerReturnAddress(stm, tmpAddr, offset) with Tac.Function {
                  val function = entry
                }
                stackSize match {
                  case 4 => buffer += Tac.Ldw(stm, tmp, tmpAddr, 0)
                  case 1 => buffer += Tac.Ldb(stm, tmp, tmpAddr, 0)
                  case n => unknownByteSizeError(n)
                }
                val res = generate(lhs(i), deref = false)
                buffer += storeInstr(stm, lhs(i).getTy, right, left, 0)
              }
            case _ =>
              assignToNonScalarWithoutFunCallError()
          }
        }
      }
    case BlockStm(stms, local) =>
      stms.foreach(generate(_)(local, buffer))
    case DelStm(exp) =>
      val addr = temporary()
      val tmp1 = generate(exp, deref = true)
      ensureNotNull(exp, tmp1)
      buffer += Tac.Add(stm, Tac.argTemporary1, tmp1, 0)
      buffer += Tac.Jal(stm, "_free")
    case ExpStm(exp) =>
      generate(exp, deref = true)
    case ForStm(VarDef(_, _, name), Range(start, next, end), body, local) =>
      val fentry = local.associatedFunEntry.getOrElse(xWithoutEnclosingFuncError("for-statement"))
      val ventry = local[VarEntry](name).getOrElse(variableNotExistError(name))
      val loopLbl = syntheticLabel(stm)
      val afterLbl = syntheticLabel(stm)

      val startExp = generate(start, deref = true)
      val endExp = generate(end, deref = true)
      val stepExp = next match {
        case Some(exp) =>
          val tmp = generate(exp, deref = true)
          buffer += Tac.Sub(stm, tmp, tmp, startExp)
          tmp
        case None =>
          // check if start < end
          val tmp = temporary()
          val lbl = syntheticLabel(stm)
          buffer += Tac.Mov(stm, tmp, 1) -- "assume positive " // assume positive increment
          buffer += Tac.Ble(stm, startExp, endExp, lbl)
          buffer += Tac.Mov(stm, tmp, -1) // start > end -> negative increment
          buffer += lbl // when jumped here, assumption was correct
          tmp
      }

      // load address of variable `ventry` and then store `startExp` at this address
      val addr = temporary()
      buffer += Tac.LoadLocalAddress(stm, addr, ventry.getOffset) -- "load address of counter variable" // load address of counter variable
      buffer += Tac.Stw(stm, startExp, addr, 0) -- "initialize counter variable" // store word => counter variable is always `int`
      buffer += Tac.Jmp(stm, afterLbl) -- "jump to condition check"
      // start label of loop
      buffer += loopLbl -- "loop header"
      generate(body)(local, buffer)
      val tmp1 = temporary()
      val tmp2 = temporary()
      buffer += Tac.LoadLocalAddress(stm, tmp1, ventry.getOffset) -- "load address of counter variable" // load address of counter variable
      buffer += Tac.Ldw(stm, tmp2, tmp1, 0) -- "load value of counter variable" // load content of counter variable
      buffer += Tac.Add(stm, tmp2, tmp2, stepExp) -- "increment by step-width" // increment counter variable
      buffer += Tac.Stw(stm, tmp2, tmp1, 0) -- "store counter variable" // store counter variable
      // end label of loop
      buffer += afterLbl -- "for-statement condition check"
      val tmp3 = temporary()
      buffer += Tac.LoadLocalAddress(stm, tmp3, ventry.getOffset) -- "load address of counter variable" // load address of counter variable
      buffer += Tac.Ldw(stm, tmp3, tmp3, 0) -- "load value of counter variable" // load content of counter variable
      val conditionLbl = syntheticLabel(stm)
      val endLbl = syntheticLabel(stm)
      buffer += Tac.Blt(stm, stepExp, Tac.zeroTemporary, conditionLbl)
      buffer += Tac.Blt(stm, tmp3, endExp, loopLbl) -- "continue"
      buffer += Tac.Jmp(stm, endLbl)
      buffer += conditionLbl
      buffer += Tac.Bgt(stm, tmp3, endExp, loopLbl) -- "continue"
      buffer += endLbl
    case IfStm(test, onTrue, onFalse) =>
      val testTmp = generate(test, deref = true)
      val lblOnFalse = syntheticLabel(stm)
      // if (expression == 0) jump to false
      buffer += Tac.Beq(stm, testTmp, Tac.zeroTemporary, lblOnFalse)
      // if (expression != 0) true-part
      generate(onTrue)
      // else defined?
      if (onFalse.isDefined) {
        val lblOnTrue = syntheticLabel(stm)
        // skip else-part
        buffer += Tac.Jmp(stm, lblOnTrue)
        // executed if expression was equal to 0
        buffer += lblOnFalse
        generate(onFalse.get)
        buffer += lblOnTrue
      } else {
        buffer += lblOnFalse
      }
    case InitStm(vars, rhs) =>
      val right = generate(rhs, deref = true)
      if (vars.isEmpty) {
        emptyLhsNotLegalError()
      } else {
        val fentry = table.associatedFunEntry.getOrElse(xWithoutEnclosingFuncError("init-statement"))
        val head = vars.head
        val ventry = table[VarEntry](head.getName).getOrElse(variableNotExistError(head.getName))
        val addr = temporary()
        buffer += Tac.LoadLocalAddress(stm, addr, ventry.getOffset)
        buffer += storeInstr(stm, ventry.getTy, right, addr, 0)
        if (vars.size > 1) {
          rhs match {
            case f: FunExp =>
              val entry = table[FunEntry](f.getName).getOrElse(functionNotExistError(f.getName))
              for (i <- 1 until vars.size) {
                val vari = vars(i)
                val ventry = table[VarEntry](vari.getName).getOrElse(variableNotExistError(vari.getName))
                val retTmp = temporary()
                val tmp = temporary()
                buffer += new Tac.CallerReturnAddress(stm, retTmp, entry.getReturnOffset(i)) with Tac.Function {
                  val function = entry
                }
                entry.getOutTys(i).stackSize match {
                  case 4 => buffer += Tac.Ldw(stm, tmp, retTmp, 0)
                  case 1 => buffer += Tac.Ldb(stm, tmp, retTmp, 0)
                  case n => unknownByteSize(n)
                }
                val addr = temporary()
                buffer += Tac.LoadLocalAddress(stm, addr, ventry.getOffset)
                buffer += storeInstr(stm, ventry.getTy, tmp, addr, 0)
              }
            case _ =>
              assignToNonScalarWithoutFunCallError()
          }
        }
      }
    case ReturnStm(ArrayBuffer(f @ FunExp(name, args, ty: TupleTy))) if ty.tys.size > 1 =>
      val entry = table.associatedFunEntry.getOrElse(foundXWithoutAssociatedFunEntryError("return statement"))
      val fentry = table[FunEntry](name).getOrElse(functionNotExistError(name))
      val endLabel = entry.syntheticEndLabel

      val first = generate(f, deref = true)
      val tmpAddr = temporary()
      buffer += new Tac.CalleeReturnAddress(f, tmpAddr, entry.getReturnOffset(0)) with Tac.Function {
        val function = entry
      }
      buffer += storeInstr(f, ty.tys(0), first, tmpAddr, 0)

      for (i <- 1 until ty.tys.size) {
        val retTmp = temporary()
        buffer += new Tac.CallerReturnAddress(f, retTmp, fentry.getReturnOffset(i)) with Tac.Function {
          val function = fentry
        }
        val tmp = temporary()
        fentry.getOutTys(i).stackSize match {
          case 4 => buffer += Tac.Ldw(f, tmp, retTmp, 0)
          case 1 => buffer += Tac.Ldb(f, tmp, retTmp, 0)
          case n => unknownByteSize(n)
        }
        val tmpAddr = temporary()
        buffer += new Tac.CalleeReturnAddress(f, tmpAddr, entry.getReturnOffset(i)) with Tac.Function {
          val function = entry
        }
        buffer += storeInstr(f, ty.tys(i), tmp, tmpAddr, 0)
      }

//      val entry = table[FunEntry](f.getName).getOrElse(functionNotExistError(f.getName))
//      for (i <- 1 until vars.size) {
//        val vari = vars(i)
//        val ventry = table[VarEntry](vari.getName).getOrElse(variableNotExistError(vari.getName))
//        val retTmp = temporary()
//        val tmp = temporary()
//        buffer += new Tac.CallerReturnAddress(stm, retTmp, entry.getReturnOffset(i)) with Tac.Function {
//          val function = entry
//        }
//        entry.getOutTys(i).stackSize match {
//          case 4 => buffer += Tac.Ldw(stm, tmp, retTmp, 0)
//          case 1 => buffer += Tac.Ldb(stm, tmp, retTmp, 0)
//          case n => unknownByteSize(n)
//        }
//        val addr = temporary()
//        buffer += Tac.LoadLocalAddress(stm, addr, ventry.getOffset)
//        buffer += storeInstr(stm, ventry.getTy, tmp, addr, 0)
//      }

      buffer += Tac.Jmp(stm, endLabel)
    case ReturnStm(exps) =>
      val entry = table.associatedFunEntry.getOrElse(foundXWithoutAssociatedFunEntryError("return statement"))
      val endLabel = entry.syntheticEndLabel

      for (i <- exps.indices) {
        val tmpAddr = temporary()
        val tmp = generate(exps(i), deref = true)
        buffer += new Tac.CalleeReturnAddress(exps(i), tmpAddr, entry.getReturnOffset(i)) with Tac.Function {
          val function = entry
        }
        buffer += storeInstr(exps(i), tmp, tmpAddr, 0)
      }
      buffer += Tac.Jmp(stm, endLabel)
    case WhileStm(test, onTrue) =>
      val loopLbl = syntheticLabel(stm)
      val afterLbl = syntheticLabel(stm)
      buffer += Tac.Jmp(stm, afterLbl) -- "skip while-loop body first time"
      buffer += loopLbl -- "start of the while-loop body"
      generate(onTrue)
      buffer += afterLbl
      val test1 = generate(test, deref = true)
      buffer += Tac.Bne(stm, test1, Tac.zeroTemporary, loopLbl.name) -- "as long as the condition is true jump back to the start of the while-loop body"
  }

  private type ComparisonBinOpPart = (Ast, Temporary, Temporary, LabelAddress) => Tac
  private object ComparisonBinOp {
    def unapply(op: BinOp): Option[ComparisonBinOpPart] =
      Option(op) collect {
        case Eq => Tac.Bne
        case Ne => Tac.Beq
        case Lt => Tac.Bge
        case Le => Tac.Bgt
        case Gt => Tac.Ble
        case Ge => Tac.Blt
      }
  }

  private type ShortCircuitedBinOpPart = (Ast, Temporary, Temporary, LabelAddress) => Tac
  private object ShortCircuitedBinOp {
    def unapply(op: BinOp): Option[ShortCircuitedBinOpPart] = Option(op) collect {
      case And => Tac.Beq
      case Or => Tac.Bne
    }
  }

  private def functionWhereDefined(name: Name)(implicit table: SymbolTable): FunEntry =
    if (table.isDefinedInFunction[FieldEntry](name)) {
      table.associatedFunEntry.getOrElse(definitionNotExistError(name))
    } else {
      val functionTable = table.associatedFunEntry.getOrElse(definitionNotExistError(name))
      val outerTable = functionTable.getFunDef.getTable
      functionWhereDefined(name)(outerTable)
    }

  private def generate(vari: Var, deref: Boolean)(implicit table: SymbolTable, buffer: ListBuffer[Tac]): Temporary = vari match {
    case NameVar(name @ Name(n), _)     =>
      val entry = table[FieldEntry](name).getOrElse(fieldNotExistError(name))
      val fun = table.associatedFunEntry.getOrElse(xWithoutEnclosingFuncError("local variable"))

      if (table.isDefinedInFunction[FieldEntry](name)) {
        buffer += Tac -- s"Access ${entry.describe} '$n' from current stackframe"
        if (deref) {
          val addr = temporary()
          val res = temporary()
          if (entry.isVarDef) {
            buffer += Tac.LoadLocalAddress(vari, addr, entry.getOffset)
          } else {
            buffer += Tac.CalleeArgumentAddress(vari, addr, entry.getOffset)
          }
          if (entry.getTy.stackSize == 4) {
            buffer += Tac.Ldw(vari, res, addr, 0)
          } else {
            buffer += Tac.Ldb(vari, res, addr, 0)
          }
          res
        } else {
          val res = temporary()
          if (entry.isVarDef) {
            buffer += Tac.LoadLocalAddress(vari, res, entry.getOffset)
          } else {
            buffer += Tac.CalleeArgumentAddress(vari, res, entry.getOffset)
          }
          res
        }
      } else {
        val outer = functionWhereDefined(name)
        buffer += Tac -- s"Access parameter or variable '$n' from lexical parent function '${outer.getFunDef.getName.name}' via display"
        val lexicalDepth = (outer.getLexicalDepth - 1) * 4
        val offset = temporary()
        val staticLink = temporary()
        buffer += Tac.Mov(vari, offset, lexicalDepth)
        buffer += Tac.Ldw(vari, staticLink, offset, display)
        if (deref) {
          val addr = temporary()
          val res = temporary()
          buffer += new Tac.LoadStaticAddress(vari, addr, staticLink, entry.getOffset) with Tac.Function {
            val function = outer
          }
          buffer += Tac.Ldw(vari, res, addr, 0)
          res
        } else {
          val res = temporary()
          buffer += new Tac.LoadStaticAddress(vari, res, staticLink, entry.getOffset) with Tac.Function {
            val function = outer
          }
          res
        }
      }
    case ArrayVar(base, index, _)   =>
      val tmp1 = generate(base, deref = true)
      ensureNotNull(vari, tmp1)
      val indexValue = generate(index, deref = true)
      val arraySize = temporary()

      val arrayTy = Ty.resolve(base.getTy).flatMap(_.cast[ArrayTy]).getOrElse(illegalBaseTyOfArrayAccessError(base.getTy))

      buffer += Tac.Ldw(vari, arraySize, tmp1, 0)
      buffer += Tac.Bgeu(vari, indexValue, arraySize, "_indexError")

      if (arrayTy.base.stackSize != 1) {
        val indexOffset = temporary()
        val indexWithSkip = temporary()
        buffer += Tac.Mul(vari, indexOffset, indexValue, arrayTy.base.stackSize)
        buffer += Tac.Add(vari, indexWithSkip, indexOffset, 4)
        if (deref) {
          val addr = temporary()
          val res = temporary()
          buffer += Tac.Add(vari, addr, tmp1, indexWithSkip)
          buffer += Tac.Ldw(vari, res, addr, 0)
          res
        } else {
          val res = temporary()
          buffer += Tac.Add(vari, res, tmp1, indexWithSkip)
          res
        }
      } else {
        val indexWithSkip = temporary()
        buffer += Tac.Add(vari, indexWithSkip, indexValue, 4)
        if (deref) {
          val addr = temporary()
          val res = temporary()
          buffer += Tac.Add(vari, addr, tmp1, indexWithSkip)
          buffer += Tac.Ldb(vari, res, addr, 0)
          res
        } else {
          val res = temporary()
          buffer += Tac.Add(vari, res, tmp1, indexWithSkip)
          res
        }
      }
    case AccessVar(base, member, _) =>
      val tmp1 = generate(base, deref = true)
      ensureNotNull(vari, tmp1)
      val rawTy = Ty.resolve(base.getTy).getOrElse(invalidTypeOfExpError(base))

      if (rawTy.isInstanceOf[UnionTy]) {
        val unionTy = rawTy.asInstanceOf[UnionTy]
        if (deref) {
          val res = temporary()
          buffer += Tac.Ldw(vari, res, tmp1, 0)
          res
        } else {
          tmp1
        }
      } else {
        val structTy = rawTy.cast[StructTy].getOrElse(invalidTypeError(rawTy))
        if (deref) {
          val addr = temporary()
          val res = temporary()
          buffer += Tac.Add(vari, addr, tmp1, structTy.offsetOf(member.name))
          buffer += Tac.Ldw(vari, res, addr, 0)
          res
        } else {
          val res = temporary()
          buffer += Tac.Add(vari, res, tmp1, structTy.offsetOf(member.name))
          res
        }
      }
  }

  private def generate(exp: Exp, deref: Boolean)(implicit table: SymbolTable, buffer: ListBuffer[Tac]): Temporary = exp match {
    case l: Lit => generate(l, deref)
    case v: Var => generate(v, deref)
    case BinExp(SingleInstBinOp(f), left, right, _) =>
      val tmp1 = generate(left, deref = true)
      val tmp2 = generate(right, deref = true)
      val res = temporary()
      buffer += f(exp, res, tmp1, tmp2)
      res
    case BinExp(ComparisonBinOp(f), left, right, _) =>
      val tmp1 = generate(left, deref = true)
      val tmp2 = generate(right, deref = true)
      val res = temporary()
      val lbl = syntheticLabel(exp)
      buffer += Tac.Mov(exp, res, Tac.zeroTemporary)
      buffer += f(exp, tmp1, tmp2, lbl)
      buffer += Tac.Mov(exp, res, 1)
      buffer += lbl
      res
    case BinExp(ShortCircuitedBinOp(f), left, right, _) =>
      val tmp1 = generate(left, deref = true)
      val res = temporary()
      val lbl1 = syntheticLabel(exp)
      // assume that result := left-value
      buffer += Tac.Mov(exp, res, tmp1)
      // AND := Beq, OR := Bne
      // skip right-value if result is already final
      val skip = f(exp, tmp1, Tac.zeroTemporary, lbl1)
      buffer += skip
      // calculate right-value
      val tmp2 = generate(right, deref = true)
      // result := right-value (left-value is true (in case of AND) / false (in case of OR))
      buffer += Tac.Mov(exp, res, tmp2)
      buffer += lbl1
      res
    case CastExp(_, exp, _) =>
      generate(exp, deref = true)
    case funExp @ FunExp(name @ Name(n), exps, _) =>
      val entry = table[FunEntry](name).getOrElse(functionNotExistError(name))

      val paramOffsets = entry.getFunDef.getParams.map { param =>
        val parEntry = entry.getFunDef.getLocalTable[ParEntry](param.getName)
          .getOrElse(parameterNotExistError(param.getName, name))
        parEntry.getOffset
      }
      if (entry.isStdFunction && n == "printc") {
        // spl does not know bytes -> use int instead
        val tmpAddr = temporary()
        val tmp = generate(exps.head, deref = true)
        buffer += new Tac.CallerArgumentAddress(funExp, tmpAddr, paramOffsets.head) with Tac.Function {
          val function = entry
        }
        buffer += Tac.Stw(funExp, tmp, tmpAddr, 0)
      } else if (entry.isStdFunction && (n == "readc" || n == "readi" || n == "time")) {
        val tmpAddr = temporary()
        // special treatment for spl-standard functions
        buffer += new Tac.CallerArgumentAddress(funExp, tmpAddr, Offset(4)) with Tac.Function {
          val function = entry
        }
        buffer += new Tac.Stw(funExp, tmpAddr, tmpAddr, 0)
      } else {
        val arguments = ArrayBuffer[(Temporary, Ty)]()
        // 1.: generate all arguments
        for (exp <- exps) {
          arguments += generate(exp, deref = true) -> exp.getTy
        }
        // 2.: store all arguments
        for (((argument, ty), offset) <- arguments.zip(paramOffsets)) {
          val tmpAddr = temporary();
          buffer += new Tac.CallerArgumentAddress(funExp, tmpAddr, offset) with Tac.Function {
            val function = entry
          }
          buffer += storeInstr(funExp, ty, argument, tmpAddr, 0)
        }
      }
      buffer += Tac.Jal(funExp, entry.syntheticName)
      val tmpAddr = temporary()
      val res = temporary()
      if (entry.getOutTys.isEmpty) {
        illegalEmptyReturnListError()
      } else if (entry.getOutTys.head != Ty.unitTy) {
        buffer += new Tac.CallerReturnAddress(funExp, tmpAddr, entry.getReturnOffset(0)) with Tac.Function {
          val function = entry
        }
        if (entry.isStdFunction && n == "readc") {
          val swap = temporary()
          buffer += Tac.Ldw(funExp, res, tmpAddr, 0)
        } else {
          entry.getOutTys.head.stackSize match {
            case 4 => buffer += Tac.Ldw(funExp, res, tmpAddr, 0)
            case 1 => buffer += Tac.Ldb(funExp, res, tmpAddr, 0)
            case n => unknownByteSize(n)
          }
        }
      }
      res
    case arr: NewArrExp => generate(arr)
    case NewObjExp(Name(name), members, ty) =>
      val resolved = Ty.resolve(ty).getOrElse(invalidTypeError(ty))
      val size = resolved match {
        case t: StructTy => t.heapSize
        case t: UnionTy  => t.heapSize
        case t           => tyIsNotComplexError(t)
      }
      val tmp1Addr = temporary()
      val tmp1 = temporary()
      val tmp2Addr = temporary()
      val tmp2 = temporary()
      val tyString = ellipsis(38, ty.toString)

      buffer += Tac.Mov(exp, tmp1, Immediate(size)) -- s"size for type '$tyString': $size"
      buffer += Tac.Add(exp, Tac.argTemporary1, tmp1, 0)
      buffer += Tac.Jal(exp, "_malloc")
      buffer += Tac.Add(exp, tmp2, Tac.returnTemporary1, 0)
      onNotNull(exp, tmp2) {
        for ((Name(member), expr) <- members) {
          val expValue = generate(expr, deref = true)
          val offs = resolved match {
            case t: StructTy => t.offsetOf(member)
            case t: UnionTy  => 0
            case t           => tyIsNotComplexError(t)
          }
          buffer += storeInstr(exp, expValue, tmp2, offs) -- s"store member '$member' of type '$tyString'"
        }
      }
      tmp2
    case UnaryExp(Size, exp, _) =>
      val tmp1 = generate(exp, deref = true)
      ensureNotNull(exp, tmp1)
      val res = temporary()
      buffer += Tac.Ldw(exp, res, tmp1, 0) -- "load size of array"
      res
    case UnaryExp(Minus, exp, _) =>
      val tmp1 = generate(exp, deref = true)
      val res = temporary()
      buffer += Tac.Sub(exp, res, Tac.zeroTemporary, tmp1)
      res
    case UnaryExp(Not, exp, _) =>
      val tmp1 = generate(exp, deref = true)
      val res = temporary()
      val lbl = syntheticLabel(exp)
      buffer += Tac.Mov(exp, res, Tac.zeroTemporary)
      buffer += Tac.Bne(exp, tmp1, Tac.zeroTemporary, lbl)
      buffer += Tac.Mov(exp, res, 1)
      buffer += lbl
      res
    case UnaryExp(Bnot, exp, _) =>
      val tmp1 = generate(exp, deref = true)
      val res = temporary()
      buffer += Tac.Xnor(exp, res, tmp1, Tac.zeroTemporary)
      res
  }

  private def generate(lit: Lit, deref: Boolean)(implicit table: SymbolTable, buffer: ListBuffer[Tac]): Temporary =
    lit match {
      case IntLit(n, _, _) =>
        withEffect(temporary()) { res =>
          buffer += Tac.Mov(lit, res, Immediate(n)) -- s"integer literal $n"
        }
      case NullLit(_) =>
        withEffect(temporary()) { res =>
          buffer += Tac.Mov(lit, res, Tac.zeroTemporary) -- "null literal"
        }
      case StringLit(s, _) =>
        val tmp1Addr = temporary()
        val tmp1 = temporary()
        val tmp2Addr = temporary()
        val tmp2 = temporary()
        buffer += Tac.Mov(lit, tmp1, s.length() + 4) -- s"space for ${s.length()} chars + 4 (size field)"
        buffer += Tac.Add(lit, Tac.argTemporary1, tmp1, 0)
        buffer += Tac.Jal(lit, "_malloc")
        buffer += Tac.Add(lit, tmp2, Tac.returnTemporary1, 0)
        onNotNull(lit, tmp2) {
          val tmp3 = temporary()
          val litr = ellipsis(40, s)

          buffer += Tac.Mov(lit, tmp3, s.length) -- s"""length of string literal "$litr""""
          buffer += Tac.Stw(lit, tmp3, tmp2, 0) -- "store the length of the string literal"

          var len = s.length
          var i = 0
          while (len >= 4) {
            val tmp4 = temporary()
            // <char-addr 1> <char-addr 2> <char-addr 3> <char-addr 4>
            val (a, b, c, d) = (s(i), s(i + 1), s(i + 2), s(i + 3))
            val data = ((a.toInt << 24) & 0xFF000000) | ((b.toInt << 16) & 0x00FF0000) | ((c.toInt << 8) & 0x0000FF00) | (d.toInt & 0x000000FF)
            val chars = Seq(a, b, c, d).map(c => s"'${escape(c, true)}'")
            val range = (i to (i + 3)).mkString(", ")
            buffer += Tac.Mov(lit, tmp4, data) -- s"string[$range] = ${chars.mkString(", ")}"
            buffer += Tac.Stw(lit, tmp4, tmp2, 4 + i)
            i += 4
            len -= 4
          }

          while (len >= 2) {
            val tmp4 = temporary()
            val (a, b) = (s(i), s(i + 1))
            val data = ((a.toInt << 8) & 0x0000FF00) | (b.toInt & 0x000000FF)
            val chars = Seq(a, b).map(c => s"'${escape(c, true)}'")
            val range = (i to (i + 1)).mkString(", ")
            buffer += Tac.Mov(lit, tmp4, data) -- s"string[$range] = ${chars.mkString(", ")}"
            buffer += Tac.Sth(lit, tmp4, tmp2, 4 + i)
            i += 2
            len -= 2
          }

          while (len >= 1) {
            val tmp4 = temporary()
            val c = s(i)
            buffer += Tac.Mov(lit, tmp4, c) -- s"string[$i] = '${escape(c, true)}'"
            buffer += Tac.Stb(lit, tmp4, tmp2, 4 + i)
            i += 1
            len -= 1
          }
        }
        tmp2
      case CharLit(c, _) =>
        withEffect(temporary()) { res =>
          buffer += Tac.Mov(lit, res, c.toInt) -- s"char literal '${escape(c, true)}'"
        }
      case BoolLit(b, _) =>
        withEffect(temporary()) { res =>
          buffer += Tac.Mov(lit, res, if (b) 1 else 0) -- s"bool literal $b"
        }
    }

  private def ellipsis(maxlen: Int, s: String): String = {
    val escaped = escape(s)
    val len     = maxlen - 4

    if (escaped.size > len) {
      escaped.substring(0, len) + " ..."
    } else {
      escaped
    }
  }

  private def ensureNotNull(ast: Ast, tmp: Temporary)(implicit table: SymbolTable, buffer: ListBuffer[Tac]): Unit = {
    buffer += Tac.Beq(ast, tmp, Tac.zeroTemporary, "_nullPointerException")
  }

  private def onNotNull(ast: Ast, tmp: Temporary)(f: => Unit)(implicit table: SymbolTable, buffer: ListBuffer[Tac]): Unit = {
    val lbl = syntheticLabel(ast)
    buffer += Tac.Beq(ast, tmp, Tac.zeroTemporary, lbl)
    f
    buffer += lbl
  }

  private type SingleInstBinOpPart = (Ast, Temporary, Temporary, Temporary) => Tac
  private object SingleInstBinOp {
    def unapply(op: BinOp): Option[SingleInstBinOpPart] = Option(op) collect {
      case Add  => Tac.Add
      case Sub  => Tac.Sub
      case Div  => Tac.Div
      case Mul  => Tac.Mul
      case Mod  => Tac.Rem
      case Band => Tac.And
      case Bor  => Tac.Or
      case Xor  => Tac.Xor
      case Sll  => Tac.Sll
      case Sar  => Tac.Sar
      case Slr  => Tac.Slr
    }
  }

}
