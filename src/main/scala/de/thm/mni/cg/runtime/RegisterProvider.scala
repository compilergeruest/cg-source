package de.thm.mni.cg.runtime

trait RegisterProvider {

  /** Returns the count of all registers */
  lazy val allRegistersCount = allRegisters.size
  /** Returns the set of all machine registers */
  val allRegisters: IndexedSeq[Register]

  /** All registers which are forbidden to use (e.g. kernel reserved registers) */
  val forbiddenRegisters: IndexedSeq[Register]

  /** The set containing all registers which may be used for return values of subroutine calls */
  val returnValueRegisters: IndexedSeq[Register]
  /** The set containing all registers which may be used for subroutine arguments */
  val argumentRegisters: IndexedSeq[Register]
  /** The set containing all general purpose registers, usually used to hold the value of subexpressions */
  val temporaryRegisters: IndexedSeq[Register]
  /** The set containing all general purpose registers, usually used to hold the value of local variables */
  val registerVariables: IndexedSeq[Register]

  /** Returns the zero register of the target machine */
  val zero: Register
  /** Returns the register which holds the stack pointer */
  val sp: Register
  /** Returns the register which holds the return address after a subroutine call */
  val rr: Register

  /** Returns the register which holds the return address after an interrupt */
  val ir: Register

}
