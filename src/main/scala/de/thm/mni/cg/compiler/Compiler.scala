package de.thm.mni.cg.compiler

import java.io.OutputStream
import scala.collection.mutable.ArrayBuffer
import scala.util.Try
import de.thm.mni.cg.backend.EcoRegisters
import de.thm.mni.cg.ir.Ssa
import de.thm.mni.cg.runtime._
import de.thm.mni.cg.util.InputSource
import de.thm.mni.cg.ir.Ifg
import de.thm.mni.cg.ir.TacCfg
import java.io.File
import de.thm.mni.cg.backend.NativeAssembler
import de.thm.mni.cg.backend.Assembler
import java.io.FileOutputStream
import de.thm.mni.cg.ir.Tac
import de.thm.mni.cg.ir.Program
import java.io.FileInputStream

final object Compiler extends AnyRef
  with Phases {

  val registerProvider: RegisterProvider = EcoRegisters

  /*
   * ==================== definition of phases: from parser to x ====================
   */

  private val upToSyntactic = syntacticAnalysis
  private val upToSemantic = upToSyntactic |> semanticAnalysis |> astOptimizations
  private val upToRuntime = upToSemantic |> runtimeInfos
  private def upToAssemblerBackend(output: OutputStream) =
    upToRuntime |> defaultBackend(output)

  /*
   * =============================== syntax analysis ================================
   */

  /**
   * Runs all syntactical analysis phases (scanner and parser)
   */
  val parse = upToSyntactic.run _

  /*
   * ============================== semantic analysis ===============================
   */

  /**
   * Runs all phases up to the data-flow-analysis
   * (scanner, parser, namer, typer, control-flow-analysis
   * and data-flow-analysis)
   */
  val semantics = upToSemantic.run _

  // =================================================================================

  /**
   * Runs all phases up to the collection of runtime information
   * (scanner, parser, namer, typer, control-flow-analysis
   * data-flow-analysis and runtimeInfos)
   */
  val runtimeInformation = upToRuntime.run _

  /**
   * Compiles source-file `in` and produces the executable `out`-file
   * out. `asm` is used as intermediate file
   */
  def compile(settings: CompilerSettings, in: File, asm: File, out: File) = {
    val fis       = new FileInputStream(in)
    val asmStream = new FileOutputStream(asm)

    for {
      _   <- upToAssemblerBackend(asmStream).run(settings, fis)
      _    = fis.close()
      _    = asmStream.close()
      obj <- assemble.run(settings, asm)
      _   <- Try(obj.deleteOnExit())
      _   <- link.run(settings, (obj, out))
      _   <- Try(obj.delete())
    } yield ()
  }

}
