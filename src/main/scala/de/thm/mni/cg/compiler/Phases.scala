package de.thm.mni.cg.compiler

import java.io.File
import java.io.OutputStream
import scala.util.Try
import de.thm.mni.cg.backend._
import de.thm.mni.cg.ir._
import de.thm.mni.cg.parser._
import de.thm.mni.cg.runtime.RuntimeOrganisation
import de.thm.mni.cg.semantic._
import de.thm.mni.cg.util._
import de.thm.mni.cg.runtime.TacGenerator
import de.thm.mni.cg.runtime.RegisterAllocator
import de.thm.mni.cg.table.SymbolTable
import de.thm.mni.cg.util.traversal.AstTraverser
import de.thm.mni.cg.util.traversal.TraverseOrder
import de.thm.mni.cg.util.traversal.PartialTraverser
import de.thm.mni.cg.optimize.AstOptimizer
import de.thm.mni.cg.optimize.TacOptimizer

trait Phases { self: Compiler.type =>

  /*
   * ============================== utility functions ===============================
   */

  /**
   * Function for wrapping a phase. This functions times
   * `f` and handles the 'verbose' command-line flag.
   */
  private def timedPhase[In, Out](phaseName: String)
                                 (f: (CompilerSettings, In) => Try[Out]): Phase[In, Out] =
    Phase[In, Out] { case (settings, in) =>
      def runPhase = f(settings, in)

      if (settings.verbose) {
        println(s"Running compiler-phase '$phaseName' ...")
        val timer  = new Timer
        timer.start()
        val result = runPhase
        val time   = showDuration(timer.stop())
        println(s"Compiler-phase '$phaseName' took $time!")
        result
      }
      else runPhase
    }

  /**
   * Helper function, where the start-production in the
   * parser can be specified.
   */
  def parseWith[T](f: Parser => T): Phase[InputSource, T] =
    timedPhase("syntactic-analysis") { case (_, input) =>
      for {
        reader    <- inputSourceToReader(input)
        tokenizer <- Try(new Tokenizer(reader))
        parser    <- Try(new Parser(tokenizer))
        result    <- Try(f(parser))
      } yield result
    }

  /*
   * =============================== lexical analysis ===============================
   */

  /**
   * Definition of the compiler phase "lexical analysis".
   *
   * This phase takes an [[de.thm.mni.cg.util.InputSource]]
   * as input (such as a [[de.thm.mni.cg.util.StringSource]]
   * or a [[de.thm.mni.cg.util.StreamSource]]) and produces
   * a [[de.thm.mni.cg.ir.Program]], which is syntactically
   * correct, but its semantics may still be wrong.
   *
   * @see [[de.thm.mni.cg.util.InputSource]]
   * @see [[de.thm.mni.cg.util.StringSource]]
   * @see [[de.thm.mni.cg.util.StreamSource]]
   * @see [[de.thm.mni.cg.ir.Program]]
   */
  val syntacticAnalysis: Phase[InputSource, Program] =
    parseWith(_.program())

  /*
   * ============================== semantic analysis ===============================
   */

  /**
   * Definition of the compiler phase "namer".
   *
   * This phase enters function- and type-definitions into
   * the symbol-table. It also creates empty tables for scopes
   * in block-, for-, if-, and while- statements, as well as for
   * function definitions.
   * In this phase functions get their synthetic name assigned.
   */
  val namer: Phase[Program, Program] = timedPhase("namer") {
    case (_, prog @ Program(scope, table)) =>
      for (_ <- Try(new NameResolver().traverse(scope)(table)))
        yield prog
  }

  /**
   * Definition of the compiler phase "typer" (type-check).
   *
   * The type-checker checks if the types are valid. It
   * assigns types to expressions and validates types.
   *
   * Prerequisite: all function- and type-definitions are
   * already inserted into the symbol-table (phase "namer"
   * has run already)
   */
  val typer: Phase[Program, Program] = timedPhase("typer") {
    case (_, prog @ Program(scope, table)) =>
      for (_ <- Try(new TypeCheck().traverse(scope)(table)))
        yield prog
  }

  /**
   * Definition of a helper compiler phase "retyper". It will
   * rerun the namer and the typecheck on an already typechecked
   * program.
   *
   * If any AST-transformations are performed, it is recommended to
   * run this phase, to be sure that no invalid types are set in the
   * program.
   */
  val retyper: Phase[Program, Program] = {
    val removeTable =
      Phase[Program, Program] {
        case (_, Program(scope, table)) =>
          val newProg = Program(scope, SymbolTable(false, None))
          Try(newProg)
      }
    removeTable |> namer |> typer
  }

  /**
   * Definition of the compiler phase "control flow analysis".
   *
   * The control flow analysis checks, whether there is
   * a return statement in every control flow path.
   *
   * Prerequisite: the program has been type-checked (phase "typer"
   * has run already)
   */
  val controlFlow: Phase[Program, Program] = timedPhase("control-flow-analysis") {
    case (_, prog @ Program(scope, table)) =>
      for (_ <- Try(new ControlFlowAnalysis().traverse(scope)(table)))
        yield prog
  }

  /**
   * Definition of the compiler phase "data flow analysis".
   *
   * The control flow analysis checks, whether a variable is
   * used before it is initialized.
   *
   * Prerequisite: the programs' control flow has already been analyzed
   * (phase "controlFlow" has run already)
   */
  val dataFlow: Phase[Program, Program] = timedPhase("data-flow-analysis") {
    case (_, program) =>
      for (_ <- Try(new DataFlowAnalysis().analyze(program)))
        yield program
  }

  /**
   * Definition of the compiler phase "semantic analysis".
   *
   * This phase takes an [[de.thm.mni.cg.ir.Program]],
   * which is not checked semantically and validates it.
   *
   * This phase does four steps:
   *
   * 1.) It enters function- and type-definitions into
   * the symbol-table. It also creates empty tables for scopes
   * in block-, for-, if-, and while- statements, as well as for
   * function definitions.
   * In this phase functions get their synthetic name assigned.
   *
   * 2.) It checks if the types are valid. It
   * assigns types to expressions and validates types.
   *
   * 3.) It checks whether there is a return statement in every
   * control flow path.
   *
   * 4.) It checks whether a variable is used before it is
   * initialized.
   */
  val semanticAnalysis = namer |> typer |> controlFlow |> dataFlow

  val astOptimizations: Phase[Program, Program] =
    timedPhase("ast-optimizations") {
      case (_, program) => Try(new AstOptimizer().optimize(program))
    }

  val tacOptimizations: Phase[Tac.TacProgram, Tac.TacProgram] =
    timedPhase("tac-optimizations") {
      case (_, program) => Try(new TacOptimizer().optimize(program))
    }

  /**
   * Definition of the compiler phase "runtime information".
   *
   * This phase collects runtime informations, calculates
   * offsets for structs and unions.
   *
   * Prerequisite: the programs' data flow has already been analyzed
   * (phase "dataFlow" has run already)
   */
  val runtimeInfos: Phase[Program, Program] = timedPhase("runtime-infos") {
    case (_, program) =>
      for (_ <- Try(new RuntimeOrganisation().organizeTypeSize(program)))
        yield program
  }

  /**
   * Definition of the compiler phase "three-address-code generation".
   *
   * This phase transforms a Program into a TacProgram.
   *
   * Prerequisite: the programs' data flow has already been analyzed
   * (phase "dataFlow" has run already) and runtime information such
   * as stack-frame size and offsets of the variables are calculated
   */
  val generateThreeAddressCode: Phase[Program, Tac.TacProgram] =
    timedPhase("generate-tac") {
      case (_, program) => Try(new TacGenerator().generate(program))
    }

  /**
   * Definition of the compiler phase "register allocator".
   *
   * This phase assigns memory locations (heap or registers) to
   * variables in the three-address-code.
   */
  val registerAllocator: Phase[Tac.TacProgram, Tac.TacProgram] =
    timedPhase("register-allocation") {
      case (_, tacProgram) =>
        Try {
          val allocator = new RegisterAllocator(registerProvider)
          allocator.allocate(tacProgram)
          tacProgram
        }
    }

  /**
   * Definition of the compiler phase "code generator".
   *
   * This phase emits eco32 assembly code on base
   * of the given three address code program. Before the eco32-asm
   * code is emitted, the register allocator runs.
   *
   * Additionally it requires the output stream, which is simply
   * the file-destination, and the old abstract-syntax-tree-program.
   *
   * Prerequisite: The output stream is writable and the program has already
   * been fully analyzed and checked.
   */
  def codeGenerator(output: OutputStream, maximalLexicalDepth: Int): Phase[Tac.TacProgram, Unit] =
    timedPhase("code-generation") {
      case (_, tacProgram) =>
        Try {
          val codegen = new CodeGenerator(output, maximalLexicalDepth, registerProvider)
          codegen.run(tacProgram)
        }
    }

  /**
   * The default backend generates the three address code,
   * runs the register allocator and then emits the eco32-
   * assembler code.
   */
  def defaultBackend(output: OutputStream): Phase[Program, Unit] = Phase { case (settings, program) =>
    val backendPhase =
      generateThreeAddressCode |>
      tacOptimizations         |>
      registerAllocator        |>
      codeGenerator(output, program.getMaxLexicalFunDepth)

    backendPhase.run(settings, program)
  }

  /**
   * Definition of the phase "assemble".
   *
   * Assembles the input file, producing the output-file which is named alike,
   * but ends with '.asm'
   */
  val assemble: Phase[File, File] =
    timedPhase("assemble") { case (_, program) =>
      new Assembler().assemble(program)
    }

  /**
   * Definition of the phase "link".
   *
   * Links the input file together with the runtime support into an executable binary file.
   */
  val link: Phase[(File, File), Unit] = timedPhase("link") {
    case (_, (input, output)) =>
      for {
        linker <- Try(new NativeLinker())
        supportObj <- relative("runtime", "support", "support.obj")
        runtimeObj <- relative("runtime", "support", "runtime.obj")
        heapManObj <- relative("runtime", "support", "heap-manager.obj")
        endObj     <- relative("runtime", "support", "end.obj")
        _ <- Try {
          linker.relocateCodeSegment(0xC0000000)
          linker.setOutputFile(output)
          linker.addInputFile(supportObj)
          linker.addInputFile(input)
          linker.addInputFile(runtimeObj)
          linker.addInputFile(heapManObj)
          linker.addInputFile(endObj)
          linker.produceMapFile()
          linker.link()
        }
      } yield ()
  }

}
