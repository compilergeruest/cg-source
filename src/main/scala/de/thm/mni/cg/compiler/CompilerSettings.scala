package de.thm.mni.cg.compiler

object CompilerSettings {

  def defaultSettings: CompilerSettings =
    CompilerSettings(
      verbose = false
    )

}

case class CompilerSettings(
  verbose: Boolean
)
