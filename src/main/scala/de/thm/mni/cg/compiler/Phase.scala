package de.thm.mni.cg.compiler

import scala.util.Try
import de.thm.mni.cg.ir.Program
import de.thm.mni.cg.util.InputSource

/**
 * Abstract definition of a compiler phase,
 * which consumes an instance of type In and
 * produces an instance of type Try[Out].
 *
 * With the combinators map and flatMap phases can
 * be chained together.
 */
sealed trait Phase[In, Out] { self =>
  def run(settings: CompilerSettings, in: In): Try[Out]

  /**
   * Transforms the output of this phase, yielding a new phase,
   * which takes an instance of type `In` as argument and produces
   * an instance of type `Out0`.
   */
  final def map[Out0](f: Out => Out0): Phase[In, Out0] =
    new Phase[In, Out0] {
      def run(settings: CompilerSettings, in: In): Try[Out0] =
        self.run(settings, in).flatMap(x => Try(f(x)))
    }

  /**
   * Concatenates two phases (`In` => `Out`, `Out` => `Out0`)
   * yielding a phase from `In` to `Out0`.
   */
  final def flatMap[Out0](next: Phase[Out, Out0]): Phase[In, Out0] =
    new Phase[In, Out0] {
      def run(settings: CompilerSettings, in: In): Try[Out0] =
        self.run(settings, in).flatMap(x => next.run(settings, x))
    }

  /** Symbolic version of flatMap */
  final def |>[Out0](next: Phase[Out, Out0]): Phase[In, Out0] =
    flatMap[Out0](next)

  /** Symbolic version of flatMap */
  final def <|[In0](prev: Phase[In0, In]): Phase[In0, Out] =
    prev flatMap[Out] this
}

object Phase {

  /**
   * Creates a phase, by defining its action.
   */
  def apply[In, Out](f: (CompilerSettings, In) => Try[Out]) =
    new Phase[In, Out] {
      def run(settings: CompilerSettings, in: In): Try[Out] =
        f(settings, in)
    }

}
