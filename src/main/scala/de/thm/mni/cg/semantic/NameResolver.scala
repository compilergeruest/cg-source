package de.thm.mni.cg.semantic

import de.thm.mni.cg.ir._
import de.thm.mni.cg.language.StdLib
import de.thm.mni.cg.language.ErrorMessages._
import de.thm.mni.cg.table._
import de.thm.mni.cg.util._
import de.thm.mni.cg.util.traversal.AstTraverser
import de.thm.mni.cg.util.traversal.TraverseOrder.PreOrder
import scala.collection.mutable.Map

/**
 * `NameResolver` is a traversal through the AST to insert
 * functions and types into the symbol table.
 */
final class NameResolver extends AstTraverser(PreOrder) {

  override protected def traverse(tdef: TyDef)(implicit table: SymbolTable) = {
    tdef.setTable(table)
    val TyDef(name, symbol) = tdef
    table.onUndefined(name, "Type") {
      symbol match {
        case ComplexTypeSymbol(components, _) =>
          duplicateComponentsDefs(components.toList, name)
        case _                                =>
          ()
      }
      table(name) = TyEntry(tdef)
    }
  }

  override protected def traverse(fstm: ForStm)(implicit table: SymbolTable) = {
    fstm.setTable(table)
    fstm.setLocalTable(SymbolTable(false, Some(table)))
    val Range(start, optNext, end) = fstm.getRange
    traverse(start)
    optNext.foreach(traverse)
    traverse(end)
  }

  private val names = Map.empty[List[String], Int]
  private def syntheticFunctionName(table: SymbolTable): List[String] = {
    require(table.isFunction, onlySyntheticNamesForFuncs)
    val fun = table.associatedFunEntry.get.getFunDef
    table.enclosingFunEntry match {
      case Some(parent) =>
        val assume = parent.getSynthetic :+ fun.getName.name
        if (names contains assume) {
          val idx = names(assume)
          names(assume) += 1
          parent.getSynthetic :+ (fun.getName.name + idx)
        } else {
          names(assume) = 0
          assume
        }
      case None => List(fun.getName.name)
    }
  }

  override protected def traverse(fdef: FunDef)(implicit table: SymbolTable) = {
    fdef.setTable(table)
    val FunDef(n @ Name(name), out, in, stm, _) = fdef
    table.onUndefined(n, "Function") {
      val entry = FunEntry(fdef)
      table(n) = entry
      fdef.setLocalTable(SymbolTable(true, Some(table)))
      entry.setSynthetic(syntheticFunctionName(fdef.getLocalTable))
    }
  }

  override protected def traverse(block: BlockStm)(implicit table: SymbolTable) = {
    block.setTable(table)
    block.setLocalTable(SymbolTable(false, Some(table)))
    require(table.getParent.isDefined, aBlockShouldHaveAParent)
    if (table.getParent.get.getParent.isEmpty) {
      StdLib.insertPrimitives(block.getLocalTable)
      StdLib.insertStdLibFunctions(block.getLocalTable)
    }
  }

  /*
   * ================================= Expressions ==================================
   */

  override protected def traverse(e: BinExp)(implicit table: SymbolTable): Unit = e.setTable(table)
  override protected def traverse(e: CastExp)(implicit table: SymbolTable): Unit = e.setTable(table)
  override protected def traverse(e: FunExp)(implicit table: SymbolTable): Unit = e.setTable(table)
  override protected def traverse(e: NewArrExp)(implicit table: SymbolTable): Unit = e.setTable(table)
  override protected def traverse(e: NewObjExp)(implicit table: SymbolTable): Unit = e.setTable(table)
  override protected def traverse(e: UnaryExp)(implicit table: SymbolTable): Unit = e.setTable(table)

  /*
   * =================================== Literals ===================================
   */

  override protected def traverse(e: BoolLit)(implicit table: SymbolTable): Unit = e.setTable(table)
  override protected def traverse(e: CharLit)(implicit table: SymbolTable): Unit = e.setTable(table)
  override protected def traverse(e: IntLit)(implicit table: SymbolTable): Unit = e.setTable(table)
  override protected def traverse(e: NullLit)(implicit table: SymbolTable): Unit = e.setTable(table)
  override protected def traverse(e: StringLit)(implicit table: SymbolTable): Unit = e.setTable(table)

  /*
   * ================================== Variables ===================================
   */

  override protected def traverse(e: AccessVar)(implicit table: SymbolTable): Unit = e.setTable(table)
  override protected def traverse(e: ArrayVar)(implicit table: SymbolTable): Unit = e.setTable(table)
  override protected def traverse(e: NameVar)(implicit table: SymbolTable): Unit = e.setTable(table)

  /*
   * ================================== Statements ==================================
   */

  override protected def traverse(e: AssignStm)(implicit table: SymbolTable): Unit = e.setTable(table)
  override protected def traverse(e: DelStm)(implicit table: SymbolTable): Unit = e.setTable(table)
  override protected def traverse(e: ExpStm)(implicit table: SymbolTable): Unit = e.setTable(table)
  override protected def traverse(e: IfStm)(implicit table: SymbolTable): Unit = e.setTable(table)
  override protected def traverse(e: InitStm)(implicit table: SymbolTable): Unit = e.setTable(table)
  override protected def traverse(e: ReturnStm)(implicit table: SymbolTable): Unit = e.setTable(table)
  override protected def traverse(e: WhileStm)(implicit table: SymbolTable): Unit = e.setTable(table)

  /*
   * ================================= Definitions ==================================
   */

  override protected def traverse(e: ParDef)(implicit table: SymbolTable): Unit = e.setTable(table)
  override protected def traverse(e: VarDef)(implicit table: SymbolTable): Unit = e.setTable(table)

  /*
   * =================================== Symbols ====================================
   */

  override protected def traverse(e: ArrayTypeSymbol)(implicit table: SymbolTable): Unit = e.setTable(table)
  override protected def traverse(e: NameTypeSymbol)(implicit table: SymbolTable): Unit = e.setTable(table)
  override protected def traverse(e: StructTypeSymbol)(implicit table: SymbolTable): Unit = e.setTable(table)
  override protected def traverse(e: UnionTypeSymbol)(implicit table: SymbolTable): Unit = e.setTable(table)

}
