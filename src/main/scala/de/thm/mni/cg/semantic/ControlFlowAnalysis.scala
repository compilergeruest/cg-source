package de.thm.mni.cg.semantic

import de.thm.mni.cg.util.traversal.AstTraverser
import de.thm.mni.cg.util.traversal.TraverseOrder._
import de.thm.mni.cg.ir._
import de.thm.mni.cg.language.ErrorMessages._
import de.thm.mni.cg.table._
import de.thm.mni.cg.semantic.Ty._
import de.thm.mni.cg.util.cache.Caching
import de.thm.mni.cg.util._

/**
 * `ControlFlowAnalysis` is a traversal through the AST to analyse the control flow
 */
final class ControlFlowAnalysis extends AstTraverser(PreOrder) with Caching {

  override protected def traverse(fdef: FunDef)(implicit table: SymbolTable) = {
    val FunDef(name, _, _, _, _) = fdef
    val tys = table[FunEntry](name).get.getOutTys
    lazy val resolvedFirstReturn = resolve(tys.head).getOrElse(typecheckShouldHaveCheckedFuncError(name))
    if (!(containsOnlyOne(tys) && resolvedFirstReturn == unitTy)) {
      val tupled =
        if (containsOnlyOne(tys)) tys.head.toString
        else tys.mkString("(", ",", ")")
      def errorReport(pos: Position) = {
        val reportingPos =
          if (pos == NoPosition) name.pos
          else pos
        error(reportingPos, missingReturn(name, tupled))
      }
      val cfg = Cfg(fdef, true)
      checkExhaustiveReturn(cfg, Set(), name.pos, errorReport)
    }
  }

  private def checkExhaustiveReturn(cfg: Cfg, seen: Set[Cfg], lastPosition: Position, onError: Position => Unit): Unit = cfg match {
    case BranchCfg(stm, _, ExitCfg) => onError(stm.pos)
    case BranchCfg(stm, ExitCfg, _) => onError(stm.pos)
    case BranchCfg(stm, onTrue, onFalse) =>
      if (!(seen contains onTrue)) {
        checkExhaustiveReturn(onTrue, seen + cfg, stm.pos, onError)
      }
      if (!(seen contains onFalse)) {
        checkExhaustiveReturn(onFalse, seen + cfg, stm.pos, onError)
      }
    case BasicBlockCfg(Nil, ExitCfg) => onError(lastPosition)
    case BasicBlockCfg(_ :+ last, ExitCfg) if !last.isInstanceOf[ReturnStm] => onError(last.pos)
    case BasicBlockCfg(stms, next) =>
      if (!(seen contains next)) {
        val newLastPosition = stms.lastOption.map(_.pos).getOrElse(lastPosition)
        checkExhaustiveReturn(next, seen + next, newLastPosition, onError)
      }
    case ExitCfg =>
  }

}
