package de.thm.mni.cg.semantic

import de.thm.mni.cg._
import de.thm.mni.cg.util.traversal.AstTraverser
import de.thm.mni.cg.util.traversal.TraverseOrder._
import de.thm.mni.cg.ir._
import de.thm.mni.cg.language.Constants._
import de.thm.mni.cg.language.ErrorMessages._
import de.thm.mni.cg.table._
import de.thm.mni.cg.semantic.Ty._
import de.thm.mni.cg.util._
import de.thm.mni.cg.util.cache._
import scala.reflect._
import scala.annotation.tailrec

/**
 * `TypeCheck` is a traversal through the AST to validate:
 *
 * - types
 * - definitions
 * - expressions
 * - statements
 */
final class TypeCheck extends AstTraverser(PostOrder) with Caching {

  /*
   * ================================= Expressions ==================================
   */

  override protected def traverse(e: BinExp)(implicit table: SymbolTable) = {
    val BinExp(op, left, right, _) = e
    val arithTy = isArithTy(left)
    val arithOp = isArithmetic(op)
    val logicTy = isLogicTy(left)
    val logicOp = isLogical(op)
    val refTy = isRefTy(left, left.getTy)
    val equalsOp = isEqualsCheck(op)

    if (refTy && equalsOp) {
      expect(left, right)
      e.setTy(boolTy)
    } else if (arithTy && arithOp || logicTy && logicOp) {
      expect(left, right)
      e setTy (op match {
        case Ne | Eq           => boolTy
        case Lt | Gt | Le | Ge => boolTy
        case _                 => left.getTy
      })
    } else if (!arithTy && arithOp) {
      error(left, arithBinExpExpects(op.token, left.getTy))
    } else if (!logicTy && logicOp) {
      error(left, logicalBinExpExpects(op.token, left.getTy))
    } else {
      internalError()
    }
  }

  override protected def traverse(e: CastExp)(implicit table: SymbolTable) = {
    val CastExp(typeSymbol, exp, _) = e

    val casted = fromSymbol(typeSymbol)
    if (casted != intTy && casted != charTy) {
      val info =
        if (isRefTy(typeSymbol, casted)) ", because casting to complex types is forbidden"
        else ""
      error(typeSymbol, cannotCast(exp.getTy, casted, info))
    }
    if (!isArithTy(exp)) {
      val info =
        if (isRefTy(exp, exp.getTy)) ", because complex types must not be casted"
        else ""
      error(exp, cannotCast(exp.getTy, casted, info))
    }
    e.setTy(casted)
  }

  override protected def traverse(e: FunExp)(implicit table: SymbolTable) = {
    val FunExp(funcName, args, _) = e
    val FunEntry(funDef) = select[FunEntry](funcName)

    val sparams = funDef.getParams.size
    val sargs = args.size

    if (sparams > sargs) error(funcName, toFewManyArgsFunc(funcName, "few"))
    if (sparams < sargs) error(funcName, toFewManyArgsFunc(funcName, "many"))

    for ((ParDef(symbol, paramName), arg) <- funDef.getParams.zip(args)) {
      val defTy = unsafeResolve(symbol, fromSymbol(symbol))
      if (!isAssignable(arg, arg.getTy, defTy)) {
        error(arg, expectedExprOfTypeForParam(defTy, paramName, funcName, arg.getTy))
      }
    }
    e setTy outTy(funDef)
  }

  override protected def traverse(e: NewArrExp)(implicit table: SymbolTable) = {
    val NewArrExp(symbol, optDims, _) = e

    requireNonUnit(invalidBaseTyForArray(unitTy), symbol)

    val (nonEmptyDims, emptyDims) = optDims.span(_.isDefined)

    val nonEmpty = emptyDims.find(_.isInstanceOf[Some[Exp]])
    if (nonEmpty.isDefined) {
      // internal error: parser won't parse this
      error(nonEmpty.flatten.get, arrayEmptyDimAfterNonEmpty)
    }

    if (nonEmptyDims.isEmpty) {
      error(symbol, arrayWithoutDimension)
    }

    val arrTy = optDims.foldLeft(fromSymbol(symbol)) {
      case (acc, _) => ArrayTy(acc)
    }

    val dims = nonEmptyDims.flatten
    for (dim <- dims) {
      expectMsg(intTy, dim)(" as an array dimension")
    }

    e setTy arrTy
  }

  override protected def traverse(e: NewObjExp)(implicit table: SymbolTable) = {
    val NewObjExp(name @ Name(n), args, _) = e
    val nameTy: Ty = NameTy(n)
    val TyEntry(_) = select[TyEntry](name)

    val ty = unsafeResolve(name, nameTy)

    if (!ty.isInstanceOf[StructTy] && !ty.isInstanceOf[UnionTy]) {
      error(name, expectedComplex(ty))
    }
    val ComplexTy(components) = ty

    val names = args.map { case (argName, _) => argName }
    duplicateComponentsInit(names, name)

    if (ty.isInstanceOf[StructTy]) {
      val notInit = components.filterNot(x => names.map(_.name).contains(x.name))
      structInit(name, notInit)
    } else {
      unionInit(name, names)
    }

    for ((cname @ Name(cn), exp) <- args) {
      val optTy = components.collect { case Component(ty, `cn`) => ty }.headOption
      val ty = optTy.getOrElse(error(cname, unknownComponentInComplexType(cname, name)))
      expect(ty, exp)
    }

    e setTy nameTy
  }

  override protected def traverse(e: UnaryExp)(implicit table: SymbolTable) = {
    val UnaryExp(op, exp, _) = e
    val ty = unsafeResolve(exp, exp.getTy)

    if (!isArithTy(exp) && isArithmetic(op)) {
      val description = if (op == Minus) "Arithmetic" else "Bitwise"
      error(exp, arithBitwiseUnaryExpects(arith = op == Minus, op.token, exp.getTy))
    } else if (!isLogicTy(exp) && (op == Not)) {
      error(exp, logicalUnaryExpects(op.token, exp.getTy))
    } else if (!ty.isInstanceOf[ArrayTy] && (op == Size)) {
      error(exp, sizeUnaryExpects(op.token, exp.getTy))
    } else {
      if (op == Size) {
        e setTy intTy
      } else {
        e setTy exp.getTy
      }
    }
  }

  /*
   * ================================== Variables ===================================
   */

  override protected def traverse(e: AccessVar)(implicit table: SymbolTable) = {
    val AccessVar(base, name, _) = e

    unsafeResolve(base, base.getTy) match {
      case ComplexTy(components) =>
        val optComp = components.find(_.name == name.name)
        val comp = optComp.getOrElse(unknownComponentError(name))
        e.setTy(comp.ty)
      case _ =>
        error(base, expectedComplex(base.getTy))
    }
  }

  override protected def traverse(e: ArrayVar)(implicit table: SymbolTable) = {
    val ArrayVar(base, index, _) = e

    unsafeResolve(base, base.getTy) match {
      case ArrayTy(abase) => e.setTy(abase)
      case _              => expectMsg(ArrayTy(base.getTy), base)(" for index access")
    }
    expectMsg(intTy, e.getIndex)(" as an array index")
  }

  override protected def traverse(e: NameVar)(implicit table: SymbolTable) = {
    val NameVar(n @ Name(name), _) = e
    val entry = select[FieldEntry](n)
    e setTy fromSymbol(entry.getFieldDef.getTypeSymbol)
  }

  /*
   * ================================== Statements ==================================
   */

  override protected def traverse(e: AssignStm)(implicit table: SymbolTable) = {
    val AssignStm(lhs, rhs) = e
    val rhsTys = rhs.getTy match {
      case TupleTy(tys) => tys.map(unsafeResolve(rhs, _))
      case ty           => List(unsafeResolve(rhs, ty))
    }

    if (lhs.size != rhsTys.size) {
      val lhsTys = tupleTy(lhs.map(_.getTy).toList)
      assignError(rhs, lhsTys, rhs.getTy)
    }

    for ((exp, rhsTy) <- lhs.zip(rhsTys)) exp match {
      case AccessVar(_, _, ty) =>
        if (!isAssignable(exp, rhsTy, ty)) assignError(rhs, ty, rhsTy)
      case ArrayVar(_, _, ty) =>
        if (!isAssignable(exp, rhsTy, ty)) assignError(rhs, ty, rhsTy)
      case NameVar(name, ty) =>
        val fentry = table[FieldEntry](name).get
        val desc = if (fentry.isParDef) fentry.describe else "constant"
        if (fentry.isConst) error(name, reassignmentToX(desc, name))
        if (!isAssignable(name, rhsTy, ty)) assignError(rhs, ty, rhsTy)
      case _ =>
        error(exp, lhsOfAssignMustBeVar)
    }
  }

  override protected def traverse(e: DelStm)(implicit table: SymbolTable) = {
    if (!hasRefTy(e.getExp)) {
      error(e.getExp, canNotFreeMemOfPrimitive(e.getExp.getTy))
    } else if (e.getExp.isInstanceOf[NullLit]) {
      error(e.getExp, canNotFreeMemOfNull)
    }
  }

  override protected def traverse(e: ForStm)(implicit table: SymbolTable) = {
    val ForStm(vari, range, body, local) = e
    val Range(start, onext, end) = range
    val VarDef(_, symbol, Name(name)) = vari

    val ty = unsafeResolve(symbol, fromSymbol(symbol))
    if (ty != intTy) {
      error(symbol, iterationExpectsInt(ty))
    }

    traverse(start)
    expectMsg(intTy, start)(" for start value in for-statement range")

    onext.foreach(traverse)
    onext.foreach(expectMsg(intTy, _)(" for next value in for-statement range"))

    traverse(end)
    expectMsg(intTy, end)(" for end value in for-statement range")
  }

  override protected def traverse(e: IfStm)(implicit table: SymbolTable) =
    expectMsg(boolTy, e.getCheck)(" in test of if-statement")

  override protected def traverse(e: InitStm)(implicit table: SymbolTable) = {
    val InitStm(defs, exp) = e

    val tyLst = unsafeResolve(exp, exp.getTy) match {
      case TupleTy(tys) => tys
      case t            => List(t)
    }

    if (defs.size != tyLst.size) {
      val expected = tupleTy(defs.map(vdef => fromSymbol(vdef.getTypeSymbol)).toList)
      assignError(exp, expected, exp.getTy)
    }

    for ((VarDef(_, symbol, name), act) <- defs.zip(tyLst)) {
      val ty = unsafeResolve(symbol, fromSymbol(symbol))
      if (!isAssignable(exp, act, ty)) {
        assignError(exp, ty, act)
      }
    }

    val usages = exp.collect { case v: NameVar => v.getName }
    for (usage <- usages) {
      if (defs.exists(_.getName == usage)) {
        error(usage, recursiveUse(usage))
      }
    }

  }

  override protected def traverse(e: ReturnStm)(implicit table: SymbolTable) = {
    val FunEntry(fdef) = table.associatedFunEntry.getOrElse(internalErrorAtReturnError())
    val out = outTy(fdef)
    val retTy = tupleTy(e.getExps.map(_.getTy).map(unsafeResolve(e, _)).toList)
    if (!isAssignable(fdef, retTy, out)) {
      error(e, illegalReturnType(out, retTy))
    }
  }

  override protected def traverse(e: WhileStm)(implicit table: SymbolTable) =
    expectMsg(boolTy, e.getCheck)(" in test of while-statement")

  /*
   * ================================= Definitions ==================================
   */

  override protected def traverse(e: ParDef)(implicit table: SymbolTable) = {
    val ParDef(symbol, n @ Name(name)) = e
    requireNonUnit(canNotDefineParameterOfUnit, symbol)
    table.onUndefined(n, "Parameter") {
      table(n) = withEffect(ParEntry(e)) {
        _.setTy(fromSymbol(symbol))
      }
    }
  }

  override protected def traverse(e: VarDef)(implicit table: SymbolTable) = {
    val VarDef(_, symbol, n @ Name(name)) = e
    requireNonUnit(canNotDefineVariableOfUnit, symbol)
    table.onUndefined(n, "Variable") {
      table(n) = withEffect(VarEntry(e)) {
        _.setTy(fromSymbol(symbol))
      }
    }
  }

  override protected def traverse(e: TyDef)(implicit table: SymbolTable) = {
    val TyDef(name @ Name(n), symbol) = e
    if (primitives contains n) {
      error(name, redefinitionOfPrimitive(name))
    }
    val entry = table[TyEntry](name).getOrElse(typeNotExistError(name))
    val ty = fromSymbol(symbol)

    if (isCyclicType(symbol, Set(name))) {
      error(symbol, cyclicDefinitionOf(name))
    }
    entry.setTy(ty)
  }

  @tailrec
  private def isCyclicType(symbol: TypeSymbol, seen: Set[Name])(implicit table: SymbolTable): Boolean = symbol match {
    case NameTypeSymbol(n) if (seen contains n) => true
    case NameTypeSymbol(name) =>
      table[TyEntry](name) match {
        case Some(entry) =>
          val ty = fromSymbol(symbol)
          if (ty.isPrimitive) false
          else isCyclicType(entry.getTyDef.getTypeSymbol, seen + name)
        case None =>
          typeNotExistError(name)
      }
    case _ => false
  }

  override protected def traverse(e: FunDef)(implicit table: SymbolTable) = {
    val FunDef(n @ Name(name), out, in, _, _) = e
    if (!containsOnlyOne(out)) {
      out.foreach(requireNonUnit(canNotMixReturnTypeWithUnit, _))
    }
    val entry = table[FunEntry](n).getOrElse(functionNotExistError(n))
    entry.setOutTys(out.map(fromSymbol).toList)
  }

  /*
   * ===================================== Syms =====================================
   */

  override protected def traverse(e: ArrayTypeSymbol)(implicit table: SymbolTable) = {
    if (e.getBase.isInstanceOf[NameTypeSymbol]) {
      requireNonUnit(canNotDefineArrayBaseOfUnit, e.getBase)
    }
  }

  override protected def traverse(e: StructTypeSymbol)(implicit table: SymbolTable) = {
    for (ir.Component(symbol, name) <- e.getMembers) {
      requireNonUnit(canNotDefineStructMemberOfUnit(name), symbol)
    }
  }

  override protected def traverse(e: UnionTypeSymbol)(implicit table: SymbolTable) = {
    for (ir.Component(symbol, name) <- e.getMembers) {
      requireNonUnit(canNotDefineUnionMemberOfUnit(name), symbol)
    }
  }

  override protected def traverse(e: NameTypeSymbol)(implicit table: SymbolTable) = {
    val NameTypeSymbol(name) = e
    val ty = fromSymbol(e)
    if (!(ty.isPrimitive || table.exists[TyEntry](name))) {
      error(e, unknownTypeWithName(name))
    }
  }

  /*
   * =============================== Helper Functions ===============================
   */

  private def requireNonUnit(text: => String, symbol: TypeSymbol)(implicit table: SymbolTable): Unit = {
    val ty = unsafeResolve(symbol, fromSymbol(symbol))
    if (ty == unitTy) {
      error(symbol, text)
    }
  }

  private def assignError(pos: Exp, lhs: Ty, rhs: Ty) =
    error(pos, s"Can't assign expression of type '$rhs' to a variable of type '$lhs'!")

  private val entryTags = Map[ClassTag[_], String](
    classTag[FunEntry]   -> "function",
    classTag[TyEntry]    -> "type",
    classTag[FieldEntry] -> "parameter or variable",
    classTag[VarEntry]   -> "variable",
    classTag[ParEntry]   -> "parameter")

  private def select[E <: TableEntry]
    (n: Name)(implicit table: SymbolTable, ct: ClassTag[E], default: DefaultsTo[E, TableEntry]): E = {

    val Name(name) = n
    val what = entryTags(ct)

    selectOrError[E](n, error(n, s"Undefined $what with name '$name'!")) {
      case e: TableEntry => error(n, s"'$name' is not a $what, but a ${e.describe}!")
    }
  }

  private def selectOrError[E <: TableEntry]
    (name: Name, error: => Nothing)(mismatch: PartialFunction[TableEntry, Nothing])
    (implicit table: SymbolTable, ct: ClassTag[E], default: DefaultsTo[E, TableEntry]): E =
    table[TableEntry](name) match {
      case Some(ct(x))                       => x
      case Some(e) if mismatch isDefinedAt e => mismatch(e)
      case None                              => error
    }

  private def expectMsg(expected: Ty, actual: Exp)(message: => String)(implicit table: SymbolTable) =
    if (!isAssignable(actual, unsafeResolve(actual, actual.getTy), unsafeResolve(actual, expected))) {
      val act = actualTy(actual.getTy)
      val msg = message
      error(actual.pos, expectedExprOfType(expected, msg, act))
    }

  private def expect(expected: Ty, actual: Exp)(implicit table: SymbolTable) =
    expectMsg(expected, actual)("")

  private def expect(expected: Exp, actual: Exp)(implicit table: SymbolTable) =
    if (!isAssignable(actual, unsafeResolve(actual, actual.getTy), unsafeResolve(expected, expected.getTy))) {
      val act = actualTy(actual.getTy)
      error(actual.pos, expectedExprOfType(expected.getTy, act))
    }

  private def actualTy(actual: Ty): String =
    if (actual == NullTy) "'null'"
    else s"expression of type '$actual'"

  private def isArithmetic(bop: BinOp) =
    bop match {
      case And | Or => false
      case _        => true
    }

  private def isArithmetic(uop: UnaryOp) =
    uop match {
      case Bnot | Minus => true
      case _            => false
    }

  private def isLogical(bop: BinOp) =
    bop match {
      case And | Or | Eq | Ne => true
      case _                  => false
    }

  private def isEqualsCheck(bop: BinOp) =
    bop match {
      case Eq | Ne => true
      case _ => false
    }

  private def forall(exp: Exp, fulfilment: Ty*)(f: (Ty, Ty) => Boolean)(implicit table: SymbolTable): Boolean =
    fulfilment.forall(f(unsafeResolve(exp, exp.getTy), _))

  private def forany(exp: Exp, fulfilment: Ty*)(f: (Ty, Ty) => Boolean)(implicit table: SymbolTable): Boolean =
    fulfilment.exists(f(unsafeResolve(exp, exp.getTy), _))

  private def isArithTy(exp: Exp)(implicit table: SymbolTable) =
    forany(exp, intTy, charTy)(_ == _)

  private def isLogicTy(exp: Exp)(implicit table: SymbolTable) =
    forall(exp, boolTy)(_ == _)

  private def hasRefTy(exp: Exp)(implicit table: SymbolTable) =
    isRefTy(exp, unsafeResolve(exp, exp.getTy))

  private def isRefTy[E <: Pos](pos: E, ty: Ty)(implicit table: SymbolTable): Boolean = ty match {
    case _: ArrayTy          => true
    case _: UnionTy          => true
    case _: StructTy         => true
    case `intTy` | `boolTy`  => false
    case `charTy` | `unitTy` => false
    case n: NameTy           => isRefTy(pos, unsafeResolve(pos, n))
    case NullTy              => true
    case TupleTy(_)          => false
  }

  /* does not resolve ty */
  private def tupleTy(lst: List[Ty]): Ty = lst match {
    case hd :: Nil => hd
    case Nil       => internalError()
    case _         => TupleTy(lst)
  }

  private def outTy(fdef: FunDef)(implicit table: SymbolTable): Ty =
    tupleTy(fdef.getOut.map(fromSymbol).toList)

  private def isAssignable[E <: Pos](pos: E, from: Ty, to: Ty)(implicit table: SymbolTable): Boolean =
    (unsafeResolve(pos, from), unsafeResolve(pos, to)) match {
      case (TupleTy(xs), TupleTy(ys)) => (xs zip ys).forall { case (x, y) => isAssignable(pos, x, y) }
      case (`NullTy`, b)              => isRefTy(pos, b)
      case (a, b)                     => a == b
    }

  private def unsafeResolve[E <: Pos](pos: E, ty: Ty)(implicit table: SymbolTable): Ty =
    resolve(ty).getOrElse(error(pos, s"Unexpected unknown type detected while trying to match against the type '$ty'!"))

}
