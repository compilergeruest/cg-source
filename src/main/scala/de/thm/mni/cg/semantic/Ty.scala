package de.thm.mni.cg.semantic

import scala.collection.mutable.ArrayBuffer
import de.thm.mni.cg.ir
import de.thm.mni.cg.ir._
import de.thm.mni.cg.semantic.Ty._
import de.thm.mni.cg.table.TyEntry
import de.thm.mni.cg.language.ErrorMessages._
import de.thm.mni.cg.util.cache.Cache
import de.thm.mni.cg.util.{ sequence, error, NoPosition }
import de.thm.mni.cg.table.SymbolTable
import de.thm.mni.cg.runtime.Offset

object Ty {
  lazy val intTy: Ty = NameTy("int")
  lazy val boolTy: Ty = NameTy("bool")
  lazy val charTy: Ty = NameTy("char")
  lazy val unitTy: Ty = NameTy("unit")
  lazy val nullTy: Ty = NullTy

  lazy val stringTy: Ty = ArrayTy(charTy)

  def fromSymbolUncached(symbol: TypeSymbol): Ty = {
    @inline
    def mk(xs: ArrayBuffer[ir.Component], f: List[Component] => Ty): Ty =
      f(xs.map {
        case ir.Component(symbol, Name(name)) =>
          Component(helper(symbol), name)
      }.toList)

    def helper(symbol: TypeSymbol): Ty = symbol match {
      case NameTypeSymbol(Name(name)) => NameTy(name)
      case ArrayTypeSymbol(symbol)    => ArrayTy(helper(symbol))
      case StructTypeSymbol(fs)       => mk(fs, StructTy(_))
      case UnionTypeSymbol(fs)        => mk(fs, UnionTy(_))
    }
    helper(symbol)
  }

  def fromSymbol(symbol: TypeSymbol)(implicit cache: Cache[TypeSymbol, Ty]): Ty = cache(symbol)

  def resolve(ty: Ty)(implicit table: SymbolTable, cache: Cache[TypeSymbol, Ty]): Option[Ty] = {
    @inline
    def mk(xs: List[Component], f: List[Component] => Ty, seen: Set[Ty]): Option[Ty] = {
      val resolved = xs.map { case Component(ty, name) =>
        helper(ty, seen + ty).map { Component(_, name) }
      }
      sequence(resolved).map(f)
    }

    def helper(ty: Ty, seen: Set[Ty]): Option[Ty] = ty match {
      case _ if ty.isPrimitive =>
        Some(ty)
      case `nullTy` =>
        Some(ty)
      case NameTy(name) if !(seen contains ty) =>
        table(Name(name)) match {
          case Some(TyEntry(TyDef(_, symbol))) => helper(fromSymbol(symbol), seen + ty)
          case Some(e)                         => error(NoPosition, xIsNotAY(name, e.describe))
          case None                            => None
        }
      case StructTy(components) if !(seen contains ty) =>
        mk(components, StructTy(_), seen)
      case UnionTy(components) if !(seen contains ty) =>
        mk(components, UnionTy(_), seen)
      case TupleTy(tys) if !(seen contains ty) =>
        val maybeResolvedTys = tys.map(helper(_, seen + ty))
        sequence(maybeResolvedTys).map(TupleTy(_))
      case ArrayTy(base) if !(seen contains ty) =>
        helper(base, seen + ty).map(ArrayTy(_))
      case _ =>
        Some(ty)
    }

    helper(ty, Set())
  }

  private def combine(xs: List[Component])(b: (Int, Int) => Int)(implicit table: SymbolTable, cache: Cache[TypeSymbol, Ty]) =
    xs.foldLeft(0) {
      case (acc, comp) => b(acc, comp.ty.stackSize)
    }

  private def calculateStackSize(ty: Ty)(implicit table: SymbolTable, cache: Cache[TypeSymbol, Ty]) =
    ty match {
      case NameTy("int")  => 4
      case NameTy("char") => 1
      case NameTy("bool") => 1
      case NameTy("unit") => 0
      case NameTy(name) =>
        resolve(ty).map(_.stackSize).getOrElse(typeNotExistError(Name(name)))
      case ArrayTy(_)   => 4
      case NullTy       => 4
      case ComplexTy(_) => 4
      case TupleTy(tys)   => calcStackSizeError("tupled ty")
    }

  private def calculateHeapSize(ty: Ty)(implicit table: SymbolTable, cache: Cache[TypeSymbol, Ty]) =
    ty match {
      case NameTy("int")  => 4
      case NameTy("char") => 1
      case NameTy("bool") => 1
      case NameTy("unit") => 0
      case NullTy         => calcHeapSizeError("null type")
      case TupleTy(_)     => calcHeapSizeError("tupled ty")
      case NameTy(name) =>
        resolve(ty).map(_.stackSize).getOrElse(typeNotExistError(Name(name)))
      case ArrayTy(_) => 4
      case StructTy(members) =>
        combine(members)(_ + _)
      case UnionTy(members) =>
        combine(members)(_ max _)
    }

}

/** `Ty` is an immutable representation of a well formed type */
sealed trait Ty {
  def isPrimitive: Boolean = this match {
    case `intTy` | `boolTy`  => true
    case `unitTy` | `charTy` => true
    case _                   => false
  }

  private var _stackSize = -1
  def stackSize(implicit table: SymbolTable, cache: Cache[TypeSymbol, Ty]): Int = {
    if (_stackSize < 0) {
      _stackSize = calculateStackSize(this)
    }
    _stackSize
  }


  private var _heapSize = -1
  def heapSize(implicit table: SymbolTable, cache: Cache[TypeSymbol, Ty]): Int = {
    if (_heapSize < 0) {
      _heapSize = calculateHeapSize(this)
    }
    _heapSize
  }

}

/** `NullTy` is a special type for the null reference */
case object NullTy extends Ty {
  override def toString = "null"
}

/** `NameTy` represents a named type (eg: int) */
case class NameTy(value: String) extends Ty {
  override def toString = value
}

/** `ArrayTy` represents an array type */
case class ArrayTy(base: Ty) extends Ty {
  override def toString = s"$base[]"
}

object ComplexTy {
  def unapply(ty: Ty): Option[List[Component]] = ty match {
    case StructTy(lst) => Some(lst)
    case UnionTy(lst)  => Some(lst)
    case _             => None
  }
}

object StructTy {
  def apply(components: (Ty, String)*) = new StructTy(components: _*)
}

/** `StructTy` represents a struct type */
case class StructTy(components: List[Component]) extends Ty {
  def this(components: (Ty, String)*) =
    this(components.map(Component.tupled).toList)

  override def toString = "struct " + components.mkString("{ ", " ", " }")

  private var _offset: Map[String, Int] = _
  def offsetOf(member: String)(implicit table: SymbolTable, cache: Cache[TypeSymbol, Ty]): Int = {
    if (_offset == null) {
      val map = collection.mutable.Map.empty[String, Int]
      var off = 0
      for (Component(ty, name) <- components) {
        val size = ty.stackSize
        val palign = Offset.alignTo(off, size)
        map += ((name, palign))
        off = palign + size
      }
      _offset = map.toMap
    }
    _offset.get(member) match {
      case Some(x) => x
      case None    => illegalMemberError(member)
    }
  }
}

object UnionTy {
  def apply(components: (Ty, String)*) = new UnionTy(components: _*)
}
/** `UnionTy` represents a struct type */
case class UnionTy(components: List[Component]) extends Ty {
  def this(components: (Ty, String)*) =
    this((components.map(Component.tupled)).toList)

  override def toString = "union " + components.mkString("{ ", " ", " }")
}

/** `Component` is a helper class, which denotes a member of a struct or a union type */
case class Component(ty: Ty, name: String) {
  override def toString = s"$ty $name;"
}

/** `TupleTy` represents a tuple */
case class TupleTy(tys: List[Ty]) extends Ty {
  override def toString = tys.mkString("(", ", ", ")")
}
