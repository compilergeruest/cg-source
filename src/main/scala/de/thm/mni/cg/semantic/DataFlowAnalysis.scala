package de.thm.mni.cg.semantic

import de.thm.mni.cg.ir._
import de.thm.mni.cg.util._
import de.thm.mni.cg.util.Position.PositionOrdering._
import de.thm.mni.cg.util.traversal.PartialDfgTraverser
import de.thm.mni.cg.util.traversal.TraverseOrder._
import de.thm.mni.cg.language.ErrorMessages._
import scala.collection.mutable.{ Set => MutSet }
import de.thm.mni.cg.table.FunEntry
import de.thm.mni.cg.util.cache.Cache
import scala.collection.mutable.ListBuffer

class DataFlowAnalysis {
  private val cache = Cache[List[String], Dfg](_ => illegalCacheUseError())
  private val seenFunctions = ListBuffer[List[String]]()
  private val debug = false

  private def print(pos: Pos): Unit =
    if (debug) Predef.print(pos.pos.line + "." + pos.pos.column + " ")
    else ()

  private def println(stm: Stm): Unit =
    if (debug) Predef.println(stm.code().replaceAll("[\n\r]+", " "))
    else ()

  private def println(any: Any): Unit =
    if (debug) Predef.println(any.toString)
    else ()

  private def println(): Unit =
    if (debug) Predef.println()
    else ()

  private def insertSmallestPos[T <: Pos](set: MutSet[T], elements: Traversable[T]): Unit =
    elements.foreach(insertSmallestPos(set, _))

  private def insertSmallestPos[T <: Pos](set: MutSet[T], element: T): Unit =
    set.find(_ == element) match {
      case Some(previous) =>
        if (element.pos < previous.pos) {
          set -= previous
          set += element
        }
      case None =>
        set += element
    }

  private implicit class PositionalMutSet[T <: Pos](set: MutSet[T]) {
    def +<=(element: T): Unit = insertSmallestPos[T](set, element)
    def ++<=(elements: Traversable[T]): Unit = insertSmallestPos[T](set, elements)
    def ++<(elements: Traversable[T]): MutSet[T] = {
      val result = MutSet[T]()
      result ++= set
      result ++<= elements
      result
    }
  }

  /** Make Dfg from FunExp's definition and call `f` with it */
  private def withFunDfg(funExp: FunExp)(f: Dfg => Unit): Unit = {
    lazy val unknownFunction = error(funExp.getName, functionNotExist(funExp.getName))

    val optEntry = funExp.getTable.get[FunEntry](funExp.getName)
    val entry = optEntry.getOrElse(unknownFunction)

    println("FUNCTION-NAME: " + entry.getSynthetic)
    f(cache(entry.getSynthetic, withEffect(Dfg(entry.getFunDef)) { dfg =>
      if (!(seenFunctions contains entry.getSynthetic)) {
        seenFunctions += entry.getSynthetic
        analyze(dfg)
        // remove parameters from the function-body's gen and kill set
        val paramNames = entry.getFunDef.getParams.map(_.getName)
        dfg.getKillSet --= paramNames
        dfg.getGenSet --= paramNames
      } else {
        println("[info] already seen function '" + entry.getSynthetic.mkString(".") + "' - no further analysis needed!")
      }
    }))
  }

  /** Insert all names beeing used in `exp` into `gen` */
  private def insertGen(gen: MutSet[Name], exp: Exp): Unit = {
    gen ++<= exp.collect { case NameVar(name, _) => name }
    val calls = exp.collect { case f: FunExp => f }
    calls.foreach(withFunDfg(_) { dfg =>
      // insert last seen positions, not first
      gen ++= dfg.getGenSet
    })
  }

  /** Insert all names beeing used in `exp` into `gen`, traversing the expressions recursively */
  private def insertLowerLevelGen(gen: MutSet[Name], lhs: Exp, upper: Boolean): Unit = lhs match {
    case ArrayVar(base, index, _) =>
      insertGen(gen, index)
      insertLowerLevelGen(gen, base, false)
    case AccessVar(base, _, _) =>
      insertLowerLevelGen(gen, base, false)
    case NameVar(name, _) if !upper =>
      gen +<= name
    case FunExp(_, args, _) =>
      args.foreach(insertGen(gen, _))
    case NewArrExp(_, dims, _) =>
      dims.foreach(_.foreach(insertGen(gen, _)))
    case NewObjExp(_, args, _) =>
      for ((_, exp) <- args) insertGen(gen, exp)
    case _ =>
  }

  /** Insert names beeing generated in `stm` into `gen` */
  private def insertGen(gen: MutSet[Name], stm: Stm): Unit = stm match {
    case AssignStm(lhs, rhs) =>
      insertGen(gen, rhs)
      lhs.foreach(insertLowerLevelGen(gen, _, true))
    case InitStm(_, rhs) =>
      insertGen(gen, rhs)
    case IfStm(test, _, _) =>
      insertGen(gen, test)
    case WhileStm(test, _) =>
      insertGen(gen, test)
    case ForStm(_, range, _, _) =>
      insertGen(gen, range.start)
      range.next.foreach(insertGen(gen, _))
      insertGen(gen, range.end)
    case ExpStm(exp) =>
      insertGen(gen, exp)
    case DelStm(exp) =>
      insertGen(gen, exp)
    case ReturnStm(exps) =>
      exps.foreach(insertGen(gen, _))
    case _ =>
  }

  /** Insert kills of function calls into `kills` */
  private def insertFunKill(kill: MutSet[Name], ast: Ast): Unit =  ast match {
    case IfStm(test, _, _) =>
      insertFunKill(kill, test)
    case WhileStm(test, _) =>
      insertFunKill(kill, test)
    case ForStm(_, range, _, _) =>
      insertFunKill(kill, range.start)
      range.next.foreach(insertFunKill(kill, _))
      insertFunKill(kill, range.end)
    case _ =>
      val calls = ast.collect { case f: FunExp => f }
      // insert last seen positions, not first
      calls.foreach(withFunDfg(_)(kill ++= _.getKillSet))
  }

  /** Insert names beeing killed in `stm` into `kill` */
  private def insertKill(kill: MutSet[Name], stm: Stm): Unit = stm match {
    case ForStm(counter, range, _, _) =>
      kill +<= counter.getName
      insertFunKill(kill, range.start)
      range.next.foreach(insertFunKill(kill, _))
      insertFunKill(kill, range.end)
    case AssignStm(lhs, rhs) =>
      lhs.foreach {
        case NameVar(name, _) => kill +<= name
        case e                => insertFunKill(kill, e)
      }
      insertFunKill(kill, rhs)
    case InitStm(lhs, _) =>
      lhs.foreach(kill +<= _.getName)
    case _: Def => ()
    case _ =>
      insertFunKill(kill, stm)
  }

  /** Returns a set with all names of variables defined in this statement */
  private def definitions(stm: Stm): Set[Name] = stm match {
    case VarDef(_, _, name) => Set(name)
    case InitStm(lhs, _)    => lhs.map(_.getName).toSet
    case _                  => Set.empty[Name]
  }

  /**
   * Returns (gens, kills) of a given dfg.
   * The dfg is shown as if it was atomic.
   */
  private def collectAtomicSets(dfg: Dfg): (MutSet[Name], MutSet[Name]) = {
    val gens = MutSet[Name]()
    val kills = MutSet[Name]()
    val defs = MutSet[Name]()

    var current = dfg
    while (!current.isInstanceOf[ExitDfg])
      current match {
        case e: StmDfg =>
          val defHere = definitions(e.getStm)
          gens ++<= e.getGenSet -- defs -- kills
          defs ++<= defHere
          kills ++<= e.getKillSet -- defs -- gens

          current = e.getNext

          val uninitialized = e.getLiveSet.filter(defHere.contains).filterNot(e.getKillSet.contains)
          for (e <- uninitialized) {
            error(e, variableMayNotHaveBeenInitialized(e))
          }
        case e: BranchDfg =>
          val g = e.getOnTrue.getGenSet ++< e.getOnFalse.getGenSet

          gens ++<= g -- kills -- defs
          gens ++<= e.getGenSet -- kills -- defs

          if (e.getOnFalse ne e.getNext) {
            val k = e.getOnTrue.getKillSet.intersect(e.getOnFalse.getKillSet)
            kills ++<= k -- gens -- defs
          }
          kills ++<= e.getKillSet -- gens -- defs

          current = e.getNext
        case e: ScopeDfg =>
          gens ++<= e.getGenSet -- defs -- kills
          kills ++<= e.getKillSet -- defs -- gens
          current = e.getNext
        case _ =>
      }

    (gens, kills)
  }

  private lazy val gather: PartialFunction[Dfg, Unit] = {
    case e @ StmDfg(stm, _) =>
      insertGen(e.getGenSet, stm)
      insertKill(e.getKillSet, stm)

      print(e.getStm)
      println(e.getStm)
      println("  - GEN: " + e.getGenSet.mkString("{ ", ", ", " }"))
      println("  - KILL: " + e.getKillSet.mkString("{ ", ", ", " }"))
      println("")
    case e @ BranchDfg(stm, _, _, _) =>
      insertGen(e.getGenSet, stm)
      insertKill(e.getKillSet, stm)

      e.getGenSet ++<= e.getOnTrue.getGenSet
      if (e.getOnFalse ne e.getNext) {
        e.getGenSet ++<= e.getOnFalse.getGenSet
      }

      if (e.getOnFalse ne e.getNext) {
        e.getKillSet ++<= e.getOnTrue.getKillSet.intersect(e.getOnFalse.getKillSet)
      }

      print(stm)
      println(stm)
      println("  - GEN: " + e.getGenSet.mkString("{ ", ", ", " }"))
      println("  - KILL: " + e.getKillSet.mkString("{ ", ", ", " }"))
      println("")
    case e @ ScopeDfg(inner, _, _) =>
      println("[info] analyzing deeper scope ...")
      analyze(inner)
      val (gens, kills) = collectAtomicSets(inner)
      e.getGenSet ++<= gens
      e.getKillSet ++<= kills

      println("{ ... }")
      println("  - GEN: " + e.getGenSet.mkString("{ ", ", ", " }"))
      println("  - KILL: " + e.getKillSet.mkString("{ ", ", ", " }"))
      println("")
  }

  private lazy val compute: PartialFunction[Dfg, Unit] = {
    case e @ StmDfg(stm, next) =>
      e.getLiveSet ++<= next.getLiveSet -- e.getKillSet ++< e.getGenSet
    case e @ BranchDfg(stm, onTrue, onFalse, _) =>
      e.getLiveSet ++<= onTrue.getLiveSet -- e.getKillSet ++< e.getGenSet
      e.getLiveSet ++<= onFalse.getLiveSet -- e.getKillSet ++< e.getGenSet
    case e @ ScopeDfg(inner, next, _) =>
      e.getLiveSet ++<= next.getLiveSet -- e.getKillSet ++< e.getGenSet
  }

  private var rec = 0
  private def analyze(dfg: Dfg): Unit = {
    rec += 1
    println()
    println(s"vvv ==================== $rec ==================== vvv")
    new PartialDfgTraverser(PostOrder, gather).traverse(dfg)
    new PartialDfgTraverser(PostOrder, compute).traverse(dfg)
    println(s"^^^ ==================== $rec ==================== ^^^")
    println()
    rec -= 1
  }

  def analyze(program: Program): Unit = {
    seenFunctions += List(program.globalScope.getName.name)
    analyze(Dfg(program.globalScope))
  }

}
